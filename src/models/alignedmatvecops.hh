#ifndef __ALIGNEDMATVECOPS__
#define __ALIGNEDMATVECOPS__

#include <iostream>
#include <iomanip>

#include <dune/common/memory/alignment.hh>

extern"C" {
  //#include <cblas.h>
}

/****************************************************************************/
// NOTE !!!
// All functions in this file assume that the matrices are stored
// column major FORTRAN style!
/****************************************************************************/

/****************************************************************************/
// compilation parameters
/****************************************************************************/

#define THRESHOLD_VALUE 64
#define UNROLL_THRESHOLD_MATMUL THRESHOLD_VALUE
#define UNROLL_THRESHOLD_ROTATE THRESHOLD_VALUE
#define UNROLL_THRESHOLD_TRANSPOSE THRESHOLD_VALUE
#define BARRIER asm volatile("": : :"memory")

#if defined __INTEL_COMPILER
#define PRAGMA_SIMD _Pragma("simd")
#elif defined __GNUC__
#define PRAGMA_SIMD _Pragma("gcc ivdep")
#else
#define PRAGMA_SIMD
#endif

template <int a, int b>
struct LessThanOrEqual
{
  enum { value = a<=b };
};

//#define AMV_USE_BLAS 1

/****************************************************************************/
// matrix multiplication
/****************************************************************************/

/** template metaprogram for matrix multiplication
 */
template<size_t I, size_t J, size_t K, size_t m, size_t k, size_t n, typename T, size_t alignment>
struct meta_matmul_loop_row
{
  enum { count=meta_matmul_loop_row<I,J,K+1,m,k,n,T,alignment>::count+1 };
  static T loop_row (const T* __restrict__ A, const T* __restrict__ B)
  {
    return A[K*m+I]*B[J*k+K] + meta_matmul_loop_row<I,J,K+1,m,k,n,T,alignment>::loop_row(A,B);
  }
};
// end loop over one row times column
template<size_t I, size_t J, size_t m, size_t k, size_t n, typename T, size_t alignment>
struct meta_matmul_loop_row<I,J,k,m,k,n,T,alignment>
{
  enum { count=0 };
  static T loop_row (const T* __restrict__ A, const T* __restrict__ B)
  {
    return 0; // A[K*m+I]*B[J*k+K];
  }
};

/** template metaprogram for matrix multiplication
 */
template<size_t I, size_t J, size_t m, size_t k, size_t n, typename T, size_t alignment>
struct meta_matmul_loop_C
{
  enum { count=meta_matmul_loop_row<I,J,0,m,k,n,T,alignment>::count+meta_matmul_loop_C<I+1,J,m,k,n,T,alignment>::count };
  static void loop_C (const T* __restrict__ A, const T* __restrict__ B, T* __restrict__ C)
  {
    C[J*m+I] = meta_matmul_loop_row<I,J,0,m,k,n,T,alignment>::loop_row(A,B);
    meta_matmul_loop_C<I+1,J,m,k,n,T,alignment>::loop_C(A,B,C);
  }
};
// end of loop over rows
template<size_t J, size_t m, size_t k, size_t n, typename T, size_t alignment>
struct meta_matmul_loop_C<m,J,m,k,n,T,alignment>
{
  enum { count=meta_matmul_loop_C<0,J+1,m,k,n,T,alignment>::count };
  static void loop_C (const T* __restrict__ A, const T* __restrict__ B, T* __restrict__ C)
  {
    meta_matmul_loop_C<0,J+1,m,k,n,T,alignment>::loop_C(A,B,C);
  }
};
// end of loop over columns
template<size_t m, size_t k, size_t n, typename T, size_t alignment>
struct meta_matmul_loop_C<0,n,m,k,n,T,alignment>
{
  enum { count=0 };
  static void loop_C (const T* __restrict__ A, const T* __restrict__ B, T* __restrict__ C)
  {
  }
};

/** unrolled matrix multiplication C=A*B

    - size of matrices given at compile time
    - matrices are given in COLUMN MAJOR storage

*/
template<size_t m, size_t k, size_t n, typename T, size_t alignment>
inline void unrolled_matmul (const T* __restrict__ A, // input matrix m x k in COLUMN MAJOR storage
                             const T* __restrict__ B, // input matrix k x n in COLUMN MAJOR storage
                             T* __restrict__ C)       //output matrix m x n in COLUMN MAJOR storage
{
  // give alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,const T,alignment);
  DUNE_ASSUME_ALIGNED(C,T,alignment);

  meta_matmul_loop_C<0,0,m,k,n,T,alignment>::loop_C(A,B,C);
}



/*
template<typename T, size_t alignment>
void looped_matmul (size_t ma, size_t na, size_t nb,
                    const T* __restrict__ A,   // input matrix ma x na in COLUMN MAJOR storage
                    const T* __restrict__ B,   // input matrix na x nb in COLUMN MAJOR storage
                    T* __restrict__ C) DUNE_NOINLINE;        //output matrix ma x nb in COLUMN MAJOR storage
*/


/** matrix multiplication with loops
 */
template<typename T, size_t alignment>
inline void looped_matmul (size_t ma, size_t na, size_t nb,
                           const T* __restrict__ A,   // input matrix ma x na in COLUMN MAJOR storage
                           const T* __restrict__ B,   // input matrix na x nb in COLUMN MAJOR storage
                           T* __restrict__ C)         //output matrix ma x nb in COLUMN MAJOR storage
{
  // alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,const T,alignment);
  DUNE_ASSUME_ALIGNED(C,T,alignment);

  BARRIER;

  // the multiplication with loop for larger sizes
  size_t i,j,k;
  for (j=0; j<nb; j++)        // loop over columns of B
    {
      size_t JB = j*na;       // start of column j in B
      size_t JC = j*ma;       // start of column j in C
      PRAGMA_SIMD
      for (i=0; i<ma; i++)    // loop over rows of A
        {
          T s=0.0;
          for (k=0; k<na; k++) s += A[k*ma+i]*B[JB+k];
          C[JC+i] = s;
        }
    }

  BARRIER;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
// matmul function with compile time size parameters
// depending on size we use unrolled/loop based functions
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

// class calling unrolled version
template<size_t m, size_t k, size_t n, typename T, size_t alignment, bool unrolled>
struct MatMulDispatch
{
  static inline void matmul (const T* __restrict__ A, // input matrix m x k in COLUMN MAJOR storage
                             const T* __restrict__ B, // input matrix k x n in COLUMN MAJOR storage
                             T* __restrict__ C)       //output matrix m x n in COLUMN MAJOR storage
  {
    // give alignment hints
    DUNE_ASSUME_ALIGNED(A,const T,alignment);
    DUNE_ASSUME_ALIGNED(B,const T,alignment);
    DUNE_ASSUME_ALIGNED(C,T,alignment);


    // std::cout << "matmul m=" << m << " k=" << k << " n=" << n << std::endl;

    // std::cout << "count=" << meta_matmul_loop_C<0,0,m,k,n,T,alignment>::count << std::endl;
    meta_matmul_loop_C<0,0,m,k,n,T,alignment>::loop_C(A,B,C);
  }
};

// specialization calling looped version
template<size_t m, size_t k, size_t n, typename T, size_t alignment>
struct MatMulDispatch<m,k,n,T,alignment,false>
{
  static inline void matmul (const T* __restrict__ A, // input matrix m x k in COLUMN MAJOR storage
                             const T* __restrict__ B, // input matrix k x n in COLUMN MAJOR storage
                             T* __restrict__ C)       //output matrix m x n in COLUMN MAJOR storage
  {
    // give alignment hints
    DUNE_ASSUME_ALIGNED(A,const T,alignment);
    DUNE_ASSUME_ALIGNED(B,const T,alignment);
    DUNE_ASSUME_ALIGNED(C,T,alignment);

    // std::cout << "matmul m=" << m << " k=" << k << " n=" << n << std::endl;
#ifdef AMV_USE_BLAS
    cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,m,n,k,1.0,A,m,B,k,0.0,C,m);
#else
    looped_matmul<T,alignment>(m,k,n,A,B,C);
#endif
  }
};

/** matrix multiplication C=A*B

    - size of matrices given at compile time
    - matrices are given in COLUMN MAJOR storage

*/
template<size_t m, size_t k, size_t n, typename T, size_t alignment>
inline void matmul (const T* __restrict__ A, // input matrix m x k in COLUMN MAJOR storage
                    const T* __restrict__ B, // input matrix k x n in COLUMN MAJOR storage
                    T* __restrict__ C)       //output matrix m x n in COLUMN MAJOR storage
{
  // give alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,const T,alignment);
  DUNE_ASSUME_ALIGNED(C,T,alignment);

  MatMulDispatch<m,k,n,T,alignment,LessThanOrEqual<k*n,UNROLL_THRESHOLD_MATMUL>::value>::matmul(A,B,C);

  // std::cout << "matmul m=" << m << " k=" << k << " n=" << n << std::endl;

  //   if (k*n<=UNROLL_THRESHOLD_MATMUL)
  //     {
  //       // std::cout << "count=" << meta_matmul_loop_C<0,0,m,k,n,T,alignment>::count << std::endl;
  //       meta_matmul_loop_C<0,0,m,k,n,T,alignment>::loop_C(A,B,C);
  //     }
  //   else
  //     {
  // #ifdef AMV_USE_BLAS
  //       cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans,m,n,k,1.0,A,m,B,k,0.0,C,m);
  // #else
  //       looped_matmul<T,alignment>(m,k,n,A,B,C);
  // #endif
  //     }
}

/****************************************************************************/
// unrolled tensor rotation for small tensors
/****************************************************************************/

/** template metaprogram for tensor rotation
    A is m x n
    B is k x (n/k)*m
*/
template<size_t I, size_t J, size_t m, size_t n, size_t k, typename T, size_t alignment>
struct meta_rotate_loop_A
{
  static void loop_A (const T* __restrict__ A, T* __restrict__ B)
  {
    B[I*n+(J/k)*k+J%k] = A[J*m+I];
    // std::cout << "I=" << I << " J=" << J << " m=" << m << " n=" << n << " k=" << k
    //        << " B[" << I*n+(J/k)*k+J%k << "] = A[" << J*m+I << "]" << std::endl;
    meta_rotate_loop_A<I,J+1,m,n,k,T,alignment>::loop_A(A,B);
  }
};
// end of loop over columns
template<size_t I, size_t m, size_t n, size_t k, typename T, size_t alignment>
struct meta_rotate_loop_A<I,n,m,n,k,T,alignment>
{
  static void loop_A (const T* __restrict__ A, T* __restrict__ B)
  {
    meta_rotate_loop_A<I+1,0,m,n,k,T,alignment>::loop_A(A,B);
  }
};
// end of loop over rows
template<size_t m, size_t n, size_t k, typename T, size_t alignment>
struct meta_rotate_loop_A<m,0,m,n,k,T,alignment>
{
  static void loop_A (const T* __restrict__ A, T* __restrict__ B)
  {
  }
};

/**  tensor rotation

     - input tensor A is m x n matrix in column major order, i.e. j*m+i
     - k is the next column dimension
     - A is m x n
     - B is k x n*m/k
*/
template<size_t m, size_t n, size_t k, typename T, size_t alignment>
inline void unrolled_rotate (const T* __restrict__ A, T* __restrict__ B)
{
  // assertions
  assert(n%k==0); // n needs to be a multiple of k

  // alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,T,alignment);

  meta_rotate_loop_A<0,0,m,n,k,T,alignment>::loop_A(A,B);
}


//template<typename T, size_t alignment>
//void looped_rotate (size_t m, size_t n, size_t k, const T* __restrict__ A, T* __restrict__ B) DUNE_NOINLINE;


/**  tensor rotation with loops for larger sizes

     - input tensor A in m x n matrix in column major order, i.e. j*m+i
     - k is the next column dimension
     - A is m x n
     - B is k x n*m/k
*/
template<typename T, size_t alignment>
inline void looped_rotate (size_t m, size_t n, size_t k, const T* __restrict__ A, T* __restrict__ B)
{
  // assertions
  assert(n%k==0); // n needs to be a multiple of k

  // alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,T,alignment);

  BARRIER;

  // the rotation implemented with loops
  size_t b=n/k;
  size_t i,r,j;
  for (i=0; i<m; i++)    // loop over rows of A
    {
      size_t I = i*b*k;
      PRAGMA_SIMD
      for (r=0; r<b; r++) { // loop over columns of A
        size_t R = r*k;
        for (j=0; j<k; j++)
          B[I+R+j] = A[(R+j)*m+i];
      }
    }

  BARRIER;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
// tensor rotation function
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

// class calling unrolled version
template<size_t m, size_t n, size_t k, typename T, size_t alignment, bool unrolled>
struct RotateDispatch
{
  static inline void rotate (const T* __restrict__ A, T* __restrict__ B)
  {
    // assertions
    assert(n%k==0); // n needs to be a multiple of k

    // alignment hints
    DUNE_ASSUME_ALIGNED(A,const T,alignment);
    DUNE_ASSUME_ALIGNED(B,T,alignment);

    // std::cout << "rotate m=" << m << " n=" << n << " k=" << k << std::endl;
    meta_rotate_loop_A<0,0,m,n,k,T,alignment>::loop_A(A,B);
  }
};

// class calling unrolled version
template<size_t m, size_t n, size_t k, typename T, size_t alignment>
struct RotateDispatch<m,n,k,T,alignment,false>
{
  static inline void rotate (const T* __restrict__ A, T* __restrict__ B)
  {
    // assertions
    assert(n%k==0); // n needs to be a multiple of k

    // alignment hints
    DUNE_ASSUME_ALIGNED(A,const T,alignment);
    DUNE_ASSUME_ALIGNED(B,T,alignment);

    // std::cout << "rotate m=" << m << " n=" << n << " k=" << k << std::endl;
    looped_rotate<T,alignment>(m,n,k,A,B);
  }
};

/**  tensor rotation

     - input tensor m x n matrix in column major order, i.e. j*m+i
     - k is the next column dimension
     - input tensor  A is m x n
     - output tensor B is k x (n/k)*m
*/
template<size_t m, size_t n, size_t k, typename T, size_t alignment>
inline void rotate (const T* __restrict__ A, T* __restrict__ B)
{
  // assertions
  assert(n%k==0); // n needs to be a multiple of k

  // alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,T,alignment);

  // std::cout << "rotate m=" << m << " n=" << n << " k=" << k << std::endl;
  RotateDispatch<m,n,k,T,alignment,LessThanOrEqual<m*n,UNROLL_THRESHOLD_ROTATE>::value>::rotate(A,B);

  // if (m*n<=UNROLL_THRESHOLD_ROTATE)
  //   meta_rotate_loop_A<0,0,m,n,k,T,alignment>::loop_A(A,B);
  // else
  //   looped_rotate<T,alignment>(m,n,k,A,B);
}

/****************************************************************************/
// unrolled matrix transposition
/****************************************************************************/

/** template metaprogram for matrix transposition

    - A and B are assumed in column major
    - input A is m x n
    - output B is n x m
*/
template<size_t I, size_t J, size_t m, size_t n, typename T, size_t alignment>
struct meta_transpose_loop
{
  static void loop (const T* __restrict__ A, T* __restrict__ B)
  {
    B[I*n+J] = A[J*m+I];
    meta_transpose_loop<I,J+1,m,n,T,alignment>::loop(A,B);
  }
};
// end of loop over columns
template<size_t I, size_t m, size_t n, typename T, size_t alignment>
struct meta_transpose_loop<I,n,m,n,T,alignment>
{
  static void loop (const T* __restrict__ A, T* __restrict__ B)
  {
    meta_transpose_loop<I+1,0,m,n,T,alignment>::loop(A,B);
  }
};
// end of loop over rows
template<size_t m, size_t n, typename T, size_t alignment>
struct meta_transpose_loop<m,0,m,n,T,alignment>
{
  static void loop (const T* __restrict__ A, T* __restrict__ B)
  {
  }
};

/**  matrix transposition

     - input matrix A is m x n matrix in column major order, i.e. j*m+i
     - A is m x n
     - B is n x m
*/
template<size_t m, size_t n, typename T, size_t alignment>
inline void unrolled_transpose (const T* __restrict__ A, T* __restrict__ B)
{
  // alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,T,alignment);

  meta_transpose_loop<0,0,m,n,T,alignment>::loop(A,B);
}

//template<typename T, size_t alignment>
//void looped_transpose (size_t m, size_t n, const T* __restrict__ A, T* __restrict__ B) DUNE_NOINLINE;

/**  matrix transposition with loops for larger sizes

     - A,B are column major
     - A is m x n
     - B is n x m
*/
template<typename T, size_t alignment>
inline void looped_transpose (size_t m, size_t n, const T* __restrict__ A, T* __restrict__ B)
{
  // alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,T,alignment);

  BARRIER;

  // the rotation implemented with loops
  size_t i,j;
  PRAGMA_SIMD
  for (i=0; i<m; i++)    // loop over rows of A
    for (j=0; j<n; j++)
      B[i*n+j] = A[j*m+i];

  BARRIER;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
// matrix transposition function
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/

// class calling unrolled version
template<size_t m, size_t n, typename T, size_t alignment, bool unrolled>
struct TransposeDispatch
{
  static inline void transpose (const T* __restrict__ A, T* __restrict__ B)
  {
    // alignment hints
    DUNE_ASSUME_ALIGNED(A,const T,alignment);
    DUNE_ASSUME_ALIGNED(B,T,alignment);

    // std::cout << "rotate m=" << m << " n=" << n << " k=" << k << std::endl;

    meta_transpose_loop<0,0,m,n,T,alignment>::loop(A,B);
  }
};

// class calling unrolled version
template<size_t m, size_t n, typename T, size_t alignment>
struct TransposeDispatch<m,n,T,alignment,false>
{
  static inline void transpose (const T* __restrict__ A, T* __restrict__ B)
  {
    // alignment hints
    DUNE_ASSUME_ALIGNED(A,const T,alignment);
    DUNE_ASSUME_ALIGNED(B,T,alignment);

    // std::cout << "rotate m=" << m << " n=" << n << " k=" << k << std::endl;
    looped_transpose<T,alignment>(m,n,A,B);
  }
};

/**  matrix transposition

     - A,B are column major
     - A is m x n
     - B is n x m
*/
template<size_t m, size_t n, typename T, size_t alignment>
inline void transpose (const T* __restrict__ A, T* __restrict__ B)
{
  // alignment hints
  DUNE_ASSUME_ALIGNED(A,const T,alignment);
  DUNE_ASSUME_ALIGNED(B,T,alignment);


  // std::cout << "rotate m=" << m << " n=" << n << " k=" << k << std::endl;
  TransposeDispatch<m,n,T,alignment,LessThanOrEqual<m*n,UNROLL_THRESHOLD_TRANSPOSE>::value>::transpose(A,B);

  // if (m*n<=UNROLL_THRESHOLD_TRANSPOSE)
  //   meta_transpose_loop<0,0,m,n,T,alignment>::loop(A,B);
  // else
  //   looped_transpose<T,alignment>(m,n,A,B);
}

#endif
