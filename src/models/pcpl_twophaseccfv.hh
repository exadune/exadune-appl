// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_PCPLTWOPHASEOP_HH
#define DUNE_PDELAB_PCPLTWOPHASEOP_HH

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>

#include<dune/geometry/referenceelements.hh>
//#include<dune/localfunctions/raviartthomas/raviartthomascube.hh>
#include<dune/localfunctions/raviartthomas/raviartthomas0q.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/idefault.hh>

namespace Dune {
  namespace PDELab {
    const double eps_numdiff = 1.e-7;

    //! enum to choose between gas and liquid phase
    enum TwoPhasePhaseName
      {
        TwoPhaseLiquid = 0,
        TwoPhaseCapillary = 1
      };


    //! /brief discontinuity is replaced with smooth function
    template<class T>
    double Blend (T x, T x0, T x1)
    {
      if (x>=x1) return 1.0;
      if (x<=x0) return 0.0;
      x = (x-x0)/(x1-x0);
      if (x<=0.5)
        return(2.*x*x);
      else
        return(1.-2.*(1.-x)*(1.-x));
    }


    //! traits class for two phase parameter class
    template<typename GV, typename RF>
    struct TwoPhaseParameterTraits
    {
      //! \brief the grid view
      typedef GV GridViewType;

      //! \brief Enum for domain dimension
      enum {
        //! \brief dimension of the domain
        dimDomain = GV::dimension
      };

      //! \brief Export type for domain field
      typedef typename GV::Grid::ctype DomainFieldType;

      //! \brief domain type
      typedef Dune::FieldVector<DomainFieldType,dimDomain> DomainType;

      //! \brief domain type
      typedef Dune::FieldVector<DomainFieldType,dimDomain-1> IntersectionDomainType;

      //! \brief Export type for range field
      typedef RF RangeFieldType;

      //! \brief range type
      typedef Dune::FieldVector<RF,GV::dimensionworld> RangeType;

      //! \brief permeability tensor type
      typedef RangeFieldType PermTensorType;

      //! grid types
      typedef typename GV::Traits::template Codim<0>::Entity ElementType;
      typedef typename GV::Intersection IntersectionType;
    };

    template<typename GV, typename RF>
    struct TwoPhaseFullTensorParameterTraits : TwoPhaseParameterTraits<GV, RF>
    {
      typedef TwoPhaseParameterTraits<GV, RF> Base;
      typedef typename Base::RangeFieldType RangeFieldType;

      //! \brief permeability tensor type
      typedef Dune::FieldMatrix<RangeFieldType,Base::dimDomain,Base::dimDomain> PermTensorType;
    };

    //! base class for parameter class
    template<class T, class Imp>
    class TwoPhaseParameterInterface
    {
    public:
      typedef T Traits;

      //! porosity
      typename Traits::RangeFieldType
      phi (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return asImp().phi(e,x);
      }

      //! entry pressure to detect media discontinuities
      typename Traits::RangeFieldType
      pe (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return asImp().pe(e,x);
      }

      //! capillary pressure function
      typename Traits::RangeFieldType
      pc (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType s_l) const
      {
        return asImp().pc(e,x,s_l);
      }

      //! inverse capillary pressure function
      typename Traits::RangeFieldType
      s_l (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
           typename Traits::RangeFieldType pc) const
      {
        return asImp().s_l(e,x,pc);
      }

      //! liquid phase relative permeability
      typename Traits::RangeFieldType
      kr_l (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
            typename Traits::RangeFieldType s_l) const
      {
        return asImp().kr_l(e,x,s_l);
      }

      //! gas phase relative permeability
      typename Traits::RangeFieldType
      kr_g (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
            typename Traits::RangeFieldType s_g) const
      {
        return asImp().kr_g(e,x,s_g);
      }

      //! liquid phase viscosity
      typename Traits::RangeFieldType
      mu_l (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
            typename Traits::RangeFieldType p_l) const
      {
        return asImp().mu_l(e,x,p_l);
      }

      //! gas phase viscosity
      typename Traits::RangeFieldType
      mu_g (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
            typename Traits::RangeFieldType p_g) const
      {
        return asImp().mu_g(e,x,p_g);
      }

      //! absolute permeability (scalar!)
      typename Traits::PermTensorType
      k_abs (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return asImp().k_abs(e,x);
      }

      //! gravity vector
      const typename Traits::RangeType& gravity () const
      {
        return asImp().gravity();
      }

      //! liquid phase molar density
      template<typename E>
      typename Traits::RangeFieldType
      nu_l (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
            typename Traits::RangeFieldType p_l) const
      {
        return asImp().nu_l(e,x,p_l);
      }

      //! gas phase molar density
      typename Traits::RangeFieldType
      nu_g (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
            typename Traits::RangeFieldType p_g) const
      {
        return asImp().nu_g(e,x,p_g);
      }

      //! liquid phase mass density
      typename Traits::RangeFieldType
      rho_l (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
             typename Traits::RangeFieldType p_l) const
      {
        return asImp().rho_l(e,x,p_l);
      }

      //! gas phase mass density
      typename Traits::RangeFieldType
      rho_g (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
             typename Traits::RangeFieldType p_g) const
      {
        return asImp().rho_g(e,x,p_g);
      }

      //! liquid phase boundary condition type
      int
      bc_l (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return asImp().bc_l(is,x,time);
      }

      //! gas phase boundary condition type
      int
      bc_g (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return asImp().bc_g(is,x,time);
      }

      //! liquid phase Dirichlet boundary condition
      typename Traits::RangeFieldType
      g_l (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return asImp().g_l(is,x,time);
      }

      //! gas phase Dirichlet boundary condition
      typename Traits::RangeFieldType
      g_g (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return asImp().g_g(is,x,time);
      }

      //! liquid phase Neumann boundary condition
      typename Traits::RangeFieldType
      j_l (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return asImp().j_l(is,x,time);
      }

      //! gas phase Neumann boundary condition
      typename Traits::RangeFieldType
      j_g (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return asImp().j_g(is,x,time);
      }

      //! liquid phase source term
      typename Traits::RangeFieldType
      q_l (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
           typename Traits::RangeFieldType time) const
      {
        return asImp().q_l(e,x,time);
      }

      //! gas phase source term
      typename Traits::RangeFieldType
      q_g (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
           typename Traits::RangeFieldType time) const
      {
        return asImp().q_g(e,x,time);
      }

      //! scale function for liquid phase
      typename Traits::RangeFieldType
      scale_l () const
      {
        return 1.0;
      }

      //! scale function for gas phase
      typename Traits::RangeFieldType
      scale_g () const
      {
        return 1.0;
      }

    private:
      Imp& asImp () {return static_cast<Imp &> (*this);}
      const Imp& asImp () const {return static_cast<const Imp &>(*this);}
    };


    // a local operator for solving the two-phase flow in liquid pressure - capillary pressure formulation
    // with two-point flux approximation
    // TP : parameter class, see above
    template<typename TP>
    class TwoPhaseTwoPointFluxOperator
      : public NumericalJacobianSkeleton<TwoPhaseTwoPointFluxOperator<TP> >,
        public NumericalJacobianApplySkeleton<TwoPhaseTwoPointFluxOperator<TP> >,
        public NumericalJacobianBoundary<TwoPhaseTwoPointFluxOperator<TP> >,
        public NumericalJacobianApplyBoundary<TwoPhaseTwoPointFluxOperator<TP> >,

        public FullSkeletonPattern,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,

        public InstationaryLocalOperatorDefaultMethods<typename TP::Traits::RangeFieldType>
    {
      enum { dim = TP::Traits::GridViewType::dimension };
      enum { liquid = TwoPhaseLiquid };
      enum { capillary = TwoPhaseCapillary };

      typedef typename TP::Traits::RangeFieldType Real;
    public:
      // pattern assembly flags
      enum { doPatternVolume = true };
      enum { doPatternSkeleton = true };

      // residual assembly flags
      enum { doAlphaSkeleton  = true };
      enum { doAlphaBoundary  = true };
      enum { doLambdaVolume   = true };
      enum { doLambdaBoundary = true };

      //! constructor: pass parameter object
      TwoPhaseTwoPointFluxOperator (const TP& tp_, Real scale_l_, Real scale_g_, bool blend_, Real blendmin_, Real blendmax_)
        : NumericalJacobianSkeleton<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), NumericalJacobianBoundary<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), tp(tp_), scale_l(scale_l_), scale_g(scale_g_), blend(blend_), blendmin(blendmin_), blendmax(blendmax_), central(false)
      {}

      //! constructor: pass parameter object
      TwoPhaseTwoPointFluxOperator (const TP& tp_, Real scale_l_, Real scale_g_)
        : NumericalJacobianSkeleton<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), NumericalJacobianBoundary<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), tp(tp_), scale_l(scale_l_), scale_g(scale_g_), blend(false), blendmin(0.0), blendmax(0.0), central(false)
      {}

      //! constructor: pass parameter object
      TwoPhaseTwoPointFluxOperator (const TP& tp_)
        : NumericalJacobianSkeleton<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), NumericalJacobianBoundary<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), tp(tp_), scale_l(tp.scale_l()), scale_g(tp.scale_g()), blend(false), blendmin(0.0), blendmax(0.0), central(false)
      {}

      //! constructor: pass parameter object
      TwoPhaseTwoPointFluxOperator (const TP& tp_,bool blend_, Real blendmin_, Real blendmax_)
        : NumericalJacobianSkeleton<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), NumericalJacobianBoundary<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), tp(tp_), scale_l(tp.scale_l()), scale_g(tp.scale_g()), blend(blend_), blendmin(blendmin_), blendmax(blendmax_), central(false)
      {}

      //! constructor: pass parameter object
      TwoPhaseTwoPointFluxOperator (const TP& tp_, bool central_)
        : NumericalJacobianSkeleton<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), NumericalJacobianBoundary<TwoPhaseTwoPointFluxOperator<TP> >(eps_numdiff), tp(tp_), scale_l(tp.scale_l()), scale_g(tp.scale_g()), blend(false), blendmin(0.0), blendmax(0.0), central(central_)
      {}


      // volume integral depending only on test functions
      template<typename EG, typename LFSV, typename R>
      void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
      {
        // select the two components
        typedef typename LFSV::template Child<liquid>::Type PLSpace;
        typedef typename LFSV::template Child<capillary>::Type PGSpace;

        // domain and range field type
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;

        // cell geometry
        const Dune::FieldVector<DF,dim>&
          cell_center_local = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
        RF cell_volume = eg.geometry().volume();
        RF q_l = tp.q_l(eg.entity(),cell_center_local,time);
        RF q_g = tp.q_g(eg.entity(),cell_center_local,time);

        // contribution from source term
        r.accumulate(lfsv,liquid,-scale_l * q_l * cell_volume);
        r.accumulate(lfsv,capillary,-scale_g * q_g * cell_volume);
      }

      // skeleton integral depending on test and ansatz functions
      // each face is only visited ONCE!
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                           R& r_s, R& r_n) const
      {
        // select the two components
        typedef typename LFSV::template Child<0>::Type PLSpace;

        // domain and range field type
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;

        // cell geometries
        const Dune::FieldVector<DF,dim>&
          inside_cell_center_local = Dune::ReferenceElements<DF,dim>::general(ig.inside()->type()).position(0,0);
        const Dune::FieldVector<DF,dim>&
          outside_cell_center_local = Dune::ReferenceElements<DF,dim>::general(ig.outside()->type()).position(0,0);
        Dune::FieldVector<DF,IG::dimension>
          inside_cell_center_global = ig.inside()->geometry().center();
        Dune::FieldVector<DF,IG::dimension>
          outside_cell_center_global = ig.outside()->geometry().center();

        // distance of cell centers
        Dune::FieldVector<DF,dim> d(outside_cell_center_global);
        d -= inside_cell_center_global;
        RF distance = d.two_norm();

        // face geometry
        const Dune::FieldVector<DF,IG::dimension-1>&
          face_local = Dune::ReferenceElements<DF,IG::dimension-1>::general(ig.geometry().type()).position(0,0);
        RF face_volume = ig.geometry().volume();

        // absolute permeability
        RF k_abs_inside = tp.k_abs(*(ig.inside()),inside_cell_center_local);
        RF k_abs_outside = tp.k_abs(*(ig.outside()),outside_cell_center_local);

        // gravity times normal
        RF gn = tp.gravity()*ig.unitOuterNormal(face_local);

        // liquid phase calculation
        RF rho_l_inside = tp.rho_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
        RF rho_l_outside = tp.rho_l(*(ig.outside()),outside_cell_center_local,x_n(lfsu_n,liquid));
        // determines liquid flow direction
        RF w_l = (x_s(lfsu_s,liquid)-x_n(lfsu_n,liquid))/distance + aavg(rho_l_inside,rho_l_outside)*gn;

        // gas phase calculation
        RF rho_g_inside = tp.rho_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
        RF rho_g_outside = tp.rho_g(*(ig.outside()),outside_cell_center_local,x_n(lfsu_n,capillary)+x_s(lfsu_s,liquid));
        // determines gas flow direction
        RF w_g = ((x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid))-(x_n(lfsu_n,capillary)+x_n(lfsu_n,liquid)))/distance + aavg(rho_g_inside,rho_g_outside)*gn;

        // get entry pressure on both sides
        RF pentry_s = tp.pe(*(ig.inside()),inside_cell_center_local);
        RF pentry_n = tp.pe(*(ig.outside()),outside_cell_center_local);

        // first equation
        RF pc_upwind, s_l_upwind_s, s_g_upwind_s, s_l_upwind_n, s_g_upwind_n;
        RF nu_l = aavg(tp.nu_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid)),
                       tp.nu_l(*(ig.outside()),outside_cell_center_local,x_n(lfsu_n,liquid)));

        // upwind capillary pressure on face
        if (w_l>=0)
          // flow from the current element
          pc_upwind = x_s(lfsu_s,capillary);
        else
          // flow to the current element
          pc_upwind = x_n(lfsu_n,capillary);

        // possibility to use non full upwind scheme
        // if the velocity relatively is small, it could be helpful
        // to replace full upwind with upwind consisting of one sided
        // upwind and capillary average
        if (blend)
          {
            RF bl = Blend(std::abs(w_l), blendmin, blendmax);
            pc_upwind = bl * pc_upwind + (1.- bl) * aavg( x_s(lfsu_s,capillary),x_n(lfsu_n,capillary));
          }
        if (central && pentry_s==pentry_n)
          {
            pc_upwind = aavg( x_s(lfsu_s,capillary),x_n(lfsu_n,capillary) );
          }

        // compute upwind value of liquid saturation
        s_l_upwind_s = tp.s_l(*(ig.inside()),inside_cell_center_local,pc_upwind);
        s_l_upwind_n = tp.s_l(*(ig.outside()),outside_cell_center_local,pc_upwind);

        // harmonic average of k_r
        RF lambda_l_inside = tp.kr_l(*(ig.inside()),inside_cell_center_local,s_l_upwind_s)/
          tp.mu_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
        RF lambda_l_outside = tp.kr_l(*(ig.outside()),outside_cell_center_local,s_l_upwind_n)/
          tp.mu_l(*(ig.outside()),outside_cell_center_local,x_n(lfsu_n,liquid));
        RF sigma_l = havg(lambda_l_inside*k_abs_inside,lambda_l_outside*k_abs_outside);

        // residual accumulation
        r_s.accumulate(lfsu_s,liquid,scale_l * nu_l * sigma_l
                       * w_l * face_volume);
        r_n.accumulate(lfsu_n,liquid,-scale_l * nu_l * sigma_l
                       * w_l * face_volume);

        // second equation
        RF nu_g = aavg(tp.nu_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid)),
                       tp.nu_g(*(ig.outside()),outside_cell_center_local,x_n(lfsu_n,capillary)+x_n(lfsu_n,liquid)));

        // new evaluation necessary only if signs differ
        if (w_l*w_g<0)
          {
            // upwind capillary pressure on face
            if (w_g>=0)
              pc_upwind = x_s(lfsu_s,capillary);
            else
              pc_upwind = x_n(lfsu_n,capillary);

            if (blend)
              {
                RF bl = Blend(std::abs(w_l), blendmin, blendmax);
                pc_upwind = bl * pc_upwind + (1.- bl) * aavg (x_s(lfsu_s,capillary),x_n(lfsu_n,capillary));
              }
            if (central && pentry_s==pentry_n)
              {
                pc_upwind = aavg( x_s(lfsu_s,capillary),x_n(lfsu_n,capillary) );
              }

            // compute upwind value of liquid saturation if necessary
            s_l_upwind_s = tp.s_l(*(ig.inside()),inside_cell_center_local,pc_upwind);
            s_l_upwind_n = tp.s_l(*(ig.outside()),outside_cell_center_local,pc_upwind);
          }
        // compute upwind value of gas saturation
        s_g_upwind_s = 1-s_l_upwind_s;
        s_g_upwind_n = 1-s_l_upwind_n;

        RF lambda_g_inside = tp.kr_g(*(ig.inside()),inside_cell_center_local,s_g_upwind_s)/
          tp.mu_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
        RF lambda_g_outside = tp.kr_g(*(ig.outside()),outside_cell_center_local,s_g_upwind_n)/
          tp.mu_g(*(ig.outside()),outside_cell_center_local,x_n(lfsu_n,capillary)+x_n(lfsu_n,liquid));
        // if (pentry_s!=pentry_n) // at media discontinuities: take lambda_g from high entry pressure region unless phase is continuous
        //   {
        //     // this is a media discontinuity
        //     if (pentry_s<pentry_n)
        //       {
        //         // self has the lower entry pressure
        //         if (x_s(lfsu_s,capillary)<pentry_n)
        //           // no continuity, take mobility from high entry pressure region
        //           lambda_g_inside = lambda_g_outside;
        //       }
        //     else
        //       {
        //         // neighbor has the lower entry pressure
        //         if (x_n(lfsu_n,capillary)<pentry_s)
        //           // no continuity, take mobility from high entry pressure region
        //           lambda_g_outside = lambda_g_inside;
        //       }
        //   }
        RF sigma_g = havg(lambda_g_inside*k_abs_inside,lambda_g_outside*k_abs_outside);

        r_s.accumulate(lfsu_s,capillary,scale_g * nu_g * sigma_g * w_g * face_volume);
        r_n.accumulate(lfsu_n,capillary,-scale_g * nu_g * sigma_g * w_g * face_volume);
      }

      // skeleton integral depending on test and ansatz functions
      // We put the Dirchlet evaluation also in the alpha term to save some geometry evaluations
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           R& r_s) const
      {
        // select the two components
        typedef typename LFSV::template Child<0>::Type PLSpace;

        // domain and range field type
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;

        // face geometry
        const Dune::FieldVector<DF,dim-1>&
          face_local = Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).position(0,0);
        RF face_volume = ig.geometry().volume();

        // evaluate boundary condition type
        int bc_l = tp.bc_l(ig.intersection(),face_local,time);
        int bc_g = tp.bc_g(ig.intersection(),face_local,time);
        if (bc_l!=1 && bc_g!=1) return; // no Dirichlet boundary conditions

        // cell geometry
        const Dune::FieldVector<DF,dim>&
          inside_cell_center_local = Dune::ReferenceElements<DF,dim>::general(ig.inside()->type()).position(0,0);
        Dune::FieldVector<DF,dim>
          inside_cell_center_global = ig.inside()->geometry().global(inside_cell_center_local);

        // distance of cell center to boundary
        Dune::FieldVector<DF,dim> d = ig.geometry().global(face_local);
        d -= inside_cell_center_global;
        RF distance = d.two_norm();

        // absolute permeability
        RF k_abs_inside = tp.k_abs(*(ig.inside()),inside_cell_center_local);

        // gravity times normal
        RF gn = tp.gravity()*ig.unitOuterNormal(face_local);


        // liquid phase Dirichlet boundary
        //
        if (bc_l==1)
          {
            RF rho_l_inside = tp.rho_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
            RF g_l = tp.g_l(ig.intersection(),face_local,time);
            RF w_l = (x_s(lfsu_s,liquid)-g_l)/distance + rho_l_inside*gn;
            RF s_l = tp.s_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary));
            RF lambda_l_inside = tp.kr_l(*(ig.inside()),inside_cell_center_local,s_l)/
              tp.mu_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
            RF sigma_l = lambda_l_inside*k_abs_inside;
            RF nu_l = tp.nu_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
            r_s.accumulate(lfsu_s,liquid,scale_l * nu_l * sigma_l
                           * w_l * face_volume);
          }

        // gas phase Dirichlet boundary
        if (bc_g==1)
          {
            RF rho_g_inside = tp.rho_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
            RF g_g = tp.g_g(ig.intersection(),face_local,time);
            RF w_g = (x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid)-g_g)/distance + rho_g_inside*gn;
            RF s_l = tp.s_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary));
            RF s_g = 1-s_l;

            RF lambda_g_inside = tp.kr_g(*(ig.inside()),inside_cell_center_local,s_g)/
              tp.mu_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
            RF sigma_g = lambda_g_inside*k_abs_inside;
            RF nu_g =tp.nu_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
            r_s.accumulate(lfsu_s,capillary,scale_g * nu_g * sigma_g
                           * w_g * face_volume);
          }
      }

      // boundary integral independent of ansatz functions
      template<typename IG, typename LFSV, typename R>
      void lambda_boundary (const IG& ig, const LFSV& lfsv, R& r_s) const
      {
        // select the two components
        typedef typename LFSV::template Child<0>::Type PLSpace;

        // domain and range field type
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;

        // face geometry
        const Dune::FieldVector<DF,dim-1>&
          face_local = Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).position(0,0);
        RF face_volume = ig.geometry().integrationElement(face_local)*
          Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).volume();

        // evaluate boundary condition type
        int bc_l = tp.bc_l(ig.intersection(),face_local,time);
        int bc_g = tp.bc_g(ig.intersection(),face_local,time);
        if (bc_l!=0 && bc_g!=0) return; // no Neumann boundary conditions

        // liquid phase Neumann boundary
        if (bc_l==0)
          {
            RF j_l = tp.j_l(ig.intersection(),face_local,time);
            r_s.accumulate(lfsv,liquid,scale_l * j_l * face_volume);
          }

        // gas phase Neumann boundary
        if (bc_g==0)
          {
            RF j_g = tp.j_g(ig.intersection(),face_local,time);
            r_s.accumulate(lfsv,capillary,scale_g * j_g * face_volume);
          }
      }

      //! set time for subsequent evaluation
      void setTime (typename TP::Traits::RangeFieldType t)
      {
        time = t;
      }

      //! set minimal boundary for Blend function
      void setBlendMin (Real blendmin_)
      {
        blendmin = blendmin_;
      }

      //! set max boundary for Blend function
      void setBlendMax (Real blendmax_)
      {
        blendmax = blendmax_;
      }

      //! use the blend function
      void setBlend (bool blend_)
      {
        blend = blend_;
      }

    private:
      const TP& tp;  // two phase parameter class
      typename TP::Traits::RangeFieldType time;
      Real scale_l, scale_g;
      bool blend;
      Real blendmin,blendmax;
      bool central;

      template<typename T>
      T aavg (T a, T b) const
      {
        return 0.5*(a+b);
      }

      template<typename T>
      T havg (T a, T b) const
      {
        T eps = 1E-30;
        return 2.0/(1.0/(a+eps) + 1.0/(b+eps));
      }
    };


    template<typename TP>
    class ControlMassOperator
      : public LocalOperatorDefaultFlags
    {
      enum { dim = TP::Traits::GridViewType::dimension };
      enum { liquid = TwoPhaseLiquid };
      enum { capillary = TwoPhaseCapillary };

      typedef typename TP::Traits::RangeFieldType Real;

    public:

      enum { doAlphaBoundary  = true };
      enum { doLambdaVolume   = true };
      enum { doLambdaBoundary = true };

      //! constructor: pass parameter object
      ControlMassOperator (const TP& tp_, Real scale_l_, Real scale_g_):
        tp(tp_), scale_l(scale_l_), scale_g(scale_g_)
      {}


      //! constructor: pass parameter object
      ControlMassOperator (const TP& tp_):
        tp(tp_), scale_l(tp.scale_l()), scale_g(tp.scale_g())
      {}



      // volume integral depending only on test functions
      template<typename EG, typename LFSV, typename R>
      void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
      {
        // select the two components
        typedef typename LFSV::template Child<liquid>::Type PLSpace;
        typedef typename LFSV::template Child<capillary>::Type PGSpace;

        // domain and range field type
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;

        // cell geometry
        const Dune::FieldVector<DF,dim>&
          cell_center_local = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
        RF cell_volume = eg.geometry().volume();
        RF q_l = tp.q_l(eg.entity(),cell_center_local,time);
        RF q_g = tp.q_g(eg.entity(),cell_center_local,time);

        // contribution from source term
        r.accumulate(lfsv,liquid,-scale_l * q_l * cell_volume);
        r.accumulate(lfsv,capillary,-scale_g * q_g * cell_volume);
      }


      // skeleton integral depending on test and ansatz functions
      // We put the Dirchlet evaluation also in the alpha term to save some geometry evaluations
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           R& r_s) const
      {
        // select the two components
        typedef typename LFSV::template Child<0>::Type PLSpace;

        // domain and range field type
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;

        // face geometry
        const Dune::FieldVector<DF,dim-1>&
          face_local = Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).position(0,0);
        RF face_volume = ig.geometry().volume();

        // evaluate boundary condition type
        int bc_l = tp.bc_l(ig.intersection(),face_local,time);
        int bc_g = tp.bc_g(ig.intersection(),face_local,time);

        // cell geometry
        const Dune::FieldVector<DF,dim>&
          inside_cell_center_local = Dune::ReferenceElements<DF,dim>::general(ig.inside()->type()).position(0,0);
        Dune::FieldVector<DF,dim>
          inside_cell_center_global = ig.inside()->geometry().global(inside_cell_center_local);

        // distance of cell center to boundary
        Dune::FieldVector<DF,dim> d = ig.geometry().global(face_local);
        d -= inside_cell_center_global;
        RF distance = d.two_norm();

        // absolute permeability
        RF k_abs_inside = tp.k_abs(*(ig.inside()),inside_cell_center_local);

        // gravity times normal
        RF gn = tp.gravity()*ig.unitOuterNormal(face_local);


        // liquid phase Dirichlet boundary
        //
        if (bc_l==1)
          {
            RF rho_l_inside = tp.rho_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
            RF g_l = tp.g_l(ig.intersection(),face_local,time);
            RF w_l = (x_s(lfsu_s,liquid)-g_l)/distance + rho_l_inside*gn;
            RF s_l = tp.s_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary));
            RF lambda_l_inside = tp.kr_l(*(ig.inside()),inside_cell_center_local,s_l)/
              tp.mu_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
            RF sigma_l = lambda_l_inside*k_abs_inside;
            RF nu_l = tp.nu_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
            r_s.accumulate(lfsu_s,liquid,scale_l  * sigma_l
                           * w_l * face_volume);
          }

        // gas phase Dirichlet boundary
        if (bc_g==1)
          {
            RF rho_g_inside = tp.rho_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
            RF g_g = tp.g_g(ig.intersection(),face_local,time);
            RF w_g = (x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid)-g_g)/distance + rho_g_inside*gn;
            RF s_l = tp.s_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary));
            RF s_g = 1-s_l;

            RF lambda_g_inside = tp.kr_g(*(ig.inside()),inside_cell_center_local,s_g)/
              tp.mu_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
            RF sigma_g = lambda_g_inside*k_abs_inside;
            RF nu_g =tp.nu_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
            r_s.accumulate(lfsu_s,capillary,scale_g * sigma_g
                           * w_g * face_volume);
          }


        // liquid phase Neumann boundary
        if (bc_l==0)
          {
            RF nu_l = tp.nu_l(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,liquid));
            RF j_l = tp.j_l(ig.intersection(),face_local,time);
            r_s.accumulate(lfsv_s,liquid,scale_l * j_l / nu_l * face_volume);
          }

        // gas phase Neumann boundary
        if (bc_g==0)
          {
            RF nu_g =tp.nu_g(*(ig.inside()),inside_cell_center_local,x_s(lfsu_s,capillary)+x_s(lfsu_s,liquid));
            RF j_g = tp.j_g(ig.intersection(),face_local,time);
            r_s.accumulate(lfsv_s,capillary,scale_g * j_g /nu_g * face_volume);
          }

      }

      // boundary integral independent of ansatz functions
      template<typename IG, typename LFSV, typename R>
      void lambda_boundary (const IG& ig, const LFSV& lfsv, R& r_s) const
      {


      }

    private:
      const TP& tp;  // two phase parameter class
      typename TP::Traits::RangeFieldType time;
      Real scale_l, scale_g;
      typename TP::Traits::RangeFieldType dt;
    };



    /** a local operator for the storage operator
     *
     * \f{align*}{
     \int_\Omega c(x) uv dx
     * \f}
     */
    template<class TP>
    class TwoPhaseOnePointTemporalOperator
      : public NumericalJacobianVolume<TwoPhaseOnePointTemporalOperator<TP> >,
        public NumericalJacobianApplyVolume<TwoPhaseOnePointTemporalOperator<TP> >,
        public FullVolumePattern,
        public LocalOperatorDefaultFlags,
        public InstationaryLocalOperatorDefaultMethods<typename TP::Traits::RangeFieldType>
    {
      enum { dim = TP::Traits::GridViewType::dimension };
      enum { liquid = TwoPhaseLiquid };
      enum { capillary = TwoPhaseCapillary };

      typedef typename TP::Traits::RangeFieldType Real;

    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };

      TwoPhaseOnePointTemporalOperator (const TP& tp_, Real scale_l_, Real scale_g_)
        : tp(tp_), scale_l(scale_l_), scale_g(scale_g_)
      {
      }

      //! constructor: pass parameter object
      TwoPhaseOnePointTemporalOperator (const TP& tp_)
        : tp(tp_), scale_l(tp.scale_l()), scale_g(tp.scale_g())
      {}

      //! set time for subsequent evaluation
      void setTime (typename TP::Traits::RangeFieldType t)
      {
        time = t;
      }

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // select the two components
        typedef typename LFSV::template Child<0>::Type PLSpace;

        // domain and range field type
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PLSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;

        // cell geometry
        const Dune::FieldVector<DF,dim>&
          cell_center_local = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
        RF cell_volume = eg.geometry().volume();

        RF phi = tp.phi(eg.entity(),cell_center_local);
        RF s_l = tp.s_l(eg.entity(),cell_center_local,x(lfsu,capillary));
        RF nu_l = tp.nu_l(eg.entity(),cell_center_local,x(lfsu,liquid));
        RF nu_g = tp.nu_g(eg.entity(),cell_center_local,x(lfsu,capillary)+x(lfsu,liquid));

        r.accumulate(lfsu,liquid,scale_l * phi * s_l * nu_l * cell_volume);
        r.accumulate(lfsu,capillary,scale_g * phi * (1-s_l) * nu_g * cell_volume);
      }


    private:
      const TP& tp;
      typename TP::Traits::RangeFieldType time;
      Real scale_l, scale_g;
    };

    template<typename TP, TwoPhasePhaseName P>
    class TwoPhaseParameterMapper;

    template<typename TP>
    struct TwoPhaseParameterMapper<TP, TwoPhaseLiquid>
    {
      typedef typename TP::Traits Traits;
      TwoPhaseParameterMapper(const TP& tp) : tp_(tp) {}

      typename Traits::RangeFieldType
      s (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
         typename Traits::RangeFieldType pc) const
      {
        return tp_.s_l(e,x,pc);
      }

      typename Traits::RangeFieldType
      kr (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType s_l) const
      {
        return tp_.kr_l(e,x,s_l);
      }

      typename Traits::RangeFieldType
      mu (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType p_l) const
      {
        return tp_.mu_l(e,x,p_l);
      }

      typename Traits::RangeFieldType
      nu (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType p_l) const
      {
        return tp_.nu_l(e,x,p_l);
      }

      typename Traits::RangeFieldType
      rho (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
           typename Traits::RangeFieldType p_l) const
      {
        return tp_.rho_l(e,x,p_l);
      }

      int
      bc (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return tp_.bc_l(is,x,time);
      }

      typename Traits::RangeFieldType
      g (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return tp_.g_l(is,x,time);
      }

      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return tp_.j_l(is,x,time);
      }

    private:
      const TP& tp_;
    };

    template<typename TP>
    struct TwoPhaseParameterMapper<TP, TwoPhaseCapillary>
    {
      typedef typename TP::Traits Traits;
      TwoPhaseParameterMapper(const TP& tp) : tp_(tp) {}

      typename Traits::RangeFieldType
      s (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
         typename Traits::RangeFieldType pc) const
      {
        return 1.0 - tp_.s_l(e,x,pc);
      }

      typename Traits::RangeFieldType
      kr (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType s_g) const
      {
        return tp_.kr_g(e,x,s_g);
      }

      typename Traits::RangeFieldType
      mu (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType p_g) const
      {
        return tp_.mu_g(e,x,p_g);
      }

      typename Traits::RangeFieldType
      nu (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType p_g) const
      {
        return tp_.nu_g(e,x,p_g);
      }

      typename Traits::RangeFieldType
      rho (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
           typename Traits::RangeFieldType p_g) const
      {
        return tp_.rho_g(e,x,p_g);
      }

      int
      bc (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return tp_.bc_g(is,x,time);
      }

      typename Traits::RangeFieldType
      g (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return tp_.g_g(is,x,time);
      }

      typename Traits::RangeFieldType
      j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return tp_.j_g(is,x,time);
      }

    private:
      const TP& tp_;
    };

    /** \brief Provide velocity/flux field for phase P

        Uses RT0 interpolation on a cell.

        @tparam T  provides TwoPhaseParameterInterface
        @tparam PL P0 function for liquid phase pressure
        @tparam PG P0 function for gas phase pressure
        @tparam P  TwoPhasePhaseName enum selecting the phase P
    */
    template<typename  TP, typename PL, typename PC, TwoPhasePhaseName P>
    class TwoPhasePhaseVelocity
      : public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<typename PL::Traits::GridViewType,
                                                                               typename PL::Traits::RangeFieldType,
                                                                               PL::Traits::GridViewType::dimension,
                                                                               Dune::FieldVector<typename PL::Traits::RangeFieldType,PL::Traits::GridViewType::dimension> >,
                                              TwoPhasePhaseVelocity<TP,PL,PC,P> >
    {
      // extract useful types
      typedef typename PL::Traits::GridViewType GV;
      typedef typename GV::Grid::ctype DF;
      typedef typename PL::Traits::RangeFieldType RF;
      typedef typename PL::Traits::RangeType RangeType;
      enum { dim = PL::Traits::GridViewType::dimension };
      typedef typename GV::Traits::template Codim<0>::Entity Element;
      typedef typename GV::IntersectionIterator IntersectionIterator;
      typedef typename IntersectionIterator::Intersection Intersection;

      const TP& tp;
      const TwoPhaseParameterMapper<TP, P> tpm;
      const PL& pl;
      const PC& pc;
      Dune::RT0QLocalFiniteElement<DF,RF,dim> rt0fe;
      typename TP::Traits::RangeFieldType time;
      bool compFlux;

      typedef typename Dune::RT0QLocalFiniteElement<DF,RF,dim>::Traits::LocalBasisType::Traits::RangeType RT0RangeType;

    public:
      typedef Dune::PDELab::GridFunctionTraits<GV,RF,dim,Dune::FieldVector<RF,dim> > Traits;

      typedef Dune::PDELab::GridFunctionBase<Traits,TwoPhasePhaseVelocity<TP,PL,PC,P> > BaseT;

      TwoPhasePhaseVelocity (const TP& tp_, const PL& pl_, const PC& pc_, bool compFlux_ = true) :
        tp(tp_), tpm(tp), pl(pl_), pc(pc_), time(0), compFlux(compFlux_) {}

      // set time where operator is to be evaluated (i.e. end of the time intervall)
      void set_time (typename TP::Traits::RangeFieldType time_)
      {
        time = time_;
      }

      inline void evaluate (const typename Traits::ElementType& e,
                            const typename Traits::DomainType& x,
                            typename Traits::RangeType& y) const
      {
        // cell geometry
        const Dune::FieldVector<DF,dim>&
          inside_cell_center_local = Dune::ReferenceElements<DF,dim>::
          general(e.type()).position(0,0);
        Dune::FieldVector<DF,dim>
          inside_cell_center_global = e.geometry().global(inside_cell_center_local);


        // absolute permeability
        RF k_abs_inside = tp.k_abs(e,inside_cell_center_local);

        // pressure evaluation
        typename PL::Traits::RangeType pl_inside, pc_inside, p_inside;
        pl.evaluate(e,inside_cell_center_local,pl_inside);
        pc.evaluate(e,inside_cell_center_local,pc_inside);
        if (P == TwoPhaseLiquid)
          p_inside = pl_inside;
        else
          p_inside = pl_inside+pc_inside;

        // density evaluation
        RF nu_inside = tpm.nu(e,inside_cell_center_local,p_inside);

        // for coefficient computation
        RF vn[2*dim];    // normal velocities
        RF coeff[2*dim]; // RT0 coefficient
        Dune::FieldMatrix<typename Traits::DomainFieldType,dim,dim>
          B = e.geometry().jacobianInverseTransposed(x); // the transformation. Assume it is linear
        RF determinant = B.determinant();

        // loop over cell neighbors
        IntersectionIterator endit = pl.getGridView().iend(e);
        for (IntersectionIterator iit = pl.getGridView().ibegin(e); iit!=endit; ++iit)
          {
            // set to zero for processor boundary
            vn[iit->indexInInside()] = 0.0;

            // face geometry
            const Dune::FieldVector<DF,dim-1>&
              face_local = Dune::ReferenceElements<DF,dim-1>::general(iit->geometry().type()).position(0,0);


            // interior face
            if (iit->neighbor())
              {
                const Dune::FieldVector<DF,dim>&
                  outside_cell_center_local = Dune::ReferenceElements<DF,dim>::
                  general(iit->outside()->type()).position(0,0);
                Dune::FieldVector<DF,dim>
                  outside_cell_center_global = iit->outside()->geometry().global(outside_cell_center_local);

                // distance of cell centers
                Dune::FieldVector<DF,dim> d(outside_cell_center_global);
                d -= inside_cell_center_global;
                RF distance = d.two_norm();

                // absolute permeability
                RF k_abs_outside = tp.k_abs(*(iit->outside()),outside_cell_center_local);

                // gravity times normal
                RF gn = tp.gravity()*iit->unitOuterNormal(face_local);

                // pressure evaluation
                typename PL::Traits::RangeType pl_outside, pc_outside, p_outside;
                pl.evaluate(*(iit->outside()),outside_cell_center_local,pl_outside);
                pc.evaluate(*(iit->outside()),outside_cell_center_local,pc_outside);
                if (P == TwoPhaseLiquid)
                  p_outside = pl_outside;
                else
                  p_outside = pc_outside+pl_outside;

                // needed for both phases
                RF pc_upwind, s_upwind;

                // P phase calculation
                RF rho_inside = tpm.rho(e,inside_cell_center_local,p_inside);
                RF rho_outside = tpm.rho(e,outside_cell_center_local,p_outside);
                RF w = (p_inside-p_outside)/distance + aavg(rho_inside,rho_outside)*gn; // determines direction
                if (w>=0) // upwind capillary pressure on face
                  {
                    pc_upwind = pc_inside;
                    s_upwind = tpm.s(e,inside_cell_center_local,pc_upwind);
                  }
                else
                  {
                    pc_upwind = pc_outside;
                    s_upwind = tpm.s(*(iit->outside()),outside_cell_center_local,pc_upwind);
                  }
                RF lambda_inside = tpm.kr(e,inside_cell_center_local,s_upwind)/
                  tpm.mu(e,inside_cell_center_local,p_inside);
                RF lambda_outside = tpm.kr(*(iit->outside()),outside_cell_center_local,s_upwind)/
                  tpm.mu(*(iit->outside()),outside_cell_center_local,p_outside);
                RF sigma = havg(lambda_inside*k_abs_inside,lambda_outside*k_abs_outside);

                // density
                RF nu_outside = tpm.nu(*(iit->outside()),outside_cell_center_local,p_outside);

                // set coefficient
                if (compFlux)
                  vn[iit->indexInInside()] = aavg(nu_inside,nu_outside) * sigma * w;
                else
                  vn[iit->indexInInside()] = sigma * w;
              }

            // boundary face
            if (iit->boundary())
              {
                // distance of cell center to boundary
                Dune::FieldVector<DF,dim> d = iit->geometry().global(face_local);
                d -= inside_cell_center_global;
                RF distance = d.two_norm();

                // evaluate boundary condition type
                int bc = tpm.bc(*iit,face_local,time);

                // gas phase Dirichlet boundary
                if (bc==1)
                  {
                    RF rho_inside = tpm.rho(e,inside_cell_center_local,p_inside);
                    RF g = tpm.g(*iit,face_local,time);
                    RF gn = tp.gravity()*iit->unitOuterNormal(face_local);
                    RF w = (p_inside-g)/distance + rho_inside*gn;
                    RF s = tpm.s(e,inside_cell_center_local,pc_inside);
                    RF lambda_inside = tpm.kr(e,inside_cell_center_local,s)/
                      tpm.mu(e,inside_cell_center_local,p_inside);
                    RF sigma = lambda_inside*k_abs_inside;

                    if (compFlux)
                      vn[iit->indexInInside()] = nu_inside * sigma * w;
                    else
                      vn[iit->indexInInside()] = sigma * w;
                  }

                // gas phase Neumann boundary
                if (bc==0)
                  {
                    RF j = tpm.j(*iit,face_local,time);
                    if (compFlux)
                      vn[iit->indexInInside()] = j;
                    else
                      vn[iit->indexInInside()] = j / nu_inside;
                  }
              }

            // compute coefficient
            Dune::FieldVector<DF,dim> vstar=iit->unitOuterNormal(face_local); // normal on tranformef element
            vstar *= vn[iit->indexInInside()];
            Dune::FieldVector<RF,dim> normalhat(0); // normal on reference element
            if (iit->indexInInside()%2==0)
              normalhat[iit->indexInInside()/2] = -1.0;
            else
              normalhat[iit->indexInInside()/2] =  1.0;
            Dune::FieldVector<DF,dim> vstarhat(0);
            B.umtv(vstar,vstarhat); // Piola backward transformation
            vstarhat *= determinant;
            coeff[iit->indexInInside()] = vstarhat*normalhat;
          }

        // compute velocity on reference element
        std::vector<RT0RangeType> rt0vectors(rt0fe.localBasis().size());
        rt0fe.localBasis().evaluateFunction(x,rt0vectors);
        typename Traits::RangeType yhat(0);
        for (unsigned int i=0; i<rt0fe.localBasis().size(); i++)
          yhat.axpy(coeff[i],rt0vectors[i]);

        // apply Piola transformation
        B.invert();
        y = 0;
        B.umtv(yhat,y);
        y /= determinant;
      }

      inline const typename Traits::GridViewType& getGridView ()
      {
        return pl.getGridView();
      }

    private:

      template<typename T>
      T aavg (T a, T b) const
      {
        return 0.5*(a+b);
      }

      template<typename T>
      T havg (T a, T b) const
      {
        T eps = 1E-30;
        return 2.0/(1.0/(a+eps) + 1.0/(b+eps));
      }
    };

    /** \brief Provide velocity/flux field for liquid phase

        Uses RT0 interpolation on a cell.

        - T  : provides TwoPhaseParameterInterface
        - PL : P0 function for liquid phase pressure
        - PG : P0 function for gas phase pressure
    */
    template<typename  TP, typename PL, typename PC>
    class V_l
      : public TwoPhasePhaseVelocity<TP,PL,PC,TwoPhaseLiquid>
    {
    public:
      V_l (const TP& tp_, const PL& pl_, const PC& pc_, bool compFlux_ = true) :
        TwoPhasePhaseVelocity<TP,PL,PC,TwoPhaseLiquid> (tp_, pl_, pc_, compFlux_) {}
    };

    /** \brief Provide velocity/flux field for gas phase

        Uses RT0 interpolation on a cell.

        - T  : provides TwoPhaseParameterInterface
        - PL : P0 function for liquid phase pressure
        - PC : P0 function for gas phase pressure
    */
    template<typename  TP, typename PL, typename PC>
    class V_g
      : public TwoPhasePhaseVelocity<TP,PL,PC,TwoPhaseCapillary>
    {
    public:
      V_g (const TP& tp_, const PL& pl_, const PC& pc_, bool compFlux_ = true) :
        TwoPhasePhaseVelocity<TP,PL,PC,TwoPhaseCapillary> (tp_, pl_, pc_, compFlux_) {}
    };


  }
}

#endif
