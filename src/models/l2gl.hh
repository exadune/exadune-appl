// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_L2GL_HH
#define DUNE_PDELAB_L2GL_HH

#include<array>
#include<cstddef>
#include<memory>
#include<vector>

#if HAVE_TBB
#include <tbb/tbb_stddef.h>
#endif

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>

#include<dune/geometry/type.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>

#include <dune/localfunctions/common/interfaceswitch.hh>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/finiteelementmap/qkdggl.hh>

namespace Dune {
  namespace PDELab {
    //! \addtogroup LocalOperator
    //! \ingroup PDELab
    //! \{

    /** a local operator for the mass operator (L_2 integral) using underintegration on Gauss-Lobatto points
     *
     * \f{align*}{
     \int_\Omega uv dx
     * \f}
     *
     * d : dimension
     * p : polynomial degree
     */
    template<int d, typename DF, typename RF, int p, bool affine_transformation=false>
    class L2GL : public NumericalJacobianApplyVolume<L2GL<d,DF,RF,p,affine_transformation> >,
    //               public FullVolumePattern,
                 public LocalOperatorDefaultFlags,
                 public InstationaryLocalOperatorDefaultMethods<double>
    {
      enum { dim = d };
      enum { n = p+1 };
      enum { N = Dune::StaticPower<n,dim>::power };

      typedef Dune::FieldVector<size_t,dim> MultiIndex;

      // compute multiindex from flat index
      template<size_t k, size_t dimension=dim> // k is the number of elements per direction
      Dune::FieldVector<size_t,dimension> mi (size_t i) const
      {
        Dune::FieldVector<size_t,dimension> alpha;
        for (std::size_t j=0; j<dimension; j++)
          {
            alpha[j] = i % k;
            i = i/k;
          }
        return alpha;
      }

      std::shared_ptr<const std::array<RF, n> > xi; // positions of Gauss Lobatto points
      std::shared_ptr<const std::array<RF, n> > w;  // weights of Gauss Lobatto points

    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };

      L2GL ()
      {
        auto xi_ = std::make_shared<std::array<RF, n> >();
        auto w_ = std::make_shared<std::array<RF, n> >();

        Dune::QkStuff::GaussLobattoLagrangePolynomials<DF,RF,p> poly;
        for (int i=0; i<n; i++)
          {
            (*xi_)[i] = poly.x(i);
            (*w_)[i] = poly.w(i);
          }
        xi = xi_;
        w = w_;
      }

#if HAVE_TBB
      //! splitting constructor, see tbb
      /**
       * Delegates to copy constructor
       */
      L2GL(L2GL &other, tbb::split) : L2GL(other) {}

      //! join other operator, see tbb
      /**
       * Does nothing.
       */
      void join(L2GL &other) { }
#endif // HAVE_TBB

      // define sparsity pattern of operator representation
      template<typename LFSU, typename LFSV, typename LocalPattern>
      void pattern_volume (const LFSU& lfsu, const LFSV& lfsv,
                           LocalPattern& pattern) const
      {
        for (size_t i=0; i<lfsu.size(); ++i)
          pattern.addLink(lfsv,i,lfsu,i);
      }

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // assertions
        assert(lfsu.size()==N);
        assert(lfsv.size()==N);

        // transformation
        RF gamma;
        if (affine_transformation)
          {
            Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
            gamma = eg.geometry().integrationElement(localcenter);
          }

        // loop over all basis functions
        for (size_t i=0; i<N; i++)
          {
            // position and weight
            MultiIndex alpha(mi<n>(i)); // multiindex of basis function
            Dune::FieldVector<DF,dim> x_alpha(0.0);                                // position of quadrature point
            for (size_t j=0; j<dim; j++) x_alpha[j] = (*xi)[alpha[j]];             // note: the xi are not consecutive
            RF weight(1.0);
            for (size_t j=0; j<dim; j++) weight *= (*w)[alpha[j]];                 // weight of quadrature point

            // evaluate transformation if necessary
            if (!affine_transformation) gamma = eg.geometry().integrationElement(x_alpha);

            // store result
            r.accumulate(lfsv,i, x(lfsu,i)*gamma*weight );
          }
      }

      // jacobian of volume term
      template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
      void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                            M & mat) const
      {
        // assertions
        assert(lfsu.size()==N);
        assert(lfsv.size()==N);

        // transformation
        RF gamma;
        if (affine_transformation)
          {
            Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
            gamma = eg.geometry().integrationElement(localcenter);
          }

        // loop over all basis functions
        for (size_t i=0; i<N; i++)
          {
            // position and weight
            MultiIndex alpha(mi<n>(i)); // multiindex of basis function
            Dune::FieldVector<DF,dim> x_alpha(0.0);                                // position of quadrature point
            for (size_t j=0; j<dim; j++) x_alpha[j] = (*xi)[alpha[j]];             // note: the xi are not consecutive
            RF weight(1.0);
            for (size_t j=0; j<dim; j++) weight *= (*w)[alpha[j]];                 // weight of quadrature point

            // evaluate transformation if necessary
            if (!affine_transformation) gamma = eg.geometry().integrationElement(x_alpha);

            // store result
            mat.accumulate(lfsv,i,lfsu,i, gamma*weight );
          }
      }
    };

    /** a local operator for the mass operator (L_2 integral) using underintegration on Gauss-Lobatto points
     *
     * \f{align*}{
     \int_\Omega uv dx
     * \f}
     *
     * d : dimension
     * p : polynomial degree
     */
    template<int d, typename DF, typename RF, int p, bool affine_transformation=false>
    class PowerL2GL : public NumericalJacobianApplyVolume<PowerL2GL<d,DF,RF,p,affine_transformation> >,
                      public FullVolumePattern,
                      public LocalOperatorDefaultFlags,
                      public InstationaryLocalOperatorDefaultMethods<double>
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };

      PowerL2GL ()
        : scalar_operator()
      {}

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        for(unsigned int i=0; i<LFSU::CHILDREN; ++i)
          scalar_operator.alpha_volume(eg,lfsu.child(i),x,lfsv.child(i),r);
      }

      // jacobian of volume term
      template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
      void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                            M& mat) const
      {
        for(unsigned int i=0; i<LFSU::CHILDREN; ++i)
          scalar_operator.jacobian_volume(eg,lfsu.child(i),x,lfsv.child(i),mat);
      }

    private:
      L2GL<d,DF,RF,p,affine_transformation> scalar_operator;
    };

    //! \} group LocalOperator
  } // namespace PDELab
} // namespace Dune

#endif
