// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef SIMPLEEQUILIBRATEDFLUX_HH
#define SIMPLEEQUILIBRATEDFLUX_HH

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/localfunctions/raviartthomas/raviartthomas0q.hh>
#include<dune/localfunctions/raviartthomas/raviartthomassimplex.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>
//#include<dune/pdelab/gridoperatorspace/gridoperatorspace.hh>
//#include<dune/pdelab/gridoperatorspace/gridoperatorspaceutilities.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include<dune/pdelab/localoperator/convectiondiffusiondg.hh>


/** \brief Provide velocity field for liquid phase

    Uses RT0 interpolation on a cell.

    - T  : provides ConvectionDiffusionParameterInterface
    - GFS  : DG GFS for concentration
    - XU  : DG DOF for concentration
*/
template<typename  T, typename GFS, typename XU>
class SimpleEquilibratedFlux
  : public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<typename GFS::Traits::GridViewType,
                                                                           typename Dune::PDELab::LocalFunctionSpace<GFS>::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType,
         GFS::Traits::GridViewType::dimension,
                                                                           Dune::FieldVector<typename Dune::PDELab::LocalFunctionSpace<GFS>::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType,GFS::Traits::GridViewType::dimension> >,
                                          SimpleEquilibratedFlux<T,GFS,XU> >
{
  // extract useful types
  typedef typename GFS::Traits::GridViewType GV;
  typedef typename GV::Traits::template Codim<0>::template Partition<Dune::Interior_Partition>::Iterator ElementIterator;
  typedef Dune::PDELab::LocalFunctionSpace<GFS> LFS;
  typedef typename GFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits LBTraits;
  typedef typename LBTraits::RangeFieldType RF;
  typedef typename GV::IndexSet IndexSet;
  typedef typename GV::Grid::ctype DF;
  enum { dim = GV::dimension };
  typedef typename GV::Traits::template Codim<0>::Entity Element;
  typedef typename GV::IntersectionIterator IntersectionIterator;
  typedef typename IntersectionIterator::Intersection Intersection;
  //typedef typename Dune::RT0QLocalFiniteElement<DF,RF,dim>::Traits::LocalBasisType::Traits::RangeType RT0RangeType;
  typedef typename Dune::RaviartThomasSimplexLocalFiniteElement<dim,DF,RF> RT0Element;
  typedef typename RT0Element::Traits::LocalBasisType::Traits::RangeType RT0RangeType;
  typedef typename Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

  const T& t;
  RT0Element rt0fe;
  mutable std::vector<RT0RangeType> rt0vectors;
  mutable Dune::FieldMatrix<DF,dim,dim> B, B_s, B_n;
  mutable RF determinant, determinant_s;
  mutable int cachedindex;
  typename T::Traits::RangeFieldType time;

  typedef Dune::FieldVector<RF,dim+1> RT0Coeffs;
  GV gv;
  const IndexSet& is;
  std::vector<RT0Coeffs> storedcoeffs;

  Dune::PDELab::ConvectionDiffusionDGWeights::Type weights;
  RF alpha;
  RF phihat_nhat[dim+1];

  // additional members for density driven flow
  typedef Dune::PDELab::LocalFunctionSpace<GFS> LFSU;
  typedef Dune::PDELab::LFSIndexCache<LFSU> LFSUCache;
  typedef typename LFSU::Traits::SizeType size_type;
  typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RFU;
  typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeTypeU;
  typedef typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianTypeU;

public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,dim,Dune::FieldVector<RF,dim> > Traits;
  typedef Dune::PDELab::GridFunctionBase<Traits,SimpleEquilibratedFlux<T,GFS,XU> > BaseT;

  SimpleEquilibratedFlux (const T& t_, const GFS& gfs, XU& u_,
                          Dune::PDELab::ConvectionDiffusionDGWeights::Type weights_=Dune::PDELab::ConvectionDiffusionDGWeights::weightsOff,
                          RF alpha_=0.0)
    : t(t_), cachedindex(-1), time(0), gv(gfs.gridView()), is(gv.indexSet()), storedcoeffs(is.size(0)),
      rt0fe(Dune::GeometryType(Dune::GeometryType::simplex,dim),0),
      rt0vectors(rt0fe.localBasis().size()), weights(weights_), alpha(alpha_)
  {
    //std::cout << "Analyse RT0 Basis on the reference element\n";
    typename Traits::DomainType x,n;

    for (size_type d=0; d<=dim; d++)
      {
        if (d<dim)
          {
            for (size_type i=0; i<dim; i++) x[i] = 0.0;
            x[d] = 0.5;
            for (size_type i=0; i<dim; i++) n[i] = 0.0;
            n[dim-1-d] = -1.0;
          }
        else
          {
            for (size_type i=0; i<dim; i++) x[i] = 0.5;
            for (size_type i=0; i<dim; i++) n[i] = 1.0;
            n /= sqrt(1.0*dim);
          }
        rt0fe.localBasis().evaluateFunction(x,rt0vectors);
        phihat_nhat[d] = rt0vectors[d]*n;
        // std::cout << "face " << d << " at x = " << x << " normal = " << n << " phihat_nhat = " << phihat_nhat[d] << std::endl;
        // for (size_type i=0; i<rt0vectors.size(); i++)
        //   std::cout << " i = " << i << "    v = " << rt0vectors[i] << std::endl;
      }
    update(gfs,u_);
  }

  void update (const GFS& gfs, XU& u_)
  {
    Dune::shared_ptr<const GFS> pgfs(stackobject_to_shared_ptr(gfs));
    Dune::shared_ptr<const XU> pc(stackobject_to_shared_ptr(u_));
    typename T::Traits::RangeType gravity;
    typename T::Traits::RangeFieldType delta_rho;
    LFSU lfsu_s(pgfs);
    LFSU lfsu_n(pgfs);
    LFSUCache lfsu_s_cache(lfsu_s);
    LFSUCache lfsu_n_cache(lfsu_n);

    // kill cache
    cachedindex = -1;

    typename XU::template LocalView<LFSUCache> u_view(u_);

    // compute RT0 coefficients for all interior cells
    for (ElementIterator it = gv.template begin<0,Dune::Interior_Partition>();
         it!=gv.template end<0,Dune::Interior_Partition>(); ++it)
      {
        // get local cell number
      int index_s = is.index(*it);

        // cell geometry
        const Dune::FieldVector<DF,dim>
          inside_cell_center_local = Dune::ReferenceElements<DF,dim>::
          general(it->type()).position(0,0);
        Dune::FieldVector<DF,dim>
          inside_cell_center_global = it->geometry().global(inside_cell_center_local);

        // absolute permeability in primary cell
        typename T::Traits::PermTensorType A_s;
        A_s = t.A(*it,inside_cell_center_local);

        // for coefficient computation
        RF vn[dim+1];    // normal velocities
        B_s = it->geometry().jacobianInverseTransposed(inside_cell_center_local); // the transformation. Assume it is linear
        determinant_s = B_s.determinant();

        lfsu_s.bind(*it);
        lfsu_s_cache.update();
        std::vector<RFU> ul_s(lfsu_s.size());
        u_view.bind(lfsu_s_cache);
        u_view.read(ul_s);
        u_view.unbind();

        // loop over cell neighbors
        IntersectionIterator endit = gv.iend(*it);
        for (IntersectionIterator iit = gv.ibegin(*it); iit!=endit; ++iit)
          {
            // set to zero for processor boundary
            vn[iit->indexInInside()] = 0.0;

            // face geometry
            const Dune::FieldVector<DF,dim-1>&
              face_local = Dune::ReferenceElements<DF,dim-1>::general(iit->geometry().type()).position(0,0);

            // unit outer normal
            const Dune::FieldVector<DF,dim> n_F = iit->centerUnitOuterNormal();

            // interior face
            if (iit->neighbor())
              {
                const Dune::FieldVector<DF,dim>
                  outside_cell_center_local = Dune::ReferenceElements<DF,dim>::
                  general(iit->outside()->type()).position(0,0);
                Dune::FieldVector<DF,dim>
                  outside_cell_center_global = iit->outside()->geometry().global(outside_cell_center_local);

                lfsu_n.bind(*(iit->outside()));
                lfsu_n_cache.update();
                std::vector<RFU> ul_n(lfsu_n.size());
                u_view.bind(lfsu_n_cache);
                u_view.read(ul_n);
                u_view.unbind();

                // face diameter; this should be revised for anisotropic meshes?
                RF h_F = std::min(iit->inside()->geometry().volume(),iit->outside()->geometry().volume())/iit->geometry().volume(); // Houston!

                // tensor times normal
                typename T::Traits::PermTensorType A_n;
                A_n = t.A(*(iit->outside()),outside_cell_center_local);
                Dune::FieldVector<RF,dim> An_F_s;
                A_s.mv(n_F,An_F_s);
                Dune::FieldVector<RF,dim> An_F_n;
                A_n.mv(n_F,An_F_n);

                // compute weights
                RF omega_s;
                RF omega_n;
                RF harmonic_average(0.0);
                if (weights==Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn)
                  {
                    RF delta_s = (An_F_s*n_F);
                    RF delta_n = (An_F_n*n_F);
                    omega_s = delta_n/(delta_s+delta_n+1e-20);
                    omega_n = delta_s/(delta_s+delta_n+1e-20);
                    harmonic_average = 2.0*delta_s*delta_n/(delta_s+delta_n+1e-20);
                  }
                else
                  {
                    omega_s = omega_n = 0.5;
                    harmonic_average = 1.0;
                  }

                // get polynomial degree
                const int order_s = lfsu_s.finiteElement().localBasis().order();
                const int order_n = lfsu_n.finiteElement().localBasis().order();
                int degree = std::max( order_s, order_n );

                // penalty factor
                RF penalty_factor = (alpha/h_F) * harmonic_average * degree*(degree+dim-1);

                // evaluate velocity field and upwinding, assume H(div) velocity field => may choose any side
                Dune::FieldVector<DF,dim> iplocal_s = iit->geometryInInside().center();
                Dune::FieldVector<DF,dim> iplocal_n = iit->geometryInOutside().center();
                typename T::Traits::RangeType b = t.b(*(iit->inside()),iplocal_s);
                RF normalflux = b*n_F;

                // evaluate u at face center
                std::vector<RangeTypeU> phi_s(lfsu_s.size());
                lfsu_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
                std::vector<RangeTypeU> phi_n(lfsu_n.size());
                lfsu_n.finiteElement().localBasis().evaluateFunction(iplocal_n,phi_n);
                RF u_s=0.0;
                for (int i=0; i<lfsu_s.size(); i++)
                  u_s += ul_s[i]*phi_s[i];
                RF u_n=0.0;
                for (int i=0; i<lfsu_n.size(); i++)
                  u_n += ul_n[i]*phi_n[i];

                // evaluate diffusive fluxes at face center
                std::vector<JacobianTypeU> gradphi_s(lfsu_s.size());
                lfsu_s.finiteElement().localBasis().evaluateJacobian(iplocal_s,gradphi_s);
                std::vector<JacobianTypeU> gradphi_n(lfsu_n.size());
                lfsu_n.finiteElement().localBasis().evaluateJacobian(iplocal_n,gradphi_n);

                std::vector<Dune::FieldVector<RF,dim> > tgradphi_s(lfsu_s.size());
                for (size_type i=0; i<lfsu_s.size(); i++) B_s.mv(gradphi_s[i][0],tgradphi_s[i]);
                B_n = iit->outside()->geometry().jacobianInverseTransposed(iplocal_n);
                std::vector<Dune::FieldVector<RF,dim> > tgradphi_n(lfsu_n.size());
                for (size_type i=0; i<lfsu_n.size(); i++) B_n.mv(gradphi_n[i][0],tgradphi_n[i]);

                Dune::FieldVector<RF,dim> gradu_s(0.0);
                for (size_type i=0; i<lfsu_s.size(); i++)
                  gradu_s.axpy(ul_s[i],tgradphi_s[i]);
                Dune::FieldVector<RF,dim> gradu_n(0.0);
                for (size_type i=0; i<lfsu_n.size(); i++)
                  gradu_n.axpy(ul_n[i],tgradphi_n[i]);

                // convective flux term
                if (normalflux>=0)
                  vn[iit->indexInInside()] += normalflux*u_s;
                else
                  vn[iit->indexInInside()] += normalflux*u_n;

                // interior penalty term
                vn[iit->indexInInside()] += penalty_factor*(u_s-u_n);

                // diffusive flux term
                vn[iit->indexInInside()] += -(omega_s*(An_F_s*gradu_s) + omega_n*(An_F_n*gradu_n));
              }

            // boundary face
            if (iit->boundary())
              {
                // face diameter; this should be revised for anisotropic meshes?
                RF h_F = iit->inside()->geometry().volume()/iit->geometry().volume(); // Houston!

                // tensor times normal
                Dune::FieldVector<RF,dim> An_F_s;
                A_s.mv(n_F,An_F_s);
                RF harmonic_average;
                if (weights==Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn)
                  harmonic_average = An_F_s*n_F;
                else
                  harmonic_average = 1.0;

                // get polynomial degree
                const int order_s = lfsu_s.finiteElement().localBasis().order();
                int degree = order_s;

                // penalty factor
                RF penalty_factor = (alpha/h_F) * harmonic_average * degree*(degree+dim-1);

                // evaluate velocity field and upwinding, assume H(div) velocity field => may choose any side
                Dune::FieldVector<DF,dim> iplocal_s = iit->geometryInInside().center();
                typename T::Traits::RangeType b = t.b(*(iit->inside()),iplocal_s);
                RF normalflux = b*n_F;

                // evaluate u at face center
                std::vector<RangeTypeU> phi_s(lfsu_s.size());
                lfsu_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
                RF u_s=0.0;
                for (int i=0; i<lfsu_s.size(); i++)
                  u_s += ul_s[i]*phi_s[i];

                // evaluate diffusive fluxes at face center
                std::vector<JacobianTypeU> gradphi_s(lfsu_s.size());
                lfsu_s.finiteElement().localBasis().evaluateJacobian(iplocal_s,gradphi_s);

                std::vector<Dune::FieldVector<RF,dim> > tgradphi_s(lfsu_s.size());
                for (size_type i=0; i<lfsu_s.size(); i++) B_s.mv(gradphi_s[i][0],tgradphi_s[i]);

                Dune::FieldVector<RF,dim> gradu_s(0.0);
                for (size_type i=0; i<lfsu_s.size(); i++)
                  gradu_s.axpy(ul_s[i],tgradphi_s[i]);

                // now check boundary conditions
                const Dune::FieldVector<DF,dim-1>&
                  face_local = Dune::ReferenceElements<DF,dim-1>::general(iit->geometry().type()).position(0,0);
                BCType bctype = t.bctype(*iit,face_local);

                if (bctype == Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann)
                  {
                    RF j = t.j(*iit,face_local);
                    vn[iit->indexInInside()] += j;
                  }
                if (bctype == Dune::PDELab::ConvectionDiffusionBoundaryConditions::Outflow)
                  {
                    RF o = t.o(*iit,face_local);
                    vn[iit->indexInInside()] += normalflux*u_s + o;
                  }
                if (bctype == Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet)
                  {
                    RF g = t.g(*it,iplocal_s);
                    RF upwindvalue;
                    if (normalflux>=0) upwindvalue=u_s; else upwindvalue=g;
                    vn[iit->indexInInside()] += normalflux*upwindvalue - An_F_s*gradu_s + penalty_factor*(u_s-g);
                  }
              }

            // compute coefficient
            Dune::FieldVector<DF,dim> vstar=n_F; // normal on transformed element
            vstar *= vn[iit->indexInInside()];
            Dune::FieldVector<RF,dim> normalhat(0); // normal on reference element
            if (iit->indexInInside()<dim)
              normalhat[dim-1-iit->indexInInside()] = -1.0;
            else
              {
                for (size_type i=0; i<dim; i++) normalhat[i] = 1.0;
                normalhat /= sqrt(1.0*dim);
              }
            Dune::FieldVector<DF,dim> vstarhat(0);
            B_s.umtv(vstar,vstarhat);
            vstarhat /= determinant_s; // Piola backward transformation
            storedcoeffs[index_s][iit->indexInInside()] = vstarhat*normalhat/phihat_nhat[iit->indexInInside()];
          }
      }
  }

  // set time where operator is to be evaluated (i.e. end of the time intervall)
  void set_time (typename T::Traits::RangeFieldType time_)
  {
    time = time_;
  }

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    // local cell number
    int index = is.index(e);

    // compute velocity on reference element
    rt0fe.localBasis().evaluateFunction(x,rt0vectors);
    typename Traits::RangeType yhat(0);
    for (unsigned int i=0; i<rt0fe.localBasis().size(); i++)
      yhat.axpy(storedcoeffs[index][i],rt0vectors[i]);

    // apply Piola transformation
    if (index != cachedindex)
      {
        B = e.geometry().jacobianTransposed(x); // the transformation. Assume it is linear
        determinant = B.determinant();
        cachedindex = index;
      }
    y = 0;
    B.umtv(yhat,y);
    y /= determinant;
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return gv;
  }
};

template<class DGGFS, class XDG, class P1GFS, class XP1>
void reconstruct_simple_potential(const DGGFS& gfsdg, XDG xdg, const P1GFS& gfsp1, XP1& xp1)
{
  // extract useful types
  typedef typename DGGFS::Traits::GridViewType GV;
  typedef Dune::PDELab::LocalFunctionSpace<DGGFS> DGLFS;
  typedef Dune::PDELab::LocalFunctionSpace<P1GFS> P1LFS;
  typedef Dune::PDELab::LFSIndexCache<DGLFS> LFSDGCache;
  typedef Dune::PDELab::LFSIndexCache<P1LFS> LFSP1Cache;
  typedef typename DGLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;
  typedef typename DGLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeTypeDG;
  typedef typename P1LFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeTypeP1;
  typedef typename GV::Grid::ctype DF;
  typedef typename GV::Traits::template Codim<0>::Entity Element;
  typedef typename GV::Traits::template Codim<0>::template Partition<Dune::Interior_Partition>::Iterator ElementIterator;
  typedef typename DGLFS::Traits::SizeType size_type;

  enum { dim = GV::dimension };

  // get grid view
  GV gv(gfsdg.gridView());

  // DG part
  Dune::shared_ptr<const DGGFS> pgfsdg(stackobject_to_shared_ptr(gfsdg));
  DGLFS lfs_dg(pgfsdg);
  LFSDGCache lfsdg_cache(lfs_dg);
  typename XDG::template LocalView<LFSDGCache> xdg_view(xdg);

  // counter
  XP1 counter(gfsp1,0.0);

  // clear potential
  xp1 = 0.0;

  // P1 part
  Dune::shared_ptr<const P1GFS> pgfsp1(stackobject_to_shared_ptr(gfsp1));
  P1LFS lfs_p1(pgfsp1);
  LFSP1Cache lfsp1_cache(lfs_p1);
  typename XP1::template LocalView<LFSP1Cache> xs_view(xp1);
  typename XP1::template LocalView<LFSP1Cache> xcounter_view(counter);

  // loop over mesh cells
  for (ElementIterator it = gv.template begin<0,Dune::Interior_Partition>();
       it!=gv.template end<0,Dune::Interior_Partition>(); ++it)
    {
      lfs_dg.bind(*it);
      lfsdg_cache.update();
      std::vector<RF> xdgl(lfs_dg.size());
      xdg_view.bind(lfsdg_cache);
      xdg_view.read(xdgl);
      xdg_view.unbind();

      lfs_p1.bind(*it);
      lfsp1_cache.update();
      std::vector<RF> sl(lfs_p1.size(),0.0);
      std::vector<RF> counterl(lfs_p1.size(),0.0);
      if (lfs_p1.size()!=dim+1) std::cout << "size=" << lfs_p1.size() << std::endl;

      // loop over all vertices
      for (size_type i=0; i<=dim; i++)
        {
          // local coordinate of vertex i
          Dune::FieldVector<DF,dim> vertex_local(0.0);
          if (i>0) vertex_local[i-1] = 1.0;

          // evaluate DG function
          RF u_dg=0.0;
          std::vector<RangeTypeDG> phi(lfs_dg.size());
          lfs_dg.finiteElement().localBasis().evaluateFunction(vertex_local,phi);
          for (size_type j=0; j<lfs_dg.size(); j++)
            u_dg += xdgl[j]*phi[j];

          // accumulate to p1 function
          sl[i] = u_dg;

          // increment counter
          counterl[i] = 1.0;
        }

      // write back results
      xs_view.bind(lfsp1_cache);
      xs_view.add(sl);
      xs_view.commit();
      xs_view.unbind();

      xcounter_view.bind(lfsp1_cache);
      xcounter_view.add(counterl);
      xcounter_view.commit();
      xcounter_view.unbind();
    }

  // compute average
  for (unsigned int i=0; i<xp1.N(); i++)
    xp1.base()[i] /= counter.base()[i];
}


template<class PROBLEM, class DGGFS, class XDG,class EFDGF, class P1GFS, class XP1, class P0GFS, class XP0>
void compute_estimator (const PROBLEM& problem, const DGGFS& gfsdg, XDG& xdg, const EFDGF& efdgf, const P1GFS& gfsp1, XP1& xp1, const P0GFS& gfsp0, XP0& eta)
{
  // extract useful types
  typedef typename DGGFS::Traits::GridViewType GV;
  typedef Dune::PDELab::LocalFunctionSpace<DGGFS> DGLFS;
  typedef Dune::PDELab::LocalFunctionSpace<P1GFS> P1LFS;
  typedef Dune::PDELab::LocalFunctionSpace<P0GFS> P0LFS;
  typedef Dune::PDELab::LFSIndexCache<DGLFS> LFSDGCache;
  typedef Dune::PDELab::LFSIndexCache<P1LFS> LFSP1Cache;
  typedef Dune::PDELab::LFSIndexCache<P0LFS> LFSP0Cache;
  typedef typename DGLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RF;
  typedef typename DGLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeTypeDG;
  typedef typename DGLFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianTypeDG;
  typedef typename P0LFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeTypeP0;
  typedef typename P1LFS::Traits::FiniteElementType::Traits::LocalBasisType::Traits::JacobianType JacobianTypeP1;
  typedef typename GV::Grid::ctype DF;
  typedef typename GV::Traits::template Codim<0>::Entity Element;
  typedef typename GV::Traits::template Codim<0>::template Partition<Dune::Interior_Partition>::Iterator ElementIterator;
  typedef typename DGLFS::Traits::SizeType size_type;

  enum { dim = GV::dimension };

  // get grid view
  GV gv(gfsdg.gridView());

  // DG part
  Dune::shared_ptr<const DGGFS> pgfsdg(stackobject_to_shared_ptr(gfsdg));
  DGLFS lfs_dg(pgfsdg);
  LFSDGCache lfsdg_cache(lfs_dg);
  typename XDG::template LocalView<LFSDGCache> xdg_view(xdg);

  // P1 part
  Dune::shared_ptr<const P1GFS> pgfsp1(stackobject_to_shared_ptr(gfsp1));
  P1LFS lfs_p1(pgfsp1);
  LFSP1Cache lfsp1_cache(lfs_p1);
  typename XP1::template LocalView<LFSP1Cache> xp1_view(xp1);

  // P0 part
  Dune::shared_ptr<const P0GFS> pgfsp0(stackobject_to_shared_ptr(gfsp0));
  P0LFS lfs_p0(pgfsp0);
  LFSP0Cache lfsp0_cache(lfs_p0);
  typename XP0::template LocalView<LFSP0Cache> eta_view(eta);
  eta = 0.0;

  // loop over mesh cells
  for (ElementIterator it = gv.template begin<0,Dune::Interior_Partition>();
       it!=gv.template end<0,Dune::Interior_Partition>(); ++it)
    {
      lfs_dg.bind(*it);
      lfsdg_cache.update();
      std::vector<RF> xdgl(lfs_dg.size());
      xdg_view.bind(lfsdg_cache);
      xdg_view.read(xdgl);
      xdg_view.unbind();

      lfs_p1.bind(*it);
      lfsp1_cache.update();
      std::vector<RF> xp1l(lfs_p1.size());
      xp1_view.bind(lfsp1_cache);
      xp1_view.read(xp1l);
      xp1_view.unbind();

      lfs_p0.bind(*it);
      lfsp0_cache.update();
      std::vector<RF> etal(lfs_p0.size(),0.0);

      // cell geometry
      const Dune::FieldVector<DF,dim>
        inside_cell_center_local = Dune::ReferenceElements<DF,dim>::general(it->type()).position(0,0);
      Dune::FieldVector<DF,dim>
        inside_cell_center_global = it->geometry().global(inside_cell_center_local);

      // transformation
      typename Element::Geometry::JacobianInverseTransposed jac;
      jac = it->geometry().jacobianInverseTransposed(inside_cell_center_local); // the transformation. Assume it is linear

      // absolute permeability
      typename PROBLEM::Traits::PermTensorType A;
      A = problem.A(*it,inside_cell_center_local);

      // loop over quadrature points
      Dune::GeometryType gt = it->geometry().type();
      const int intorder = 2*lfs_dg.finiteElement().localBasis().order();
      const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);
      RF sum(0.0);
      for (typename Dune::QuadratureRule<DF,dim>::const_iterator qit=rule.begin(); qit!=rule.end(); ++qit)
        {
          // evaluate u at quadratue point
          std::vector<RangeTypeDG> phi(lfs_dg.size());
          lfs_dg.finiteElement().localBasis().evaluateFunction(qit->position(),phi);
          RF u=0.0;
          for (int i=0; i<lfs_dg.size(); i++)
            u += xdgl[i]*phi[i];

          // grad u at quadrature point for DG function
          std::vector<JacobianTypeDG> gradphi(lfs_dg.size());
          lfs_dg.finiteElement().localBasis().evaluateJacobian(qit->position(),gradphi);
          std::vector<Dune::FieldVector<RF,dim> > tgradphi(lfs_dg.size());
          for (size_type i=0; i<lfs_dg.size(); i++) jac.mv(gradphi[i][0],tgradphi[i]);
          Dune::FieldVector<RF,dim> gradu(0.0);
          for (size_type i=0; i<lfs_dg.size(); i++)
            gradu.axpy(xdgl[i],tgradphi[i]);

          // grad u at quadrature point for s_h
          std::vector<JacobianTypeP1> gradphip1(lfs_p1.size());
          lfs_p1.finiteElement().localBasis().evaluateJacobian(qit->position(),gradphip1);
          std::vector<Dune::FieldVector<RF,dim> > tgradphip1(lfs_p1.size());
          for (size_type i=0; i<lfs_p1.size(); i++) jac.mv(gradphip1[i][0],tgradphip1[i]);
          Dune::FieldVector<RF,dim> grads_h(0.0);
          for (size_type i=0; i<lfs_p1.size(); i++)
            grads_h.axpy(xp1l[i],tgradphip1[i]);

          // integration element
          RF factor = qit->weight() * it->geometry().integrationElement(qit->position());

          // flux difference
          Dune::FieldVector<RF,dim> flux;
          A.mv(gradu,flux);
          typename PROBLEM::Traits::RangeType b = problem.b(*it,qit->position());
          flux.axpy(-u,b);
          typename EFDGF::Traits::RangeType sigma;
          efdgf.evaluate(*it,qit->position(),sigma);
          flux += sigma;
          sum += flux.two_norm2()*factor;

          // nonconformity
          A.mv(gradu,flux);
          Dune::FieldVector<RF,dim> Agrads_h;
          A.mv(grads_h,Agrads_h);
          flux -= Agrads_h;
          sum += flux.two_norm2()*factor;

        }

      etal[0] = sum;

      // write back results
      eta_view.bind(lfsp0_cache);
      eta_view.add(etal);
      eta_view.commit();
      eta_view.unbind();
    }
}


#endif
