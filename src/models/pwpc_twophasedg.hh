// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PM_PWPC_TWOPHASEDG_HH
#define DUNE_PM_PWPC_TWOPHASEDG_HH

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>

#include<dune/geometry/referenceelements.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>

#include<dune/pdelab/finiteelement/localbasiscache.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>

#include"pwpc_twophasedgparam.hh"

namespace Dune {
  namespace PM {

    // enums for method parameters
    struct PwPc_TwoPhaseDGMethod
    {
      enum Type { NIPG, SIPG };
    };

    struct PwPc_TwoPhaseDGWeights
    {
      enum Type { weightsOn, weightsOff };
    };

    /** \brief Weighted interior penalty discontinous Galerkin scheme for two phase flow in porous media

        Template parameters are:
        Param: the parameter class
        FEM1:  finite element map for pw (needed for the cache)
        FEM2:  finite element map for pc (needed for the cache)
    */
    template<typename Param, typename FEM1, typename FEM2>
    class PwPc_TwoPhaseDG
      : public Dune::PDELab::NumericalJacobianVolume<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianApplyVolume<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianSkeleton<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianApplySkeleton<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianBoundary<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianApplyBoundary<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >,
        public Dune::PDELab::FullSkeletonPattern,
        public Dune::PDELab::FullVolumePattern,
        public Dune::PDELab::LocalOperatorDefaultFlags,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename Param::Traits::RangeFieldType>
    {
      enum { dim = Param::Traits::GridViewType::dimension };

      typedef typename Param::Traits::RangeFieldType Real;
      typedef typename PwPc_TwoPhaseDGBoundaryConditions::Type BCType;

    public:
      // pattern assembly flags
      enum { doPatternVolume = true };
      enum { doPatternSkeleton = true };

      // residual assembly flags
      enum { doAlphaVolume  = true };
      enum { doAlphaSkeleton  = true };
      enum { doAlphaBoundary  = true };
      enum { doLambdaVolume  = true };

      //! constructor: pass parameter object
      PwPc_TwoPhaseDG (Param& param_,
                       PwPc_TwoPhaseDGMethod::Type method_=PwPc_TwoPhaseDGMethod::SIPG,
                       PwPc_TwoPhaseDGWeights::Type weights_=PwPc_TwoPhaseDGWeights::weightsOn,
                       Real alpha_=5.0,
                       int quadrature_factor_=3)
        : Dune::PDELab::NumericalJacobianVolume<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianApplyVolume<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianSkeleton<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianApplySkeleton<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianBoundary<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianApplyBoundary<PwPc_TwoPhaseDG<Param,FEM1,FEM2> >(1e-7),
        param(param_), method(method_), weights(weights_),
        alpha(alpha_), quadrature_factor(quadrature_factor_),
        cachepw(10), cachepc(10)
      {
        theta = -1.0;
        if (method==PwPc_TwoPhaseDGMethod::SIPG) theta = 1.0;
      }

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PwPc_TwoPhaseDG");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw = lfsv.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc = lfsv.template getChild<1>();

        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = EG::Geometry::dimension;
        const int dimw = EG::Geometry::dimensionworld;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        int orderpw = lfspw.finiteElement().localBasis().order();
        int orderpc = lfspc.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(orderpw,orderpc);
        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

        // evaluate permeability tensor at cell center, assume it is constant over elements
        typename Param::Traits::PermTensorType K;
        Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(gt).position(0,0);
        K = param.K(eg.entity(),localcenter);

        // jacobian of transformation (assume that transformation is affine)
        Dune::FieldMatrix<DF,dimw,dim> jact(eg.geometry().jacobianInverseTransposed(localcenter));
        RF integrationElement = eg.geometry().integrationElement(localcenter);

        // loop over quadrature points
        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            //=================================
            // evaluate pw
            //=================================

            // evaluate basis functions for pressure (we assume Galerkin method lfsu=lfsv)
            const std::vector<RangeType>& phipw = cachepw[orderpw].evaluateFunction(it->position(),lfspw.finiteElement().localBasis());
            const std::vector<JacobianType>& jacpw = cachepw[orderpw].evaluateJacobian(it->position(),lfspw.finiteElement().localBasis());

            // evaluate pw
            RF pw=0.0;
            for (size_type i=0; i<lfspw.size(); i++) pw += x(lfspw,i)*phipw[i];

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > gradphipw(lfspw.size());
            for (size_type i=0; i<lfspw.size(); i++)
              jact.mv(jacpw[i][0],gradphipw[i]);

            // compute gradient of pw
            Dune::FieldVector<RF,dim> gradpw(0.0);
            for (size_type i=0; i<lfspw.size(); i++)
              gradpw.axpy(x(lfspw,i),gradphipw[i]);

            //=================================
            // evaluate pc
            //=================================

            // evaluate basis functions for saturation (we assume Galerkin method lfsu=lfsv)
            const std::vector<RangeType>& phipc = cachepc[orderpc].evaluateFunction(it->position(),lfspc.finiteElement().localBasis());
            const std::vector<JacobianType>& jacpc = cachepc[orderpc].evaluateJacobian(it->position(),lfspc.finiteElement().localBasis());

            // evaluate pc
            RF pc=0.0;
            for (size_type i=0; i<lfspc.size(); i++) pc += x(lfspc,i)*phipc[i];

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > gradphipc(lfspc.size());
            for (size_type i=0; i<lfspc.size(); i++)
              jact.mv(jacpc[i][0],gradphipc[i]);

            // compute gradient of pc
            Dune::FieldVector<RF,dim> gradpc(0.0);
            for (size_type i=0; i<lfspc.size(); i++)
              gradpc.axpy(x(lfspc,i),gradphipc[i]);

            //=================================
            // evaluate parameters
            //=================================

            RF sw = param.psi(eg.entity(),localcenter,pc);
            RF sn = 1.0-sw;
            RF rhow = param.rho1();
            RF krw = param.kr1(eg.entity(),localcenter,sw);
            RF muw = param.mu1();
            RF rhon = param.rho2();
            RF krn = param.kr2(eg.entity(),localcenter,sn);
            RF mun = param.mu2();

            // compute phase mobilities
            RF lambdaw = krw/muw;
            RF lambdan = krn/mun;

            // compute phase velocities velocity q_x = K(gradpw_x - rho_x*g)
            Dune::FieldVector<RF,dim> v,qw,qn;
            v = gradpw; v.axpy(-rhow,param.gravity()); K.mv(v,qw);
            v = gradpw; v += gradpc; v.axpy(-rhon,param.gravity()); K.mv(v,qn);

            RF factor = it->weight() * integrationElement;

            //=================================
            // assemble pressure equation
            //=================================

            // integrate (lambdaw*qw + lambdan*qn) * grad phi
            Dune::FieldVector<RF,dim> u(0.0);
            u.axpy(lambdaw,qw);
            u.axpy(lambdan,qn);
            for (size_type i=0; i<lfspw.size(); i++)
              r.accumulate(lfspw,i,(u*gradphipw[i])*factor);

            //=================================
            // assemble saturation equation
            //=================================

            // integrate lambdan * K( grad pw + grad pc - rhon*g) * grad phi
            for (size_type i=0; i<lfspc.size(); i++)
              r.accumulate(lfspc,i,(qn*gradphipc[i])*lambdan*factor);
          }
      }

      // skeleton integral depending on test and ansatz functions
      // each face is only visited ONCE!
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                           R& r_s, R& r_n) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PwPc_TwoPhaseDG");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw_s = lfsv_s.template getChild<0>();
        const PwSpace& lfspw_n = lfsv_n.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc_s = lfsv_s.template getChild<1>();
        const PcSpace& lfspc_n = lfsv_n.template getChild<1>();

        // domain and range field type
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = IG::dimension;
        int orderpw_s = lfspw_s.finiteElement().localBasis().order();
        int orderpw_n = lfspw_n.finiteElement().localBasis().order();
        int orderpc_s = lfspc_s.finiteElement().localBasis().order();
        int orderpc_n = lfspc_n.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(std::max(orderpw_s,orderpw_n),std::max(orderpc_s,orderpc_n))+1;

        // evaluate permeability tensors and normal
        const Dune::FieldVector<DF,dim>&
          inside_local = Dune::ReferenceElements<DF,dim>::general(ig.inside()->type()).position(0,0);
        const Dune::FieldVector<DF,dim>&
          outside_local = Dune::ReferenceElements<DF,dim>::general(ig.outside()->type()).position(0,0);
        typename Param::Traits::PermTensorType K_s, K_n;
        K_s = param.K(*(ig.inside()),inside_local);
        K_n = param.K(*(ig.outside()),outside_local);
        Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
        Dune::FieldVector<RF,dim> Kn_F_s; K_s.mv(n_F,Kn_F_s);
        Dune::FieldVector<RF,dim> Kn_F_n; K_n.mv(n_F,Kn_F_n);

        // face diameter
        RF h_F;
        if (dim>1)
          h_F = std::min(ig.inside()->geometry().volume(),ig.outside()->geometry().volume())/ig.geometry().volume(); // Houston!
        else
          h_F = std::min(ig.inside()->geometry().volume(),ig.outside()->geometry().volume());

        // get entry pressure on both sides
        RF pentry_s = param.pe(*(ig.inside()),inside_local);
        RF pentry_n = param.pe(*(ig.outside()),outside_local);

        // pre-compute permeability dependent part of weights
        RF delta_s = (Kn_F_s*n_F);
        RF delta_n = (Kn_F_n*n_F);
        RF delta_havg = 2.0*delta_s*delta_n/(delta_s+delta_n);
        RF omega_s,omega_n;
        if ( weights==PwPc_TwoPhaseDGWeights::weightsOn )
          {
            omega_s = delta_n/(delta_s+delta_n);
            omega_n = delta_s/(delta_s+delta_n);
          }
        else omega_s = omega_n = 0.5;

        // precompute part of penalty factor
        RF penaltyw =  (alpha/h_F)*std::max(orderpw_s,orderpw_n)*(std::max(orderpw_s,orderpw_n)+dim-1);
        RF penaltyc =  (alpha/h_F)*std::max(orderpc_s,orderpc_n)*(std::max(orderpc_s,orderpc_n)+dim-1);

        // select quadrature rule
        Dune::GeometryType gtface = ig.geometryInInside().type();
        const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

        // transformation; ASSUME it is affine
        Dune::FieldVector<DF,dim> iplocal_s(0.0);
        Dune::FieldVector<DF,dim> iplocal_n(0.0);
        Dune::FieldMatrix<DF,dim,dim> jact_s(ig.inside()->geometry().jacobianInverseTransposed(iplocal_s));
        Dune::FieldMatrix<DF,dim,dim> jact_n(ig.outside()->geometry().jacobianInverseTransposed(iplocal_n));

        // loop over quadrature points and integrate normal flux
        for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // position of quadrature point in local coordinates of elements
            iplocal_s = ig.geometryInInside().global(it->position());
            iplocal_n = ig.geometryInOutside().global(it->position());

            //=================================
            // evaluate pw
            //=================================

            // evaluate basis functions
            const std::vector<RangeType>& phipw_s = cachepw[orderpw_s].evaluateFunction(iplocal_s,lfspw_s.finiteElement().localBasis());
            const std::vector<RangeType>& phipw_n = cachepw[orderpw_n].evaluateFunction(iplocal_n,lfspw_n.finiteElement().localBasis());

            // evaluate pressure
            RF pw_s=0.0; for (size_type i=0; i<lfspw_s.size(); i++) pw_s += x_s(lfspw_s,i)*phipw_s[i];
            RF pw_n=0.0; for (size_type i=0; i<lfspw_n.size(); i++) pw_n += x_n(lfspw_n,i)*phipw_n[i];

            // evaluate gradient of basis functions (we assume Galerkin method lfsu=lfsv)
            const std::vector<JacobianType>& gradphipw_s = cachepw[orderpw_s].evaluateJacobian(iplocal_s,lfspw_s.finiteElement().localBasis());
            const std::vector<JacobianType>& gradphipw_n = cachepw[orderpw_n].evaluateJacobian(iplocal_n,lfspw_n.finiteElement().localBasis());

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > tgradphipw_s(lfspw_s.size());
            for (size_type i=0; i<lfspw_s.size(); i++) jact_s.mv(gradphipw_s[i][0],tgradphipw_s[i]);
            std::vector<Dune::FieldVector<RF,dim> > tgradphipw_n(lfspw_n.size());
            for (size_type i=0; i<lfspw_n.size(); i++) jact_n.mv(gradphipw_n[i][0],tgradphipw_n[i]);

            // compute gradient of pw
            Dune::FieldVector<RF,dim> gradpw_s(0.0);
            for (size_type i=0; i<lfspw_s.size(); i++) gradpw_s.axpy(x_s(lfspw_s,i),tgradphipw_s[i]);
            Dune::FieldVector<RF,dim> gradpw_n(0.0);
            for (size_type i=0; i<lfspw_n.size(); i++) gradpw_n.axpy(x_n(lfspw_n,i),tgradphipw_n[i]);

            //=================================
            // evaluate pc
            //=================================

            // evaluate basis functions
            const std::vector<RangeType>& phipc_s = cachepc[orderpc_s].evaluateFunction(iplocal_s,lfspc_s.finiteElement().localBasis());
            const std::vector<RangeType>& phipc_n = cachepc[orderpc_n].evaluateFunction(iplocal_n,lfspc_n.finiteElement().localBasis());

            // evaluate pressure
            RF pc_s=0.0; for (size_type i=0; i<lfspc_s.size(); i++) pc_s += x_s(lfspc_s,i)*phipc_s[i];
            RF pc_n=0.0; for (size_type i=0; i<lfspc_n.size(); i++) pc_n += x_n(lfspc_n,i)*phipc_n[i];

            // evaluate gradient of basis functions (we assume Galerkin method lfsu=lfsv)
            const std::vector<JacobianType>& gradphipc_s = cachepc[orderpc_s].evaluateJacobian(iplocal_s,lfspc_s.finiteElement().localBasis());
            const std::vector<JacobianType>& gradphipc_n = cachepc[orderpc_n].evaluateJacobian(iplocal_n,lfspc_n.finiteElement().localBasis());

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > tgradphipc_s(lfspc_s.size());
            for (size_type i=0; i<lfspc_s.size(); i++) jact_s.mv(gradphipc_s[i][0],tgradphipc_s[i]);
            std::vector<Dune::FieldVector<RF,dim> > tgradphipc_n(lfspc_n.size());
            for (size_type i=0; i<lfspc_n.size(); i++) jact_n.mv(gradphipc_n[i][0],tgradphipc_n[i]);

            // compute gradient of pressure
            Dune::FieldVector<RF,dim> gradpc_s(0.0);
            for (size_type i=0; i<lfspc_s.size(); i++) gradpc_s.axpy(x_s(lfspc_s,i),tgradphipc_s[i]);
            Dune::FieldVector<RF,dim> gradpc_n(0.0);
            for (size_type i=0; i<lfspc_n.size(); i++) gradpc_n.axpy(x_n(lfspc_n,i),tgradphipc_n[i]);

            //=================================
            //  evaluate parameters
            //=================================

            RF sw_s = param.psi(*(ig.inside()),inside_local,pc_s);
            RF sw_n = param.psi(*(ig.outside()),outside_local,pc_n);
            RF sn_s = 1.0-sw_s;
            RF sn_n = 1.0-sw_n;
            RF rhow = param.rho1();
            RF krw_s = param.kr1(*(ig.inside()),inside_local,sw_s);
            RF krw_n = param.kr1(*(ig.outside()),outside_local,sw_n);
            RF muw = param.mu1();
            RF rhon = param.rho2();
            RF krn_s = param.kr2(*(ig.inside()),inside_local,sn_s);
            RF krn_n = param.kr2(*(ig.outside()),outside_local,sn_n);
            RF mun = param.mu2();

            // compute phase mobilities
            RF lambdaw_s = krw_s/muw;
            RF lambdaw_n = krw_n/muw;
            RF lambdan_s = krn_s/mun;
            RF lambdan_n = krn_n/mun;
            RF lambda_s = lambdaw_s+lambdan_s;
            RF lambda_n = lambdaw_n+lambdan_n;

            // compute phase velocities velocity q_x = K(gradpw_x - rho_x*g)
            Dune::FieldVector<RF,dim> v,qw_s,qw_n,qn_s,qn_n;
            v = gradpw_s; v.axpy(-rhow,param.gravity()); K_s.mv(v,qw_s); qw_s *= -1.0;
            v = gradpw_n; v.axpy(-rhow,param.gravity()); K_n.mv(v,qw_n); qw_n *= -1.0;
            v = gradpw_s; v += gradpc_s; v.axpy(-rhon,param.gravity()); K_s.mv(v,qn_s); qn_s *= -1.0;
            v = gradpw_n; v += gradpc_n; v.axpy(-rhon,param.gravity()); K_n.mv(v,qn_n); qn_n *= -1.0;

            // integration factor
            RF factor = it->weight() * ig.geometry().integrationElement(it->position());

            //=================================
            // assemble pressure equation
            //=================================

            // standard flux term
            RF uavg = (((qw_s*n_F)*lambdaw_s + (qn_s*n_F)*lambdan_s)*omega_s + ((qw_n*n_F)*lambdaw_n + (qn_n*n_F)*lambdan_n)*omega_n)*factor;
            for (size_type i=0; i<lfspw_s.size(); i++) r_s.accumulate(lfspw_s,i,uavg*phipw_s[i]);
            for (size_type i=0; i<lfspw_n.size(); i++) r_n.accumulate(lfspw_n,i,uavg*(-1.0*phipw_n[i]));

            // consistency term
            RF cfactorw = -theta * (pw_s-pw_n) * factor;
            for (size_type i=0; i<lfspw_s.size(); i++)
              r_s.accumulate(lfspw_s,i,cfactorw * omega_s * lambda_s * (Kn_F_s*tgradphipw_s[i]));
            for (size_type i=0; i<lfspw_n.size(); i++)
              r_n.accumulate(lfspw_n,i,cfactorw * omega_n * lambda_n * (Kn_F_n*tgradphipw_n[i]));

            // IP term
            RF havgw;
            {
              RF lambdadelta_s = lambda_s*delta_s;
              RF lambdadelta_n = lambda_n*delta_n;
              havgw = 2.0*lambdadelta_s*lambdadelta_n/(lambdadelta_s+lambdadelta_n);
            }
            RF iptermw = penaltyw * havgw * (pw_s-pw_n) * factor;
            for (size_type i=0; i<lfspw_s.size(); i++)
              r_s.accumulate(lfspw_s,i,iptermw * phipw_s[i]);
            for (size_type i=0; i<lfspw_n.size(); i++)
              r_n.accumulate(lfspw_n,i,iptermw * (-1.0*phipw_n[i]));

            //=================================
            // assemble saturation equation
            //=================================

            // compute averaged normal flux for nonwetting phase
            RF qn_avg = (qn_s*n_F)*omega_s + (qn_n*n_F)*omega_n;

            // upwinding of capillary pressure
            RF pc_upwind;
            if (qn_avg>=0) pc_upwind = pc_s; else pc_upwind = pc_n;
            RF sn_upwind_s = 1.0-param.psi(*(ig.inside()),inside_local,pc_upwind);
            RF sn_upwind_n = 1.0-param.psi(*(ig.outside()),outside_local,pc_upwind);
            RF xi_s = param.kr2(*(ig.inside()),inside_local,sn_upwind_s);
            RF xi_n = param.kr2(*(ig.outside()),outside_local,sn_upwind_n);
            RF lambdan_upwind = 2.0*xi_s*xi_n/(xi_s+xi_n+1E-16)/mun; // Assume that krn>=0
            RF lambdan_face = lambdan_upwind;
            // if (pentry_s!=pentry_n)
            //   {
            //     // this is a media discontinuity
            //     if (pentry_s<pentry_n)
            //       {
            //         // self has the lower entry pressure
            //         if (pc_s<pentry_n)
            //           // no continuity, take mobility from high entry pressure region
            //           lambdan_face = lambdan_n;
            //       }
            //     else
            //       {
            //         // neighbor has the lower entry pressure
            //         if (pc_n<pentry_s)
            //           // no continuity, take mobility from high entry pressure region
            //           lambdan_face = lambdan_s;
            //       }
            //   }

            // standard flux term
            RF fluxn = lambdan_face * qn_avg * factor;
            for (size_type i=0; i<lfspc_s.size(); i++) r_s.accumulate(lfspc_s,i,fluxn * phipc_s[i]);
            for (size_type i=0; i<lfspc_n.size(); i++) r_n.accumulate(lfspc_n,i,fluxn * (-1.0*phipc_n[i]));

            // consistency term & IP term
            RF cfactorc = -theta * lambdan_face * (pc_s-pc_n) * factor;
            RF iptermc = penaltyc * 0.5*(lambdan_s+lambdan_n) * delta_havg * (pc_s-pc_n) * factor;
            if (pentry_s!=pentry_n)
              {
                // media discont. with entry pressure reached
                iptermc = penaltyc * lambdan_face * delta_havg * (pc_s-pc_n) * factor;

                // check if entry pressure is not yet reached
                if (pentry_s<pentry_n)
                  {
                    // self has the lower entry pressure
                    if (pc_s<pentry_n)
                      {
                        // no continuity, pc_n should be entry pressure
                        cfactorc = -theta * lambdan_face * (pentry_n-pc_n) * factor;
                        iptermc = penaltyc * lambdan_face * delta_havg * (pentry_n-pc_n) * factor;
                      }
                  }
                else
                  {
                    // neighbor has the lower entry pressure
                    if (pc_n<pentry_s)
                      {
                        // no continuity, pc_s should be entry pressure
                        cfactorc = -theta * lambdan_face * (pc_s-pentry_s) * factor;
                        iptermc = penaltyc * lambdan_face * delta_havg * (pc_s-pentry_s) * factor;
                      }
                  }
              }
            for (size_type i=0; i<lfspc_s.size(); i++)
              r_s.accumulate(lfspc_s,i,cfactorc * omega_s * (Kn_F_s*tgradphipc_s[i]) + iptermc * phipc_s[i]);
            for (size_type i=0; i<lfspc_n.size(); i++)
              r_n.accumulate(lfspc_n,i,cfactorc * omega_n * (Kn_F_n*tgradphipc_n[i]) + iptermc * (-1.0*phipc_n[i]));
          }
      }

      // skeleton integral depending on test and ansatz functions
      // We put the Dirchlet evaluation also in the alpha term to save some geometry evaluations
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           R& r_s) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PwPc_TwoPhaseDG");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw_s = lfsv_s.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc_s = lfsv_s.template getChild<1>();

        // domain and range field type
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = IG::dimension;
        int orderpw_s = lfspw_s.finiteElement().localBasis().order();
        int orderpc_s = lfspc_s.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(orderpw_s,orderpc_s)+1;

        // evaluate permeability tensor and normal
        const Dune::FieldVector<DF,dim>&
          inside_local = Dune::ReferenceElements<DF,dim>::general(ig.inside()->type()).position(0,0);
        typename Param::Traits::PermTensorType K_s;
        K_s = param.K(*(ig.inside()),inside_local);
        Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
        Dune::FieldVector<RF,dim> Kn_F_s; K_s.mv(n_F,Kn_F_s);
        RF delta_s = (Kn_F_s*n_F);

        // face diameter
        RF h_F;
        if (dim>1)
          h_F = ig.inside()->geometry().volume()/ig.geometry().volume(); // Houston!
        else
          h_F = ig.inside()->geometry().volume();

        // precompute part of penalty factor
        RF penaltyw =  (alpha/h_F)*orderpw_s*(orderpw_s+dim-1);
        RF penaltyc =  (alpha/h_F)*orderpc_s*(orderpc_s+dim-1);

        // select quadrature rule
        Dune::GeometryType gtface = ig.geometryInInside().type();
        const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

        // transformation; ASSUME it is affine
        Dune::FieldVector<DF,dim> iplocal_s(0.0);
        Dune::FieldMatrix<DF,dim,dim> jact_s(ig.inside()->geometry().jacobianInverseTransposed(iplocal_s));

        // evaluate boundary condition
        const Dune::FieldVector<DF,dim-1>
          face_local = Dune::ReferenceElements<DF,dim-1>::general(gtface).position(0,0);
        BCType bctype1 = param.bc1(ig.intersection(),face_local,time);
        BCType bctype2 = param.bc2(ig.intersection(),face_local,time);

        // loop over quadrature points and integrate normal flux
        for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // position of quadrature point in local coordinates of elements
            Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().global(it->position());

            //=================================
            // evaluate pw
            //=================================

            // evaluate basis functions
            const std::vector<RangeType>& phipw_s = cachepw[orderpw_s].evaluateFunction(iplocal_s,lfspw_s.finiteElement().localBasis());

            // evaluate pressure
            RF pw_s=0.0;
            for (size_type i=0; i<lfspw_s.size(); i++) pw_s += x_s(lfspw_s,i)*phipw_s[i];

            // evaluate gradient of basis functions (we assume Galerkin method lfsu=lfsv)
            const std::vector<JacobianType>& gradphipw_s = cachepw[orderpw_s].evaluateJacobian(iplocal_s,lfspw_s.finiteElement().localBasis());

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > tgradphipw_s(lfspw_s.size());
            for (size_type i=0; i<lfspw_s.size(); i++) jact_s.mv(gradphipw_s[i][0],tgradphipw_s[i]);

            // compute gradient of pw
            Dune::FieldVector<RF,dim> gradpw_s(0.0);
            for (size_type i=0; i<lfspw_s.size(); i++) gradpw_s.axpy(x_s(lfspw_s,i),tgradphipw_s[i]);

            //=================================
            // evaluate pc
            //=================================

            // evaluate basis functions
            const std::vector<RangeType>& phipc_s = cachepc[orderpc_s].evaluateFunction(iplocal_s,lfspc_s.finiteElement().localBasis());

            // evaluate pressure
            RF pc_s=0.0; for (size_type i=0; i<lfspc_s.size(); i++) pc_s += x_s(lfspc_s,i)*phipc_s[i];

            // evaluate gradient of basis functions (we assume Galerkin method lfsu=lfsv)
            const std::vector<JacobianType>& gradphipc_s = cachepc[orderpc_s].evaluateJacobian(iplocal_s,lfspc_s.finiteElement().localBasis());

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > tgradphipc_s(lfspc_s.size());
            for (size_type i=0; i<lfspc_s.size(); i++) jact_s.mv(gradphipc_s[i][0],tgradphipc_s[i]);

            // compute gradient of pressure
            Dune::FieldVector<RF,dim> gradpc_s(0.0);
            for (size_type i=0; i<lfspc_s.size(); i++) gradpc_s.axpy(x_s(lfspc_s,i),tgradphipc_s[i]);

            //=================================
            //  evaluate parameters
            //=================================

            RF sw_s = param.psi(*(ig.inside()),inside_local,pc_s);
            RF sn_s = 1.0-sw_s;
            RF rhow = param.rho1();
            RF krw_s = param.kr1(*(ig.inside()),inside_local,sw_s);
            RF muw = param.mu1();
            RF rhon = param.rho2();
            RF krn_s = param.kr2(*(ig.inside()),inside_local,sn_s);
            RF mun = param.mu2();

            // compute phase mobilities
            RF lambdaw_s = krw_s/muw;
            RF lambdan_s = krn_s/mun;
            RF lambda_s = lambdaw_s+lambdan_s;

            // compute phase velocities velocity q_x = K(gradpw_x - rho_x*g)
            Dune::FieldVector<RF,dim> v,qw_s,qn_s;
            v = gradpw_s; v.axpy(-rhow,param.gravity()); K_s.mv(v,qw_s); qw_s *= -1.0;
            v = gradpw_s; v += gradpc_s; v.axpy(-rhon,param.gravity()); K_s.mv(v,qn_s); qn_s *= -1.0;

            // integration factor
            RF factor = it->weight() * ig.geometry().integrationElement(it->position());

            //=================================
            // assemble pressure equation
            //=================================

            // flux boundary condition
            if (bctype1 == PwPc_TwoPhaseDGBoundaryConditions::Flux)
              {
                // evaluate flux boundary condition
                RF j1 = param.j1(ig.intersection(),it->position(),time);

                // integrate
                for (size_type i=0; i<lfspw_s.size(); i++)
                  r_s.accumulate(lfspw_s,i,j1 * phipw_s[i] * factor);
              }

            // Dirichlet boundary condition
            if (bctype1 == PwPc_TwoPhaseDGBoundaryConditions::Dirichlet)
              {
                // standard flux term
                RF uavg = ((qw_s*n_F)*lambdaw_s + (qn_s*n_F)*lambdan_s) * factor;
                for (size_type i=0; i<lfspw_s.size(); i++) r_s.accumulate(lfspw_s,i,uavg*phipw_s[i]);

                // evaluate Dirichlet boundary condition
                RF pw_n = param.g1(ig.intersection(),it->position(),time);

                // consistency term
                RF cfactorw = -theta * lambda_s * (pw_s-pw_n) * factor;
                for (size_type i=0; i<lfspw_s.size(); i++)
                  r_s.accumulate(lfspw_s,i,cfactorw * (Kn_F_s*tgradphipw_s[i]));

                // IP term
                RF iptermw = penaltyw * lambda_s * delta_s * (pw_s-pw_n) * factor;
                for (size_type i=0; i<lfspw_s.size(); i++)
                  r_s.accumulate(lfspw_s,i,iptermw * phipw_s[i]);
              }

            //=================================
            // assemble saturation equation
            //=================================

            // flux boundary condition
            if (bctype2 == PwPc_TwoPhaseDGBoundaryConditions::Flux)
              {
                // evaluate flux boundary condition
                RF j2 = param.j2(ig.intersection(),it->position(),time);

                // integrate
                for (size_type i=0; i<lfspc_s.size(); i++)
                  r_s.accumulate(lfspc_s,i,j2 * phipc_s[i] * factor);
              }

            // Dirichlet boundary condition
            if (bctype2 == PwPc_TwoPhaseDGBoundaryConditions::Dirichlet)
              {
                // compute normal flux for nonwetting phase
                RF qn_avg = qn_s*n_F;

                // upwinding of capillary pressure
                RF lambdan_upwind = lambdan_s;

                // standard flux term
                RF fluxn = lambdan_upwind * qn_avg * factor;
                for (size_type i=0; i<lfspc_s.size(); i++) r_s.accumulate(lfspc_s,i,fluxn * phipc_s[i]);

                // evaluate Dirichlet boundary condition
                RF pc_n = param.g2(ig.intersection(),it->position(),time);

                // consistency term
                RF cfactorc = -theta * lambdan_upwind * (pc_s-pc_n) * factor;
                for (size_type i=0; i<lfspc_s.size(); i++)
                  r_s.accumulate(lfspc_s,i,cfactorc * (Kn_F_s*tgradphipc_s[i]));

                // IP term
                RF iptermc = penaltyc * lambdan_upwind * delta_s * (pc_s-pc_n) * factor;
                for (size_type i=0; i<lfspc_s.size(); i++)
                  r_s.accumulate(lfspc_s,i,iptermc * phipc_s[i]);
              }
          }
      }

      // volume integral depending only on test functions
      template<typename EG, typename LFSV, typename R>
      void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PwPc_TwoPhaseDG");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw = lfsv.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc = lfsv.template getChild<1>();

        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = EG::Geometry::dimension;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        int orderpw = lfspw.finiteElement().localBasis().order();
        int orderpc = lfspc.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(orderpw,orderpc);
        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

        // jacobian of transformation (assume that transformation is affine)
        Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(gt).position(0,0);
        RF integrationElement = eg.geometry().integrationElement(localcenter);

        // loop over quadrature points
        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // evaluate shape functions
            const std::vector<RangeType>& phipw = cachepw[orderpw].evaluateFunction(it->position(),lfspw.finiteElement().localBasis());

            // evaluate right hand side parameter function
            Real f1 = param.f1(eg.entity(),it->position(),time);

            // integrate f
            RF factor = it->weight() * integrationElement;
            for (size_type i=0; i<lfspw.size(); i++)
              r.accumulate(lfspw,i,-f1 * phipw[i] * factor);

            // evaluate shape functions
            const std::vector<RangeType>& phipc = cachepc[orderpc].evaluateFunction(it->position(),lfspc.finiteElement().localBasis());

            // evaluate right hand side parameter function
            Real f2 = param.f2(eg.entity(),it->position(),time);

            // integrate f
            for (size_type i=0; i<lfspc.size(); i++)
              r.accumulate(lfspc,i,-f2 * phipc[i] * factor);
          }
      }

      //! set time for subsequent evaluation
      void setTime (typename Param::Traits::RangeFieldType t)
      {
        time = t;
        param.setTime(t);
      }

    private:
      Param& param;  // two phase parameter class
      PwPc_TwoPhaseDGMethod::Type method;
      PwPc_TwoPhaseDGWeights::Type weights;
      Real alpha;
      int quadrature_factor;
      Real theta;
      typedef typename FEM1::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType1;
      typedef typename FEM2::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType2;
      typedef Dune::PDELab::LocalBasisCache<LocalBasisType1> Cache1;
      typedef Dune::PDELab::LocalBasisCache<LocalBasisType2> Cache2;
      std::vector<Cache1> cachepw;
      std::vector<Cache2> cachepc;
      mutable Real time;
    };


    /** a local operator for the mass operator (L_2 integral)
     *
     * \f{align*}{
     \int_\Omega uv dx
     * \f}
     */
    template<class Param, typename FEM1, typename FEM2>
    class PwPc_TwoPhaseDGTemporal : public Dune::PDELab::NumericalJacobianApplyVolume<PwPc_TwoPhaseDGTemporal<Param,FEM1,FEM2> >,
                                    public Dune::PDELab::NumericalJacobianVolume<PwPc_TwoPhaseDGTemporal<Param,FEM1,FEM2> >,
                                    public Dune::PDELab::FullVolumePattern,
                                    public Dune::PDELab::LocalOperatorDefaultFlags,
                                    public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };

      PwPc_TwoPhaseDGTemporal (Param& param_, int quadrature_factor_=3)
        : Dune::PDELab::NumericalJacobianApplyVolume<PwPc_TwoPhaseDGTemporal<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianVolume<PwPc_TwoPhaseDGTemporal<Param,FEM1,FEM2> >(1e-7),
        param(param_), quadrature_factor(quadrature_factor_),
        cachepw(10), cachepc(10)
      {}

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PwPc_TwoPhaseDG");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw = lfsv.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc = lfsv.template getChild<1>();

        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = EG::Geometry::dimension;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        int orderpw = lfspw.finiteElement().localBasis().order();
        int orderpc = lfspc.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(orderpw,orderpc);
        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

        // jacobian of transformation (assume that transformation is affine)
        Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(gt).position(0,0);
        RF integrationElement = eg.geometry().integrationElement(localcenter);

        // loop over quadrature points
        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            //=================================
            // evaluate pc
            //=================================

            // evaluate basis functions for saturation (we assume Galerkin method lfsu=lfsv)
            const std::vector<RangeType>& phipc = cachepc[orderpc].evaluateFunction(it->position(),lfspc.finiteElement().localBasis());

            // evaluate pc
            RF pc=0.0;
            for (size_type i=0; i<lfspc.size(); i++) pc += x(lfspc,i)*phipc[i];

            //=================================
            // assemble saturation equation
            //=================================

            // evaluate parameters phase 2
            RF sw = param.psi(eg.entity(),it->position(),pc);
            RF sn = 1.0-sw;
            RF porosity = param.phi(eg.entity(),it->position());

            // porosity*sn*phi_i
            RF storage2 = porosity * sn * it->weight() * integrationElement;
            for (size_type i=0; i<lfspc.size(); i++)
              r.accumulate(lfspc,i,storage2*phipc[i]);
          }
      }

      //! set time for subsequent evaluation
      void setTime (typename Param::Traits::RangeFieldType t)
      {
        time = t;
      }

    private:
      Param& param;
      int quadrature_factor;
      typename Param::Traits::RangeFieldType time;
      typedef typename FEM1::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType1;
      typedef typename FEM2::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType2;
      typedef Dune::PDELab::LocalBasisCache<LocalBasisType1> Cache1;
      typedef Dune::PDELab::LocalBasisCache<LocalBasisType2> Cache2;
      std::vector<Cache1> cachepw;
      std::vector<Cache2> cachepc;
    };
  }
}

#endif
