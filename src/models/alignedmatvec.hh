#ifndef __ALIGNEDMATVEC__
#define __ALIGNEDMATVEC__

#include <iostream>
#include <iomanip>

/****************************************************************************/
/****************************************************************************/
// dynamic allocation of aligned memory blocks
/****************************************************************************/
/****************************************************************************/

/** get a block of aligned memory

    This function is based on malloc and allocates sufficient
    extra memory to provide an aligned pointer;
    use the correspondig free function to release the memory.
*/
void* aligned_malloc (size_t size, size_t alignment=16)
{
  // alignment must be reasonable
  assert(alignment>=sizeof(void*));
  assert(alignment%sizeof(void*)==0);

  // get a block of memory the pointer type is pointer to pointer to void to be able to do arithmetic
  void **p = (void**) malloc(size+alignment+sizeof(void*)); // get block of sufficient size
  if (p==0) return p; // not enough memory

  // align the pointer
  void **aligned_p = p;
  while ( ((size_t)aligned_p)%alignment!=0)
    {
      aligned_p++;
    }

  // now put the original pointer just before that address
  if ( ((size_t)aligned_p)-((size_t)p) >= sizeof(void*) )
    {
      void **pp = aligned_p;
      pp--;
      *pp = p;
      return (void *) aligned_p;
    }
  else
    {
      aligned_p = aligned_p + (alignment/sizeof(void*));
      void **pp = aligned_p;
      pp--;
      *pp = p;
      return (void *) aligned_p;
    }
}

/** free a block of memory allocated through aligned_malloc
 */
void aligned_free (void* p)
{
  void **pp = (void**) p;
  pp--;
  free(*pp);
}

/****************************************************************************/
/****************************************************************************/
/* matrix class with
   - aligned memory allocation
   - possible padding of rows and columns
   - row and column-wise access
   - export of underlying pointer for BLAS-like operations
*/
/****************************************************************************/
/****************************************************************************/

/** Provide a memory aligned matrix with padded rows and colums allowing row-wise and column-wise layout

    - alignment is in bytes
    - padding means allocated rows and columns is a multiple of padding
    - padding=1 means that exactly m rows and m columns are allocated
    - matrix allows row and columnwise access
*/
template<typename T=double, size_t alignment=16, size_t padding=1>
class AlignedMatrix
{
  typedef T member_type;
  typedef size_t size_type;

public:
  //! make a new matrix allocated on the heap
  AlignedMatrix (size_t _m, size_t _n, bool verbose=false) : m(_m), n(_n)
  {
    // make padded_m, padded_n multiples of padding
    if (alignment%sizeof(T)!=0) throw std::bad_alloc();
    padded_m = m;
    if (m%padding!=0) padded_m = (m/padding+1)*padding;
    padded_n = n;
    if (n%padding!=0) padded_n = (n/padding+1)*padding;

    // allocate aligned array
    p = (T*) aligned_malloc(padded_m*padded_n*sizeof(T),alignment);
    if (p==0) throw std::bad_alloc();
    if (verbose) {
      std::cout << "allocating aligned matrix" << std::endl;
      std::cout << "m=" << std::dec << std::uppercase << m << " padded to " << padded_m << std::endl;
      std::cout << "n=" << std::dec << std::uppercase << n << " padded to " << padded_n << std::endl;
      std::cout << "p=" << std::hex << std::uppercase << p << std::endl;
    }
  }

  //! copy constructor
  AlignedMatrix (const AlignedMatrix& mat)
  {
    m = mat.m;
    n = mat.n;
    padded_m = m;
    if (m%padding!=0) padded_m = (m/padding+1)*padding;
    padded_n = n;
    if (n%padding!=0) padded_n = (n/padding+1)*padding;

    // allocate aligned array
    p = (T*) aligned_malloc(padded_m*padded_n*sizeof(T),alignment);
    if (p==0) throw std::bad_alloc();
  }

  //! destroy the matrix
  ~AlignedMatrix ()
  {
    aligned_free(p);
  }

  //! taking address returns the internal pointer
  T* data () { return p; }

  //! taking address returns the internal pointer
  const T* data () const { return p; }

  //! set new size
  void resize (size_t newm, size_t newn)
  {
    if (newm*newn>padded_m*padded_n)
      {
        m = newm;
        n = newn;
      }
    else
      {
        // throw away old matrix
        aligned_free(p);

        // set new size and reallocate
        m = newm;
        n = newn;

        // make padded_m, padded_n multiples of padding
        if (alignment%sizeof(T)!=0) throw std::bad_alloc();
        padded_m = m;
        if (m%padding!=0) padded_m = (m/padding+1)*padding;
        padded_n = n;
        if (n%padding!=0) padded_n = (n/padding+1)*padding;

        // allocate aligned array
        p = (T*) aligned_malloc(padded_m*padded_n*sizeof(T),alignment);
        if (p==0) throw std::bad_alloc();
      }
  }

  //! get number of rows in use
  size_t rows () const { return m;}

  //! get number of columns in use
  size_t cols () const { return n;}

  //! get number of rows available
  size_t padded_rows () const { return padded_m;}

  //! get number of columns available
  size_t padded_cols () const { return padded_n;}

  //! access matrix elements as one big vector
  const T& operator[] (size_t i) const {return p[i];}

  //! access matrix elements as one big vector
  T& operator[] (size_t i) {return p[i];}

  //! access with j fastest; uses padding
  const T& rowmajoraccess (size_t i, size_t j) const
  {
    return p[i*padded_n+j];
  }

  //! access with j fastest; uses padding
  T& rowmajoraccess (size_t i, size_t j)
  {
    return p[i*padded_n+j];
  }

  //! access with i fastest; uses padding
  const T& colmajoraccess (size_t i, size_t j) const
  {
    return p[j*padded_m+i];
  }

  //! access with i fastest; uses padding
  T& colmajoraccess (size_t i, size_t j)
  {
    return p[j*padded_m+i];
  }

private:
  size_t m,n;
  size_t padded_m,padded_n;
  T* p;
};

/****************************************************************************/
/****************************************************************************/
/* vector class with
   - aligned memory allocation
   - export of underlying pointer for BLAS-like operations
*/
/****************************************************************************/
/****************************************************************************/

/** Provide a memory aligned vector

    - alignment is in bytes
*/
template<typename T=double, size_t alignment=16>
class AlignedVector
{
  typedef T member_type;
  typedef size_t size_type;

public:
  //! make a new vector
  AlignedVector (size_t _n, bool verbose=false) : n(_n)
  {
    // allocate aligned array
    p = (T*) aligned_malloc(n*sizeof(T),alignment);
    if (p==0) throw std::bad_alloc();
    if (verbose) {
      std::cout << "allocating aligned vector" << std::endl;
      std::cout << "p=" << std::hex << std::uppercase << p << std::endl;
    }
  }

  AlignedVector()
    : n(0)
    , p(nullptr)
  {}

  //! copy constructor
  AlignedVector (const AlignedVector& vec)
  {
    n = vec.n;
    p = (T*) aligned_malloc(n*sizeof(T),alignment);
    if (p==0) throw std::bad_alloc();
  }

  //! assignment operator
  AlignedVector& operator=(const AlignedVector& vec)
  {
    if (p)
      aligned_free(p);
    n = vec.n;
    p = (T*) aligned_malloc(n*sizeof(T),alignment);
    if (p==0) throw std::bad_alloc();
    std::copy(vec.p,vec.p+n,p);
    return *this;
  }

  //! free vector
  ~AlignedVector ()
  {
    if (p)
      aligned_free(p);
  }

  //! resize vector
  void resize (int _n)
  {
    if (_n<=n)
      {
        n = _n;
        return;
      }
    if (p)
      aligned_free(p);
    n = _n;
    p = (T*) aligned_malloc(n*sizeof(T),alignment);
    if (p==0) throw std::bad_alloc();
  }

  //! taking address returns internal pointer to data
  T* data () { return p; }

  //! taking address returns internal pointer to data
  const T* data () const { return p; }

  //! get number of elements in vector
  size_t size () const { return n;}

  //! element access
  const T& operator[] (size_t i) const {return p[i];}

  //! element access
  T& operator[] (size_t i) {return p[i];}

private:
  size_t n;
  T* p;
};

#endif
