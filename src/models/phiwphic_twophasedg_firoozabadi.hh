// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_PM_PHIWPHIC_TWOPHASEDG_FIROOZABADI_HH
#define DUNE_PM_PHIWPHIC_TWOPHASEDG_FIROOZABADI_HH

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>

#include<dune/geometry/referenceelements.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>

#include<dune/pdelab/finiteelement/localbasiscache.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>

#include"pwpc_twophasedgparam.hh"

namespace Dune {
  namespace PM {

    // enums for method parameters
    struct PhiwPhic_TwoPhaseDG_Firoozabadi_Method
    {
      enum Type { NIPG, SIPG };
    };

    struct PhiwPhic_TwoPhaseDG_Firoozabadi_Weights
    {
      enum Type { weightsOn, weightsOff };
    };

    /** \brief Weighted interior penalty discontinous Galerkin scheme for two phase flow in porous media

        This is the version based on the formulation described in

        Hussein Hoteit and Abbas Firoozabadi. Numerical modeling of two-phase flow in heterogeneous
        permeable media with different capillarity pressures. Advances in Water Resources, 31(1):56 – 73, 2008.

        This formulation is based on taking the potentials

        phi_\alpha = p_\alpha - \rho_\alpha depth(x),   phi_c = p_c - (\rho_n-\rho_w) depth(x)

        as primary variables. Note that the porosity is also called phi in the parameter class!
        Within this implementation here we use the names pw, pc for the potentials. When pressures
        are needed they are explicitly denoted by pressure_w, pressure_c.

        Template parameters are:
        Param: the parameter class
        FEM1:  finite element map for pw (needed for the cache)
        FEM2:  finite element map for pc (needed for the cache)
    */
    template<typename Param, typename FEM1, typename FEM2>
    class PhiwPhic_TwoPhaseDG_Firoozabadi
      : public Dune::PDELab::NumericalJacobianVolume<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianApplyVolume<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianSkeleton<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianApplySkeleton<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianBoundary<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianApplyBoundary<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >,
        public Dune::PDELab::FullSkeletonPattern,
        public Dune::PDELab::FullVolumePattern,
        public Dune::PDELab::LocalOperatorDefaultFlags,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename Param::Traits::RangeFieldType>
    {
      enum { dim = Param::Traits::GridViewType::dimension };

      typedef typename Param::Traits::RangeFieldType Real;
      typedef typename PwPc_TwoPhaseDGBoundaryConditions::Type BCType;

    public:
      // pattern assembly flags
      enum { doPatternVolume = true };
      enum { doPatternSkeleton = true };

      // residual assembly flags
      enum { doAlphaVolume  = true };
      enum { doAlphaSkeleton  = true };
      enum { doAlphaBoundary  = true };
      enum { doLambdaVolume  = true };

      //! constructor: pass parameter object
      PhiwPhic_TwoPhaseDG_Firoozabadi (Param& param_,
                                       PhiwPhic_TwoPhaseDG_Firoozabadi_Method::Type method_=PhiwPhic_TwoPhaseDG_Firoozabadi_Method::SIPG,
                                       PhiwPhic_TwoPhaseDG_Firoozabadi_Weights::Type weights_=PhiwPhic_TwoPhaseDG_Firoozabadi_Weights::weightsOn,
                                       Real alpha_=5.0,
                                       int quadrature_factor_=3)
        : Dune::PDELab::NumericalJacobianVolume<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianApplyVolume<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianSkeleton<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianApplySkeleton<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianBoundary<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianApplyBoundary<PhiwPhic_TwoPhaseDG_Firoozabadi<Param,FEM1,FEM2> >(1e-7),
        param(param_), method(method_), weights(weights_),
        alpha(alpha_), quadrature_factor(quadrature_factor_),
        cachepw(10), cachepc(10)
      {
        theta = -1.0;
        if (method==PhiwPhic_TwoPhaseDG_Firoozabadi_Method::SIPG) theta = 1.0;
      }

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PhiwPhic_TwoPhaseDG_Firoozabadi");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw = lfsv.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc = lfsv.template getChild<1>();

        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = EG::Geometry::dimension;
        const int dimw = EG::Geometry::dimensionworld;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        int orderpw = lfspw.finiteElement().localBasis().order();
        int orderpc = lfspc.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(orderpw,orderpc);
        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

        // evaluate permeability tensor at cell center, assume it is constant over elements
        typename Param::Traits::PermTensorType K;
        Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(gt).position(0,0);
        K = param.K(eg.entity(),localcenter);

        // jacobian of transformation (assume that transformation is affine)
        Dune::FieldMatrix<DF,dimw,dim> jact(eg.geometry().jacobianInverseTransposed(localcenter));
        RF integrationElement = eg.geometry().integrationElement(localcenter);

        // loop over quadrature points
        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // global coordinate of integration point
            Dune::FieldVector<DF,dim> ipglobal = eg.geometry().global(it->position());

            //=================================
            // evaluate pw
            //=================================

            // evaluate basis functions for potential (we assume Galerkin method lfsu=lfsv)
            const std::vector<RangeType>& basispw = cachepw[orderpw].evaluateFunction(it->position(),lfspw.finiteElement().localBasis());
            const std::vector<JacobianType>& jacpw = cachepw[orderpw].evaluateJacobian(it->position(),lfspw.finiteElement().localBasis());

            // evaluate pw
            RF pw=0.0;
            for (size_type i=0; i<lfspw.size(); i++) pw += x(lfspw,i)*basispw[i];

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > gradbasispw(lfspw.size());
            for (size_type i=0; i<lfspw.size(); i++)
              jact.mv(jacpw[i][0],gradbasispw[i]);

            // compute gradient of pw
            Dune::FieldVector<RF,dim> gradpw(0.0);
            for (size_type i=0; i<lfspw.size(); i++)
              gradpw.axpy(x(lfspw,i),gradbasispw[i]);

            //=================================
            // evaluate pc
            //=================================

            // evaluate basis functions for saturation (we assume Galerkin method lfsu=lfsv)
            const std::vector<RangeType>& basispc = cachepc[orderpc].evaluateFunction(it->position(),lfspc.finiteElement().localBasis());
            const std::vector<JacobianType>& jacpc = cachepc[orderpc].evaluateJacobian(it->position(),lfspc.finiteElement().localBasis());

            // evaluate pc
            RF pc=0.0;
            for (size_type i=0; i<lfspc.size(); i++) pc += x(lfspc,i)*basispc[i];

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > gradbasispc(lfspc.size());
            for (size_type i=0; i<lfspc.size(); i++)
              jact.mv(jacpc[i][0],gradbasispc[i]);

            // compute gradient of pc
            Dune::FieldVector<RF,dim> gradpc(0.0);
            for (size_type i=0; i<lfspc.size(); i++)
              gradpc.axpy(x(lfspc,i),gradbasispc[i]);

            //=================================
            // evaluate parameters
            //=================================

            RF rhow = param.rho1();
            RF rhon = param.rho2();
            RF pressure_c = pc + (rhon-rhow)*param.depth(ipglobal);
            RF sw = param.psi(eg.entity(),localcenter,pressure_c);
            RF sn = 1.0-sw;
            RF krw = param.kr1(eg.entity(),localcenter,sw);
            RF muw = param.mu1();
            RF krn = param.kr2(eg.entity(),localcenter,sn);
            RF mun = param.mu2();

            // compute phase mobilities
            RF lambdaw = krw/muw;
            RF lambdan = krn/mun;
            RF lambdat = lambdaw+lambdan;

            // compute total velocity components va = -lambdat*K*\nabla\phi_w, vc = -\lambda_n*K*\nabla\phi_c
            Dune::FieldVector<RF,dim> v,va,vc;
            v = gradpw;  K.mv(v,va); va *= -(lambdaw+lambdan);
            v = gradpc;  K.mv(v,vc); vc *= -lambdan;

            RF factor = it->weight() * integrationElement;

            //=================================
            // assemble potential equation
            //=================================

            // integrate -vt * grad basis fct = -(va+vc) * grad basis fct
            Dune::FieldVector<RF,dim> vt(va); vt += vc;
            for (size_type i=0; i<lfspw.size(); i++)
              r.accumulate(lfspw,i,-(vt*gradbasispw[i])*factor);

            //=================================
            // assemble saturation equation
            //=================================

            // integrate -vn * grad basis fct = -(f_n va + vc) * grad basis fct
            Dune::FieldVector<RF,dim> vn(va); vn *= lambdan/lambdat; vn += vc;
            for (size_type i=0; i<lfspc.size(); i++)
              r.accumulate(lfspc,i,-(vn*gradbasispc[i])*factor);
          }
      }

      // skeleton integral depending on test and ansatz functions
      // each face is only visited ONCE!
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                           R& r_s, R& r_n) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PhiwPhic_TwoPhaseDG_Firoozabadi");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw_s = lfsv_s.template getChild<0>();
        const PwSpace& lfspw_n = lfsv_n.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc_s = lfsv_s.template getChild<1>();
        const PcSpace& lfspc_n = lfsv_n.template getChild<1>();

        // domain and range field type
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = IG::dimension;
        int orderpw_s = lfspw_s.finiteElement().localBasis().order();
        int orderpw_n = lfspw_n.finiteElement().localBasis().order();
        int orderpc_s = lfspc_s.finiteElement().localBasis().order();
        int orderpc_n = lfspc_n.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(std::max(orderpw_s,orderpw_n),std::max(orderpc_s,orderpc_n))+1;

        // evaluate permeability tensors and normal
        const Dune::FieldVector<DF,dim>&
          inside_local = Dune::ReferenceElements<DF,dim>::general(ig.inside()->type()).position(0,0);
        const Dune::FieldVector<DF,dim>&
          outside_local = Dune::ReferenceElements<DF,dim>::general(ig.outside()->type()).position(0,0);
        typename Param::Traits::PermTensorType K_s, K_n;
        K_s = param.K(*(ig.inside()),inside_local);
        K_n = param.K(*(ig.outside()),outside_local);
        Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
        Dune::FieldVector<RF,dim> Kn_F_s; K_s.mv(n_F,Kn_F_s);
        Dune::FieldVector<RF,dim> Kn_F_n; K_n.mv(n_F,Kn_F_n);

        // face diameter
        RF h_F;
        if (dim>1)
          h_F = std::min(ig.inside()->geometry().volume(),ig.outside()->geometry().volume())/ig.geometry().volume(); // Houston!
        else
          h_F = std::min(ig.inside()->geometry().volume(),ig.outside()->geometry().volume());

        // get entry pressure on both sides
        RF pentry_s = param.pe(*(ig.inside()),inside_local);
        RF pentry_n = param.pe(*(ig.outside()),outside_local);

        // pre-compute permeability dependent part of weights
        RF delta_s = (Kn_F_s*n_F);
        RF delta_n = (Kn_F_n*n_F);
        RF delta_havg = 2.0*delta_s*delta_n/(delta_s+delta_n);
        RF omega_s,omega_n;
        if ( weights==PhiwPhic_TwoPhaseDG_Firoozabadi_Weights::weightsOn )
          {
            omega_s = delta_n/(delta_s+delta_n);
            omega_n = delta_s/(delta_s+delta_n);
          }
        else omega_s = omega_n = 0.5;

        // precompute part of penalty factor
        RF penaltyw =  (alpha/h_F)*std::max(orderpw_s,orderpw_n)*(std::max(orderpw_s,orderpw_n)+dim-1);
        RF penaltyc =  (alpha/h_F)*std::max(orderpc_s,orderpc_n)*(std::max(orderpc_s,orderpc_n)+dim-1);

        // select quadrature rule
        Dune::GeometryType gtface = ig.geometryInInside().type();
        const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

        // transformation; ASSUME it is affine
        Dune::FieldVector<DF,dim> iplocal_s(0.0);
        Dune::FieldVector<DF,dim> iplocal_n(0.0);
        Dune::FieldVector<DF,dim> ipglobal(0.0);
        Dune::FieldMatrix<DF,dim,dim> jact_s(ig.inside()->geometry().jacobianInverseTransposed(iplocal_s));
        Dune::FieldMatrix<DF,dim,dim> jact_n(ig.outside()->geometry().jacobianInverseTransposed(iplocal_n));

        // loop over quadrature points and integrate normal flux
        for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // position of quadrature point in local coordinates of elements
            iplocal_s = ig.geometryInInside().global(it->position());
            iplocal_n = ig.geometryInOutside().global(it->position());

            // global coordinate of quadrature point
            ipglobal = ig.geometry().global(it->position());
            RF depth = param.depth(ipglobal);

            //=================================
            // evaluate pw
            //=================================

            // evaluate basis functions
            const std::vector<RangeType>& basispw_s = cachepw[orderpw_s].evaluateFunction(iplocal_s,lfspw_s.finiteElement().localBasis());
            const std::vector<RangeType>& basispw_n = cachepw[orderpw_n].evaluateFunction(iplocal_n,lfspw_n.finiteElement().localBasis());

            // evaluate potential
            RF pw_s=0.0; for (size_type i=0; i<lfspw_s.size(); i++) pw_s += x_s(lfspw_s,i)*basispw_s[i];
            RF pw_n=0.0; for (size_type i=0; i<lfspw_n.size(); i++) pw_n += x_n(lfspw_n,i)*basispw_n[i];

            // evaluate gradient of basis functions (we assume Galerkin method lfsu=lfsv)
            const std::vector<JacobianType>& gradbasispw_s = cachepw[orderpw_s].evaluateJacobian(iplocal_s,lfspw_s.finiteElement().localBasis());
            const std::vector<JacobianType>& gradbasispw_n = cachepw[orderpw_n].evaluateJacobian(iplocal_n,lfspw_n.finiteElement().localBasis());

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > tgradbasispw_s(lfspw_s.size());
            for (size_type i=0; i<lfspw_s.size(); i++) jact_s.mv(gradbasispw_s[i][0],tgradbasispw_s[i]);
            std::vector<Dune::FieldVector<RF,dim> > tgradbasispw_n(lfspw_n.size());
            for (size_type i=0; i<lfspw_n.size(); i++) jact_n.mv(gradbasispw_n[i][0],tgradbasispw_n[i]);

            // compute gradient of pw
            Dune::FieldVector<RF,dim> gradpw_s(0.0);
            for (size_type i=0; i<lfspw_s.size(); i++) gradpw_s.axpy(x_s(lfspw_s,i),tgradbasispw_s[i]);
            Dune::FieldVector<RF,dim> gradpw_n(0.0);
            for (size_type i=0; i<lfspw_n.size(); i++) gradpw_n.axpy(x_n(lfspw_n,i),tgradbasispw_n[i]);

            //=================================
            // evaluate pc
            //=================================

            // evaluate basis functions
            const std::vector<RangeType>& basispc_s = cachepc[orderpc_s].evaluateFunction(iplocal_s,lfspc_s.finiteElement().localBasis());
            const std::vector<RangeType>& basispc_n = cachepc[orderpc_n].evaluateFunction(iplocal_n,lfspc_n.finiteElement().localBasis());

            // evaluate potential
            RF pc_s=0.0; for (size_type i=0; i<lfspc_s.size(); i++) pc_s += x_s(lfspc_s,i)*basispc_s[i];
            RF pc_n=0.0; for (size_type i=0; i<lfspc_n.size(); i++) pc_n += x_n(lfspc_n,i)*basispc_n[i];

            // evaluate gradient of basis functions (we assume Galerkin method lfsu=lfsv)
            const std::vector<JacobianType>& gradbasispc_s = cachepc[orderpc_s].evaluateJacobian(iplocal_s,lfspc_s.finiteElement().localBasis());
            const std::vector<JacobianType>& gradbasispc_n = cachepc[orderpc_n].evaluateJacobian(iplocal_n,lfspc_n.finiteElement().localBasis());

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > tgradbasispc_s(lfspc_s.size());
            for (size_type i=0; i<lfspc_s.size(); i++) jact_s.mv(gradbasispc_s[i][0],tgradbasispc_s[i]);
            std::vector<Dune::FieldVector<RF,dim> > tgradbasispc_n(lfspc_n.size());
            for (size_type i=0; i<lfspc_n.size(); i++) jact_n.mv(gradbasispc_n[i][0],tgradbasispc_n[i]);

            // compute gradient of potential
            Dune::FieldVector<RF,dim> gradpc_s(0.0);
            for (size_type i=0; i<lfspc_s.size(); i++) gradpc_s.axpy(x_s(lfspc_s,i),tgradbasispc_s[i]);
            Dune::FieldVector<RF,dim> gradpc_n(0.0);
            for (size_type i=0; i<lfspc_n.size(); i++) gradpc_n.axpy(x_n(lfspc_n,i),tgradbasispc_n[i]);

            //=================================
            //  evaluate parameters
            //=================================

            RF rhow = param.rho1();
            RF rhon = param.rho2();
            RF pressurec_s = pc_s + (rhon-rhow)*depth;
            RF pressurec_n = pc_n + (rhon-rhow)*depth;
            RF sw_s = param.psi(*(ig.inside()),inside_local,pressurec_s);
            RF sw_n = param.psi(*(ig.outside()),outside_local,pressurec_n);
            RF sn_s = 1.0-sw_s;
            RF sn_n = 1.0-sw_n;
            RF krw_s = param.kr1(*(ig.inside()),inside_local,sw_s);
            RF krw_n = param.kr1(*(ig.outside()),outside_local,sw_n);
            RF muw = param.mu1();
            RF krn_s = param.kr2(*(ig.inside()),inside_local,sn_s);
            RF krn_n = param.kr2(*(ig.outside()),outside_local,sn_n);
            RF mun = param.mu2();

            // compute phase mobilities
            RF lambdaw_s = krw_s/muw;
            RF lambdaw_n = krw_n/muw;
            RF lambdan_s = krn_s/mun;
            RF lambdan_n = krn_n/mun;
            RF lambdat_s = lambdaw_s+lambdan_s;
            RF lambdat_n = lambdaw_n+lambdan_n;


            // compute velocity directional parts (excluding the mobilities)
            Dune::FieldVector<RF,dim> v,qa_s,qa_n,qc_s,qc_n;
            v = gradpw_s; K_s.mv(v,qa_s); qa_s *= -1.0;
            v = gradpw_n; K_n.mv(v,qa_n); qa_n *= -1.0;
            v = gradpc_s; K_s.mv(v,qc_s); qc_s *= -1.0;
            v = gradpc_n; K_n.mv(v,qc_n); qc_n *= -1.0;

            // integration factor
            RF factor = it->weight() * ig.geometry().integrationElement(it->position());

            //=================================
            // assemble potential equation
            //=================================

            RF havgw;
            {
              RF lambdatdelta_s = lambdat_s*delta_s;
              RF lambdatdelta_n = lambdat_n*delta_n;
              havgw = 2.0*lambdatdelta_s*lambdatdelta_n/(lambdatdelta_s+lambdatdelta_n);
            }

            // standard flux term
            RF vtavg = (((qa_s*n_F)*lambdat_s + (qc_s*n_F)*lambdan_s)*omega_s + ((qa_n*n_F)*lambdat_n + (qc_n*n_F)*lambdan_n)*omega_n)*factor;
            for (size_type i=0; i<lfspw_s.size(); i++) r_s.accumulate(lfspw_s,i,vtavg*basispw_s[i]);
            for (size_type i=0; i<lfspw_n.size(); i++) r_n.accumulate(lfspw_n,i,vtavg*(-1.0*basispw_n[i]));

            // consistency term
            RF cfactorw = -theta * (pw_s-pw_n) * factor;
            for (size_type i=0; i<lfspw_s.size(); i++)
              r_s.accumulate(lfspw_s,i,cfactorw * omega_s * lambdat_s * (Kn_F_s*tgradbasispw_s[i]));
            for (size_type i=0; i<lfspw_n.size(); i++)
              r_n.accumulate(lfspw_n,i,cfactorw * omega_n * lambdat_n * (Kn_F_n*tgradbasispw_n[i]));

            // IP term
            RF iptermw = penaltyw * havgw * (pw_s-pw_n) * factor;
            for (size_type i=0; i<lfspw_s.size(); i++)
              r_s.accumulate(lfspw_s,i,iptermw * basispw_s[i]);
            for (size_type i=0; i<lfspw_n.size(); i++)
              r_n.accumulate(lfspw_n,i,iptermw * (-1.0*basispw_n[i]));

            //=================================
            // assemble saturation equation
            //=================================

            // velocity reconstruction including penalty term
            RF va_hat_normal = (qa_s*n_F)*lambdat_s*omega_s + (qa_n*n_F)*lambdat_n*omega_n + penaltyw*havgw*(pw_s-pw_n);

            // upwinding of capillary pressure
            RF pressurec_upwind;
            if (va_hat_normal>=0)
              pressurec_upwind = pc_s + (rhon-rhow)*depth;
            else
              pressurec_upwind = pc_n + (rhon-rhow)*depth;

            // evaluate saturation on either side with upwinded capi pressure
            RF sw_upwind_s = param.psi(*(ig.inside()),inside_local,pressurec_upwind);
            RF sw_upwind_n = param.psi(*(ig.outside()),outside_local,pressurec_upwind);
            RF sn_upwind_s = 1.0-sw_upwind_s;
            RF sn_upwind_n = 1.0-sw_upwind_n;

            // evaluate relative permeabilities
            RF krw_upwind_s = param.kr1(*(ig.inside()), inside_local, sw_upwind_s);
            RF krw_upwind_n = param.kr1(*(ig.outside()),outside_local,sw_upwind_n);
            RF krn_upwind_s = param.kr2(*(ig.inside()), inside_local, sn_upwind_s);
            RF krn_upwind_n = param.kr2(*(ig.outside()),outside_local,sn_upwind_n);

            // harmonic average of n-mobility
            RF lambdan_upwind = 2.0*krn_upwind_s*krn_upwind_n/(krn_upwind_s+krn_upwind_n+1E-16)/mun; // Assume that krn>=0

            // harmonic average of fractional flow function
            RF fn_s = (krn_upwind_s/mun)/( (krw_upwind_s/muw)+(krn_upwind_s/mun) );
            RF fn_n = (krn_upwind_n/mun)/( (krw_upwind_n/muw)+(krn_upwind_n/mun) );
            RF fn_upwind = 2.0*fn_s*fn_n/(fn_s+fn_n+1E-16); // Assume that krn>=0

            // convective flux term
            RF fluxn = fn_upwind*va_hat_normal*factor;
            for (size_type i=0; i<lfspc_s.size(); i++) r_s.accumulate(lfspc_s,i,fluxn * basispc_s[i]);
            for (size_type i=0; i<lfspc_n.size(); i++) r_n.accumulate(lfspc_n,i,fluxn * (-1.0*basispc_n[i]));

            // compute jump and penalty term
            RF Jump = pc_s-pc_n;
            RF Penalty =  penaltyc * 0.5 *(lambdan_s+lambdan_n) * delta_havg; // this is needed at least to get things going
            RF lambdan_star_s = lambdan_s;
            RF lambdan_star_n = lambdan_n;
            if (pentry_s!=pentry_n)
              {
                // we are at a media discontinuity
                // modify jump when entry pressure is not yet reached
                if (pentry_s<pentry_n)
                  {
                    // self has the lower entry pressure
                    if (pc_s<pentry_n-(rhon-rhow)*depth)
                      {
                        // no continuity, pc_n should be entry pressure
                        Jump = (pentry_n-(rhon-rhow)*depth) - pc_n;
                      }
                  }
                else
                  {
                    // neighbor has the lower entry pressure
                    if (pc_n<pentry_s-(rhon-rhow)*depth)
                      {
                        // no continuity, pc_s should be entry pressure
                        Jump = pc_s - (pentry_s-(rhon-rhow)*depth);
                      }
                  }
              }

            // diffusive flux term
            fluxn = ( (qc_s*n_F)*lambdan_star_s*omega_s+(qc_n*n_F)*lambdan_star_n*omega_n )*factor;
            for (size_type i=0; i<lfspc_s.size(); i++) r_s.accumulate(lfspc_s,i,fluxn * basispc_s[i]);
            for (size_type i=0; i<lfspc_n.size(); i++) r_n.accumulate(lfspc_n,i,fluxn * (-1.0*basispc_n[i]));

            // consistency term & IP term
            RF cfactorc = -theta * Jump * factor;
            RF iptermc = Penalty * Jump * factor;

            // accumulate
            for (size_type i=0; i<lfspc_s.size(); i++)
              r_s.accumulate(lfspc_s,i,cfactorc * lambdan_star_s * omega_s * (Kn_F_s*tgradbasispc_s[i]) + iptermc * basispc_s[i]);
            for (size_type i=0; i<lfspc_n.size(); i++)
              r_n.accumulate(lfspc_n,i,cfactorc * lambdan_star_n * omega_n * (Kn_F_n*tgradbasispc_n[i]) + iptermc * (-1.0*basispc_n[i]));
          }
      }

      // skeleton integral depending on test and ansatz functions
      // We put the Dirchlet evaluation also in the alpha term to save some geometry evaluations
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           R& r_s) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PhiwPhic_TwoPhaseDG_Firoozabadi");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw_s = lfsv_s.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc_s = lfsv_s.template getChild<1>();

        // domain and range field type
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = IG::dimension;
        int orderpw_s = lfspw_s.finiteElement().localBasis().order();
        int orderpc_s = lfspc_s.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(orderpw_s,orderpc_s)+1;

        // evaluate permeability tensor and normal
        const Dune::FieldVector<DF,dim>&
          inside_local = Dune::ReferenceElements<DF,dim>::general(ig.inside()->type()).position(0,0);
        typename Param::Traits::PermTensorType K_s;
        K_s = param.K(*(ig.inside()),inside_local);
        Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
        Dune::FieldVector<RF,dim> Kn_F_s; K_s.mv(n_F,Kn_F_s);
        RF delta_s = (Kn_F_s*n_F);

        // face diameter
        RF h_F;
        if (dim>1)
          h_F = ig.inside()->geometry().volume()/ig.geometry().volume(); // Houston!
        else
          h_F = ig.inside()->geometry().volume();

        // precompute part of penalty factor
        RF penaltyw =  (alpha/h_F)*orderpw_s*(orderpw_s+dim-1);
        RF penaltyc =  (alpha/h_F)*orderpc_s*(orderpc_s+dim-1);

        // select quadrature rule
        Dune::GeometryType gtface = ig.geometryInInside().type();
        const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);

        // transformation; ASSUME it is affine
        Dune::FieldVector<DF,dim> iplocal_s(0.0);
        Dune::FieldVector<DF,dim> ipglobal(0.0);
        Dune::FieldMatrix<DF,dim,dim> jact_s(ig.inside()->geometry().jacobianInverseTransposed(iplocal_s));

        // evaluate boundary condition
        const Dune::FieldVector<DF,dim-1>
          face_local = Dune::ReferenceElements<DF,dim-1>::general(gtface).position(0,0);
        BCType bctype1 = param.bc1(ig.intersection(),face_local,time);
        BCType bctype2 = param.bc2(ig.intersection(),face_local,time);

        // loop over quadrature points and integrate normal flux
        for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // position of quadrature point in local coordinates of elements
            Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().global(it->position());

            // global coordinate of quadrature point
            ipglobal = ig.geometry().global(it->position());
            RF depth = param.depth(ipglobal);

            //=================================
            // evaluate pw
            //=================================

            // evaluate basis functions
            const std::vector<RangeType>& basispw_s = cachepw[orderpw_s].evaluateFunction(iplocal_s,lfspw_s.finiteElement().localBasis());

            // evaluate potential w
            RF pw_s=0.0;
            for (size_type i=0; i<lfspw_s.size(); i++) pw_s += x_s(lfspw_s,i)*basispw_s[i];

            // evaluate gradient of basis functions (we assume Galerkin method lfsu=lfsv)
            const std::vector<JacobianType>& gradbasispw_s = cachepw[orderpw_s].evaluateJacobian(iplocal_s,lfspw_s.finiteElement().localBasis());

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > tgradbasispw_s(lfspw_s.size());
            for (size_type i=0; i<lfspw_s.size(); i++) jact_s.mv(gradbasispw_s[i][0],tgradbasispw_s[i]);

            // compute gradient of pw
            Dune::FieldVector<RF,dim> gradpw_s(0.0);
            for (size_type i=0; i<lfspw_s.size(); i++) gradpw_s.axpy(x_s(lfspw_s,i),tgradbasispw_s[i]);

            //=================================
            // evaluate pc
            //=================================

            // evaluate basis functions
            const std::vector<RangeType>& basispc_s = cachepc[orderpc_s].evaluateFunction(iplocal_s,lfspc_s.finiteElement().localBasis());

            // evaluate potential c
            RF pc_s=0.0; for (size_type i=0; i<lfspc_s.size(); i++) pc_s += x_s(lfspc_s,i)*basispc_s[i];

            // evaluate gradient of basis functions (we assume Galerkin method lfsu=lfsv)
            const std::vector<JacobianType>& gradbasispc_s = cachepc[orderpc_s].evaluateJacobian(iplocal_s,lfspc_s.finiteElement().localBasis());

            // transform gradients of shape functions to real element
            std::vector<Dune::FieldVector<RF,dim> > tgradbasispc_s(lfspc_s.size());
            for (size_type i=0; i<lfspc_s.size(); i++) jact_s.mv(gradbasispc_s[i][0],tgradbasispc_s[i]);

            // compute gradient of pressure
            Dune::FieldVector<RF,dim> gradpc_s(0.0);
            for (size_type i=0; i<lfspc_s.size(); i++) gradpc_s.axpy(x_s(lfspc_s,i),tgradbasispc_s[i]);

            //=================================
            //  evaluate parameters
            //=================================

            RF rhow = param.rho1();
            RF rhon = param.rho2();
            RF pressurec_s = pc_s + (rhon-rhow)*depth;
            RF sw_s = param.psi(*(ig.inside()),inside_local,pressurec_s);
            RF sn_s = 1.0-sw_s;
            RF krw_s = param.kr1(*(ig.inside()),inside_local,sw_s);
            RF muw = param.mu1();
            RF krn_s = param.kr2(*(ig.inside()),inside_local,sn_s);
            RF mun = param.mu2();

            // compute phase mobilities
            RF lambdaw_s = krw_s/muw;
            RF lambdan_s = krn_s/mun;
            RF lambdat_s = lambdaw_s+lambdan_s;

            // compute velocity directional parts (excluding the mobilities)
            Dune::FieldVector<RF,dim> v,qa_s,qc_s;
            v = gradpw_s; K_s.mv(v,qa_s); qa_s *= -1.0;
            v = gradpc_s; K_s.mv(v,qc_s); qc_s *= -1.0;

            // integration factor
            RF factor = it->weight() * ig.geometry().integrationElement(it->position());

            //=================================
            // assemble pressure equation
            //=================================

            // reconstruct velocity for saturation equation
            RF va_hat_normal = (qa_s*n_F)*lambdat_s;

            // flux boundary condition
            if (bctype1 == PwPc_TwoPhaseDGBoundaryConditions::Flux)
              {
                // evaluate flux boundary condition
                RF j1 = param.j1(ig.intersection(),it->position(),time);

                // integrate
                for (size_type i=0; i<lfspw_s.size(); i++)
                  r_s.accumulate(lfspw_s,i,j1 * basispw_s[i] * factor);
              }

            // Dirichlet boundary condition
            if (bctype1 == PwPc_TwoPhaseDGBoundaryConditions::Dirichlet)
              {
                // standard flux term
                RF vtavg = ((qa_s*n_F)*lambdat_s + (qc_s*n_F)*lambdan_s) * factor;
                for (size_type i=0; i<lfspw_s.size(); i++) r_s.accumulate(lfspw_s,i,vtavg*basispw_s[i]);

                // evaluate Dirichlet boundary condition (b.c. are always given as pressures)
                RF pw_n = param.g1(ig.intersection(),it->position(),time)-rhow*depth;

                // consistency term
                RF cfactorw = -theta * lambdat_s * (pw_s-pw_n) * factor;
                for (size_type i=0; i<lfspw_s.size(); i++)
                  r_s.accumulate(lfspw_s,i,cfactorw * (Kn_F_s*tgradbasispw_s[i]));

                // IP term
                RF iptermw = penaltyw * lambdat_s * delta_s * (pw_s-pw_n) * factor;
                for (size_type i=0; i<lfspw_s.size(); i++)
                  r_s.accumulate(lfspw_s,i,iptermw * basispw_s[i]);

                // reconstruct velocity
                va_hat_normal += penaltyw * lambdat_s * delta_s * (pw_s-pw_n);
              }

            //=================================
            // assemble saturation equation
            //=================================

            // flux boundary condition
            if (bctype2 == PwPc_TwoPhaseDGBoundaryConditions::Flux)
              {
                // evaluate flux boundary condition
                RF j2 = param.j2(ig.intersection(),it->position(),time);

                // integrate
                for (size_type i=0; i<lfspc_s.size(); i++)
                  r_s.accumulate(lfspc_s,i,j2 * basispc_s[i] * factor);
              }

            // Dirichlet boundary condition
            if (bctype2 == PwPc_TwoPhaseDGBoundaryConditions::Dirichlet)
              {
                // standard flux term
                RF fluxn = ((lambdan_s/lambdat_s)*va_hat_normal + lambdan_s*(qc_s*n_F)) * factor;
                for (size_type i=0; i<lfspc_s.size(); i++) r_s.accumulate(lfspc_s,i,fluxn * basispc_s[i]);

                // evaluate Dirichlet boundary condition (b.c. are always given as pressures)
                RF pc_n = param.g2(ig.intersection(),it->position(),time)-(rhon-rhow)*depth;

                // consistency term
                RF cfactorc = -theta * lambdan_s * (pc_s-pc_n) * factor;
                for (size_type i=0; i<lfspc_s.size(); i++)
                  r_s.accumulate(lfspc_s,i,cfactorc * (Kn_F_s*tgradbasispc_s[i]));

                // IP term
                RF iptermc = penaltyc * lambdan_s * delta_s * (pc_s-pc_n) * factor;
                for (size_type i=0; i<lfspc_s.size(); i++)
                  r_s.accumulate(lfspc_s,i,iptermc * basispc_s[i]);
              }
          }
      }

      // volume integral depending only on test functions
      template<typename EG, typename LFSV, typename R>
      void lambda_volume (const EG& eg, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PhiwPhic_TwoPhaseDG_Firoozabadi");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw = lfsv.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc = lfsv.template getChild<1>();

        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = EG::Geometry::dimension;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        int orderpw = lfspw.finiteElement().localBasis().order();
        int orderpc = lfspc.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(orderpw,orderpc);
        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

        // jacobian of transformation (assume that transformation is affine)
        Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(gt).position(0,0);
        RF integrationElement = eg.geometry().integrationElement(localcenter);

        // loop over quadrature points
        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // evaluate shape functions
            const std::vector<RangeType>& basispw = cachepw[orderpw].evaluateFunction(it->position(),lfspw.finiteElement().localBasis());

            // evaluate right hand side parameter function
            Real f1 = param.f1(eg.entity(),it->position(),time);

            // integrate f
            RF factor = it->weight() * integrationElement;
            for (size_type i=0; i<lfspw.size(); i++)
              r.accumulate(lfspw,i,-f1 * basispw[i] * factor);

            // evaluate shape functions
            const std::vector<RangeType>& basispc = cachepc[orderpc].evaluateFunction(it->position(),lfspc.finiteElement().localBasis());

            // evaluate right hand side parameter function
            Real f2 = param.f2(eg.entity(),it->position(),time);

            // integrate f
            for (size_type i=0; i<lfspc.size(); i++)
              r.accumulate(lfspc,i,-f2 * basispc[i] * factor);
          }
      }

      //! set time for subsequent evaluation
      void setTime (typename Param::Traits::RangeFieldType t)
      {
        time = t;
        param.setTime(t);
      }

    private:
      Param& param;  // two phase parameter class
      PhiwPhic_TwoPhaseDG_Firoozabadi_Method::Type method;
      PhiwPhic_TwoPhaseDG_Firoozabadi_Weights::Type weights;
      Real alpha;
      int quadrature_factor;
      Real theta;
      typedef typename FEM1::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType1;
      typedef typename FEM2::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType2;
      typedef Dune::PDELab::LocalBasisCache<LocalBasisType1> Cache1;
      typedef Dune::PDELab::LocalBasisCache<LocalBasisType2> Cache2;
      std::vector<Cache1> cachepw;
      std::vector<Cache2> cachepc;
      mutable Real time;
    };


    /** a local operator for the mass operator (L_2 integral)
     *
     * \f{align*}{
     \int_\Omega uv dx
     * \f}
     */
    template<class Param, typename FEM1, typename FEM2>
    class PhiwPhic_TwoPhaseDG_Firoozabadi_Temporal
      : public Dune::PDELab::NumericalJacobianApplyVolume<PhiwPhic_TwoPhaseDG_Firoozabadi_Temporal<Param,FEM1,FEM2> >,
        public Dune::PDELab::NumericalJacobianVolume<PhiwPhic_TwoPhaseDG_Firoozabadi_Temporal<Param,FEM1,FEM2> >,
        public Dune::PDELab::FullVolumePattern,
        public Dune::PDELab::LocalOperatorDefaultFlags,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };

      PhiwPhic_TwoPhaseDG_Firoozabadi_Temporal (Param& param_, int quadrature_factor_=3)
        : Dune::PDELab::NumericalJacobianApplyVolume<PhiwPhic_TwoPhaseDG_Firoozabadi_Temporal<Param,FEM1,FEM2> >(1e-7),
        Dune::PDELab::NumericalJacobianVolume<PhiwPhic_TwoPhaseDG_Firoozabadi_Temporal<Param,FEM1,FEM2> >(1e-7),
        param(param_), quadrature_factor(quadrature_factor_),
        cachepw(10), cachepc(10)
      {}

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong function space for PhiwPhic_TwoPhaseDG_Firoozabadi");
        typedef typename LFSV::template Child<0>::Type PwSpace;
        const PwSpace& lfspw = lfsv.template getChild<0>();
        typedef typename LFSV::template Child<1>::Type PcSpace;
        const PcSpace& lfspc = lfsv.template getChild<1>();

        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::JacobianType JacobianType;
        typedef typename PwSpace::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename PwSpace::Traits::SizeType size_type;

        // dimensions
        const int dim = EG::Geometry::dimension;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        int orderpw = lfspw.finiteElement().localBasis().order();
        int orderpc = lfspc.finiteElement().localBasis().order();
        const int intorder = quadrature_factor*std::max(orderpw,orderpc);
        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

        // jacobian of transformation (assume that transformation is affine)
        Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(gt).position(0,0);
        RF integrationElement = eg.geometry().integrationElement(localcenter);

        // loop over quadrature points
        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // global coordinate of integration point
            Dune::FieldVector<DF,dim> ipglobal = eg.geometry().global(it->position());

            //=================================
            // evaluate pc
            //=================================

            // evaluate basis functions for saturation (we assume Galerkin method lfsu=lfsv)
            const std::vector<RangeType>& basispc = cachepc[orderpc].evaluateFunction(it->position(),lfspc.finiteElement().localBasis());

            // evaluate pc
            RF pc=0.0;
            for (size_type i=0; i<lfspc.size(); i++) pc += x(lfspc,i)*basispc[i];

            //=================================
            // assemble saturation equation
            //=================================

            // evaluate parameters phase 2
            RF rhow = param.rho1();
            RF rhon = param.rho2();
            RF pressure_c = pc + (rhon-rhow)*param.depth(ipglobal);
            RF sw = param.psi(eg.entity(),it->position(),pressure_c);
            RF sn = 1.0-sw;
            RF porosity = param.phi(eg.entity(),it->position());

            // porosity*sn*phi_i
            RF storage2 = porosity * sn * it->weight() * integrationElement;
            for (size_type i=0; i<lfspc.size(); i++)
              r.accumulate(lfspc,i,storage2*basispc[i]);
          }
      }

      //! set time for subsequent evaluation
      void setTime (typename Param::Traits::RangeFieldType t)
      {
        time = t;
      }

    private:
      Param& param;
      int quadrature_factor;
      typename Param::Traits::RangeFieldType time;
      typedef typename FEM1::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType1;
      typedef typename FEM2::Traits::FiniteElementType::Traits::LocalBasisType LocalBasisType2;
      typedef Dune::PDELab::LocalBasisCache<LocalBasisType1> Cache1;
      typedef Dune::PDELab::LocalBasisCache<LocalBasisType2> Cache2;
      std::vector<Cache1> cachepw;
      std::vector<Cache2> cachepc;
    };
  } // Finished version 0 on March, 5, 2013 in train from Oslo to Katrineholm (Stockholm)
}

#endif
