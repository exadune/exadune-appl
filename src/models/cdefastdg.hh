// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_CONVECTIONDIFFUSIONDGFAST_HH
#define DUNE_PDELAB_CONVECTIONDIFFUSIONDGFAST_HH

#include <cstddef>

#include<dune/common/power.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include<dune/pdelab/finiteelement/localbasiscache.hh>
#include<dune/pdelab/finiteelementmap/qkdg.hh>
#include<dune/pdelab/finiteelementmap/qkdggl.hh>

#include "alignedmatvec.hh"
#include "alignedmatvecops.hh"
#include "sumfactorization.hh"

namespace Dune {
  namespace PDELab {

    struct ConvectionDiffusionDGFastMethod
    {
      enum Type { NIPG, SIPG, IPG };
    };

    struct ConvectionDiffusionDGFastWeights
    {
      enum Type { weightsOn, weightsOff };
    };

    /** a local operator for solving the convection-diffusion equation with discontinuous Galerkin
     *
     * \f{align*}{
     *   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \\
     *                                              u &=& g \mbox{ on } \partial\Omega_D \\
     *                (b(x,u) - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N \\
     *                        -(A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_O
     * \f}
     * Note:
     *  - This formulation is valid for velocity fields which are non-divergence free.
     *  - Outflow boundary conditions should only be set on the outflow boundary
     *
     * \tparam T model of ConvectionDiffusionParameterInterface
     * \tparam d dimension
     * \tparam p polynomial degree
     * \tparam q quadrature order for Gauss-Legendre -> number of points is q/2+1
     * \tparam affine_transformation set to true if transformation from reference element is affine
     * \tparam constant_A indicates that diffusion coefficient A is constant PER ELEMENT
     * \tparam zero_A set to true if A is zero (first order equation)
     * \tparam zero_b set to true if b is zero (no convective term)
     * \tparam zero_c set to true if c is zero (no sink term)
     * \tparam zero_f set to true if f is zero (no sink term)
     */
    template<typename T, int p, int q,
             bool affine_transformation=false, bool constant_A=false,
             bool zero_A=false, bool zero_b=false, bool zero_c=false, bool zero_f=false>
    class ConvectionDiffusionDGFast
      : public Dune::PDELab::NumericalJacobianApplyVolume<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >,
        public Dune::PDELab::NumericalJacobianApplySkeleton<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >,
        public Dune::PDELab::NumericalJacobianApplyBoundary<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >,
    //public Dune::PDELab::NumericalJacobianVolume<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >,
    //public Dune::PDELab::NumericalJacobianSkeleton<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >,
    //public Dune::PDELab::NumericalJacobianBoundary<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >,
        public Dune::PDELab::FullSkeletonPattern,
        public Dune::PDELab::FullVolumePattern,
        public Dune::PDELab::LocalOperatorDefaultFlags,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<typename T::Traits::RangeFieldType>
    {
      enum { dim = T::Traits::GridViewType::dimension };
      enum { n = p+1 }; // number of basis functions per direction
      enum { N = Dune::StaticPower<n,dim>::power };
      enum { Nface = Dune::StaticPower<n,dim-1>::power };
      enum { m = q/2+1 }; // number of quadrature points per direction
      enum { M = Dune::StaticPower<m,dim>::power };
      enum { Mface = Dune::StaticPower<m,dim-1>::power };
      enum { alignment = 64 };

      typedef typename T::Traits::RangeFieldType Real;
      typedef typename T::Traits::DomainFieldType DF;
      typedef typename T::Traits::RangeFieldType RF;
      typedef Dune::FieldMatrix<DF,dim,dim> JacobianType;
      typedef typename ConvectionDiffusionBoundaryConditions::Type BCType;

      typedef Dune::FieldVector<size_t,dim> MultiIndex;
      typedef Dune::FieldVector<size_t,dim-1> FaceMultiIndex;
      typedef AlignedMatrix<RF,alignment> AMatrix;
      typedef AlignedVector<RF,alignment> AVector;

      // choose the polynomials to be used
      //typedef Dune::QkStuff::EquidistantLagrangePolynomials<DF,RF,p> Polynomials1D;
      typedef Dune::QkStuff::GaussLobattoLagrangePolynomials<DF,RF,p> Polynomials1D;

      // compute multiindex from flat index
      template<size_t k, size_t dimension=dim> // k is the number of elements per direction
      Dune::FieldVector<size_t,dimension> mi (size_t i) const
      {
        Dune::FieldVector<size_t,dimension> alpha;
        for (std::size_t j=0; j<dimension; j++)
          {
            alpha[j] = i % k;
            i = i/k;
          }
        return alpha;
      }

      // compute flat index from multiindex
      template<size_t k, size_t dimension=dim> // k is the number of elements per direction
      size_t index (Dune::FieldVector<size_t,dimension> alpha) const
      {
        size_t i=alpha[dim-1];
        for (int r=dim-2; r>=0; r--) i = k*i+alpha[r];
        return i;
      }

      // swap components x[r] and x[0] in a vector
      template<typename X>
      void permute (X& x, size_t r) const
      {
        std::swap(x[r],x[0]);
      }

    public:
      // pattern assembly flags
      enum { doPatternVolume = true };
      enum { doPatternSkeleton = true };

      // residual assembly flags
      enum { doAlphaVolume  = true };
      enum { doAlphaSkeleton  = true };
      enum { doAlphaBoundary  = true };
      enum { doLambdaVolume  = true };



      struct Cache
      {

        // shared
        Polynomials1D poly;
        RF xi[m];                             // 1D quadrature points
        RF w[m];                              // 1D quadrature weights

        size_t npower[dim];                   // n^k
        size_t mpower[dim];                   // m^k

        AMatrix Theta;                        // 1-D basis functions at quadrature points
        std::vector<AMatrix> Theta_s; // 1-D basis functions at quadrature points
        std::vector<AMatrix> Theta_n; // 1-D basis functions at quadrature points
        AMatrix ThetaT;                       // 1-D basis functions at quadrature points transposed
        std::vector<AMatrix> ThetaT_s;// 1-D basis functions at quadrature points transposed
        std::vector<AMatrix> ThetaT_n;// 1-D basis functions at quadrature points transposed
        AMatrix dTheta;                       // derivative of 1-D basis functions at quatrature points
        std::vector<AMatrix> dTheta_s;// derivatives of 1-D basis functions at quadrature points
        std::vector<AMatrix> dTheta_n;// derivatives of 1-D basis functions at quadrature points
        AMatrix dThetaT;                      // derivative of 1-D basis functions at quatrature points transposed
        std::vector<AMatrix> dThetaT_s;// derivatives of 1-D basis functions at quadrature points transposed
        std::vector<AMatrix> dThetaT_n;// derivatives of 1-D basis functions at quadrature points transposed

        // precomputed special face theta matrices
        AMatrix faceTheta0;                   // 1xn matrix, basis functions at 0
        AMatrix faceTheta1;                   // 1xn matrix, basis functions at 1
        AMatrix facedTheta0;                  // 1xn matrix, basis functions at 0
        AMatrix facedTheta1;                  // 1xn matrix, basis functions at 1
        AMatrix faceThetaT0;                  // 1xn matrix, basis functions at 0, transposed
        AMatrix faceThetaT1;                  // 1xn matrix, basis functions at 1, transposed
        AMatrix facedThetaT0;                 // 1xn matrix, basis functions at 0, transposed
        AMatrix facedThetaT1;                 // 1xn matrix, basis functions at 1, transposed

        Cache()
          : Theta(m,n), Theta_s(dim,Theta), Theta_n(dim,Theta),
            ThetaT(n,m), ThetaT_s(dim,ThetaT), ThetaT_n(dim,ThetaT),
            dTheta(m,n), dTheta_s(dim,dTheta), dTheta_n(dim,dTheta),
            dThetaT(n,m), dThetaT_s(dim,dThetaT), dThetaT_n(dim,dThetaT),
            faceTheta0(1,n), faceTheta1(1,n),
            facedTheta0(1,n), facedTheta1(1,n),
            faceThetaT0(n,1), faceThetaT1(n,1),
            facedThetaT0(n,1), facedThetaT1(n,1)
        {
          const Dune::QuadratureRule<DF,1>& rule = Dune::QuadratureRules<DF,1>::rule(Dune::GeometryType::cube,q);
          size_t count=0;
          for (typename Dune::QuadratureRule<DF,1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
            {
              size_t group=count/2;
              size_t member=count%2;
              size_t newj;
              if (member==1) newj=group; else newj=m-1-group;
              xi[newj] = it->position()[0];
              w[newj] = it->weight();
              // std::cout << "j=" << count << " newj=" << newj
              //           << " xi=" << it->position()[0]
              //           << " w=" << it->weight()
              //           << std::endl;
              count++;
            }
          for (size_t j=0; j<m/2; j++)
            if (xi[j]>0.5)
              {
                RF temp=xi[j];
                xi[j] = xi[m-1-j];
                xi[m-1-j] = temp;
                temp=w[j];
                w[j] = w[m-1-j];
                w[m-1-j] = temp;
              }

          for (size_t i=0; i<m; i++)
            for (size_t j=0; j<n; j++)
              {
                Theta.colmajoraccess(i,j) = poly.p(j,xi[i]);
                ThetaT.colmajoraccess(j,i) = poly.p(j,xi[i]);
                dTheta.colmajoraccess(i,j) = poly.dp(j,xi[i]);
                dThetaT.colmajoraccess(j,i) = poly.dp(j,xi[i]);
              }
          Theta_s[0].resize(1,n);
          Theta_n[0].resize(1,n);
          ThetaT_s[0].resize(n,1);
          ThetaT_n[0].resize(n,1);
          dTheta_s[0].resize(1,n);
          dTheta_n[0].resize(1,n);
          dThetaT_s[0].resize(n,1);
          dThetaT_n[0].resize(n,1);

          // compute powers of n,m
          npower[0] = 1;
          mpower[0] = 1;
          for (size_t k=1; k<dim; k++)
            {
              npower[k] = n*npower[k-1];
              mpower[k] = m*mpower[k-1];
            }

          // special Theta matrices at boundary of faces
          for (size_t j=0; j<n; j++) // basis functions
            {
              faceTheta0.colmajoraccess(0,j) = poly.p(j,0.0);
              faceTheta1.colmajoraccess(0,j) = poly.p(j,1.0);
              faceThetaT0.colmajoraccess(j,0) = poly.p(j,0.0);
              faceThetaT1.colmajoraccess(j,0) = poly.p(j,1.0);
              facedTheta0.colmajoraccess(0,j) = poly.dp(j,0.0);
              facedTheta1.colmajoraccess(0,j) = poly.dp(j,1.0);
              facedThetaT0.colmajoraccess(j,0) = poly.dp(j,0.0);
              facedThetaT1.colmajoraccess(j,0) = poly.dp(j,1.0);
            }
        }


      };



      //! constructor: pass parameter object
      ConvectionDiffusionDGFast (T& param_,
                                 ConvectionDiffusionDGFastMethod::Type method_=ConvectionDiffusionDGFastMethod::NIPG,
                                 ConvectionDiffusionDGFastWeights::Type weights_=ConvectionDiffusionDGFastWeights::weightsOff,
                                 Real alpha_=0.0,
                                 bool block_pattern_optimization = false
                                 )
        : Dune::PDELab::NumericalJacobianApplyVolume<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >(1.0e-7),
        Dune::PDELab::NumericalJacobianApplySkeleton<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >(1.0e-7),
        Dune::PDELab::NumericalJacobianApplyBoundary<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >(1.0e-7),
      //Dune::PDELab::NumericalJacobianVolume<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >(1.0e-7),
      //Dune::PDELab::NumericalJacobianSkeleton<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >(1.0e-7),
      //Dune::PDELab::NumericalJacobianBoundary<ConvectionDiffusionDGFast<T,p,q,affine_transformation,constant_A,zero_A,zero_b,zero_c> >(1.0e-7),
        Dune::PDELab::FullSkeletonPattern(block_pattern_optimization),
        Dune::PDELab::FullVolumePattern(block_pattern_optimization),
        param(param_), method(method_), weights(weights_),
        alpha(alpha_),
        _cache(std::make_shared<Cache>()),
        C(N), C_s(N), C_n(N),
        U(M), U_s(Mface), U_n(Mface),
        gradU(dim,U), gradU_s(dim,U_s), gradU_n(dim,U_n),
        X(dim,U), Xface_s(Mface), Xface_n(Mface), X_s(dim,Xface_s), X_n(dim,Xface_n), Xc(M),
        R(N), R_s(N), R_n(N),
        matrices(dim)
      {
        // assertions
        assert(p>=1);
        assert(q>=1);
        assert(dim>=1 && dim<=3);

        theta = 1.0;
        if (method==ConvectionDiffusionDGFastMethod::NIPG) theta = -1.0;
        if (method==ConvectionDiffusionDGFastMethod::IPG) theta = 0.0;

        // std::cout << "CDEFastDG: dim=" << dim
        //           << " degree=" << p
        //           << " size=" << Dune::QkStuff::QkSize<p,dim>::value
        //           << std::endl;

        // std::cout << std::endl << "enumeration test (direction 0 fastest):" << std::endl;
        // for (size_t i=0; i<Dune::QkStuff::QkSize<p,d>::value ; i++)
        //   {
        //     Dune::FieldVector<int,d> alpha(Dune::QkStuff::multiindex<p,d>(i));
        //     Dune::FieldVector<double,d> x(0.0);
        //     for (size_t j=0; j<d; j++) x[j] = (1.0*alpha[j])/p;

        //     std::cout << "i=" << i << " alpha=" << alpha << " x=" << x << std::endl;
        //   }

        // std::cout << std::endl << "quadrature rule of order " << q << "=> m=" << q/2+1 << std::endl;

        // std::cout << std::endl;
        // for (size_t j=0; j<m; j++)
        //     std::cout << "j=" << j
        //               << " xi=" << xi[j]
        //               << " w=" << w[j]
        //               << std::endl;

        // std::cout << std::endl << "enumeration test of quadrature points (direction 0 fastest):" << std::endl;
        // for (size_t i=0; i<Dune::QkStuff::QkSize<m-1,dim>::value ; i++)
        //   {
        //     MultiIndex alpha(mi<m>(i));
        //     Dune::FieldVector<double,dim> x(0.0);
        //     for (size_t j=0; j<dim; j++) x[j] = xi[alpha[j]];

        //     std::cout << "i=" << i << " alpha=" << alpha << " x=" << x << std::endl;
        //   }

        // evaluate the n 1D basis functions at m quadrature points

        // std::cout << std::setw(3) << "Th" ;
        // for (size_t j=0; j<n; j++)
        //   std::cout << std::setw(10) << j;
        // std::cout << std::endl;
        // for (size_t i=0; i<m; i++)
        //   {
        //     std::cout << std::setw(3) << i ;
        //     for (size_t j=0; j<n; j++)
        //       std::cout << std::setw(10) << std::scientific << std::showpoint << std::setprecision(2) << Theta.colmajoraccess(i,j);
        //     std::cout << std::endl;
        //   }
        // std::cout << std::endl;

        // std::cout << std::setw(3) << "Th'" ;
        // for (size_t j=0; j<n; j++)
        //   std::cout << std::setw(10) << j;
        // std::cout << std::endl;
        // for (size_t i=0; i<m; i++)
        //   {
        //     std::cout << std::setw(3) << i ;
        //     for (size_t j=0; j<n; j++)
        //       std::cout << std::setw(10) << std::scientific << std::showpoint << std::setprecision(2) << dTheta.colmajoraccess(i,j);
        //     std::cout << std::endl;
        //   }
        // std::cout << std::endl;

      }

#if HAVE_TBB

      // splitting
      ConvectionDiffusionDGFast(const ConvectionDiffusionDGFast& other, tbb::split)
        : Dune::PDELab::FullSkeletonPattern(other)
        , Dune::PDELab::FullVolumePattern(other)
        , param(other.param)
        , method(other.method)
        , weights(other.weights)
        , alpha(other.alpha)
        , theta(other.theta)
        , time(other.time)
        , _cache(other._cache)
        , C(other.C)
        , C_s(other.C_s)
        , C_n(other.C_n)
        , U(other.U)
        , U_s(other.U_s)
        , U_n(other.U_n)
        , gradU(other.gradU)
        , gradU_s(other.gradU_s)
        , gradU_n(other.gradU_n)
        , X(other.X)
        , Xface_s(other.Xface_s)
        , Xface_n(other.Xface_n)
        , X_s(other.X_s)
        , X_n(other.X_n)
        , Xc(other.Xc)
        , R(other.R)
        , R_s(other.R_s)
        , R_n(other.R_n)
        , matrices(other.matrices)
      {}

      void join(ConvectionDiffusionDGFast&)
      {}

#endif // HAVE_TBB

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename XType, typename LFSV, typename RType>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const XType& x, const LFSV& lfsv, RType& r) const
      {
        // std::cout << "alpha_volume: center=" << eg.geometry().center() << " time=" << time << std::endl;
        // assertions
        assert(lfsu.size()==N);
        assert(lfsv.size()==N);

        // extract degrees of freedom => order is direction 0 fastest
        for (size_t i=0; i<N; i++) C[i] = x(lfsu,i);

        // compute values of u at quadrature points
        if (!zero_c || !zero_b) sumfact.multiply(cache().Theta,C,U);

        // compute values of gradient of u at quadrature points
        if (!zero_A)
          for (size_t k=0; k<dim; k++)
            sumfact.multiply(cache().Theta,cache().dTheta,k,C,gradU[k]);

        // transformation
        //typename EG::Geometry::JacobianInverseTransposed jac;
        JacobianType jac;
        RF gamma;
        if (affine_transformation)
          {
            Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
            jac = eg.geometry().jacobianInverseTransposed(localcenter);
            gamma = eg.geometry().integrationElement(localcenter);
          }

        // parameters
        typename T::Traits::PermTensorType A;
        typename T::Traits::PermTensorType Ahat;
        typename T::Traits::RangeType b;
        typename T::Traits::RangeType bhat;
        typename T::Traits::RangeFieldType c;
        if (!zero_A && constant_A)
          {
            Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
            A = param.A(eg.entity(),localcenter);
          }

        // precompute transformed coefficients if possible
        if (affine_transformation && constant_A && !zero_A)
          {
            for (int i=0; i<dim; i++)
              for (int j=0; j<dim; j++)
                {
                  Ahat[i][j] = 0.0;
                  for (int r=0; r<dim; r++)
                    for (int s=0; s<dim; s++)
                      Ahat[i][j] += jac[r][i]*A[r][s]*jac[s][j];
                }
          }

        // prepare input values
        for (size_t i=0; i<M; i++) // loop over all quadrature points
          {
            // position and weight
            MultiIndex alpha(mi<m>(i)); // multiindex of quadrature point
            Dune::FieldVector<DF,dim> x_alpha(0.0);                                // position of quadrature point
            for (size_t j=0; j<dim; j++) x_alpha[j] = cache().xi[alpha[j]];                // note: the xi are not consecutive
            RF weight(1.0);
            for (size_t j=0; j<dim; j++) weight *= cache().w[alpha[j]];                    // weight of quadrature point

            // evaluate transformation if necessary
            if (!affine_transformation)
              {
                jac = eg.geometry().jacobianInverseTransposed(x_alpha);
                gamma = eg.geometry().integrationElement(x_alpha);
              }

            // evaluate parameters if necessary
            if (!zero_A && !constant_A) A = param.A(eg.entity(),x_alpha);
            if (!zero_b) { b = param.b(eg.entity(),x_alpha); jac.mtv(b,bhat);}
            if (!zero_c) c = param.c(eg.entity(),x_alpha);

            // recompute transformed coefficients if necessary
            if (!zero_A && (!affine_transformation || !constant_A) )
              {
                for (int i=0; i<dim; i++)
                  for (int j=0; j<dim; j++)
                    {
                      Ahat[i][j] = 0.0;
                      for (int r=0; r<dim; r++)
                        for (int s=0; s<dim; s++)
                          Ahat[i][j] += jac[r][i]*A[r][s]*jac[s][j];
                    }
              }

            // now assemble coefficients
            for (size_t k=0; k<dim; k++) X[k][i] = 0.0;
            if (!zero_A)
              for (size_t k=0; k<dim; k++)
                for (size_t l=0; l<dim; l++) {
                  X[k][i] += Ahat[k][l]*gradU[l][i]*gamma*weight;
                }
            if (!zero_b)
              for (size_t k=0; k<dim; k++) X[k][i] -= U[i]*bhat[k]*gamma*weight; // Note the minus sign!
            if (!zero_c)
              Xc[i] = c*U[i]*gamma*weight;
          }

        // compute the residuals
        if (!zero_c) {
          sumfactT.multiply(cache().ThetaT,Xc,R);
          for (size_t i=0; i<N; i++) r.accumulate(lfsv,i,R[i]);
        }
        if (!zero_A || !zero_b)
          for (size_t k=0; k<dim; k++) {
            sumfactT.multiply(cache().ThetaT,cache().dThetaT,k,X[k],R);
            for (size_t i=0; i<N; i++) r.accumulate(lfsv,i,R[i]);
          }
      }

      // jacobian of volume term (added Sep 12, 2014)
      template<typename EG, typename LFSU, typename XX, typename LFSV, typename MAT>
      void jacobian_volume (const EG& eg, const LFSU& lfsu, const XX& xx, const LFSV& lfsv,
                            MAT& mat) const
      {
        // transformation
        JacobianType jac;
        RF gamma;
        if (affine_transformation) // then precompute
          {
            Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
            jac = eg.geometry().jacobianInverseTransposed(localcenter);
            gamma = eg.geometry().integrationElement(localcenter);
          }

        // parameters
        typename T::Traits::PermTensorType A;
        typename T::Traits::PermTensorType Ahat;
        typename T::Traits::RangeType b;
        typename T::Traits::RangeType bhat;
        typename T::Traits::RangeFieldType c;
        if (!zero_A && constant_A) // then preevaluate
          {
            Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
            A = param.A(eg.entity(),localcenter);
          }

        // precompute transformed coefficients if possible
        if (affine_transformation && constant_A && !zero_A)
          {
            for (int i=0; i<dim; i++)
              for (int j=0; j<dim; j++)
                {
                  Ahat[i][j] = 0.0;
                  for (int r=0; r<dim; r++)
                    for (int s=0; s<dim; s++)
                      Ahat[i][j] += jac[r][i]*A[r][s]*jac[s][j];
                }
          }

        // ============
        // We compute the Jacobian columnwise (i.e. ansatz functions) and use
        // sum factorization only over all rows (i.e. test functions)
        // ============

        // loop over all ansatz functions
        for (size_t j=0; j<N; j++)
          {
            MultiIndex multi_j(mi<n>(j)); // multiindex of ansatz function

            // prepare input values for all quadrature points
            for (size_t k=0; k<M; k++) // loop over all quadrature points
              {
                // position and weight of quadrature point
                MultiIndex multi_k(mi<m>(k)); // multiindex of quadrature point
                Dune::FieldVector<DF,dim> x_k(0.0);                                // position of quadrature point
                for (size_t j=0; j<dim; j++) x_k[j] = cache().xi[multi_k[j]];              // note: the xi are not consecutive (?)
                RF weight(1.0);
                for (size_t j=0; j<dim; j++) weight *= cache().w[multi_k[j]];              // weight of quadrature point

                // evaluate transformation if necessary
                if (!affine_transformation)
                  {
                    jac = eg.geometry().jacobianInverseTransposed(x_k);
                    gamma = eg.geometry().integrationElement(x_k);
                  }

                // evaluate parameters if necessary
                if (!zero_A && !constant_A) A = param.A(eg.entity(),x_k);
                if (!zero_b) { b = param.b(eg.entity(),x_k); jac.mtv(b,bhat);}
                if (!zero_c) c = param.c(eg.entity(),x_k);

                // recompute transformed coefficients if necessary
                if (!zero_A && (!affine_transformation || !constant_A) )
                  {
                    for (int i=0; i<dim; i++)
                      for (int j=0; j<dim; j++)
                        {
                          Ahat[i][j] = 0.0;
                          for (int r=0; r<dim; r++)
                            for (int s=0; s<dim; s++)
                              Ahat[i][j] += jac[r][i]*A[r][s]*jac[s][j];
                        }
                  }

                // now assemble coefficients

                // trial function and its gradient at quadrature point
                for (size_t alpha=0; alpha<dim; alpha++) X[alpha][k] = 0.0;
                RF trialfunction=1.0;
                for (size_t l=0; l<dim; l++) trialfunction *= cache().Theta.colmajoraccess(multi_k[l],multi_j[l]);
                RF gradient_trialfunction[dim];
                for (size_t beta=0; beta<dim; beta++) {
                  gradient_trialfunction[beta] = 1.0;
                  for (size_t l=0; l<dim; l++)
                    if (l==beta)
                      gradient_trialfunction[beta] *= cache().dTheta.colmajoraccess(multi_k[l],multi_j[l]);
                    else
                      gradient_trialfunction[beta] *= cache().Theta.colmajoraccess(multi_k[l],multi_j[l]);
                }

                // Diffusion term
                if (!zero_A)
                  for (size_t alpha=0; alpha<dim; alpha++)
                    for (size_t beta=0; beta<dim; beta++)
                      X[alpha][k] += gradient_trialfunction[beta]*Ahat[alpha][beta]*gamma*weight;
                if (!zero_b)
                  for (size_t alpha=0; alpha<dim; alpha++)
                    X[alpha][k] -= trialfunction*bhat[alpha]*gamma*weight; // Note the minus sign!
                if (!zero_c)
                  Xc[k] = c*trialfunction*gamma*weight;
              }

            // compute the column and accumulate to Jacobian
            if (!zero_c) {
              sumfactT.multiply(cache().ThetaT,Xc,R);
              for (size_t i=0; i<N; i++) mat.accumulate(lfsu,i,lfsv,j,R[i]);
            }
            if (!zero_A || !zero_b)
              for (size_t alpha=0; alpha<dim; alpha++) {
                sumfactT.multiply(cache().ThetaT,cache().dThetaT,alpha,X[alpha],R);
                for (size_t i=0; i<N; i++) mat.accumulate(lfsu,i,lfsv,j,R[i]);
              }
          } // end loop over all columns
      }

      // skeleton integral depending on test and ansatz functions
      // each face is only visited ONCE!
      template<typename IG, typename LFSU, typename X, typename LFSV, typename RType>
      void alpha_skeleton (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                           RType& r_s, RType& r_n) const
      {
        //std::cout << std::endl << "===> SKELETON" << std::endl;

        auto inside_cell = ig.inside();
        auto outside_cell = ig.outside();

        // permutation of directions in the volume refelem
        size_t facedir_s = ig.indexInInside()/2; // direction of the face = the dimension to be reduced
        size_t facedir_n = ig.indexInOutside()/2;
        size_t facemod_s = ig.indexInInside()%2; // 0: face at 0.0, 1: face at 1.0
        size_t facemod_n = ig.indexInOutside()%2;

        // Volume -> face : direction 0 should be face
        size_t permute_s[dim];  // permutation of directions on volume element
        size_t permute_n[dim];  // permutation of directions on volume element
        for (size_t i=0; i<dim; i++) permute_s[i] = permute_n[i] = i;
        std::swap(permute_s[0],permute_s[facedir_s]);
        std::swap(permute_n[0],permute_n[facedir_n]);

        // Face -> volume : direction dim-1 should be face
        size_t permuteT_s[dim];  // permutation of directions on volume element
        size_t permuteT_n[dim];  // permutation of directions on volume element
        for (size_t i=0; i<dim; i++) permuteT_s[i] = permuteT_n[i] = i;
        std::swap(permuteT_s[dim-1],permuteT_s[facedir_s]);
        std::swap(permuteT_n[dim-1],permuteT_n[facedir_n]);
        // std::cout << "PERMUTATION of directions in volume element" << std::endl;
        // for (size_t i=0; i<dim; i++)
        //   std::cout << " self " << i << " <-> " << permute_s[i]
        //             << " | neighbor " << i << " <-> " << permute_n[i]
        //             << std::endl;

        // extract degrees of freedom => order is direction 0 fastest
        for (size_t i=0; i<N; i++)
          {
            MultiIndex i_multi(mi<n>(i));
            size_t inew = i + (i_multi[0]-i_multi[facedir_s])*cache().npower[facedir_s] - i_multi[0] + i_multi[facedir_s];
            C_s[inew] = x_s(lfsu_s,i);
          }
        for (size_t i=0; i<N; i++)
          {
            MultiIndex i_multi(mi<n>(i));
            size_t inew = i + (i_multi[0]-i_multi[facedir_n])*cache().npower[facedir_n] - i_multi[0] + i_multi[facedir_n];
            C_n[inew] = x_n(lfsu_n,i);
          }

        // tensor product quadrature points on face (in coordinates of the volume refelem!)
        // RF xi_s[m][dim], xi_n[m][dim]; // [i][0] is the constant coordinate, [i][k] corresponds to dir k-1 on face refelem
        // for (size_t i=0; i<m; i++)
        //   {
        //     Dune::FieldVector<DF,dim-1> qp(xi[i]); // loops through diagonal on refelem
        //     Dune::FieldVector<DF,dim> qplocal_s = ig.geometryInInside().global(qp);
        //     Dune::FieldVector<DF,dim> qplocal_n = ig.geometryInOutside().global(qp);
        //     for (size_t k=0; k<dim; k++)
        //       {
        //         xi_s[i][k] = qplocal_s[permute_s[k]];
        //         xi_n[i][k] = qplocal_n[permute_n[k]];
        //       }
        //   }
        // now prepare the matrices to transform coefficients into values at Gauss points
        // Note that in general we have a different matrix in every direction since
        // the positions of the quadrature points might be different in case of nonconforming meshes.
        // direction 0 has one quadrature point
        // for (size_t j=0; j<n; j++) // basis functions
        //   {
        //     Theta_s[0].colmajoraccess(0,j) = poly.p(j,xi_s[0][0]);
        //     Theta_n[0].colmajoraccess(0,j) = poly.p(j,xi_n[0][0]);
        //     ThetaT_s[0].colmajoraccess(j,0) = Theta_s[0].colmajoraccess(0,j);
        //     ThetaT_n[0].colmajoraccess(j,0) = Theta_n[0].colmajoraccess(0,j);
        //   }
        // for (size_t k=1; k<dim; k++) // the directions with more than one quadrature point
        //   for (size_t j=0; j<n; j++) // basis functions
        //     for (size_t i=0; i<m; i++)  // quadrature points
        //       {
        //         Theta_s[k].colmajoraccess(i,j) = poly.p(j,xi_s[i][k]);
        //         Theta_n[k].colmajoraccess(i,j) = poly.p(j,xi_n[i][k]);
        //         ThetaT_s[k].colmajoraccess(j,i) = Theta_s[k].colmajoraccess(i,j);
        //         ThetaT_n[k].colmajoraccess(j,i) = Theta_n[k].colmajoraccess(i,j);
        //       }
        // if (!zero_A)
        //   {
        //     // compute gradients of basis fuctions
        //     for (size_t j=0; j<n; j++) // basis functions
        //       {
        //         dTheta_s[0].colmajoraccess(0,j) = poly.dp(j,xi_s[0][0]);
        //         dTheta_n[0].colmajoraccess(0,j) = poly.dp(j,xi_n[0][0]);
        //         dThetaT_s[0].colmajoraccess(j,0) = dTheta_s[0].colmajoraccess(0,j);
        //         dThetaT_n[0].colmajoraccess(j,0) = dTheta_n[0].colmajoraccess(0,j);
        //       }
        //     for (size_t k=1; k<dim; k++) // the directions with more than one quadrature point
        //       for (size_t j=0; j<n; j++) // basis functions
        //         for (size_t i=0; i<m; i++)  // quadrature points
        //           {
        //             dTheta_s[k].colmajoraccess(i,j) = poly.dp(j,xi_s[i][k]);
        //             dTheta_n[k].colmajoraccess(i,j) = poly.dp(j,xi_n[i][k]);
        //             dThetaT_s[k].colmajoraccess(j,i) = dTheta_s[k].colmajoraccess(i,j);
        //             dThetaT_n[k].colmajoraccess(j,i) = dTheta_n[k].colmajoraccess(i,j);
        //           }
        //   }

        // compute values of u at quadrature points of face
        // for (size_t l=0; l<dim; l++) matrices[l] = &(Theta_s[l]);
        // sumfact.multiply_face(matrices,C_s,U_s);
        // for (size_t l=0; l<dim; l++) matrices[l] = &(Theta_n[l]);
        // sumfact.multiply_face(matrices,C_n,U_n);
        // compute values of gradU on the volume refelem at quadrature points of face
        // if (!zero_A)
        //   for (size_t k=0; k<dim; k++) // for all directions of the volume reference element (not permuted)
        //     {
        //       for (size_t l=0; l<dim; l++)
        //         if (permute_s[l]==k)
        //           matrices[l] = &(dTheta_s[l]);
        //         else
        //           matrices[l] = &(Theta_s[l]);
        //       sumfact.multiply_face(matrices,C_s,gradU_s[k]);
        //       for (size_t l=0; l<dim; l++)
        //         if (permute_n[l]==k)
        //           matrices[l] = &(dTheta_n[l]);
        //         else
        //           matrices[l] = &(Theta_n[l]);
        //       sumfact.multiply_face(matrices,C_n,gradU_n[k]);
        //     }

        // new version using precomputed Theta matrices
        for (size_t l=1; l<dim; l++) matrices[l] = &cache().Theta;
        if (facemod_s==0) matrices[0]=&cache().faceTheta0; else matrices[0]=&cache().faceTheta1;
        sumfact.multiply_face(matrices,C_s,U_s);
        if (facemod_n==0) matrices[0]=&cache().faceTheta0; else matrices[0]=&cache().faceTheta1;
        sumfact.multiply_face(matrices,C_n,U_n);

        // compute values of gradU on the volume refelem at quadrature points of face
        if (!zero_A)
          for (size_t k=0; k<dim; k++) // for all directions of the volume reference element (not permuted)
            {
              if (facemod_s==0) {
                if (permute_s[0]==k) matrices[0] = &cache().facedTheta0; else matrices[0] = &cache().faceTheta0;
              } else {
                if (permute_s[0]==k) matrices[0] = &cache().facedTheta1; else matrices[0] = &cache().faceTheta1;
              }
              for (size_t l=1; l<dim; l++)
                if (permute_s[l]==k) matrices[l] = &cache().dTheta; else matrices[l] = &cache().Theta;
              sumfact.multiply_face(matrices,C_s,gradU_s[k]);

              if (facemod_n==0) {
                if (permute_n[0]==k) matrices[0] = &cache().facedTheta0; else matrices[0] = &cache().faceTheta0;
              } else {
                if (permute_n[0]==k) matrices[0] = &cache().facedTheta1; else matrices[0] = &cache().faceTheta1;
              }
              for (size_t l=1; l<dim; l++)
                if (permute_n[l]==k) matrices[l] = &cache().dTheta; else matrices[l] = &cache().Theta;
              sumfact.multiply_face(matrices,C_n,gradU_n[k]);
            }
        // ==== end version using precomputed Theta matrices


        // transformation
        JacobianType jac_s, jac_n;
        RF gamma;
        Dune::FieldVector<DF,dim> n_F;
        if (affine_transformation)
          {
            Dune::FieldVector<DF,dim>  inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
            Dune::FieldVector<DF,dim> outside_local = Dune::ReferenceElements<DF,dim>::general(outside_cell.type()).position(0,0);
            jac_s = inside_cell.geometry().jacobianInverseTransposed(inside_local);
            jac_n = outside_cell.geometry().jacobianInverseTransposed(outside_local);
            Dune::FieldVector<DF,dim-1> qp(0.0);
            gamma = ig.geometry().integrationElement(qp);
            n_F = ig.centerUnitOuterNormal();
          }

        // parameters
        typename T::Traits::PermTensorType A_s, A_n;
        typename T::Traits::RangeType b; // velocity * normal is continuous!
        if (!zero_A && constant_A)
          {
            Dune::FieldVector<DF,dim>  inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
            Dune::FieldVector<DF,dim> outside_local = Dune::ReferenceElements<DF,dim>::general(outside_cell.type()).position(0,0);
            A_s = param.A(inside_cell,inside_local);
            A_n = param.A(outside_cell,outside_local);
          }

        // compute transformed coefficients and weights
        Dune::FieldVector<RF,dim> An_F_s, An_F_n;
        Dune::FieldVector<RF,dim> ahat_s, ahat_n; // n_F*(A_x jac_x)
        RF omega_s(0.5), omega_n(0.5), harmonic_average(1.0);
        if (!zero_A && affine_transformation && constant_A)
          {
            A_s.mv(n_F,An_F_s);
            A_n.mv(n_F,An_F_n);
            jac_s.mtv(An_F_s,ahat_s);
            jac_n.mtv(An_F_n,ahat_n);
            if (weights==ConvectionDiffusionDGFastWeights::weightsOn)
              {
                RF delta_s = (An_F_s*n_F);
                RF delta_n = (An_F_n*n_F);
                omega_s = delta_n/(delta_s+delta_n+1e-20);
                omega_n = delta_s/(delta_s+delta_n+1e-20);
                harmonic_average = 2.0*delta_s*delta_n/(delta_s+delta_n+1e-20);
              }
          }

        // mesh parameter
        RF h_F = std::min(inside_cell.geometry().volume(),outside_cell.geometry().volume())/ig.geometry().volume(); // Houston!

        // prepare input values
        // std::cout << "PREPARE INPUT VALUES ..." << std::endl;
        for (size_t i=0; i<Mface; i++) // loop over all quadrature points
          {
            // position and weight
            FaceMultiIndex ialpha(mi<m,dim-1>(i));       // multiindex of quadrature point on face refelem
            Dune::FieldVector<DF,dim-1> qp;            // position of quadrature point in face refelem
            for (size_t k=0; k<dim-1; k++) qp[k] = cache().xi[ialpha[k]];
            Dune::FieldVector<DF,dim> qplocal_s(ig.geometryInInside().global(qp));// position of quadrature point in volume refelem
            Dune::FieldVector<DF,dim> qplocal_n(ig.geometryInOutside().global(qp));// position of quadrature point in volume refelem
            RF weight(1.0);
            for (size_t j=0; j<dim-1; j++) weight *= cache().w[ialpha[j]];  // weight of quadrature point in face refelem

            // permuted index on face -> works only up to 3D!
            size_t i_s=i;
            if (facedir_s==2) i_s=ialpha[0]*m+ialpha[1];
            size_t i_n=i;
            if (facedir_n==2) i_n=ialpha[0]*m+ialpha[1];

            // std::cout << "multiindex: " << ialpha
            //           << " qp: " << qp
            //           << " qplocal_s " << qplocal_s
            //           << " qplocal_n " << qplocal_n
            //           << " weight: " << weight
            //           << std::endl;

            // evaluate transformation if necessary
            if (!affine_transformation) {
              jac_s = inside_cell.geometry().jacobianInverseTransposed(qplocal_s);
              jac_n = outside_cell.geometry().jacobianInverseTransposed(qplocal_n);
              gamma = ig.geometry().integrationElement(qp);
              n_F = ig.unitOuterNormal(qp);
            }

            // evaluate parameters if necessary
            if (!zero_A && !constant_A) {
              A_s = param.A(inside_cell,qplocal_s);
              A_n = param.A(outside_cell,qplocal_n);
            }

            // recompute derived parameters
            if (!zero_A && (!affine_transformation || !constant_A) )
              {
                A_s.mv(n_F,An_F_s);
                A_n.mv(n_F,An_F_n);
                jac_s.mtv(An_F_s,ahat_s);
                jac_n.mtv(An_F_n,ahat_n);
                if (weights==ConvectionDiffusionDGFastWeights::weightsOn)
                  {
                    RF delta_s = (An_F_s*n_F);
                    RF delta_n = (An_F_n*n_F);
                    omega_s = delta_n/(delta_s+delta_n+1e-20);
                    omega_n = delta_s/(delta_s+delta_n+1e-20);
                    harmonic_average = 2.0*delta_s*delta_n/(delta_s+delta_n+1e-20);
                  }
              }

            // convection, consistency and interior penalty term
            Xface_s[i_s] = Xface_n[i_n] = 0.0; // we need two vectors because they might be numbered differently
                                               // but up to the numbering they have the same values!
            if (!zero_b)
              {
                b = param.b(inside_cell,qplocal_s);
                RF flux = b*n_F;
                RF absflux = std::abs(flux);
                RF value = flux*0.5*(U_s[i_s]+U_n[i_n]) + absflux*0.5*(U_s[i_s]-U_n[i_n]);
                Xface_s[i_s] += value; // convective term
                Xface_n[i_n] += value; // convective term
              }
            if (!zero_A)
              {
                for (size_t k=0; k<dim; k++)
                  {
                    RF value = (gradU_s[k][i_s]*ahat_s[k]*omega_s + gradU_n[k][i_n]*ahat_n[k]*omega_n);
                    Xface_s[i_s] -= value; // consistency term
                    Xface_n[i_n] -= value; // consistency term
                  }
                RF value = (U_s[i_s]-U_n[i_n])*(alpha/h_F)*harmonic_average*p*(p+dim-1);
                Xface_s[i_s] += value; // interior penalty term
                Xface_n[i_n] += value; // interior penalty term
              }
            Xface_s[i_s] *= gamma*weight;
            Xface_n[i_n] *= gamma*weight;

            // the SIPG/NIPG penalty term
            if (!zero_A)
              if (method!=ConvectionDiffusionDGFastMethod::IPG)
                for (size_t k=0; k<dim; k++)
                  {
                    X_s[k][i_s] = (U_s[i_s]-U_n[i_n])*ahat_s[k]*omega_s*gamma*weight;
                    X_n[k][i_n] = (U_s[i_s]-U_n[i_n])*ahat_n[k]*omega_n*gamma*weight;
                  }
          }

        // exchange both dimensions due to slowest/fastest
        if (dim==3 && (facedir_s==0 || facedir_s==2))
          {
            for (size_t j=1; j<m; j++)
              for (size_t i=0; i<j; i++)
                std::swap(Xface_s[j*m+i],Xface_s[i*m+j]);
            for (size_t k=0; k<dim; k++)
              for (size_t j=1; j<m; j++)
                for (size_t i=0; i<j; i++)
                  std::swap(X_s[k][j*m+i],X_s[k][i*m+j]);
          }
        if (dim==3 && (facedir_n==0 || facedir_n==2))
          {
            for (size_t j=1; j<m; j++)
              for (size_t i=0; i<j; i++)
                std::swap(Xface_n[j*m+i],Xface_n[i*m+j]);
            for (size_t k=0; k<dim; k++)
              for (size_t j=1; j<m; j++)
                for (size_t i=0; i<j; i++)
                  std::swap(X_n[k][j*m+i],X_n[k][i*m+j]);
          }

        // compute residuals VERSION with individual face Theta matrices
        // for (size_t l=0; l<dim; l++) matrices[l] = &(ThetaT_s[l]); // test function in self
        // sumfactT.multiply_faceT(matrices,Xface_s,R);
        // for (size_t i=0; i<N; i++) R_s[i] = R[i];
        // for (size_t l=0; l<dim; l++) matrices[l] = &(ThetaT_n[l]); // test function in neighbour
        // sumfactT.multiply_faceT(matrices,Xface_n,R);
        // for (size_t i=0; i<N; i++) R_n[i] = -R[i]; // minus due to jump of test function
        // if (!zero_A)
        //   if (method!=ConvectionDiffusionDGFastMethod::IPG) // the gradient penalty term
        //     for (size_t k=0; k<dim; k++)
        //       {
        //         // test function in self
        //         for (size_t l=0; l<dim; l++)
        //           if (permute_s[l]==k)
        //             matrices[l] = &(dThetaT_s[l]);
        //           else
        //             matrices[l] = &(ThetaT_s[l]);
        //         sumfactT.multiply_faceT(matrices,X_s[k],R);
        //         for (size_t i=0; i<N; i++) R_s[i] -= theta*R[i];
        //         // test function in neighbor
        //         for (size_t l=0; l<dim; l++)
        //           if (permute_n[l]==k)
        //             matrices[l] = &(dThetaT_n[l]);
        //           else
        //             matrices[l] = &(ThetaT_n[l]);
        //         sumfactT.multiply_faceT(matrices,X_n[k],R);
        //         for (size_t i=0; i<N; i++) R_n[i] -= theta*R[i];
        //       }

        // NEW version with precomputed face Theta matrices
        for (size_t l=0; l<dim-1; l++) matrices[l] = &cache().ThetaT;
        if (facemod_s==0) matrices[dim-1]=&cache().faceThetaT0; else matrices[dim-1]=&cache().faceThetaT1;
        sumfactT.multiply_faceT(matrices,Xface_s,R);
        for (size_t i=0; i<N; i++) R_s[i] = R[i];
        if (facemod_n==0) matrices[dim-1]=&cache().faceThetaT0; else matrices[dim-1]=&cache().faceThetaT1;
        sumfactT.multiply_faceT(matrices,Xface_n,R);
        for (size_t i=0; i<N; i++) R_n[i] = -R[i]; // minus due to jump of test function
        if (!zero_A)
          if (method!=ConvectionDiffusionDGFastMethod::IPG) // the gradient penalty term
            for (size_t k=0; k<dim; k++)
              {
                // test function in self
                for (size_t l=0; l<dim-1; l++)
                  if (permuteT_s[l]==k) matrices[l] = &cache().dThetaT; else matrices[l] = &cache().ThetaT;
                if (facemod_s==0) {
                  if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT0; else matrices[dim-1] = &cache().faceThetaT0;
                } else {
                  if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT1; else matrices[dim-1] = &cache().faceThetaT1;
                }
                sumfactT.multiply_faceT(matrices,X_s[k],R);
                for (size_t i=0; i<N; i++) R_s[i] -= theta*R[i];

                // test function in neighbor
                for (size_t l=0; l<dim-1; l++)
                  if (permuteT_n[l]==k) matrices[l] = &cache().dThetaT; else matrices[l] = &cache().ThetaT;
                if (facemod_n==0) {
                  if (permuteT_n[dim-1]==k) matrices[dim-1] = &cache().facedThetaT0; else matrices[dim-1] = &cache().faceThetaT0;
                } else {
                  if (permuteT_n[dim-1]==k) matrices[dim-1] = &cache().facedThetaT1; else matrices[dim-1] = &cache().faceThetaT1;
                }
                sumfactT.multiply_faceT(matrices,X_n[k],R);
                for (size_t i=0; i<N; i++) R_n[i] -= theta*R[i];
              }
        // end version with precomputed Theta matrices

        // write back residuals
        for (size_t i=0; i<N; i++)
          {
            MultiIndex i_multi(mi<n>(i));
            size_t inew = i + (i_multi[dim-1]-i_multi[facedir_s])*cache().npower[facedir_s] + (i_multi[facedir_s]-i_multi[dim-1])*cache().npower[dim-1];
            r_s.accumulate(lfsv_s,i,R_s[inew]);
          }
        for (size_t i=0; i<N; i++)
          {
            MultiIndex i_multi(mi<n>(i));
            size_t inew = i + (i_multi[dim-1]-i_multi[facedir_n])*cache().npower[facedir_n] + (i_multi[facedir_n]-i_multi[dim-1])*cache().npower[dim-1];
            r_n.accumulate(lfsv_n,i,R_n[inew]);
          }
      }

      // analytical Jacobian for skeleton terms (started Sep 12, 2014)
      template<typename IG, typename LFSU, typename XX, typename LFSV, typename MAT>
      void jacobian_skeleton (const IG& ig,
                              const LFSU& lfsu_s, const XX& x_s, const LFSV& lfsv_s,
                              const LFSU& lfsu_n, const XX& x_n, const LFSV& lfsv_n,
                              MAT& mat_ss, MAT& mat_sn,
                              MAT& mat_ns, MAT& mat_nn) const
      {

        auto inside_cell = ig.inside();
        auto outside_cell = ig.outside();

        // permutation of directions in the volume refelem
        size_t facedir_s = ig.indexInInside()/2; // direction of the face = the dimension to be reduced
        size_t facedir_n = ig.indexInOutside()/2;
        size_t facemod_s = ig.indexInInside()%2; // 0: face at 0.0, 1: face at 1.0
        size_t facemod_n = ig.indexInOutside()%2;

        // Volume -> face : direction 0 should be face
        size_t permute_s[dim];  // permutation of directions on volume element
        size_t permute_n[dim];  // permutation of directions on volume element
        for (size_t i=0; i<dim; i++) permute_s[i] = permute_n[i] = i;
        std::swap(permute_s[0],permute_s[facedir_s]);
        std::swap(permute_n[0],permute_n[facedir_n]);

        // Face -> volume : direction dim-1 should be face
        size_t permuteT_s[dim];  // permutation of directions on volume element
        size_t permuteT_n[dim];  // permutation of directions on volume element
        for (size_t i=0; i<dim; i++) permuteT_s[i] = permuteT_n[i] = i;
        std::swap(permuteT_s[dim-1],permuteT_s[facedir_s]);
        std::swap(permuteT_n[dim-1],permuteT_n[facedir_n]);

        // transformation
        JacobianType jac_s, jac_n;
        RF gamma;
        Dune::FieldVector<DF,dim> n_F;
        if (affine_transformation)
          {
            Dune::FieldVector<DF,dim>  inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
            Dune::FieldVector<DF,dim> outside_local = Dune::ReferenceElements<DF,dim>::general(outside_cell.type()).position(0,0);
            jac_s = inside_cell.geometry().jacobianInverseTransposed(inside_local);
            jac_n = outside_cell.geometry().jacobianInverseTransposed(outside_local);
            Dune::FieldVector<DF,dim-1> qp(0.0);
            gamma = ig.geometry().integrationElement(qp);
            n_F = ig.centerUnitOuterNormal();
          }

        // parameters
        typename T::Traits::PermTensorType A_s, A_n;
        typename T::Traits::RangeType b; // velocity * normal is continuous!
        if (!zero_A && constant_A)
          {
            Dune::FieldVector<DF,dim>  inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
            Dune::FieldVector<DF,dim> outside_local = Dune::ReferenceElements<DF,dim>::general(outside_cell.type()).position(0,0);
            A_s = param.A(inside_cell,inside_local);
            A_n = param.A(outside_cell,outside_local);
          }

        // compute transformed coefficients and weights
        Dune::FieldVector<RF,dim> An_F_s, An_F_n;
        Dune::FieldVector<RF,dim> ahat_s, ahat_n; // n_F*(A_x jac_x)
        RF omega_s(0.5), omega_n(0.5), harmonic_average(1.0);
        if (!zero_A && affine_transformation && constant_A)
          {
            A_s.mv(n_F,An_F_s);
            A_n.mv(n_F,An_F_n);
            jac_s.mtv(An_F_s,ahat_s);
            jac_n.mtv(An_F_n,ahat_n);
            if (weights==ConvectionDiffusionDGFastWeights::weightsOn)
              {
                RF delta_s = (An_F_s*n_F);
                RF delta_n = (An_F_n*n_F);
                omega_s = delta_n/(delta_s+delta_n+1e-20);
                omega_n = delta_s/(delta_s+delta_n+1e-20);
                harmonic_average = 2.0*delta_s*delta_n/(delta_s+delta_n+1e-20);
              }
          }

        // mesh parameter
        RF h_F = std::min(inside_cell.geometry().volume(),outside_cell.geometry().volume())/ig.geometry().volume(); // Houston!

        // loop over all ansatz functions (i.e. columns of the Jacobian blocks)
        for (size_t j=0; j<N; j++)
          {
            MultiIndex multi_j(mi<n>(j)); // multiindex of ansatz function

            // prepare input values for sumfact
            // std::cout << "PREPARE INPUT VALUES ..." << std::endl;
            for (size_t i=0; i<Mface; i++) // loop over all quadrature points
              {
                // position and weight
                FaceMultiIndex multi_i(mi<m,dim-1>(i));    // multiindex of quadrature point on face
                Dune::FieldVector<DF,dim-1> qp;            // position of quadrature point in face refelem
                for (size_t k=0; k<dim-1; k++) qp[k] = cache().xi[multi_i[k]];
                Dune::FieldVector<DF,dim> qplocal_s(ig.geometryInInside().global(qp)); // position of quadrature point in volume refelem (inside)
                Dune::FieldVector<DF,dim> qplocal_n(ig.geometryInOutside().global(qp));// position of quadrature point in volume refelem (outside)
                RF weight(1.0);
                for (size_t j=0; j<dim-1; j++) weight *= cache().w[multi_i[j]];  // weight of quadrature point in face refelem

                // permuted index on face -> works only up to 3D!
                size_t i_s=i;
                if (facedir_s==2) i_s=multi_i[0]*m+multi_i[1];
                size_t i_n=i;
                if (facedir_n==2) i_n=multi_i[0]*m+multi_i[1];

                // // integer coordinates of the quadrature point in the volume elements
                MultiIndex multi_i_s;
                for (size_t l=0; l<dim-1; l++) multi_i_s[l+1] = multi_i[l]; // fill in face multiindex
                if (facedir_s==2) std::swap(multi_i_s[1],multi_i_s[2]);     // swap due to permutation on volume not preserving the order of remaining components
                if (facemod_s==0) multi_i_s[0] = 0; else multi_i_s[0] = m-1;// add first component
                std::swap(multi_i_s[0],multi_i_s[facedir_s]);               // go back to original numbering in volume
                // // now we should have (xi[multi_i_s[dim-1]],...,xi[multi_i_s[0]]) == qplocal_s
                // {
                //   bool ok(true);
                //   for (int ii=0; ii<dim; ii++)
                //     if ( ii!=facedir_s && std::abs(xi[multi_i_s[ii]]-qplocal_s[ii])>1e-10 ) ok=false;
                //   if (!ok) {
                //     std::cout << "incorrect volume multiindex self" << " qp=" << qplocal_s;
                //     for (int ii=0; ii<dim; ii++) std::cout << " " << multi_i_s[ii];
                //     std::cout << std::endl;
                //   }
                // }

                MultiIndex multi_i_n;
                for (size_t l=0; l<dim-1; l++) multi_i_n[l+1] = multi_i[l]; // fill in face multiindex
                if (facedir_n==2) std::swap(multi_i_n[1],multi_i_n[2]);     // swap due to permutation on volume not preserving the order of remaining components
                if (facemod_n==0) multi_i_n[0] = 0; else multi_i_n[0] = m-1;// add first component
                std::swap(multi_i_n[0],multi_i_n[facedir_n]);               // go back to original numbering in volume
                // // now we should have (xi[multi_i_n[dim-1]],...,xi[multi_i_n[0]]) == qplocal_n
                // {
                //   bool ok(true);
                //   for (int ii=0; ii<dim; ii++)
                //     if ( ii!=facedir_n && std::abs(xi[multi_i_n[ii]]-qplocal_n[ii])>1e-10 ) ok=false;
                //   if (!ok) {
                //     std::cout << "incorrect volume multiindex neig" << " qp=" << qplocal_n;
                //     for (int ii=0; ii<dim; ii++) std::cout << " " << multi_i_n[ii];
                //     std::cout << std::endl;
                //   }
                // }

                // std::cout << "multiindex: " << multi_i
                //           << " qp: " << qp
                //           << " qplocal_s " << qplocal_s
                //           << " qplocal_n " << qplocal_n
                //           << " weight: " << weight
                //           << std::endl;

                // evaluate transformation if necessary
                if (!affine_transformation) {
                  jac_s = inside_cell.geometry().jacobianInverseTransposed(qplocal_s);
                  jac_n = outside_cell.geometry().jacobianInverseTransposed(qplocal_n);
                  gamma = ig.geometry().integrationElement(qp);
                  n_F = ig.unitOuterNormal(qp);
                }

                // evaluate parameters if necessary
                if (!zero_A && !constant_A) {
                  A_s = param.A(inside_cell,qplocal_s);
                  A_n = param.A(outside_cell,qplocal_n);
                }

                // recompute derived parameters
                if (!zero_A && (!affine_transformation || !constant_A) )
                  {
                    A_s.mv(n_F,An_F_s);
                    A_n.mv(n_F,An_F_n);
                    jac_s.mtv(An_F_s,ahat_s);
                    jac_n.mtv(An_F_n,ahat_n);
                    if (weights==ConvectionDiffusionDGFastWeights::weightsOn)
                      {
                        RF delta_s = (An_F_s*n_F);
                        RF delta_n = (An_F_n*n_F);
                        omega_s = delta_n/(delta_s+delta_n+1e-20);
                        omega_n = delta_s/(delta_s+delta_n+1e-20);
                        harmonic_average = 2.0*delta_s*delta_n/(delta_s+delta_n+1e-20);
                      }
                  }

                // values of trial function j and its gradients at quadrature point in self and neighbor
                RF trialfunction_s=1.0;
                for (size_t l=0; l<dim; l++)
                  if (l!=facedir_s)
                    trialfunction_s *= cache().Theta.colmajoraccess(multi_i_s[l],multi_j[l]); // poly.p(multi_j[l],qplocal_s[l]); //
                  else
                    if (facemod_s==0)
                      trialfunction_s *= cache().faceTheta0.colmajoraccess(0,multi_j[l]);
                    else
                      trialfunction_s *= cache().faceTheta1.colmajoraccess(0,multi_j[l]);

                RF gradient_trialfunction_s[dim];
                for (size_t beta=0; beta<dim; beta++) {
                  gradient_trialfunction_s[beta] = 1.0;
                  for (size_t l=0; l<dim; l++)
                    if (l!=facedir_s) {
                      if (l==beta)
                        gradient_trialfunction_s[beta] *= cache().dTheta.colmajoraccess(multi_i_s[l],multi_j[l]); //poly.dp(multi_j[l],qplocal_s[l]);
                      else
                        gradient_trialfunction_s[beta] *= cache().Theta.colmajoraccess(multi_i_s[l],multi_j[l]); //poly.p(multi_j[l],qplocal_s[l]);
                    }
                    else if (facemod_s==0)
                      {
                        if (l==beta)
                          gradient_trialfunction_s[beta] *= cache().facedTheta0.colmajoraccess(0,multi_j[l]); //poly.dp(multi_j[l],qplocal_s[l]);
                        else
                          gradient_trialfunction_s[beta] *= cache().faceTheta0.colmajoraccess(0,multi_j[l]); //poly.p(multi_j[l],qplocal_s[l]);
                      }
                    else
                      {
                        if (l==beta)
                          gradient_trialfunction_s[beta] *= cache().facedTheta1.colmajoraccess(0,multi_j[l]);
                        else
                          gradient_trialfunction_s[beta] *= cache().faceTheta1.colmajoraccess(0,multi_j[l]);
                      }
                }
                RF trialfunction_n=1.0;
                for (size_t l=0; l<dim; l++)
                  if (l!=facedir_n)
                    trialfunction_n *= cache().Theta.colmajoraccess(multi_i_n[l],multi_j[l]);
                  else
                    if (facemod_n==0)
                      trialfunction_n *= cache().faceTheta0.colmajoraccess(0,multi_j[l]);
                    else
                      trialfunction_n *= cache().faceTheta1.colmajoraccess(0,multi_j[l]);
                RF gradient_trialfunction_n[dim];
                for (size_t beta=0; beta<dim; beta++) {
                  gradient_trialfunction_n[beta] = 1.0;
                  for (size_t l=0; l<dim; l++)
                    if (l!=facedir_n) {
                      if (l==beta)
                        gradient_trialfunction_n[beta] *= cache().dTheta.colmajoraccess(multi_i_n[l],multi_j[l]); //poly.dp(multi_j[l],qplocal_n[l]);
                      else
                        gradient_trialfunction_n[beta] *= cache().Theta.colmajoraccess(multi_i_n[l],multi_j[l]); //poly.p(multi_j[l],qplocal_n[l]);
                    }
                    else if (facemod_n==0)
                      {
                        if (l==beta)
                          gradient_trialfunction_n[beta] *= cache().facedTheta0.colmajoraccess(0,multi_j[l]); //poly.dp(multi_j[l],qplocal_n[l]);
                        else
                          gradient_trialfunction_n[beta] *= cache().faceTheta0.colmajoraccess(0,multi_j[l]); //poly.p(multi_j[l],qplocal_n[l]);
                      }
                    else
                      {
                        if (l==beta)
                          gradient_trialfunction_n[beta] *= cache().facedTheta1.colmajoraccess(0,multi_j[l]);
                        else
                          gradient_trialfunction_n[beta] *= cache().faceTheta1.colmajoraccess(0,multi_j[l]);
                      }
                }

                // convection, consistency and interior penalty term
                Xface_s[i_s] = Xface_n[i_n] = 0.0; // each side has its input vector using the corresponding basis function
                if (!zero_b)
                  {
                    b = param.b(inside_cell,qplocal_s);
                    RF flux = b*n_F;
                    RF absflux = std::abs(flux);
                    Xface_s[i_s] += flux*0.5*(trialfunction_s) + absflux*0.5*(trialfunction_s);
                    Xface_n[i_n] += flux*0.5*(trialfunction_n) - absflux*0.5*(trialfunction_n); // minus sign is from upwind
                  }
                if (!zero_A)
                  {
                    for (size_t k=0; k<dim; k++)
                      {
                        Xface_s[i_s] -= gradient_trialfunction_s[k]*ahat_s[k]*omega_s; // consistency term, minus is from minus sign in weak form
                        Xface_n[i_n] -= gradient_trialfunction_n[k]*ahat_n[k]*omega_n; // consistency term, minus is from minus sign in weak form
                      }
                    Xface_s[i_s] += trialfunction_s*(alpha/h_F)*harmonic_average*p*(p+dim-1); // interior penalty term
                    Xface_n[i_n] -= trialfunction_n*(alpha/h_F)*harmonic_average*p*(p+dim-1); // interior penalty term, minus sign is from jump in trial function
                  }
                Xface_s[i_s] *= gamma*weight; // scale with integration element and weight
                Xface_n[i_n] *= gamma*weight; // scale with integration element and weight

                // the SIPG/NIPG penalty term
                if (!zero_A)
                  if (method!=ConvectionDiffusionDGFastMethod::IPG)
                    for (size_t k=0; k<dim; k++)
                      {
                        X_s[k][i_s] =  trialfunction_s*ahat_s[k]*omega_s*gamma*weight; // Note: theta (SIPG/NIPG) will be included below
                        X_n[k][i_n] = -trialfunction_n*ahat_n[k]*omega_n*gamma*weight; // minus is from the jump of the trial function
                      }
              }

            // exchange both dimensions on face when needed
            // this is due to the fact that for the residual caluculation the
            // cropped direction is the outermost
            if (dim==3 && (facedir_s==0 || facedir_s==2))
              {
                for (size_t j=1; j<m; j++)
                  for (size_t i=0; i<j; i++)
                    std::swap(Xface_s[j*m+i],Xface_s[i*m+j]);
                for (size_t k=0; k<dim; k++)
                  for (size_t j=1; j<m; j++)
                    for (size_t i=0; i<j; i++)
                      std::swap(X_s[k][j*m+i],X_s[k][i*m+j]);
              }
            if (dim==3 && (facedir_n==0 || facedir_n==2))
              {
                for (size_t j=1; j<m; j++)
                  for (size_t i=0; i<j; i++)
                    std::swap(Xface_n[j*m+i],Xface_n[i*m+j]);
                for (size_t k=0; k<dim; k++)
                  for (size_t j=1; j<m; j++)
                    for (size_t i=0; i<j; i++)
                      std::swap(X_n[k][j*m+i],X_n[k][i*m+j]);
              }

            // now compute the columns where the *trial function* is from self
            for (size_t l=0; l<dim-1; l++) matrices[l] = &cache().ThetaT;

            if (facemod_s==0) matrices[dim-1]=&cache().faceThetaT0; else matrices[dim-1]=&cache().faceThetaT1;
            sumfactT.multiply_faceT(matrices,Xface_s,R);
            for (size_t i=0; i<N; i++) R_s[i] = R[i];

            if (facemod_n==0) matrices[dim-1]=&cache().faceThetaT0; else matrices[dim-1]=&cache().faceThetaT1;
            sumfactT.multiply_faceT(matrices,Xface_s,R);
            for (size_t i=0; i<N; i++) R_n[i] = -R[i]; // minus due to jump of test function

            if (!zero_A)
              if (method!=ConvectionDiffusionDGFastMethod::IPG) // the gradient penalty term
                for (size_t k=0; k<dim; k++)
                  {
                    // test function in self
                    for (size_t l=0; l<dim-1; l++)
                      if (permuteT_s[l]==k) matrices[l] = &cache().dThetaT; else matrices[l] = &cache().ThetaT;
                    if (facemod_s==0) {
                      if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT0; else matrices[dim-1] = &cache().faceThetaT0;
                    } else {
                      if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT1; else matrices[dim-1] = &cache().faceThetaT1;
                    }
                    sumfactT.multiply_faceT(matrices,X_s[k],R);
                    for (size_t i=0; i<N; i++) R_s[i] -= theta*R[i]; // minus due to minus sign in weak form

                    // test function in neighbor
                    for (size_t l=0; l<dim-1; l++)
                      if (permuteT_n[l]==k) matrices[l] = &cache().dThetaT; else matrices[l] = &cache().ThetaT;
                    if (facemod_n==0) {
                      if (permuteT_n[dim-1]==k) matrices[dim-1] = &cache().facedThetaT0; else matrices[dim-1] = &cache().faceThetaT0;
                    } else {
                      if (permuteT_n[dim-1]==k) matrices[dim-1] = &cache().facedThetaT1; else matrices[dim-1] = &cache().faceThetaT1;
                    }
                    sumfactT.multiply_faceT(matrices,X_s[k],R);
                    for (size_t i=0; i<N; i++) R_n[i] -= theta*R[i]; // minus due to minus sign in weak form
                  }

            // accumulate column
            for (size_t i=0; i<N; i++)
              {
                MultiIndex i_multi(mi<n>(i));
                size_t inew = i + (i_multi[dim-1]-i_multi[facedir_s])*cache().npower[facedir_s] + (i_multi[facedir_s]-i_multi[dim-1])*cache().npower[dim-1];
                mat_ss.accumulate(lfsv_s,i,lfsu_s,j,R_s[inew]);
              }
            for (size_t i=0; i<N; i++)
              {
                MultiIndex i_multi(mi<n>(i));
                size_t inew = i + (i_multi[dim-1]-i_multi[facedir_n])*cache().npower[facedir_n] + (i_multi[facedir_n]-i_multi[dim-1])*cache().npower[dim-1];
                mat_ns.accumulate(lfsv_n,i,lfsu_s,j,R_n[inew]);
              }

            // now compute the columns where the *trial function* is from neighbor
            for (size_t l=0; l<dim-1; l++) matrices[l] = &cache().ThetaT;

            if (facemod_s==0) matrices[dim-1]=&cache().faceThetaT0; else matrices[dim-1]=&cache().faceThetaT1;
            sumfactT.multiply_faceT(matrices,Xface_n,R);
            for (size_t i=0; i<N; i++) R_s[i] = R[i];

            if (facemod_n==0) matrices[dim-1]=&cache().faceThetaT0; else matrices[dim-1]=&cache().faceThetaT1;
            sumfactT.multiply_faceT(matrices,Xface_n,R);
            for (size_t i=0; i<N; i++) R_n[i] = -R[i]; // minus due to jump of test function

            if (!zero_A)
              if (method!=ConvectionDiffusionDGFastMethod::IPG) // the gradient penalty term
                for (size_t k=0; k<dim; k++)
                  {
                    // test function in self
                    for (size_t l=0; l<dim-1; l++)
                      if (permuteT_s[l]==k) matrices[l] = &cache().dThetaT; else matrices[l] = &cache().ThetaT;
                    if (facemod_s==0) {
                      if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT0; else matrices[dim-1] = &cache().faceThetaT0;
                    } else {
                      if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT1; else matrices[dim-1] = &cache().faceThetaT1;
                    }
                    sumfactT.multiply_faceT(matrices,X_n[k],R);
                    for (size_t i=0; i<N; i++) R_s[i] -= theta*R[i]; // minus due to minus sign in weak form

                    // test function in neighbor
                    for (size_t l=0; l<dim-1; l++)
                      if (permuteT_n[l]==k) matrices[l] = &cache().dThetaT; else matrices[l] = &cache().ThetaT;
                    if (facemod_n==0) {
                      if (permuteT_n[dim-1]==k) matrices[dim-1] = &cache().facedThetaT0; else matrices[dim-1] = &cache().faceThetaT0;
                    } else {
                      if (permuteT_n[dim-1]==k) matrices[dim-1] = &cache().facedThetaT1; else matrices[dim-1] = &cache().faceThetaT1;
                    }
                    sumfactT.multiply_faceT(matrices,X_n[k],R);
                    for (size_t i=0; i<N; i++) R_n[i] -= theta*R[i]; // minus due to minus sign in weak form
                  }

            // accumulate column
            for (size_t i=0; i<N; i++)
              {
                MultiIndex i_multi(mi<n>(i));
                size_t inew = i + (i_multi[dim-1]-i_multi[facedir_s])*cache().npower[facedir_s] + (i_multi[facedir_s]-i_multi[dim-1])*cache().npower[dim-1];
                mat_sn.accumulate(lfsv_s,i,lfsu_n,j,R_s[inew]);
              }
            for (size_t i=0; i<N; i++)
              {
                MultiIndex i_multi(mi<n>(i));
                size_t inew = i + (i_multi[dim-1]-i_multi[facedir_n])*cache().npower[facedir_n] + (i_multi[facedir_n]-i_multi[dim-1])*cache().npower[dim-1];
                mat_nn.accumulate(lfsv_n,i,lfsu_n,j,R_n[inew]);
              }
          }
      }

      // boundary integral depending on test and ansatz functions
      // We put the Dirchlet evaluation also in the alpha term to save some geometry evaluations
      template<typename IG, typename LFSU, typename X, typename LFSV, typename RType>
      void alpha_boundary (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           RType& r_s) const
      {
        //std::cout << std::endl << "===> BOUNDARY" << std::endl;

        auto inside_cell = ig.inside();

        // permutation of directions in the volume refelem
        size_t facedir_s = ig.indexInInside()/2; // direction of the face = the dimension to be reduced
        size_t facemod_s = ig.indexInInside()%2; // 0: face at 0.0, 1: face at 1.0

        // Volume -> face : direction 0 should be face
        size_t permute_s[dim];  // permutation of directions on volume element
        for (size_t i=0; i<dim; i++) permute_s[i] = i;
        std::swap(permute_s[0],permute_s[facedir_s]);

        // Face -> volume : direction dim-1 should be face
        size_t permuteT_s[dim];  // permutation of directions on volume element
        for (size_t i=0; i<dim; i++) permuteT_s[i] = i;
        std::swap(permuteT_s[dim-1],permuteT_s[facedir_s]);

        // std::cout << "PERMUTATION of directions in volume element" << std::endl;
        // for (size_t i=0; i<dim; i++)
        //   std::cout << " self " << i << " <-> " << permute_s[i]
        //             << std::endl;

        // extract degrees of freedom => order is direction 0 fastest
        // need to swap direction facedir_s with 0 while extracting data
        for (size_t i=0; i<N; i++)
          {
            MultiIndex i_multi(mi<n>(i));
            size_t inew = i + (i_multi[0]-i_multi[facedir_s])*cache().npower[facedir_s] - i_multi[0] + i_multi[facedir_s];
            C_s[inew] = x_s(lfsu_s,i);
          }

        // tensor product quadrature points on face (in coordinates of the volume refelem!)
        // uses permuted directions, i.e. [i][0] is the constant coordinate, [i][k] corresp. to original direction permute_s[k]
        // RF xi_s[m][dim];
        // for (size_t i=0; i<m; i++)
        //   {
        //     Dune::FieldVector<DF,dim-1> qp(xi[i]); // loops through diagonal on face refelem
        //     Dune::FieldVector<DF,dim> qplocal_s = ig.geometryInInside().global(qp); // map to volume refelem
        //     for (size_t k=0; k<dim; k++) xi_s[i][k] = qplocal_s[permute_s[k]];
        //   }
        // now prepare the matrices to transform coefficients into values at Gauss points
        // Note that in general we have a different matrix in every direction since
        // the positions of the quadrature points might be different in case of nonconforming meshes.
        // However, we assume that the 1D basis functions are the same in every direction
        // direction 0 has but one quadrature point
        // for (size_t j=0; j<n; j++) // basis functions
        //   {
        //     Theta_s[0].colmajoraccess(0,j) = poly.p(j,xi_s[0][0]);
        //     ThetaT_s[0].colmajoraccess(j,0) = Theta_s[0].colmajoraccess(0,j);
        //   }
        // for (size_t k=1; k<dim; k++) // the directions with more than one quadrature point
        //   for (size_t j=0; j<n; j++) // basis functions
        //     for (size_t i=0; i<m; i++)  // quadrature points
        //       {
        //         Theta_s[k].colmajoraccess(i,j) = poly.p(j,xi_s[i][k]);
        //         ThetaT_s[k].colmajoraccess(j,i) = Theta_s[k].colmajoraccess(i,j);
        //       }
        // if (!zero_A)
        //   {
        //     // compute gradients of basis fuctions
        //     for (size_t j=0; j<n; j++) // basis functions
        //       {
        //         dTheta_s[0].colmajoraccess(0,j) = poly.dp(j,xi_s[0][0]);
        //         dThetaT_s[0].colmajoraccess(j,0) = dTheta_s[0].colmajoraccess(0,j);
        //       }
        //     for (size_t k=1; k<dim; k++) // the directions with more than one quadrature point
        //       for (size_t j=0; j<n; j++) // basis functions
        //         for (size_t i=0; i<m; i++)  // quadrature points
        //           {
        //             dTheta_s[k].colmajoraccess(i,j) = poly.dp(j,xi_s[i][k]);
        //             dThetaT_s[k].colmajoraccess(j,i) = dTheta_s[k].colmajoraccess(i,j);
        //           }
        //   }

        // compute values of u at quadrature points of face

        // for (size_t l=0; l<dim; l++) matrices[l] = &(Theta_s[l]);
        // sumfact.multiply_face(matrices,C_s,U_s);
        // // compute values of gradU on the volume refelem at quadrature points of face
        // if (!zero_A)
        //   for (size_t k=0; k<dim; k++) // for all directions of the volume reference element (not permuted)
        //     {
        //       for (size_t l=0; l<dim; l++)
        //         if (permute_s[l]==k)
        //           matrices[l] = &(dTheta_s[l]);
        //         else
        //           matrices[l] = &(Theta_s[l]);
        //       sumfact.multiply_face(matrices,C_s,gradU_s[k]);
        //     }

        // NEW version with precomputed face Theta matrices
        for (size_t l=1; l<dim; l++) matrices[l] = &cache().Theta;
        if (facemod_s==0) matrices[0]=&cache().faceTheta0; else matrices[0]=&cache().faceTheta1;
        sumfact.multiply_face(matrices,C_s,U_s);
        // compute values of gradU on the volume refelem at quadrature points of face
        if (!zero_A)
          for (size_t k=0; k<dim; k++) // for all directions of the volume reference element (not permuted)
            {
              if (facemod_s==0) {
                if (permute_s[0]==k) matrices[0] = &cache().facedTheta0; else matrices[0] = &cache().faceTheta0;
              } else {
                if (permute_s[0]==k) matrices[0] = &cache().facedTheta1; else matrices[0] = &cache().faceTheta1;
              }
              for (size_t l=1; l<dim; l++)
                if (permute_s[l]==k) matrices[l] = &cache().dTheta; else matrices[l] = &cache().Theta;
              sumfact.multiply_face(matrices,C_s,gradU_s[k]);
            }
        // end NEW version

        // transformation
        JacobianType jac_s;
        RF gamma;
        Dune::FieldVector<DF,dim> n_F;
        if (affine_transformation)
          {
            Dune::FieldVector<DF,dim>  inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
            jac_s = inside_cell.geometry().jacobianInverseTransposed(inside_local);
            Dune::FieldVector<DF,dim-1> qp(0.0);
            gamma = ig.geometry().integrationElement(qp);
            n_F = ig.centerUnitOuterNormal();
          }

        // parameters
        typename T::Traits::PermTensorType A_s;
        typename T::Traits::RangeType b(0.0); // velocity * normal is continuous!
        if (!zero_A && constant_A)
          {
            Dune::FieldVector<DF,dim>  inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
            A_s = param.A(inside_cell,inside_local);
          }

        // compute transformed coefficients and weights
        Dune::FieldVector<RF,dim> An_F_s;
        Dune::FieldVector<RF,dim> ahat_s; // n_F*(A_x jac_x)
        RF harmonic_average(1.0);
        if (!zero_A && affine_transformation && constant_A)
          {
            A_s.mv(n_F,An_F_s);
            jac_s.mtv(An_F_s,ahat_s);
            if (weights==ConvectionDiffusionDGFastWeights::weightsOn)
              {
                harmonic_average = An_F_s*n_F;
              }
          }

        // mesh parameter
        RF h_F = inside_cell.geometry().volume()/ig.geometry().volume(); // Houston!

        // prepare input values
        // std::cout << "PREPARE INPUT VALUES ..." << std::endl;
        for (size_t i=0; i<Mface; i++) // enumerate all quadrature points
          {
            FaceMultiIndex ialpha(mi<m,dim-1>(i));      // provide i as d-1-dimensional multiindex
            Dune::FieldVector<DF,dim-1> qp;             // position of quadrature point in face refelem
            for (size_t k=0; k<dim-1; k++) qp[k] = cache().xi[ialpha[k]];
            size_t i_s=i;
            if (facedir_s==2) i_s=ialpha[0]*m+ialpha[1];
            Dune::FieldVector<DF,dim> qplocal_s(ig.geometryInInside().global(qp));// position of quadrature point in volume refelem
            RF weight(1.0);
            for (size_t j=0; j<dim-1; j++) weight *= cache().w[ialpha[j]];  // weight of quadrature point in face refelem

            // std::cout << " index: " << i
            //           << " multiindex: " << ialpha
            //           << " qp: " << qp
            //           << " qplocal_s " << qplocal_s
            //           << " weight: " << weight
            //           << std::endl;

            // evaluate transformation if necessary
            if (!affine_transformation) {
              jac_s = inside_cell.geometry().jacobianInverseTransposed(qplocal_s);
              gamma = ig.geometry().integrationElement(qp);
              n_F = ig.unitOuterNormal(qp);
            }

            // evaluate parameters if necessary
            if (!zero_A && !constant_A) {
              A_s = param.A(inside_cell,qplocal_s);
            }

            // recompute derived parameters
            if (!zero_A && (!affine_transformation || !constant_A) )
              {
                A_s.mv(n_F,An_F_s);
                jac_s.mtv(An_F_s,ahat_s);
                if (weights==ConvectionDiffusionDGFastWeights::weightsOn)
                  {
                    harmonic_average = An_F_s*n_F;
                  }
              }

            // evaluate boundary condition
            BCType bctype = param.bctype(ig.intersection(),qp);

            // clear coefficients
            Xface_s[i_s] = 0.0;
            for (size_t k=0; k<dim; k++) X_s[k][i_s] = 0.0;

            // Neumann boundary condition
            if (bctype == ConvectionDiffusionBoundaryConditions::Neumann)
              {
                RF j = param.j(ig.intersection(),qp);
                Xface_s[i_s] += j*gamma*weight;
                continue;
              }

            // outflow boundary condition
            if (bctype == ConvectionDiffusionBoundaryConditions::Outflow)
              {
                RF flux(0.0);
                if (!zero_b) b = param.b(inside_cell,qplocal_s);
                flux = b*n_F;

                if (flux<-1e-12)
                  DUNE_THROW(Dune::Exception,
                             "Outflow boundary condition on inflow! [b("
                             << ig.geometry().global(qp) << ") = "
                             << b << ")");

                RF o = param.o(ig.intersection(),qp);
                Xface_s[i_s] += (U_s[i_s]*flux + o)*gamma*weight;
                continue;
              }

            // Dirichlet boundary condition
            if (bctype != ConvectionDiffusionBoundaryConditions::Dirichlet) continue;
            RF g = param.g(inside_cell,qplocal_s);
            if (!zero_A)
              {
                for (size_t k=0; k<dim; k++)
                  Xface_s[i_s] -= gradU_s[k][i_s]*ahat_s[k]*gamma*weight; // consistency term
                Xface_s[i_s] += (U_s[i_s]-g)*(alpha/h_F)*harmonic_average*p*(p+dim-1)*gamma*weight; // interior penalty term
              }
            if (!zero_b) {
              b = param.b(inside_cell,qplocal_s);
              RF flux = b*n_F;
              RF absflux = std::abs(flux);
              RF value = flux*0.5*(U_s[i_s]+g) + absflux*0.5*(U_s[i_s]-g);
              Xface_s[i_s] += value*gamma*weight; // upwind flux
            }

            // the SIPG/NIPG penalty term
            if (!zero_A)
              if (method!=ConvectionDiffusionDGFastMethod::IPG)
                for (size_t k=0; k<dim; k++)
                  {
                    X_s[k][i_s] = (U_s[i_s]-g)*ahat_s[k]*gamma*weight;
                  }
          }

        // exchange both dimensions due to slowest/fastest
        if (dim==3 && (facedir_s==0 || facedir_s==2))
          {
            for (size_t j=1; j<m; j++)
              for (size_t i=0; i<j; i++)
                std::swap(Xface_s[j*m+i],Xface_s[i*m+j]);
            for (size_t k=0; k<dim; k++)
              for (size_t j=1; j<m; j++)
                for (size_t i=0; i<j; i++)
                  std::swap(X_s[k][j*m+i],X_s[k][i*m+j]);
          }

        // compute residuals; OLD version with individual Theta matrices
        // for (size_t l=0; l<dim; l++) matrices[l] = &(ThetaT_s[l]); // test function in self
        // sumfactT.multiply_faceT(matrices,Xface_s,R);
        // for (size_t i=0; i<N; i++) R_s[i] = R[i];
        // if (!zero_A)
        //   if (method!=ConvectionDiffusionDGFastMethod::IPG) // the gradient penalty term
        //     for (size_t k=0; k<dim; k++)
        //       {
        //         // test function in self
        //         for (size_t l=0; l<dim; l++)
        //           if (permute_s[l]==k)
        //             matrices[l] = &(dThetaT_s[l]);
        //           else
        //             matrices[l] = &(ThetaT_s[l]);
        //         sumfactT.multiply_faceT(matrices,X_s[k],R);
        //         for (size_t i=0; i<N; i++) R_s[i] -= theta*R[i];
        //       }

        // NEW version with precomputed face Theta matrices
        for (size_t l=0; l<dim-1; l++) matrices[l] = &cache().ThetaT;
        if (facemod_s==0) matrices[dim-1]=&cache().faceThetaT0; else matrices[dim-1]=&cache().faceThetaT1;
        sumfactT.multiply_faceT(matrices,Xface_s,R);
        for (size_t i=0; i<N; i++) R_s[i] = R[i];
        if (!zero_A)
          if (method!=ConvectionDiffusionDGFastMethod::IPG) // the gradient penalty term
            for (size_t k=0; k<dim; k++)
              {
                // test function in self
                for (size_t l=0; l<dim-1; l++)
                  if (permuteT_s[l]==k) matrices[l] = &cache().dThetaT; else matrices[l] = &cache().ThetaT;
                if (facemod_s==0) {
                  if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT0; else matrices[dim-1] = &cache().faceThetaT0;
                } else {
                  if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT1; else matrices[dim-1] = &cache().faceThetaT1;
                }
                sumfactT.multiply_faceT(matrices,X_s[k],R);
                for (size_t i=0; i<N; i++) R_s[i] -= theta*R[i];
              }
        // end NEW version

        // write back residuals
        {
          // size_t k=facedir_s; // the direction to be swapped
          for (size_t i=0; i<N; i++)
            {
              MultiIndex i_multi(mi<n>(i));
              size_t inew = i + (i_multi[dim-1]-i_multi[facedir_s])*cache().npower[facedir_s] + (i_multi[facedir_s]-i_multi[dim-1])*cache().npower[dim-1];
              r_s.accumulate(lfsv_s,i,R_s[inew]);
            }
        }
      }

      // analytical Jacobian for boundary terms (started Sep 12, 2014)
      template<typename IG, typename LFSU, typename XX, typename LFSV, typename MAT>
      void jacobian_boundary (const IG& ig,
                              const LFSU& lfsu_s, const XX& x_s, const LFSV& lfsv_s,
                              MAT& mat_ss) const
      {
        //std::cout << std::endl << "===> BOUNDARY" << std::endl;

        auto inside_cell = ig.inside();

        // permutation of directions in the volume refelem
        size_t facedir_s = ig.indexInInside()/2; // direction of the face = the dimension to be reduced
        size_t facemod_s = ig.indexInInside()%2; // 0: face at 0.0, 1: face at 1.0

        // Volume -> face : direction 0 should be face
        size_t permute_s[dim];  // permutation of directions on volume element
        for (size_t i=0; i<dim; i++) permute_s[i] = i;
        std::swap(permute_s[0],permute_s[facedir_s]);

        // Face -> volume : direction dim-1 should be face
        size_t permuteT_s[dim];  // permutation of directions on volume element
        for (size_t i=0; i<dim; i++) permuteT_s[i] = i;
        std::swap(permuteT_s[dim-1],permuteT_s[facedir_s]);

        // transformation
        JacobianType jac_s;
        RF gamma;
        Dune::FieldVector<DF,dim> n_F;
        if (affine_transformation)
          {
            Dune::FieldVector<DF,dim>  inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
            jac_s = inside_cell.geometry().jacobianInverseTransposed(inside_local);
            Dune::FieldVector<DF,dim-1> qp(0.0);
            gamma = ig.geometry().integrationElement(qp);
            n_F = ig.centerUnitOuterNormal();
          }

        // parameters
        typename T::Traits::PermTensorType A_s;
        typename T::Traits::RangeType b(0.0); // velocity * normal is continuous!
        if (!zero_A && constant_A)
          {
            Dune::FieldVector<DF,dim>  inside_local = Dune::ReferenceElements<DF,dim>::general(inside_cell.type()).position(0,0);
            A_s = param.A(inside_cell,inside_local);
          }

        // compute transformed coefficients and weights
        Dune::FieldVector<RF,dim> An_F_s;
        Dune::FieldVector<RF,dim> ahat_s; // n_F*(A_x jac_x)
        RF harmonic_average(1.0);
        if (!zero_A && affine_transformation && constant_A)
          {
            A_s.mv(n_F,An_F_s);
            jac_s.mtv(An_F_s,ahat_s);
            if (weights==ConvectionDiffusionDGFastWeights::weightsOn)
              {
                harmonic_average = An_F_s*n_F;
              }
          }

        // mesh parameter
        RF h_F = inside_cell.geometry().volume()/ig.geometry().volume(); // Houston!

        // loop over all ansatz functions (i.e. columns of the Jacobian blocks)
        for (size_t j=0; j<N; j++)
          {
            MultiIndex multi_j(mi<n>(j)); // multiindex of ansatz function

            // prepare input values
            // std::cout << "PREPARE INPUT VALUES ..." << std::endl;
            for (size_t i=0; i<Mface; i++) // enumerate all quadrature points
              {
                FaceMultiIndex multi_i(mi<m,dim-1>(i));      // provide i as d-1-dimensional multiindex
                Dune::FieldVector<DF,dim-1> qp;             // position of quadrature point in face refelem
                for (size_t k=0; k<dim-1; k++) qp[k] = cache().xi[multi_i[k]];
                Dune::FieldVector<DF,dim> qplocal_s(ig.geometryInInside().global(qp));// position of quadrature point in volume refelem
                RF weight(1.0);
                for (size_t j=0; j<dim-1; j++) weight *= cache().w[multi_i[j]];  // weight of quadrature point in face refelem

                // permuted index on face -> works only up to 3D!
                size_t i_s=i;
                if (facedir_s==2) i_s=multi_i[0]*m+multi_i[1];

                // integer coordinates of the quadrature point in the volume elements
                MultiIndex multi_i_s;
                for (size_t l=0; l<dim-1; l++) multi_i_s[l+1] = multi_i[l]; // fill in face multiindex
                if (facedir_s==2) std::swap(multi_i_s[1],multi_i_s[2]);     // swap due to permutation on volume not preserving the order of remaining components
                if (facemod_s==0) multi_i_s[0] = 0; else multi_i_s[0] = m-1;// add first component
                std::swap(multi_i_s[0],multi_i_s[facedir_s]);               // go back to original numbering in volume
                // now we should have (xi[multi_i_s[dim-1]],...,xi[multi_i_s[0]]) == qplocal_s

                // evaluate transformation if necessary
                if (!affine_transformation) {
                  jac_s = inside_cell.geometry().jacobianInverseTransposed(qplocal_s);
                  gamma = ig.geometry().integrationElement(qp);
                  n_F = ig.unitOuterNormal(qp);
                }

                // evaluate parameters if necessary
                if (!zero_A && !constant_A) {
                  A_s = param.A(inside_cell,qplocal_s);
                }

                // recompute derived parameters
                if (!zero_A && (!affine_transformation || !constant_A) )
                  {
                    A_s.mv(n_F,An_F_s);
                    jac_s.mtv(An_F_s,ahat_s);
                    if (weights==ConvectionDiffusionDGFastWeights::weightsOn)
                      {
                        harmonic_average = An_F_s*n_F;
                      }
                  }

                // evaluate boundary condition
                BCType bctype = param.bctype(ig.intersection(),qp);

                // clear coefficients
                Xface_s[i_s] = 0.0;
                for (size_t k=0; k<dim; k++) X_s[k][i_s] = 0.0;

                // values of trial function j and its gradients at quadrature point in self and neighbor
                RF trialfunction_s=1.0;
                for (size_t l=0; l<dim; l++)
                  if (l!=facedir_s)
                    trialfunction_s *= cache().Theta.colmajoraccess(multi_i_s[l],multi_j[l]); // poly.p(multi_j[l],qplocal_s[l]); //
                  else
                    if (facemod_s==0)
                      trialfunction_s *= cache().faceTheta0.colmajoraccess(0,multi_j[l]);
                    else
                      trialfunction_s *= cache().faceTheta1.colmajoraccess(0,multi_j[l]);
                RF gradient_trialfunction_s[dim];
                for (size_t beta=0; beta<dim; beta++) {
                  gradient_trialfunction_s[beta] = 1.0;
                  for (size_t l=0; l<dim; l++)
                    if (l!=facedir_s) {
                      if (l==beta)
                        gradient_trialfunction_s[beta] *= cache().dTheta.colmajoraccess(multi_i_s[l],multi_j[l]); //poly.dp(multi_j[l],qplocal_s[l]);
                      else
                        gradient_trialfunction_s[beta] *= cache().Theta.colmajoraccess(multi_i_s[l],multi_j[l]); //poly.p(multi_j[l],qplocal_s[l]);
                    }
                    else if (facemod_s==0)
                      {
                        if (l==beta)
                          gradient_trialfunction_s[beta] *= cache().facedTheta0.colmajoraccess(0,multi_j[l]); //poly.dp(multi_j[l],qplocal_s[l]);
                        else
                          gradient_trialfunction_s[beta] *= cache().faceTheta0.colmajoraccess(0,multi_j[l]); //poly.p(multi_j[l],qplocal_s[l]);
                      }
                    else
                      {
                        if (l==beta)
                          gradient_trialfunction_s[beta] *= cache().facedTheta1.colmajoraccess(0,multi_j[l]);
                        else
                          gradient_trialfunction_s[beta] *= cache().faceTheta1.colmajoraccess(0,multi_j[l]);
                      }
                }


                // RF trialfunction_s=1.0;
                // for (size_t l=0; l<dim; l++) trialfunction_s *= poly.p(multi_j[l],qplocal_s[l]); //Theta.colmajoraccess(multi_i_s[l],multi_j[l]);
                // RF gradient_trialfunction_s[dim];
                // for (size_t beta=0; beta<dim; beta++) {
                //   gradient_trialfunction_s[beta] = 1.0;
                //   for (size_t l=0; l<dim; l++)
                //     if (l==beta)
                //       gradient_trialfunction_s[beta] *= poly.dp(multi_j[l],qplocal_s[l]); //dTheta.colmajoraccess(multi_i_s[l],multi_j[l]);
                //     else
                //       gradient_trialfunction_s[beta] *= poly.p(multi_j[l],qplocal_s[l]); // Theta.colmajoraccess(multi_i_s[l],multi_j[l]);
                // }

                // Neumann boundary condition
                if (bctype == ConvectionDiffusionBoundaryConditions::Neumann)
                  {
                    continue;
                  }

                // outflow boundary condition
                if (bctype == ConvectionDiffusionBoundaryConditions::Outflow)
                  {
                    RF flux(0.0);
                    if (!zero_b) b = param.b(inside_cell,qplocal_s);
                    flux = b*n_F;

                    if (flux<-1e-12)
                      DUNE_THROW(Dune::Exception,
                                 "Outflow boundary condition on inflow! [b("
                                 << ig.geometry().global(qp) << ") = "
                                 << b << ")");
                    Xface_s[i_s] += trialfunction_s*flux*gamma*weight;
                    continue;
                  }

                // Dirichlet boundary condition
                if (bctype != ConvectionDiffusionBoundaryConditions::Dirichlet) continue;
                if (!zero_A)
                  {
                    for (size_t k=0; k<dim; k++)
                      Xface_s[i_s] -= gradient_trialfunction_s[k]*ahat_s[k]*gamma*weight; // consistency term
                    Xface_s[i_s] += trialfunction_s*(alpha/h_F)*harmonic_average*p*(p+dim-1)*gamma*weight; // interior penalty term
                  }
                if (!zero_b) {
                  b = param.b(inside_cell,qplocal_s);
                  RF flux = b*n_F;
                  RF absflux = std::abs(flux);
                  RF value = flux*0.5*trialfunction_s + absflux*0.5*trialfunction_s;
                  Xface_s[i_s] += value*gamma*weight; // upwind flux
                }

                // the SIPG/NIPG penalty term
                if (!zero_A)
                  if (method!=ConvectionDiffusionDGFastMethod::IPG)
                    for (size_t k=0; k<dim; k++)
                      {
                        X_s[k][i_s] = trialfunction_s*ahat_s[k]*gamma*weight;
                      }
              }

            // exchange both dimensions due to slowest/fastest
            if (dim==3 && (facedir_s==0 || facedir_s==2))
              {
                for (size_t j=1; j<m; j++)
                  for (size_t i=0; i<j; i++)
                    std::swap(Xface_s[j*m+i],Xface_s[i*m+j]);
                for (size_t k=0; k<dim; k++)
                  for (size_t j=1; j<m; j++)
                    for (size_t i=0; i<j; i++)
                      std::swap(X_s[k][j*m+i],X_s[k][i*m+j]);
              }

            // NEW version with precomputed face Theta matrices
            for (size_t l=0; l<dim-1; l++) matrices[l] = &cache().ThetaT;
            if (facemod_s==0) matrices[dim-1]=&cache().faceThetaT0; else matrices[dim-1]=&cache().faceThetaT1;
            sumfactT.multiply_faceT(matrices,Xface_s,R);
            for (size_t i=0; i<N; i++) R_s[i] = R[i];
            if (!zero_A)
              if (method!=ConvectionDiffusionDGFastMethod::IPG) // the gradient penalty term
                for (size_t k=0; k<dim; k++)
                  {
                    // test function in self
                    for (size_t l=0; l<dim-1; l++)
                      if (permuteT_s[l]==k) matrices[l] = &cache().dThetaT; else matrices[l] = &cache().ThetaT;
                    if (facemod_s==0) {
                      if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT0; else matrices[dim-1] = &cache().faceThetaT0;
                    } else {
                      if (permuteT_s[dim-1]==k) matrices[dim-1] = &cache().facedThetaT1; else matrices[dim-1] = &cache().faceThetaT1;
                    }
                    sumfactT.multiply_faceT(matrices,X_s[k],R);
                    for (size_t i=0; i<N; i++) R_s[i] -= theta*R[i];
                  }
            // end NEW version

            // accumulate column
            for (size_t i=0; i<N; i++)
              {
                MultiIndex i_multi(mi<n>(i));
                size_t inew = i + (i_multi[dim-1]-i_multi[facedir_s])*cache().npower[facedir_s] + (i_multi[facedir_s]-i_multi[dim-1])*cache().npower[dim-1];
                mat_ss.accumulate(lfsv_s,i,lfsu_s,j,R_s[inew]);
              }
          }
      }

      // volume integral depending only on test functions
      template<typename EG, typename LFSV, typename RType>
      void lambda_volume (const EG& eg, const LFSV& lfsv, RType& r) const
      {
        // quit if nothing to do
        if (zero_f) return;

        // transformation
        RF gamma;
        if (affine_transformation)
          {
            Dune::FieldVector<DF,dim> localcenter = Dune::ReferenceElements<DF,dim>::general(eg.geometry().type()).position(0,0);
            gamma = eg.geometry().integrationElement(localcenter);
          }

        // prepare input values
        for (size_t i=0; i<M; i++) // loop over all quadrature points
          {
            // position and weight
            MultiIndex alpha(mi<m>(i)); // multiindex of quadrature point
            Dune::FieldVector<DF,dim> x_alpha(0.0);                                // position of quadrature point
            for (size_t j=0; j<dim; j++) x_alpha[j] = cache().xi[alpha[j]];                // note: the xi are not consecutive
            RF weight(1.0);
            for (size_t j=0; j<dim; j++) weight *= cache().w[alpha[j]];                    // weight of quadrature point

            // evaluate transformation if necessary
            if (!affine_transformation)
              gamma = eg.geometry().integrationElement(x_alpha);

            // evaluate right hand side parameter function
            RF f = param.f(eg.entity(),x_alpha);

            // now assemble coefficients
            Xc[i] = -f*gamma*weight;
          }

        // compute the residuals
        sumfactT.multiply(cache().ThetaT,Xc,R);
        for (size_t i=0; i<N; i++) r.accumulate(lfsv,i,R[i]);
      }

      //! set time in parameter class
      void setTime (double t)
      {
        time = t;
        param.setTime(t);
      }

      Cache& cache()
      {
        return *_cache;
      }

      const Cache& cache() const
      {
        return *_cache;
      };

    private:
      // copy this for now to simplify the splitting constructor
      T param;  // two phase parameter class
      ConvectionDiffusionDGFastMethod::Type method;
      ConvectionDiffusionDGFastWeights::Type weights;
      Real alpha;
      Real theta;
      Real time;

      SumFactorizer<dim,m,n,RF,alignment> sumfact;
      SumFactorizer<dim,n,m,RF,alignment> sumfactT;

      std::shared_ptr<Cache> _cache;

      // per instance
      mutable AVector C;                    // solution coefficients (size = # basis functions N)
      mutable AVector C_s, C_n;             // solution coefficients (size = # basis functions N)
      mutable AVector U;                    // solution at quadrature points (size = # basis functions M)
      mutable AVector U_s, U_n;             // solution at quadrature points of face (size = # basis functions M/m)
      mutable std::vector<AVector> gradU;   // gradient at quadrature points (size = # number of quadrature points M)
      mutable std::vector<AVector> gradU_s; // gradient at quadrature points (size = # number of quadrature points on face)
      mutable std::vector<AVector> gradU_n; // gradient at quadrature points (size = # number of quadrature points on face)
      mutable std::vector<AVector> X;       // input values at quadrature points (size = # number of quadrature points M)
      // matrix count: (8*d+4) * m*n
      // coefficient count : (7+4*d) * n^d
      mutable AVector Xface_s;              // input values at quadrature points on face
      mutable AVector Xface_n;              // input values at quadrature points on face
      mutable std::vector<AVector> X_s,X_n; // input values at quadrature points on face

      mutable AVector Xc;                   // input values at quadrature points for sink term

      mutable AVector R;                    // output residual (size = # basis functions N)
      mutable AVector R_s;                  // output residual (size = # basis functions N)
      mutable AVector R_n;                  // output residual (size = # basis functions N)
      // coefficient count : (5+2*d) * n^(d-1)

      // matrix count: 8 * m*n

      // ===> total 3D: 47*n^2 + 19*n^3
      // legacy version for comparison: 4*(n^3+6*n^2)*n^3
      // some sizes in kilobytes
      //         SF   legacy
      // Q1     2.7     8
      // Q4    28.4   600

      mutable std::vector<const AMatrix*> matrices; // matrices for each stage in sumfact

    };
  }
}
#endif
