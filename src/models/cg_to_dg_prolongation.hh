// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_CG_TO_DG_PROLONGATION_HH
#define DUNE_PDELAB_CG_TO_DG_PROLONGATION_HH

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>
#include<dune/common/dynmatrix.hh>

#include<dune/geometry/type.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/quadraturerules.hh>

#include<dune/localfunctions/common/interfaceswitch.hh>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>

namespace Dune {
  namespace PDELab {

    //! \addtogroup LocalOperator
    //! \ingroup PDELab
    //! \{

    /** a local operator to assemble the CG to DG prolongation matrix
     *
     */
    class CG_to_DG_Prolongation : public NumericalJacobianApplyVolume<CG_to_DG_Prolongation>,
                                  public FullVolumePattern,
                                  public LocalOperatorDefaultFlags,
                                  public InstationaryLocalOperatorDefaultMethods<double>
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };

      CG_to_DG_Prolongation (int intorder_=2)
      {}

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // Method needs to be there to be able to call jacobian
      }

      // jacobian of volume term
      template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
      void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                            M & mat) const
      {
        // Switches between local and global interface
        typedef FiniteElementInterfaceSwitch<
          typename LFSU::Traits::FiniteElementType
          > FESwitchU;
        typedef FiniteElementInterfaceSwitch<
          typename LFSV::Traits::FiniteElementType
          > FESwitchV;
        typedef BasisInterfaceSwitch<
          typename FESwitchU::Basis
          > BasisSwitch;

        // domain and range field type
        typedef typename BasisSwitch::DomainField DF;
        typedef typename BasisSwitch::RangeField RF;
        typedef typename BasisSwitch::Range RangeType;
        typedef typename LFSU::Traits::SizeType size_type;

        // dimensions
        const int dim = EG::Geometry::dimension;

        // select quadrature rule
        Dune::GeometryType gt = eg.geometry().type();
        int intorder = 2+2*std::max(lfsu.finiteElement().localBasis().order(),lfsv.finiteElement().localBasis().order());

        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,intorder);

        // matrices
        //std::cout << "n_DG=" << n_DG << " n_CG=" << n_CG << std::endl;
        size_type n_DG = lfsv.size();
        size_type n_CG = lfsu.size();
        typedef Dune::DynamicMatrix<RF> MATRIX;
        MATRIX MDG(n_DG,n_DG,0.0); // DG mass matrix in element
        MATRIX B(n_DG,n_CG,0.0); // right hand side matrix

        // loop over quadrature points
        std::vector<RangeType> phi(n_DG); // values of DG basis functions
        std::vector<RangeType> psi(n_CG); // values of CG basis functions

        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            // evaluate basis functions
            FESwitchU::basis(lfsu.finiteElement()).evaluateFunction(it->position(),psi); // eval CG basis ("Ansatz functions")
            FESwitchV::basis(lfsv.finiteElement()).evaluateFunction(it->position(),phi); // eval DG basis ("Test functions")

            RF factor = it->weight() * eg.geometry().integrationElement(it->position());

            // accumulate to local mass matrix
            for (size_type i=0; i<n_DG; i++)
              for (size_type j=0; j<n_DG; j++)
                MDG[i][j] += phi[j]*phi[i]*factor;

            // accumulate to rhs matrix
            for (size_type i=0; i<n_DG; i++)
              for (size_type j=0; j<n_CG; j++)
                B[i][j] += psi[j]*phi[i]*factor;
          }

        // invert mass matrix
        MDG.invert();

        // compute B = MDG^-1 B
        B.leftmultiply(MDG);

        // write result
        for (size_type i=0; i<n_DG; i++)
          for (size_type j=0; j<n_CG; j++)
            mat.accumulate(lfsv,i,lfsu,j, B[i][j] );
      }
    };



    /** a local operator to assemble the CG to DG prolongation matrix for a system with 2 components
     *
     */
    class CG_to_DG_Prolongation_S2 : public NumericalJacobianApplyVolume<CG_to_DG_Prolongation_S2>,
                                     public FullVolumePattern,
                                     public LocalOperatorDefaultFlags,
                                     public InstationaryLocalOperatorDefaultMethods<double>
    {
    public:
      // pattern assembly flags
      enum { doPatternVolume = true };

      // residual assembly flags
      enum { doAlphaVolume = true };

      CG_to_DG_Prolongation_S2 ()
      {}

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // Method needs to be there to be able to call jacobian
      }

      // jacobian of volume term
      template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
      void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                            M & mat) const
      {
        // assert function space structure
        dune_static_assert((LFSU::CHILDREN == 2), "You seem to use the wrong trial function space for CG_to_DG_Prolongation_S2");
        dune_static_assert((LFSV::CHILDREN == 2), "You seem to use the wrong test function space for CG_to_DG_Prolongation_S2");

        // domain and range field type
        typedef FiniteElementInterfaceSwitch<typename LFSU::template Child<0>::Type::Traits::FiniteElementType> FESwitchU;
        typedef BasisInterfaceSwitch<typename FESwitchU::Basis> BasisSwitch;
        typedef typename BasisSwitch::DomainField DF;
        typedef typename BasisSwitch::RangeField RF;
        typedef typename BasisSwitch::Range RangeType;
        typedef typename LFSU::Traits::SizeType size_type;

        // dimensions and element type
        const int dim = EG::Geometry::dimension;
        Dune::GeometryType gt = eg.geometry().type();

        //============
        // component 0
        //============

        // get child types
        typedef typename LFSV::template Child<0>::Type LFSV0;
        const LFSV0& lfsv0 = lfsv.template getChild<0>();
        typedef typename LFSU::template Child<0>::Type LFSU0;
        const LFSU0& lfsu0 = lfsu.template getChild<0>();

        // Switches between local and global interface
        typedef FiniteElementInterfaceSwitch<typename LFSU0::Traits::FiniteElementType> FESwitchU0;
        typedef FiniteElementInterfaceSwitch<typename LFSV0::Traits::FiniteElementType> FESwitchV0;

        // select quadrature rule
        int intorder0 = 2+2*std::max(lfsu0.finiteElement().localBasis().order(),lfsv0.finiteElement().localBasis().order());
        const Dune::QuadratureRule<DF,dim>& rule0 = Dune::QuadratureRules<DF,dim>::rule(gt,intorder0);

        // matrices
        //std::cout << "n_DG=" << n_DG << " n_CG=" << n_CG << std::endl;
        size_type n_DG0 = lfsv0.size();
        size_type n_CG0 = lfsu0.size();
        typedef Dune::DynamicMatrix<RF> MATRIX0;
        MATRIX0 MDG0(n_DG0,n_DG0,0.0); // DG mass matrix in element
        MATRIX0 B0(n_DG0,n_CG0,0.0); // right hand side matrix

        // loop over quadrature points
        std::vector<RangeType> phi0(n_DG0); // values of DG basis functions
        std::vector<RangeType> psi0(n_CG0); // values of CG basis functions

        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule0.begin(); it!=rule0.end(); ++it)
          {
            // evaluate basis functions
            FESwitchU0::basis(lfsu0.finiteElement()).evaluateFunction(it->position(),psi0); // eval CG basis ("Ansatz functions")
            FESwitchV0::basis(lfsv0.finiteElement()).evaluateFunction(it->position(),phi0); // eval DG basis ("Test functions")

            RF factor = it->weight() * eg.geometry().integrationElement(it->position());

            // accumulate to local mass matrix
            for (size_type i=0; i<n_DG0; i++)
              for (size_type j=0; j<n_DG0; j++)
                MDG0[i][j] += phi0[j]*phi0[i]*factor;

            // accumulate to rhs matrix
            for (size_type i=0; i<n_DG0; i++)
              for (size_type j=0; j<n_CG0; j++)
                B0[i][j] += psi0[j]*phi0[i]*factor;
          }

        // invert mass matrix
        MDG0.invert();

        // compute B = MDG^-1 B
        B0.leftmultiply(MDG0);

        // write result
        for (size_type i=0; i<n_DG0; i++)
          for (size_type j=0; j<n_CG0; j++)
            mat.accumulate(lfsv0,i,lfsu0,j, B0[i][j] );

        //============
        // component 1
        //============

        // get child types
        typedef typename LFSV::template Child<1>::Type LFSV1;
        const LFSV1& lfsv1 = lfsv.template getChild<1>();
        typedef typename LFSU::template Child<1>::Type LFSU1;
        const LFSU1& lfsu1 = lfsu.template getChild<1>();

        // Switches between local and global interface
        typedef FiniteElementInterfaceSwitch<typename LFSU1::Traits::FiniteElementType> FESwitchU1;
        typedef FiniteElementInterfaceSwitch<typename LFSV1::Traits::FiniteElementType> FESwitchV1;

        // select quadrature rule
        int intorder1 = 2+2*std::max(lfsu1.finiteElement().localBasis().order(),lfsv1.finiteElement().localBasis().order());
        const Dune::QuadratureRule<DF,dim>& rule1 = Dune::QuadratureRules<DF,dim>::rule(gt,intorder1);

        // matrices
        //std::cout << "n_DG=" << n_DG << " n_CG=" << n_CG << std::endl;
        size_type n_DG1 = lfsv1.size();
        size_type n_CG1 = lfsu1.size();
        typedef Dune::DynamicMatrix<RF> MATRIX1;
        MATRIX1 MDG1(n_DG1,n_DG1,0.0); // DG mass matrix in element
        MATRIX1 B1(n_DG1,n_CG1,0.0); // right hand side matrix

        // loop over quadrature points
        std::vector<RangeType> phi1(n_DG1); // values of DG basis functions
        std::vector<RangeType> psi1(n_CG1); // values of CG basis functions

        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule1.begin(); it!=rule1.end(); ++it)
          {
            // evaluate basis functions
            FESwitchU1::basis(lfsu1.finiteElement()).evaluateFunction(it->position(),psi1); // eval CG basis ("Ansatz functions")
            FESwitchV1::basis(lfsv1.finiteElement()).evaluateFunction(it->position(),phi1); // eval DG basis ("Test functions")

            RF factor = it->weight() * eg.geometry().integrationElement(it->position());

            // accumulate to local mass matrix
            for (size_type i=0; i<n_DG1; i++)
              for (size_type j=0; j<n_DG1; j++)
                MDG1[i][j] += phi1[j]*phi1[i]*factor;

            // accumulate to rhs matrix
            for (size_type i=0; i<n_DG1; i++)
              for (size_type j=0; j<n_CG1; j++)
                B1[i][j] += psi1[j]*phi1[i]*factor;
          }

        // invert mass matrix
        MDG1.invert();

        // compute B = MDG^-1 B
        B1.leftmultiply(MDG1);

        // write result
        for (size_type i=0; i<n_DG1; i++)
          for (size_type j=0; j<n_CG1; j++)
            mat.accumulate(lfsv1,i,lfsu1,j, B1[i][j] );

      }
    };

    //! \} group LocalOperator
  } // namespace PDELab
} // namespace Dune

#endif
