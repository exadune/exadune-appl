#ifndef __SUMFACTORIZATION__
#define __SUMFACTORIZATION__

#include<assert.h>

#include<dune/common/timer.hh>

#include"alignedmatvec.hh"
#include"alignedmatvecops.hh"

/** compute linear map of tensors using sum factorization method

    computes linear map of a d-dimensional tensor of the form

    y_[id,..,i1] = sum_jd=0^n .. sum_jd1=0^n A^(d)_[id][jd]*...*A^(1)_[i1][j1] x_[jd,..,j1]

    - In this implementation all matrices A^(k) of size m x n are identical
    - therefore x has size n^d
    - and y has size m^d
*/
template<size_t d, size_t m, size_t n, typename T, size_t alignment>
class SumFactorizer
{
};

template<size_t d, size_t m, size_t n, typename T, size_t alignment>
class SumFactorizerBase
{
public:
  //! count number of operations in sum factorization
  size_t count ()
  {
    size_t N=1; for (size_t i=0; i<d; i++) N *= n;
    size_t M=1; for (size_t i=0; i<d; i++) M *= m;
    size_t ops=0;

    size_t R = n;   // row size in H
    size_t C = N/n; // column size in H

    // for all dimensions in turn
    for (size_t k=d; k>0; k--) {
      // define block size in column direction
      size_t B = C/n;
      if (k==1) B = C/m;

      size_t Rout = n;
      if (k==1) Rout = m;
      size_t Cout = B*m;

      // the matrix multiplication with rotated storage of result
      ops += m*C*n*2;

      // prepare next stage; swap pointers
      R = Rout;
      C = Cout;
    }
    return ops;
  }
};


template<size_t m, size_t n, typename T, size_t alignment>
class SumFactorizer<3,m,n,T,alignment> : public SumFactorizerBase<3,m,n,T,alignment>
{
  enum {d=3};
  enum { N = Dune::StaticPower<n,d>::power };
  enum { M = Dune::StaticPower<m,d>::power };
  typedef AlignedMatrix<T,alignment,1> AMatrix;
  typedef AlignedVector<T,alignment> AVector;

public:
  //! make Factorizer object
  SumFactorizer ()
  {
    buffer1 = (T*) aligned_malloc(std::max(static_cast<size_t>(M),static_cast<size_t>(N))*sizeof(T),alignment);
    if (buffer1==0) throw std::bad_alloc();
    buffer2 = (T*) aligned_malloc(std::max(static_cast<size_t>(M),static_cast<size_t>(N))*sizeof(T),alignment);
    if (buffer2==0) throw std::bad_alloc();
  }

  //! free internal memory
  ~SumFactorizer ()
  {
    aligned_free(buffer2);
    aligned_free(buffer1);
  }

  //! tensor multiplication
  void multiply (const AMatrix& A, // the matrix A=A^(1)=..=A^(d)
                 const AVector& x,   // x in lexicographic order with jd fastest
                 AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A.rows()==m);
    assert(A.cols()==n);
    assert(x.size()==N);
    assert(y.size()==M);
    // std::cout << "A.rows=" << A.rows() << " should be " << m << std::endl;
    // std::cout << "A.cols=" << A.cols() << " should be " << n << std::endl;
    // std::cout << "x.size=" << x.size() << " should be " << N << std::endl;
    // std::cout << "y.size=" << y.size() << " should be " << M << std::endl;

    // dimension 1
    matmul<m,n,n*n,T,alignment>(A.data(),x.data(),buffer2);
    rotate<m,n*n,n,T,alignment>(buffer2,buffer1);

    // dimension 2
    matmul<m,n,m*n,T,alignment>(A.data(),buffer1,buffer2);
    rotate<m,m*n,n,T,alignment>(buffer2,buffer1);

    // dimension 3
    matmul<m,n,m*m,T,alignment>(A.data(),buffer1,buffer2);
    rotate<m,m*m,m,T,alignment>(buffer2,y.data());
  }

  //! tensor multiplication
  void multiply (const AMatrix& A, // the matrix A=A^(i); i!=k
                 const AMatrix& Ak,// the matrix A=A^(k)
                 size_t k,                              // the selected dimension with another matrix
                 const AVector& x,   // x in lexicographic order with jd fastest
                 AVector& y) const   // output y in lexicographic order with id fastest
  {
    // assertions
    assert(A.rows()==m);
    assert(A.cols()==n);
    assert(Ak.rows()==m);
    assert(Ak.cols()==n);
    assert(x.size()==N);
    assert(y.size()==M);

    // dimension 1
    if (k==0)
      matmul<m,n,n*n,T,alignment>(Ak.data(),x.data(),buffer2);
    else
      matmul<m,n,n*n,T,alignment>(A.data(),x.data(),buffer2);
    rotate<m,n*n,n,T,alignment>(buffer2,buffer1);

    // dimension 2
    if (k==1)
      matmul<m,n,n*n,T,alignment>(Ak.data(),buffer1,buffer2);
    else
      matmul<m,n,m*n,T,alignment>(A.data(),buffer1,buffer2);
    rotate<m,m*n,n,T,alignment>(buffer2,buffer1);

    // dimension 3
    if (k==2)
      matmul<m,n,n*n,T,alignment>(Ak.data(),buffer1,buffer2);
    else
      matmul<m,n,m*m,T,alignment>(A.data(),buffer1,buffer2);
    rotate<m,m*m,m,T,alignment>(buffer2,y.data());
  }

  /**  tensor multiplication -- face version

       direction A[0] has size 1 x n
       all other directions have size m x n
  */
  void multiply_face (const std::vector<const AMatrix*>& A, // the matrices A^(1), ..., A^(d)
                      const AVector& x,   // x in lexicographic order with jd fastest
                      AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A[0]->rows()==1);
    assert(A[0]->cols()==n);
    assert(A[1]->rows()==m);
    assert(A[1]->cols()==n);
    assert(A[2]->rows()==m);
    assert(A[2]->cols()==n);
    assert(x.size()==N);
    assert(y.size()==m*m);

    // dimension 1
    matmul<1,n,n*n,T,alignment>(A[0]->data(),x.data(),buffer2);
    rotate<1,n*n,n,T,alignment>(buffer2,buffer1);

    // dimension 2
    matmul<m,n,n,T,alignment>(A[1]->data(),buffer1,buffer2);
    rotate<m,n,n,T,alignment>(buffer2,buffer1);

    // dimension 3
    matmul<m,n,m,T,alignment>(A[2]->data(),buffer1,buffer2);
    rotate<m,m,m,T,alignment>(buffer2,y.data());
  }

  /**  tensor multiplication -- face version

       direction A[dim-1] has size m x 1
       all other directions have size m x n
  */
  void multiply_faceT (const std::vector<const AMatrix*>& A, // the matrices A^(1), ..., A^(d)
                       const AVector& x,   // x in lexicographic order with jd fastest
                       AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A[0]->rows()==m);
    assert(A[0]->cols()==n);
    assert(A[1]->rows()==m);
    assert(A[1]->cols()==n);
    assert(A[2]->rows()==m);
    assert(A[2]->cols()==1);
    assert(x.size()==n*n);
    assert(y.size()==M);

    // dimension 0
    matmul<m,n,n,T,alignment>(A[0]->data(),x.data(),buffer2);
    rotate<m,n,n,T,alignment>(buffer2,buffer1);
    //transpose<m,n,T,alignment>(buffer2,buffer1);

    // dimension 2
    matmul<m,n,m,T,alignment>(A[1]->data(),buffer1,buffer2);
    rotate<m,m,1,T,alignment>(buffer2,buffer1);
    //transpose<m,m,T,alignment>(buffer2,buffer1);

    // dimension 3
    matmul<m,1,m*m,T,alignment>(A[2]->data(),buffer1,buffer2);
    rotate<m,m*m,m,T,alignment>(buffer2,y.data());
  }

private:
  mutable T *buffer1;
  mutable T *buffer2;
};


template<size_t m, size_t n, typename T, size_t alignment>
class SumFactorizer<2,m,n,T,alignment> : public SumFactorizerBase<2,m,n,T,alignment>
{
  enum {d=2};
  enum { N = Dune::StaticPower<n,d>::power };
  enum { M = Dune::StaticPower<m,d>::power };
  typedef AlignedMatrix<T,alignment,1> AMatrix;
  typedef AlignedVector<T,alignment> AVector;

public:
  //! make Factorizer object
  SumFactorizer ()
  {
    buffer1 = (T*) aligned_malloc(std::max(static_cast<size_t>(M),static_cast<size_t>(N))*sizeof(T),alignment);
    if (buffer1==0) throw std::bad_alloc();
    buffer2 = (T*) aligned_malloc(std::max(static_cast<size_t>(M),static_cast<size_t>(N))*sizeof(T),alignment);
    if (buffer2==0) throw std::bad_alloc();
  }

  //! free internal memory
  ~SumFactorizer ()
  {
    aligned_free(buffer2);
    aligned_free(buffer1);
  }

  //! tensor multiplication
  void multiply (const AMatrix& A, // the matrix A=A^(1)=..=A^(d)
                 const AVector& x,   // x in lexicographic order with jd fastest
                 AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A.rows()==m);
    assert(A.cols()==n);
    assert(x.size()==N);
    assert(y.size()==M);

    // dimension 1
    matmul<m,n,n,T,alignment>(A.data(),x.data(),buffer2);
    transpose<m,n,T,alignment>(buffer2,buffer1);

    // dimension 2
    matmul<m,n,m,T,alignment>(A.data(),buffer1,buffer2);
    transpose<m,m,T,alignment>(buffer2,y.data());
  }

  //! tensor multiplication
  void multiply (const AMatrix& A, // the matrix A=A^(i); i!=k
                 const AMatrix& Ak,// the matrix A=A^(k)
                 size_t k,                              // the selected dimension with another matrix
                 const AVector& x,   // x in lexicographic order with jd fastest
                 AVector& y) const   // output y in lexicographic order with id fastest
  {
    // assertions
    assert(A.rows()==m);
    assert(A.cols()==n);
    assert(Ak.rows()==m);
    assert(Ak.cols()==n);
    assert(x.size()==N);
    assert(y.size()==M);

    // dimension 1
    if (k==0)
      matmul<m,n,n,T,alignment>(Ak.data(),x.data(),buffer2);
    else
      matmul<m,n,n,T,alignment>(A.data(),x.data(),buffer2);
    transpose<m,n,T,alignment>(buffer2,buffer1);

    // dimension 2
    if (k==1)
      matmul<m,n,m,T,alignment>(Ak.data(),buffer1,buffer2);
    else
      matmul<m,n,m,T,alignment>(A.data(),buffer1,buffer2);
    transpose<m,m,T,alignment>(buffer2,y.data());
  }

  /**  tensor multiplication -- face version

       direction A[0] has size 1 x n
       all other directions have size m x n
  */
  void multiply_face (const std::vector<const AMatrix*>& A, // the matrices A^(1), ..., A^(d)
                      const AVector& x,   // x in lexicographic order with jd fastest
                      AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A[0]->rows()==1);
    assert(A[0]->cols()==n);
    assert(A[1]->rows()==m);
    assert(A[1]->cols()==n);
    assert(x.size()==N);
    assert(y.size()==m);

    // dimension 1
    matmul<1,n,n,T,alignment>(A[0]->data(),x.data(),buffer2);
    transpose<1,n,T,alignment>(buffer2,buffer1);

    // dimension 2
    matmul<m,n,1,T,alignment>(A[1]->data(),buffer1,buffer2);
    transpose<m,1,T,alignment>(buffer2,y.data());
  }


  /**  tensor multiplication -- face version

       direction A[0] has size m x 1
       all other directions have size m x n
  */
  void multiply_faceT (const std::vector<const AMatrix*>& A, // the matrices A^(1), ..., A^(d)
                       const AVector& x,   // x in lexicographic order with jd fastest
                       AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A[0]->rows()==m);
    assert(A[0]->cols()==n);
    assert(A[1]->rows()==m);
    assert(A[1]->cols()==1);
    assert(x.size()==n);
    assert(y.size()==M);

    // dimension 1
    matmul<m,n,1,T,alignment>(A[0]->data(),x.data(),buffer2);
    transpose<m,1,T,alignment>(buffer2,buffer1);

    // dimension 2
    matmul<m,1,m,T,alignment>(A[1]->data(),buffer1,buffer2);
    transpose<m,m,T,alignment>(buffer2,y.data());
  }

private:
  mutable T *buffer1;
  mutable T *buffer2;
};


template<size_t m, size_t n, typename T, size_t alignment>
class SumFactorizer<1,m,n,T,alignment>
{
  enum {d=1};
  typedef AlignedMatrix<T,alignment,1> AMatrix;
  typedef AlignedVector<T,alignment> AVector;

public:
  //! tensor multiplication
  void multiply (const AMatrix& A, // the matrix A=A^(1)=..=A^(d)
                 const AVector& x,   // x in lexicographic order with jd fastest
                 AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A.rows()==m);
    assert(A.cols()==n);
    assert(x.size()==n);
    assert(y.size()==m);

    for (size_t i1=0; i1<m; i1++) {
      double s=0.0;
      for (size_t j1=0; j1<n; j1++)
        s += A[j1*m+i1]*x[j1];
      y[i1] = s;
    }
  }

  void multiply (const AMatrix& A, // the matrix A=A^(i); i!=k
                 const AMatrix& Ak,// the matrix A=A^(k)
                 size_t k,                              // the selected dimension with another matrix
                 const AVector& x,   // x in lexicographic order with jd fastest
                 AVector& y) const   // output y in lexicographic order with id fastest
  {
    // assertions
    assert(Ak.rows()==m);
    assert(Ak.cols()==n);
    assert(x.size()==n);
    assert(y.size()==m);
    assert(k=0);

    for (size_t i1=0; i1<m; i1++) {
      double s=0.0;
      for (size_t j1=0; j1<n; j1++)
        s += Ak[j1*m+i1]*x[j1];
      y[i1] = s;
    }
  }

  /**  tensor multiplication -- face version

       direction A[0] has size 1 x n
       all other directions have size m x n
  */
  void multiply_face (const std::vector<const AMatrix*>& A, // the matrices A^(1), ..., A^(d)
                      const AVector& x,   // x in lexicographic order with jd fastest
                      AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A[0]->rows()==1);
    assert(A[0]->cols()==n);
    assert(x.size()==n);
    assert(y.size()==1);

    double s=0.0;
    for (size_t j1=0; j1<n; j1++)
      s += A[0]->operator[](j1)*x[j1];
    y[0] = s;
  }

  /**  tensor multiplication -- face version

       direction A[0] has size m x 1
       all other directions have size m x n
  */
  void multiply_faceT (const std::vector<const AMatrix*>& A, // the matrices A^(1), ..., A^(d)
                       const AVector& x,   // x in lexicographic order with jd fastest
                       AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    assert(A[0]->rows()==m);
    assert(A[0]->cols()==1);
    assert(x.size()==1);
    assert(y.size()==m);

    double s=0.0;
    for (size_t i1=0; i1<n; i1++)
      y[i1] = A[0]->operator[](i1)*x[0];
  }

  //! count number of operations in sum factorization
  size_t count () const
  {
    return m*n*2;
  }
};


/** compute linear map of tensors using naive method

    computes linear map of a d-dimensional tensor of the form

    y_[i1,..,id] = sum_j1=0^n .. sum_jd=0^n A^(1)_[i1][j1]...A^(d)_[id][jd] x_[j1,..,jd]

    - In this implementation all matrices A^(k) of size ma x na are identical
    - therefore x has size na^d
    - and y has size ma^d
*/
template<size_t d, typename T, size_t alignment>
class TensorLinearMapping
{
  typedef AlignedMatrix<T,alignment,1> AMatrix;
  typedef AlignedVector<T,alignment> AVector;

public:

  //! tensor multiplication
  void multiply (const AMatrix& A, // the matrix A=A^(1)=..=A^(d) column major
                 const AVector& x,   // x in lexicographic order with jd fastest
                 AVector& y) const   // output y in lexicographic order with jd fastest
  {
    // assertions
    size_t m = A.rows();
    size_t n = A.cols();
    size_t N=1; for (size_t i=0; i<d; i++) N *= n;
    assert(x.size()==N);
    size_t M=1; for (size_t i=0; i<d; i++) M *= m;
    assert(y.size()==M);
    assert(d>=1 && d<=3);

    if (d==3)
      {
        for (size_t i1=0; i1<m; i1++)
          for (size_t i2=0; i2<m; i2++)
            for (size_t i3=0; i3<m; i3++) {
              double s=0.0;
              for (size_t j1=0; j1<n; j1++)
                for (size_t j2=0; j2<n; j2++)
                  for (size_t j3=0; j3<n; j3++)
                    s += A[j1*m+i1]*A[j2*m+i2]*A[j3*m+i3]*x[(j1*n+j2)*n+j3];
              y[(i1*m+i2)*m+i3] = s;
            }
        return;
      }
    if (d==2)
      {
        for (size_t i1=0; i1<m; i1++)
          for (size_t i2=0; i2<m; i2++) {
            double s=0.0;
            for (size_t j1=0; j1<n; j1++)
              for (size_t j2=0; j2<n; j2++)
                s += A[j1*m+i1]*A[j2*m+i2]*x[j1*n+j2];
            y[i1*m+i2] = s;
          }
        return;
      }
    if (d==1)
      {
        for (size_t i1=0; i1<m; i1++) {
          double s=0.0;
          for (size_t j1=0; j1<n; j1++)
            s += A[j1*m+i1]*x[j1];
          y[i1] = s;
        }
        return;
      }
  }

  //! count number of operations in sum factorization
  size_t count (const AMatrix& A) const
  {
    size_t m = A.rows();
    size_t n = A.cols();
    size_t N=1; for (size_t i=0; i<d; i++) N *= n;
    size_t M=1; for (size_t i=0; i<d; i++) M *= m;
    return M*N*(d+1);
  }
};


/** Run sum factorization an measure flop rate
 */
template<size_t d, size_t p, size_t q, typename T, size_t alignment>
class TestFlopsSumFact
{
public:
  void run (size_t iter)
  {
    const size_t n=p+1;
    const size_t m=q;

    size_t N=1; for (size_t i=0; i<d; i++) N *= n;
    size_t M=1; for (size_t i=0; i<d; i++) M *= m;
    double h = 1.0/((double)(m+1)); // use as "closed formula"

    AlignedMatrix<T,alignment> A(m,n); // row major matrix with A_ij = lagrange(j,xi)
    for (size_t i=0; i<m; i++)
      for (size_t j=0; j<n; j++)
        A.colmajoraccess(i,j) = lagrange(p,j,(i+1)*h);

    AlignedVector<T,alignment> x(N);   // the degrees of freedom
    for (size_t i=0; i<x.size(); i++) x[i] = 0.3333*i;

    AlignedVector<T,alignment> y(M);   // values of basis functions at quadrature points

    SumFactorizer<d,m,n,T,alignment> f;

    Dune::Timer timer;
    timer.reset();

    y[0] = 3.14;
    for (int it=0; it<iter; it++)
      {
        x[0] = y[0];
        f.multiply(A,x,y);
      }

    double scnds = timer.elapsed();
    double ops=(double)f.count();

    std::cout << std::setw(2) << p << " & "
              << std::setw(2) << q << " & "
              << std::setw(3) << std::fixed << std::noshowpoint << std::setprecision(0) << ops/N << " & "
              << std::setw(10) << std::scientific << std::showpoint << std::setprecision(3) << scnds/iter << " & "
              << std::setw(8) << std::fixed << std::showpoint << std::setprecision(3) << iter*ops/scnds/1e9 <<  "\\\\ %"
              << std::setw(9) << std::scientific << std::showpoint << std::setprecision(1) << y[0]
              << std::endl;
    // std::cout << "iterations=" << iter
    //         << " total ops=" <<  iter*ops
    //         << " seconds=" << scnds
    //         <<std::endl;
  }

private:
  // 1D Lagrange polynomials at equidistant knots
  T lagrange (int n, int i, T x)
  {
    T h = 1.0/((T)n);
    T value=1.0;
    for (int j=0; j<=n; j++)
      if (j!=i)
        value *= (x-j*h)/(i*h-j*h);
    return value;
  }
};


/** Test sum factorization implementation

    - interpolate a given function F by Lagrange polynomials of degree p
    - evaluate at q equidistant quadrature points
*/
template<size_t d, size_t p, size_t q, typename T, size_t alignment>
class TestFunction
{
public:

  template<typename F>
  bool run (F f, int verbose=1)
  {
    assert(d>=2 && d<=3);

    const size_t n=p+1;
    const size_t m=q;

    size_t N=1; for (size_t i=0; i<d; i++) N *= n;
    size_t M=1; for (size_t i=0; i<d; i++) M *= m;
    double hm = 1.0/((double)(m+1)); // use as "closed formula"

    // initialize column major matrix with A_ij = lagrange(j,xi)
    AlignedMatrix<T,alignment> A(m,n,false);
    for (size_t i=0; i<m; i++)
      for (size_t j=0; j<n; j++)
        A.colmajoraccess(i,j) = lagrange(p,j,(i+1)*hm);
    if (verbose>=2)
      {
        std::cout << "A_ij = lagrange(j,xi)" << std::endl;
        std::cout << std::setw(3) << "A" ;
        for (size_t j=0; j<n; j++)
          std::cout << std::setw(10) << j;
        std::cout << std::endl;
        for (size_t i=0; i<m; i++)
          {
            std::cout << std::setw(3) << i ;
            for (size_t j=0; j<n; j++)
              std::cout << std::setw(10) << std::scientific << std::showpoint << std::setprecision(2) << A.colmajoraccess(i,j);
            std::cout << std::endl;
          }
        std::cout << std::endl;
      }

    // initialize x with values of f at Gauss points
    AlignedVector<T,alignment> x(N,false);   // the degrees of freedom
    if (d==3)
      {
        T hn = 1.0/((T)(n-1));
        T X[3];
        for (size_t j0=0; j0<n; j0++)
          for (size_t j1=0; j1<n; j1++)
            for (size_t j2=0; j2<n; j2++)
              {
                X[0] = j0*hn;
                X[1] = j1*hn;
                X[2] = j2*hn;
                size_t J = (j0*n+j1)*n+j2;
                x[J] = f(X);
              }
      }
    if (d==2)
      {
        T hn = 1.0/((T)(n-1));
        T X[2];
        for (size_t j0=0; j0<n; j0++)
          for (size_t j1=0; j1<n; j1++)
            {
              X[0] = j0*hn;
              X[1] = j1*hn;
              size_t J = j0*n+j1;
              x[J] = f(X);
            }
      }
    if (verbose>=2)
      {
        std::cout << "x_j1j2j3 = f(...)" << std::endl;
        if (d==3)
          {
            T hn = 1.0/((T)(n-1));
            T X[3];
            for (size_t j0=0; j0<n; j0++)
              for (size_t j1=0; j1<n; j1++)
                for (size_t j2=0; j2<n; j2++)
                  {
                    X[0] = j0*hn;
                    X[1] = j1*hn;
                    X[2] = j2*hn;
                    size_t J = (j0*n+j1)*n+j2;
                    std::cout << std::setw(3) << j0
                              << std::setw(3) << j1
                              << std::setw(3) << j2
                              << std::setw(10) << std::scientific << std::showpoint << std::setprecision(2) << X[0]
                              << std::setw(10) << std::scientific << std::showpoint << std::setprecision(2) << X[1]
                              << std::setw(10) << std::scientific << std::showpoint << std::setprecision(2) << X[2]
                              << std::setw(5) << J
                              << std::setw(16) << std::scientific << std::showpoint << std::setprecision(8) << x[J]
                              << std::endl;
                  }
            std::cout << std::endl;
          }
        if (d==2)
          {
            T hn = 1.0/((T)(n-1));
            T X[2];
            for (size_t j0=0; j0<n; j0++)
              for (size_t j1=0; j1<n; j1++)
                {
                  X[0] = j0*hn;
                  X[1] = j1*hn;
                  size_t J = j0*n+j1;
                  std::cout << std::setw(3) << j0
                            << std::setw(3) << j1
                            << std::setw(10) << std::scientific << std::showpoint << std::setprecision(2) << X[0]
                            << std::setw(10) << std::scientific << std::showpoint << std::setprecision(2) << X[1]
                            << std::setw(5) << J
                            << std::setw(16) << std::scientific << std::showpoint << std::setprecision(8) << x[J]
                            << std::endl;
                }
            std::cout << std::endl;
          }
      }

    // output : values of interpolated f at Gauss points
    AlignedVector<T,alignment> y_exact(M,false);
    AlignedVector<T,alignment> y_naive(M,false);
    AlignedVector<T,alignment> y_sumfact(M,false);
    if (d==3)
      {
        T Y[3];
        for (size_t i0=0; i0<m; i0++)
          for (size_t i1=0; i1<m; i1++)
            for (size_t i2=0; i2<m; i2++)
              {
                Y[0] = (i0+1)*hm;
                Y[1] = (i1+1)*hm;
                Y[2] = (i2+1)*hm;
                size_t I = (i0*m+i1)*m+i2;
                y_exact[I] = f(Y);
              }
      }
    if (d==2)
      {
        T Y[2];
        for (size_t i0=0; i0<m; i0++)
          for (size_t i1=0; i1<m; i1++)
            {
              Y[0] = (i0+1)*hm;
              Y[1] = (i1+1)*hm;
              size_t I = i0*m+i1;
              y_exact[I] = f(Y);
            }
      }

    SumFactorizer<d,m,n,T,alignment> map_sumfact;
    TensorLinearMapping<d,T,alignment> map_naive;

    map_sumfact.multiply(A,x,y_sumfact);
    map_naive.multiply(A,x,y_naive);

    // compare all results with exact values
    T error_naive=0, error_sumfact=0;
    for (size_t i=0; i<M; i++)
      {
        error_naive += (y_exact[i]-y_naive[i])*(y_exact[i]-y_naive[i]);
        error_sumfact += (y_exact[i]-y_sumfact[i])*(y_exact[i]-y_sumfact[i]);
      }
    if (verbose>=2)
      {
        std::cout << "compare exact, naive, sumfact" << std::endl;
        if (d==3)
          {
            for (size_t i0=0; i0<m; i0++)
              for (size_t i1=0; i1<m; i1++)
                for (size_t i2=0; i2<m; i2++)
                  {
                    size_t I = (i0*m+i1)*m+i2;
                    std::cout << std::setw(3) << i0
                              << std::setw(3) << i1
                              << std::setw(3) << i2
                              << std::setw(5) << I
                              << std::setw(16) << std::scientific << std::showpoint << std::setprecision(8) << y_exact[I]
                              << std::setw(16) << std::scientific << std::showpoint << std::setprecision(8) << y_naive[I]
                              << std::setw(16) << std::scientific << std::showpoint << std::setprecision(8) << y_sumfact[I]
                              << std::endl;
                  }
          }
        if (d==2)
          {
            for (size_t i0=0; i0<m; i0++)
              for (size_t i1=0; i1<m; i1++)
                {
                  size_t I = i0*m+i1;
                  std::cout << std::setw(3) << i0
                            << std::setw(3) << i1
                            << std::setw(5) << I
                            << std::setw(16) << std::scientific << std::showpoint << std::setprecision(8) << y_exact[I]
                            << std::setw(16) << std::scientific << std::showpoint << std::setprecision(8) << y_naive[I]
                            << std::setw(16) << std::scientific << std::showpoint << std::setprecision(8) << y_sumfact[I]
                            << std::endl;
                }
          }
        std::cout << std::endl;
      }
    if (verbose>=1)
      {
        std::cout << "d=" << d << " p=" << p << " q=" << q << std::endl;
        std::cout << "  error naive   "
                  << std::setw(25) << std::scientific << std::showpoint << std::setprecision(15) << std::sqrt(error_naive)
                  << std::endl;
        std::cout << "  error sumfact "
                  << std::setw(25) << std::scientific << std::showpoint << std::setprecision(15) << std::sqrt(error_sumfact)
                  << std::endl;
      }
    // I don't think this is used, but GCC complained about missing return statement
    return true;
  }

private:
  /** 1D Lagrange polynomials at equidistant knots

      - n: polynomial degree
      - i: 0<=i<=n
      - x: evaluation point
  */
  T lagrange (int n, int i, T x)
  {
    T h = 1.0/((T)n);
    T value=1.0;
    for (int j=0; j<=n; j++)
      if (j!=i)
        value *= (x-j*h)/(i*h-j*h);
    return value;
  }
};

/** Run sum factorization an measure flop rate
 */
template<size_t d, size_t p, size_t q, typename T, size_t alignment>
class TestFlopsNaive
{
public:
  void run (size_t iter)
  {
    const size_t n=p+1;
    const size_t m=q;

    size_t N=1; for (size_t i=0; i<d; i++) N *= n;
    size_t M=1; for (size_t i=0; i<d; i++) M *= m;
    double h = 1.0/((double)(m+1)); // use as "closed formula"

    AlignedMatrix<T,alignment> A(m,n); // row major matrix with A_ij = lagrange(j,xi)
    for (size_t i=0; i<m; i++)
      for (size_t j=0; j<n; j++)
        A.colmajoraccess(i,j) = lagrange(p,j,(i+1)*h);

    AlignedVector<T,alignment> x(N);   // the degrees of freedom
    for (size_t i=0; i<x.size(); i++) x[i] = 0.3333*i;

    AlignedVector<T,alignment> y(M);   // values of basis functions at quadrature points

    TensorLinearMapping<d,T,alignment> f;

    Dune::Timer timer;
    timer.reset();

    y[0] = 3.14;
    for (int it=0; it<iter; it++)
      {
        x[0] = y[0];
        f.multiply(A,x,y);
      }

    double scnds = timer.elapsed();
    double ops=(double)f.count(A);

    std::cout << std::setw(2) << p << " & "
              << std::setw(2) << q << " & "
              << std::setw(3) << std::fixed << std::noshowpoint << std::setprecision(0) << ops/N << " & "
              << std::setw(10) << std::scientific << std::showpoint << std::setprecision(3) << scnds/iter << " & "
              << std::setw(8) << std::fixed << std::showpoint << std::setprecision(3) << iter*ops/scnds/1e9 <<  "\\\\ %"
              << std::setw(9) << std::scientific << std::showpoint << std::setprecision(1) << y[0]
              << std::endl;
  }

private:
  // 1D Lagrange polynomials at equidistant knots
  T lagrange (int n, int i, T x)
  {
    T h = 1.0/((T)n);
    T value=1.0;
    for (int j=0; j<=n; j++)
      if (j!=i)
        value *= (x-j*h)/(i*h-j*h);
    return value;
  }
};


#endif
