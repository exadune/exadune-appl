// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DFDarcyVelocityFromHeadCCFV_HH
#define DFDarcyVelocityFromHeadCCFV_HH

#include <cstddef>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/localfunctions/raviartthomas/raviartthomascube.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/common/function.hh>
//#include<dune/pdelab/gridoperatorspace/gridoperatorspace.hh>
//#include<dune/pdelab/gridoperatorspace/gridoperatorspaceutilities.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>

// A DataHandle class to exchange RT0 coefficients in the overlap
template<class GV, class V> // mapper type and vector type
class VectorExchange
  : public Dune::CommDataHandleIF<VectorExchange<GV,V>,
                                  typename V::value_type>
{
  typedef typename GV::IndexSet IndexSet;
  typedef typename IndexSet::IndexType IndexType;

  GV gv;
  V& c;
  const IndexSet& indexSet;

public:
  //! export type of data for message buffer
  typedef typename V::value_type DataType;

  //! constructor
  VectorExchange (const GV& gv_, V& c_)
    : gv(gv_), c(c_), indexSet(gv.indexSet())
  {}

  //! returns true if data for this codim should be communicated
  bool contains (int dim, int codim) const
  {
    return (codim==0);
  }

  //! returns true if size per entity of given dim and codim is a constant
  bool fixedsize (int dim, int codim) const
  {
    return true;
  }

  /*! how many objects of type DataType have to be sent for a given entity

    Note: Only the sender side needs to know this size.
  */
  template<class EntityType>
  size_t size (EntityType& e) const
  {
    return 1;
  }

  //! pack data from user to message buffer
  template<class MessageBuffer, class EntityType>
  void gather (MessageBuffer& buff, const EntityType& e) const
  {
    buff.write(c[indexSet.index(e)]);
  }

  /*! unpack data from message buffer to user

    n is the number of objects sent by the sender
  */
  template<class MessageBuffer, class EntityType>
  void scatter (MessageBuffer& buff, const EntityType& e, size_t n)
  {
    DataType x;
    buff.read(x);
    c[indexSet.index(e)]=x;
  }
};

/** \brief Provide velocity field for liquid phase

    Uses RT0 interpolation on a cell.

    - T  : provides ConvectionDiffusionParameterInterface
    - PL : P0 function for liquid phase pressure
    - GFSC  : DG GFS for concentration
    - XC  : DG DOF for concentration
*/
template<typename  T, typename PL, typename GFSC, typename XC>
class DFDarcyVelocityFromHeadCCFV
  : public Dune::PDELab::GridFunctionBase<Dune::PDELab::GridFunctionTraits<typename PL::Traits::GridViewType,
                                                                           typename PL::Traits::RangeFieldType,
                                                                           PL::Traits::GridViewType::dimension,
                                                                           Dune::FieldVector<typename PL::Traits::RangeFieldType,PL::Traits::GridViewType::dimension> >,
                                          DFDarcyVelocityFromHeadCCFV<T,PL,GFSC,XC> >
{
  // extract useful types
  typedef typename PL::Traits::GridViewType GV;
  typedef typename GV::IndexSet IndexSet;
  typedef typename GV::Grid::ctype DF;
  typedef typename PL::Traits::RangeFieldType RF;
  typedef typename PL::Traits::RangeType RangeType;
  enum { dim = PL::Traits::GridViewType::dimension };
  typedef typename GV::Traits::template Codim<0>::Entity Element;
  typedef typename GV::IntersectionIterator IntersectionIterator;
  typedef typename IntersectionIterator::Intersection Intersection;
  typedef typename Dune::RaviartThomasCubeLocalFiniteElement<DF, RF, dim, 0>::Traits::LocalBasisType::Traits::RangeType RT0RangeType;
  typedef typename Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

  const T& t;
  const PL& pl;
  Dune::RaviartThomasCubeLocalFiniteElement<DF, RF, dim, 0> rt0fe;
  mutable std::vector<RT0RangeType> rt0vectors;
  mutable Dune::FieldMatrix<DF,dim,dim> B;
  mutable RF determinant;
  mutable int cachedindex;
  typename T::Traits::RangeFieldType time;

  typedef Dune::FieldVector<RF,2*dim> RT0Coeffs;
  GV gv;
  const IndexSet& is;
  std::vector<RT0Coeffs> storedcoeffs;

  // additional members for density driven flow
  typedef Dune::PDELab::LocalFunctionSpace<GFSC> LFSC;
  typedef Dune::PDELab::LFSIndexCache<LFSC> LFSCCache;
  typedef typename LFSC::Traits::SizeType size_type;
  typedef typename LFSC::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeFieldType RFC;
  typedef typename LFSC::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType RangeTypeC;



public:
  typedef Dune::PDELab::GridFunctionTraits<GV,RF,dim,Dune::FieldVector<RF,dim> > Traits;
  typedef Dune::PDELab::GridFunctionBase<Traits,DFDarcyVelocityFromHeadCCFV<T,PL,GFSC,XC> > BaseT;

  DFDarcyVelocityFromHeadCCFV (const T& t_, const PL& pl_, const GFSC& gfs, XC& c_)
    : t(t_), pl(pl_), rt0vectors(rt0fe.localBasis().size()), cachedindex(-1),
      time(0), gv(pl_.getGridView()), is(gv.indexSet()),
      storedcoeffs(is.size(0))
  {
    update(gfs,c_);
  }

  void update (const GFSC& gfs, XC& c_)
  {
    Dune::shared_ptr<const GFSC> pgfs(stackobject_to_shared_ptr(gfs));
    Dune::shared_ptr<const XC> pc(stackobject_to_shared_ptr(c_));
    typename T::Traits::RangeType gravity;
    typename T::Traits::RangeFieldType delta_rho;
    LFSC lfsc_s(pgfs);
    LFSC lfsc_n(pgfs);
    LFSCCache lfsc_s_cache(lfsc_s);
    LFSCCache lfsc_n_cache(lfsc_n);

    gravity = t.gravity();
    delta_rho = t.delta_rho();

    typename XC::template LocalView<LFSCCache> c_view(c_);

    // compute RT0 coefficients for all interior cells
    for (const auto& cell : elements(gv,Dune::Partitions::interior))
      {
        // get local cell number
        int index = is.index(cell);

        // cell geometry
        const Dune::FieldVector<DF,dim>
          inside_cell_center_local = Dune::ReferenceElements<DF,dim>::
          general(cell.type()).position(0,0);
        Dune::FieldVector<DF,dim>
          inside_cell_center_global = cell.geometry().global(inside_cell_center_local);

        // absolute permeability in primary cell
        typename T::Traits::PermTensorType tensor_inside;
        tensor_inside = t.A(cell,inside_cell_center_local);

        // pressure evaluation
        typename PL::Traits::RangeType pl_inside;
        pl.evaluate(cell,inside_cell_center_local,pl_inside);

        // for coefficient computation
        RF vn[2*dim];    // normal velocities
        Dune::FieldMatrix<typename Traits::DomainFieldType,dim,dim>
          B = cell.geometry().jacobianInverseTransposed(inside_cell_center_local); // the transformation. Assume it is linear
        RF determinant = B.determinant();

        // loop over cell neighbors
        for (const auto& is : intersections(pl.getGridView(),cell))
          {
            // set to zero for processor boundary
            vn[is.indexInInside()] = 0.0;

            // face geometry
            const Dune::FieldVector<DF,dim-1>&
              face_local = Dune::ReferenceElements<DF,dim-1>::general(is.geometry().type()).position(0,0);

            auto inside_cell = is.inside();

            // interior face
            if (is.neighbor())
              {
                auto outside_cell = is.outside();

                const Dune::FieldVector<DF,dim>
                  outside_cell_center_local = Dune::ReferenceElements<DF,dim>::
                  general(outside_cell.type()).position(0,0);
                Dune::FieldVector<DF,dim>
                  outside_cell_center_global = outside_cell.geometry().global(outside_cell_center_local);

                // distance of cell centers
                Dune::FieldVector<DF,dim> d(outside_cell_center_global);
                d -= inside_cell_center_global;
                RF distance = d.two_norm();

                lfsc_s.bind(inside_cell);
                lfsc_s_cache.update();
                std::vector<RFC> cl_s(lfsc_s.size());
                c_view.bind(lfsc_s_cache);
                c_view.read(cl_s);
                c_view.unbind();

                lfsc_n.bind(outside_cell);
                lfsc_n_cache.update();
                std::vector<RFC> cl_n(lfsc_n.size());
                c_view.bind(lfsc_n_cache);
                c_view.read(cl_n);
                c_view.unbind();

                // evaluate average concentration at face center
                RF cavg(0.0);
                Dune::FieldVector<DF,dim> iplocal_s = is.geometryInInside().center();
                Dune::FieldVector<DF,dim> iplocal_n = is.geometryInOutside().center();
                std::vector<RangeTypeC> phi_s(lfsc_s.size());
                lfsc_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
                std::vector<RangeTypeC> phi_n(lfsc_n.size());
                lfsc_n.finiteElement().localBasis().evaluateFunction(iplocal_n,phi_n);
                RF c_s=0.0;
                for (std::size_t i=0; i<lfsc_s.size(); i++)
                  c_s += cl_s[i]*phi_s[i];
                RF c_n=0.0;
                for (std::size_t i=0; i<lfsc_n.size(); i++)
                  c_n += cl_n[i]*phi_n[i];
                cavg += 0.5*(c_s+c_n);

                // absolute permeability
                typename T::Traits::PermTensorType tensor_outside;
                tensor_outside = t.A(outside_cell,outside_cell_center_local);
                const Dune::FieldVector<DF,dim> n_F = is.centerUnitOuterNormal();
                Dune::FieldVector<RF,dim> An_F;
                tensor_inside.mv(n_F,An_F);
                RF k_inside = n_F*An_F/t.viscosity(c_s);
                tensor_outside.mv(n_F,An_F);
                RF k_outside = n_F*An_F/t.viscosity(c_n);
                RF k_avg = 2.0/(1.0/(k_inside+1E-30) + 1.0/(k_outside+1E-30));

                // pressure evaluation
                typename PL::Traits::RangeType pl_outside;
                pl.evaluate(outside_cell,outside_cell_center_local,pl_outside);

                // set coefficient
                vn[is.indexInInside()] = k_avg*(pl_inside-pl_outside)/distance;

                // addition for density driven flow gravity term
                RF g=0.0;
                for (int i=0; i<dim; i++) g+=n_F[i]*gravity[i];
                if (std::abs(g)>1e-8)
                  {

                    // const int intorder =
                    //   std::max(lfsc_s.finiteElement().localBasis().order(),
                    //            lfsc_n.finiteElement().localBasis().order())+1;
                    // Dune::GeometryType gtface = iit->geometryInInside().type();
                    // const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);
                    // for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
                    //   {
                    //     // position of quadrature point in local coordinates of elements
                    //     Dune::FieldVector<DF,dim> iplocal_s = iit->geometryInInside().global(it->position());
                    //     Dune::FieldVector<DF,dim> iplocal_n = iit->geometryInOutside().global(it->position());
                    //     // evaluate basis functions
                    //     std::vector<RangeTypeC> phi_s(lfsc_s.size());
                    //     lfsc_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
                    //     std::vector<RangeTypeC> phi_n(lfsc_n.size());
                    //     lfsc_n.finiteElement().localBasis().evaluateFunction(iplocal_n,phi_n);
                    //     // evaluate u
                    //     RF c_s=0.0;
                    //     for (int i=0; i<lfsc_s.size(); i++)
                    //       c_s += cl_s[i]*phi_s[i];
                    //     RF c_n=0.0;
                    //     for (int i=0; i<lfsc_n.size(); i++)
                    //       c_n += cl_n[i]*phi_n[i];

                    //     // contribution to residual on inside element, other residual is computed by symmetric call
                    //     cavg += 0.5*(c_s+c_n)*it->weight();
                    //   }

                    vn[is.indexInInside()] += k_avg * delta_rho * g * cavg;
                  }
              }

            // boundary face
            if (is.boundary())
              {
                // distance of cell center to boundary
                Dune::FieldVector<DF,dim> d = is.geometry().global(face_local);
                d -= inside_cell_center_global;
                RF distance = d.two_norm();

                // evaluate boundary condition type
                BCType bctype;
                bctype = t.bctype(is,face_local);

                // liquid phase Dirichlet boundary
                if (bctype==Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet)
                  {
                    // evaluate average concentration at face center
                    lfsc_s.bind(inside_cell);
                    lfsc_s_cache.update();
                    std::vector<RFC> cl_s(lfsc_s.size());
                    c_view.bind(lfsc_s_cache);
                    c_view.read(cl_s);
                    c_view.unbind();
                    Dune::FieldVector<DF,dim> iplocal_s = is.geometryInInside().center();
                    std::vector<RangeTypeC> phi_s(lfsc_s.size());
                    lfsc_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
                    RF c_s=0.0;
                    for (std::size_t i=0; i<lfsc_s.size(); i++)
                      c_s += cl_s[i]*phi_s[i];

                    RF g_l = t.g(inside_cell,iplocal_s);
                    const Dune::FieldVector<DF,dim> n_F = is.centerUnitOuterNormal();
                    Dune::FieldVector<RF,dim> An_F;
                    tensor_inside.mv(n_F,An_F);
                    RF k_inside = n_F*An_F/t.viscosity(c_s);
                    vn[is.indexInInside()] = k_inside * (pl_inside-g_l)/distance;

                    // addition for density driven flow gravity term
                    RF g=0.0;
                    for (int i=0; i<dim; i++) g+=n_F[i]*gravity[i];
                    if (std::abs(g)>=1e-8)
                      {
                        // contribution to residual on inside element, other residual is computed by symmetric call
                        vn[is.indexInInside()] += c_s * k_inside * delta_rho * g;
                      }
                  }

                // liquid phase Neumann boundary
                if (bctype==Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann)
                  {
                    typename T::Traits::RangeFieldType j = t.j(is,face_local);
                    vn[is.indexInInside()] = j;
                  }


              }

            // compute coefficient
            Dune::FieldVector<DF,dim> vstar=is.centerUnitOuterNormal(); // normal on tranformef element
            vstar *= vn[is.indexInInside()];
            Dune::FieldVector<RF,dim> normalhat(0); // normal on reference element
            if (is.indexInInside()%2==0)
              normalhat[is.indexInInside()/2] = -1.0;
            else
              normalhat[is.indexInInside()/2] =  1.0;
            Dune::FieldVector<DF,dim> vstarhat(0);
            B.umtv(vstar,vstarhat); // Piola backward transformation
            vstarhat *= determinant;
            storedcoeffs[index][is.indexInInside()] = vstarhat*normalhat;
          }
      }

    // communicate coefficients in overlap
    VectorExchange<GV,std::vector<RT0Coeffs> > dh(gv,storedcoeffs);
    if (gv.grid().comm().size()>1)
      gv.grid().communicate(dh,Dune::InteriorBorder_All_Interface,Dune::ForwardCommunication);
  }

  // set time where operator is to be evaluated (i.e. end of the time intervall)
  void set_time (typename T::Traits::RangeFieldType time_)
  {
    time = time_;
  }

  inline void evaluate (const typename Traits::ElementType& e,
                        const typename Traits::DomainType& x,
                        typename Traits::RangeType& y) const
  {
    // local cell number
    int index = is.index(e);

    // compute velocity on reference element
    rt0fe.localBasis().evaluateFunction(x,rt0vectors);
    typename Traits::RangeType yhat(0);
    for (unsigned int i=0; i<rt0fe.localBasis().size(); i++)
      yhat.axpy(storedcoeffs[index][i],rt0vectors[i]);

    // apply Piola transformation
    if (index != cachedindex)
      {
        B = e.geometry().jacobianTransposed(x); // the transformation. Assume it is linear
        determinant = B.determinant();
        cachedindex = index;
      }
    y = 0;
    B.umtv(yhat,y);
    y *= determinant;
  }

  inline const typename Traits::GridViewType& getGridView () const
  {
    return pl.getGridView();
  }
};

#endif
