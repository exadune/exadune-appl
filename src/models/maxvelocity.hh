// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef COMPUTE_MAXIMUM_VELOCITY_HH
#define COMPUTE_MAXIMUM_VELOCITY_HH

#include <dune/grid/common/partitionset.hh>

/** \brief compute maximum velocity from vector-valued grid function
 */
template<typename T>
typename T::Traits::RangeFieldType maxvelocity (const T& t)
{
  // extract useful types
  typedef typename T::Traits::GridViewType GV;
  typedef typename T::Traits::RangeFieldType RF;
  typedef typename T::Traits::DomainType DomainType;
  typedef typename T::Traits::RangeType RangeType;

  // variables
  const int dim = T::Traits::GridViewType::dimension;
  RF maximum = 0.0;
  const GV& gv(t.getGridView());

  // loop over the grid
  for (const auto& cell : elements(gv,Dune::Partitions::interior))
    {
      for (int d=0; d<dim; d++)
        {
          DomainType x0(0.5);
          x0[d] = 0.0;
          RangeType y0;
          t.evaluate(cell,x0,y0);
          maximum = std::max(maximum,y0.two_norm());
          DomainType x1(0.5);
          x1[d] = 1.0;
          RangeType y1;
          t.evaluate(cell,x1,y1);
          maximum = std::max(maximum,y1.two_norm());
        }
    }
  maximum = gv.comm().max(maximum);
  return maximum;
}

#endif
