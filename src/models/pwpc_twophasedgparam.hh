// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PM_PWPC_TWOPHASEDGPARAMETERS_HH
#define DUNE_PM_PWPC_TWOPHASEDGPARAMETERS_HH

#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/static_assert.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/geometrywrapper.hh>

namespace Dune {
  namespace PM {
    //! \addtogroup LocalOperator
    //! \ingroup PM
    //!

    /** Parameter class for the two-phase flow model in phase pressure saturation formulation
     *
     */

    //! traits class for two phase parameter class
    template<typename GV, typename RF>
    struct PwPc_TwoPhaseDGTraits
    {
      //! \brief the grid view
      typedef GV GridViewType;

      //! \brief Enum for domain dimension
      enum {
        //! \brief dimension of the domain
        dimDomain = GV::dimension
      };

      //! \brief Export type for domain field
      typedef typename GV::Grid::ctype DomainFieldType;

      //! \brief domain type
      typedef Dune::FieldVector<DomainFieldType,dimDomain> DomainType;

      //! \brief domain type
      typedef Dune::FieldVector<DomainFieldType,dimDomain-1> IntersectionDomainType;

      //! \brief Export type for range field
      typedef RF RangeFieldType;

      //! \brief range type
      typedef Dune::FieldVector<RF,GV::dimensionworld> RangeType;

      //! \brief permeability tensor type
      typedef Dune::FieldMatrix<RangeFieldType,dimDomain,dimDomain> PermTensorType;

      //! grid types
      typedef typename GV::Traits::template Codim<0>::Entity ElementType;
      typedef typename GV::Intersection IntersectionType;
    };

    struct PwPc_TwoPhaseDGBoundaryConditions
    {
      enum Type { Dirichlet, Flux };
    };

    /*! Define interface for the two-phase pressure saturation formulation.
     */
    template<typename GV, typename RF>
    class PwPc_TwoPhaseDGIF
    {
    public:
      typedef PwPc_TwoPhaseDGTraits<GV,RF> Traits;

      typedef PwPc_TwoPhaseDGBoundaryConditions::Type BCType;

      //! porosity
      typename Traits::RangeFieldType
      phi (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 1.0;
      }

      //! density phase 1 (p1 is the pressure of phase 1)
      typename Traits::RangeFieldType
      rho1 () const
      {
        return 1.0;
      }

      //! density phase 2 (p2 is the pressure of phase 2)
      typename Traits::RangeFieldType
      rho2 () const
      {
        return 1.0;
      }

      //! tensor diffusion coefficient
      typename Traits::PermTensorType
      K (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return typename Traits::PermTensorType();
      }

      //! relative permeability phase 1 (s is the unknown saturation); Assume kr1 >=0
      typename Traits::RangeFieldType
      kr1 (const typename Traits::ElementType& e, const typename Traits::DomainType& x, typename Traits::RangeFieldType s1) const
      {
        return 1.0;
      }

      //! relative permeability phase 2 (s is the unknown saturation); Assume kr1 >=0
      typename Traits::RangeFieldType
      kr2 (const typename Traits::ElementType& e, const typename Traits::DomainType& x, typename Traits::RangeFieldType s2) const
      {
        return 1.0;
      }

      //! compute saturation from capillary pressure
      typename Traits::RangeFieldType
      psi (const typename Traits::ElementType& e, const typename Traits::DomainType& x, typename Traits::RangeFieldType pc) const
      {
        return 1.0;
      }

      //! entry pressure to detect media discontinuities
      typename Traits::RangeFieldType
      pe (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
      {
        return 1.0;
      }

      //! dynamic viscosity phase 1 (p1 is the pressure of phase 1)
      typename Traits::RangeFieldType
      mu1 () const
      {
        return 1.0;
      }

      //! dynamic viscosity phase 2 (p2 is the pressure of phase 2)
      typename Traits::RangeFieldType
      mu2 () const
      {
        return 1.0;
      }

      //! gravity vector
      const typename Traits::RangeType& gravity () const
      {
        return 0.0;
      }

      //! depth function for potential-based formulations; NOTE that xglobal is in global coordinates!!
      typename Traits::RangeFieldType
      depth (const typename Traits::DomainType& xglobal) const
      {
        return 0.0;
      }

      //! BOUNDARY condition type phase 1
      PwPc_TwoPhaseDGBoundaryConditions::Type
      bc1 (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return PwPc_TwoPhaseDGBoundaryConditions::Dirichlet;
      }

      //! boundary condition type phase 2
      PwPc_TwoPhaseDGBoundaryConditions::Type
      bc2 (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return PwPc_TwoPhaseDGBoundaryConditions::Dirichlet;
      }

      //! Dirichlet boundary condition phase 1
      typename Traits::RangeFieldType
      g1 (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return 0.0;
      }

      //! Dirichlet boundary condition phase 2
      typename Traits::RangeFieldType
      g2 (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return 0.0;
      }

      //! Neumann boundary condition first equation: total VOLUME fluxes!
      typename Traits::RangeFieldType
      j1 (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return 0.0;
      }

      //! Neumann boundary condition phase 2 VOLUME fluxes!
      typename Traits::RangeFieldType
      j2 (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x, typename Traits::RangeFieldType time) const
      {
        return 0.0;
      }

      //! source term first equation: total VOLUME fluxes!
      typename Traits::RangeFieldType
      f1 (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType time) const
      {
        return 0.0;
      }

      //! source term phase 2, VOLUME fluxes!
      typename Traits::RangeFieldType
      f2 (const typename Traits::ElementType& e, const typename Traits::DomainType& x,
          typename Traits::RangeFieldType time) const
      {
        return 0.0;
      }

      //! set time for subsequent evaluation
      void setTime (typename Traits::RangeFieldType t)
      {
      }

    };

    //! \} group PM
  } // namespace PM
} // namespace Dune

#endif
