// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifndef DUNE_PDELAB_DF_CONVECTIONDIFFUSIONCCFV_HH
#define DUNE_PDELAB_DF_CONVECTIONDIFFUSIONCCFV_HH

#include <cstddef>

#if HAVE_TBB
#include "tbb/tbb_stddef.h"
#endif

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/geometry/referenceelements.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
//#include<dune/pdelab/gridoperatorspace/gridoperatorspace.hh>
//#include<dune/pdelab/gridoperatorspace/gridoperatorspaceutilities.hh>
#include<dune/pdelab/localoperator/pattern.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>

namespace Dune {
  namespace PDELab {

    /** a local operator for solving convection-diffusion equation with standard FEM
     *
     * \f{align*}{
     *   \nabla\cdot(-A(x) (\nabla u - C*drho*G) + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \\
     *                                              u &=& g \mbox{ on } \partial\Omega_D \\
     *                (b(x,u) - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N \\
     *                        -(A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_O
     * \f}
     * Note:
     *  - This formulation is valid for velocity fields which are non-divergence free.
     *  - Assumes that the tensor is diagonal !
     *  - Outflow boundary conditions should only be set on the outflow boundary
     *
     * \tparam T model of ConvectionDiffusionParameterInterface
     */
    template<typename T, typename GFSC, typename XC>
    class DFConvectionDiffusionCCFV : public NumericalJacobianApplySkeleton<DFConvectionDiffusionCCFV<T,GFSC,XC> >,
                                      public NumericalJacobianApplyBoundary<DFConvectionDiffusionCCFV<T,GFSC,XC> >,
                                      public NumericalJacobianApplyVolume<DFConvectionDiffusionCCFV<T,GFSC,XC> >,
    // public NumericalJacobianSkeleton<DFConvectionDiffusionCCFV<T,GFSC,XC> >,
    // public NumericalJacobianBoundary<DFConvectionDiffusionCCFV<T,GFSC,XC> >,
    // public NumericalJacobianVolume<DFConvectionDiffusionCCFV<T,GFSC,XC> >,
                                      public FullSkeletonPattern,
                                      public FullVolumePattern,
                                      public LocalOperatorDefaultFlags,
                                      public InstationaryLocalOperatorDefaultMethods<typename T::Traits::RangeFieldType>

    {
      typedef typename ConvectionDiffusionBoundaryConditions::Type BCType;

      // additional members for density driven flow
      Dune::shared_ptr<const GFSC> pgfs;
      Dune::shared_ptr<XC> pc;
      typename T::Traits::RangeType gravity;
      typename T::Traits::RangeFieldType delta_rho;
      typedef Dune::PDELab::LocalFunctionSpace<GFSC> LFSC;
      mutable LFSC lfsc_s;
      mutable LFSC lfsc_n;
      typedef Dune::PDELab::LFSIndexCache<LFSC> LFSCCache;
      mutable LFSCCache lfsc_s_cache;
      mutable LFSCCache lfsc_n_cache;
      typedef typename LFSC::Traits::SizeType size_type;
      typedef typename LFSC::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeFieldType RFC;
      typedef typename LFSC::Traits::FiniteElementType::
        Traits::LocalBasisType::Traits::RangeType RangeTypeC;

    public:
      // pattern assembly flags
      enum { doPatternVolume = true };
      enum { doPatternSkeleton = true };

      // residual assembly flags
      enum { doAlphaVolume    = true };
      enum { doAlphaSkeleton  = true };
      enum { doAlphaBoundary  = true };
      enum { doLambdaVolume   = false };
      enum { doLambdaSkeleton = false };
      enum { doLambdaBoundary = false };

      DFConvectionDiffusionCCFV (T& param_, const GFSC& gfs, XC& c_)
        : pgfs(stackobject_to_shared_ptr(gfs)),
          pc(stackobject_to_shared_ptr(c_)),
          lfsc_s(pgfs), lfsc_n(pgfs),
          lfsc_s_cache(lfsc_s), lfsc_n_cache(lfsc_n),
          param(param_)
      {
        gravity = param.gravity();
        delta_rho = param.delta_rho();
      }

#if HAVE_TBB
      // splitting
      DFConvectionDiffusionCCFV(const DFConvectionDiffusionCCFV &other,
                                tbb::split)
        : pgfs(other.pgfs),
          pc(other.pc),
          gravity(other.gravity),
          delta_rho(other.delta_rho),
          lfsc_s(other.lfsc_s), lfsc_n(other.lfsc_n),
          // make sure to initialize the caches with the new LFS'
          lfsc_s_cache(lfsc_s), lfsc_n_cache(lfsc_n),
          param(other.param)
      { }

      // joining
      void join(DFConvectionDiffusionCCFV &)
      {
        // nothing to do
      }
#endif // HAVE_TBB

      // volume integral depending on test and ansatz functions
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // domain and range field type
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;

        // dimensions
        const int dim = EG::Geometry::dimension;

        // cell center
        const Dune::FieldVector<DF,dim>
          inside_local(Dune::ReferenceElements<DF,dim>::general(eg.entity().type()).position(0,0));

        // evaluate source and sink term
        typename T::Traits::RangeFieldType f = param.f(eg.entity(),inside_local);
        typename T::Traits::RangeFieldType c = param.c(eg.entity(),inside_local);

        // and accumulate
        r.accumulate(lfsu,0,(c*x(lfsu,0)-f)*eg.geometry().volume());
      }

      // jacobian of volume term
      template<typename EG, typename LFSU, typename X, typename LFSV, typename M>
      void jacobian_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv,
                            M& mat) const
      {
        // domain and range field type
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;

        // dimensions
        const int dim = EG::Geometry::dimension;

        // cell center
        const Dune::FieldVector<DF,dim>
          inside_local(Dune::ReferenceElements<DF,dim>::general(eg.entity().type()).position(0,0));

        // evaluate source and sink term
        typename T::Traits::RangeFieldType f = param.f(eg.entity(),inside_local);
        typename T::Traits::RangeFieldType c = param.c(eg.entity(),inside_local);

        // and accumulate
        mat.accumulate(lfsu,0,lfsu,0,c*eg.geometry().volume());
      }

      // skeleton integral depending on test and ansatz functions
      // each face is only visited ONCE!
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                           R& r_s, R& r_n) const
      {
        // domain and range field type
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        const int dim = IG::dimension;

        auto inside_cell = ig.inside();
        auto outside_cell = ig.outside();

        // face volume for integration
        RF face_volume = ig.geometry().volume();

        // cell centers in references elements
        const Dune::FieldVector<DF,dim>
          inside_local = Dune::ReferenceElements<DF,IG::dimension>::general(inside_cell.type()).position(0,0);
        const Dune::FieldVector<DF,dim>
          outside_local = Dune::ReferenceElements<DF,IG::dimension>::general(outside_cell.type()).position(0,0);

        // evaluate concentrations
        typename XC::template LocalView<LFSCCache> c_view(*pc);

        lfsc_s.bind(inside_cell);
        lfsc_s_cache.update();
        std::vector<RFC> cl_s(lfsc_s.size());
        c_view.bind(lfsc_s_cache);
        c_view.read(cl_s);
        c_view.unbind();

        lfsc_n.bind(outside_cell);
        lfsc_n_cache.update();
        std::vector<RFC> cl_n(lfsc_n.size());
        c_view.bind(lfsc_n_cache);
        c_view.read(cl_n);
        c_view.unbind();

        // evaluate average concentration at face center
        RF cavg(0.0);
        Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().center();
        Dune::FieldVector<DF,dim> iplocal_n = ig.geometryInOutside().center();
        std::vector<RangeTypeC> phi_s(lfsc_s.size());
        lfsc_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
        std::vector<RangeTypeC> phi_n(lfsc_n.size());
        lfsc_n.finiteElement().localBasis().evaluateFunction(iplocal_n,phi_n);
        RF c_s=0.0;
        for (std::size_t i=0; i<lfsc_s.size(); i++)
          c_s += cl_s[i]*phi_s[i];
        RF c_n=0.0;
        for (std::size_t i=0; i<lfsc_n.size(); i++)
          c_n += cl_n[i]*phi_n[i];
        cavg += 0.5*(c_s+c_n);

        // evaluate diffusion coefficient from either side and take harmonic average
        typename T::Traits::PermTensorType tensor_inside;
        tensor_inside = param.A(inside_cell,inside_local);
        typename T::Traits::PermTensorType tensor_outside;
        tensor_outside = param.A(outside_cell,outside_local);
        const Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
        Dune::FieldVector<RF,dim> An_F;
        tensor_inside.mv(n_F,An_F);
        RF k_inside = n_F*An_F/param.viscosity(c_s);
        tensor_outside.mv(n_F,An_F);
        RF k_outside = n_F*An_F/param.viscosity(c_n);
        RF k_avg = 2.0/(1.0/(k_inside+1E-30) + 1.0/(k_outside+1E-30));

        // evaluate convective term
        typename T::Traits::RangeType b = param.b(inside_cell,iplocal_s);
        RF vn = b*n_F;
        RF u_upwind=0;
        if (vn>=0) u_upwind = x_s(lfsu_s,0); else u_upwind = x_n(lfsu_n,0);

        // cell centers in global coordinates
        Dune::FieldVector<DF,IG::dimension>
          inside_global = inside_cell.geometry().global(inside_local);
        Dune::FieldVector<DF,IG::dimension>
          outside_global = outside_cell.geometry().global(outside_local);

        // distance between the two cell centers
        inside_global -= outside_global;
        RF distance = inside_global.two_norm();

        // contribution to residual on inside element, other residual is computed by symmetric call
        r_s.accumulate(lfsu_s,0,(u_upwind*vn)*face_volume+k_avg*(x_s(lfsu_s,0)-x_n(lfsu_n,0))*face_volume/distance);
        r_n.accumulate(lfsu_n,0,-(u_upwind*vn)*face_volume-k_avg*(x_s(lfsu_s,0)-x_n(lfsu_n,0))*face_volume/distance);

        // const int intorder =
        //     std::max(lfsc_s.finiteElement().localBasis().order(),
        //              lfsc_n.finiteElement().localBasis().order())+1;
        // Dune::GeometryType gtface = ig.geometryInInside().type();
        // const Dune::QuadratureRule<DF,dim-1>& rule = Dune::QuadratureRules<DF,dim-1>::rule(gtface,intorder);
        // RF cavg(0.0);
        // for (typename Dune::QuadratureRule<DF,dim-1>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        //   {
        //     // position of quadrature point in local coordinates of elements
        //     Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().global(it->position());
        //     Dune::FieldVector<DF,dim> iplocal_n = ig.geometryInOutside().global(it->position());
        //     // evaluate basis functions
        //     std::vector<RangeTypeC> phi_s(lfsc_s.size());
        //     lfsc_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
        //     std::vector<RangeTypeC> phi_n(lfsc_n.size());
        //     lfsc_n.finiteElement().localBasis().evaluateFunction(iplocal_n,phi_n);
        //     // evaluate u
        //     RF c_s=0.0;
        //     for (int i=0; i<lfsc_s.size(); i++)
        //       c_s += cl_s[i]*phi_s[i];
        //     RF c_n=0.0;
        //     for (int i=0; i<lfsc_n.size(); i++)
        //       c_n += cl_n[i]*phi_n[i];
        //     cavg += 0.5*(c_s+c_n)*it->weight();
        //   }
        // contribution to residual on inside element, other residual is computed by symmetric call
        RF g=0.0;
        for (int i=0; i<dim; i++) g+=n_F[i]*gravity[i];
        if (std::abs(g)<1e-8) return; // face is orthogonal to gravity direction
        r_s.accumulate(lfsu_s,0, cavg * k_avg * delta_rho * g * face_volume);
        r_n.accumulate(lfsu_n,0,-cavg * k_avg * delta_rho * g * face_volume);
      }

      template<typename IG, typename LFSU, typename X, typename LFSV, typename M>
      void jacobian_skeleton (const IG& ig,
                              const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                              const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                              M& mat_ss, M& mat_sn,
                              M& mat_ns, M& mat_nn) const
      {
        // domain and range field type
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        const int dim = IG::dimension;

        // center in face's reference element
        const Dune::FieldVector<DF,dim-1>
          face_local = Dune::ReferenceElements<DF,IG::dimension-1>::general(ig.geometry().type()).position(0,0);

        // face volume for integration
        RF face_volume = ig.geometry().integrationElement(face_local)
          *Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).volume();

        auto inside_cell = ig.inside();
        auto outside_cell = ig.outside();

        // cell centers in references elements
        const Dune::FieldVector<DF,dim>
          inside_local = Dune::ReferenceElements<DF,IG::dimension>::general(inside_cell.type()).position(0,0);
        const Dune::FieldVector<DF,dim>
          outside_local = Dune::ReferenceElements<DF,IG::dimension>::general(outside_cell.type()).position(0,0);

        // evaluate concentrations
        typename XC::template LocalView<LFSCCache> c_view(*pc);

        lfsc_s.bind(inside_cell);
        lfsc_s_cache.update();
        std::vector<RFC> cl_s(lfsc_s.size());
        c_view.bind(lfsc_s_cache);
        c_view.read(cl_s);
        c_view.unbind();

        lfsc_n.bind(outside_cell);
        lfsc_n_cache.update();
        std::vector<RFC> cl_n(lfsc_n.size());
        c_view.bind(lfsc_n_cache);
        c_view.read(cl_n);
        c_view.unbind();

        // evaluate average concentration at face center
        Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().center();
        Dune::FieldVector<DF,dim> iplocal_n = ig.geometryInOutside().center();
        std::vector<RangeTypeC> phi_s(lfsc_s.size());
        lfsc_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
        std::vector<RangeTypeC> phi_n(lfsc_n.size());
        lfsc_n.finiteElement().localBasis().evaluateFunction(iplocal_n,phi_n);
        RF c_s=0.0;
        for (std::size_t i=0; i<lfsc_s.size(); i++)
          c_s += cl_s[i]*phi_s[i];
        RF c_n=0.0;
        for (std::size_t i=0; i<lfsc_n.size(); i++)
          c_n += cl_n[i]*phi_n[i];

        // evaluate diffusion coefficient from either side and take harmonic average
        typename T::Traits::PermTensorType tensor_inside;
        tensor_inside = param.A(inside_cell,inside_local);
        typename T::Traits::PermTensorType tensor_outside;
        tensor_outside = param.A(outside_cell,outside_local);
        const Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
        Dune::FieldVector<RF,dim> An_F;
        tensor_inside.mv(n_F,An_F);
        RF k_inside = n_F*An_F/param.viscosity(c_s);
        tensor_outside.mv(n_F,An_F);
        RF k_outside = n_F*An_F/param.viscosity(c_n);
        RF k_avg = 2.0/(1.0/(k_inside+1E-30) + 1.0/(k_outside+1E-30));

        // evaluate convective term
        typename T::Traits::RangeType b = param.b(inside_cell,iplocal_s);
        RF vn = b*n_F;

        // cell centers in global coordinates
        Dune::FieldVector<DF,IG::dimension>
          inside_global = inside_cell.geometry().global(inside_local);
        Dune::FieldVector<DF,IG::dimension>
          outside_global = outside_cell.geometry().global(outside_local);

        // distance between the two cell centers
        inside_global -= outside_global;
        RF distance = inside_global.two_norm();

        // contribution to residual on inside element, other residual is computed by symmetric call
        mat_ss.accumulate(lfsu_s,0,lfsu_s,0,   k_avg*face_volume/distance );
        mat_ns.accumulate(lfsu_n,0,lfsu_s,0,  -k_avg*face_volume/distance );
        mat_sn.accumulate(lfsu_s,0,lfsu_n,0,  -k_avg*face_volume/distance );
        mat_nn.accumulate(lfsu_n,0,lfsu_n,0,   k_avg*face_volume/distance );
        if (vn>=0)
          {
            mat_ss.accumulate(lfsu_s,0,lfsu_s,0,   vn*face_volume );
            mat_ns.accumulate(lfsu_n,0,lfsu_s,0,  -vn*face_volume );
          }
        else
          {
            mat_sn.accumulate(lfsu_s,0,lfsu_n,0,   vn*face_volume );
            mat_nn.accumulate(lfsu_n,0,lfsu_n,0,  -vn*face_volume );
          }
      }

      // skeleton integral depending on test and ansatz functions
      // We put the Dirchlet evaluation also in the alpha term to save some geometry evaluations
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary (const IG& ig,
                           const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                           R& r_s) const
      {
        // domain and range field type
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        const int dim = IG::dimension;

        // center in face's reference element
        const Dune::FieldVector<DF,dim-1>
          face_local = Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).position(0,0);

        // face volume for integration
        RF face_volume = ig.geometry().integrationElement(face_local)
          *Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).volume();

        auto inside_cell = ig.inside();

        // cell center in reference element
        const Dune::FieldVector<DF,dim>
          inside_local = Dune::ReferenceElements<DF,IG::dimension>::general(inside_cell.type()).position(0,0);

        // evaluate boundary condition type
        BCType bctype;
        bctype = param.bctype(ig.intersection(),face_local);

        if (bctype==ConvectionDiffusionBoundaryConditions::Dirichlet)
          {
            // Dirichlet boundary
            // distance between cell center and face center
            Dune::FieldVector<DF,dim> inside_global = inside_cell.geometry().global(inside_local);
            Dune::FieldVector<DF,dim> outside_global = ig.geometry().global(face_local);
            inside_global -= outside_global;
            RF distance = inside_global.two_norm();

            // evaluate average concentration at face center
            typename XC::template LocalView<LFSCCache> c_view(*pc);
            lfsc_s.bind(inside_cell);
            lfsc_s_cache.update();
            std::vector<RFC> cl_s(lfsc_s.size());
            c_view.bind(lfsc_s_cache);
            c_view.read(cl_s);
            c_view.unbind();
            Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().center();
            std::vector<RangeTypeC> phi_s(lfsc_s.size());
            lfsc_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
            RF c_s=0.0;
            for (std::size_t i=0; i<lfsc_s.size(); i++)
              c_s += cl_s[i]*phi_s[i];

            // evaluate diffusion coefficient
            const Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
            typename T::Traits::PermTensorType tensor_inside;
            tensor_inside = param.A(inside_cell,inside_local);
            Dune::FieldVector<RF,dim> An_F;
            tensor_inside.mv(n_F,An_F);
            RF k_inside = n_F*An_F/param.viscosity(c_s);

            // evaluate boundary condition function
            RF gval = param.g(inside_cell,iplocal_s);

            // velocity needed for convection term
            typename T::Traits::RangeType b = param.b(inside_cell,iplocal_s);
            const Dune::FieldVector<DF,dim> n = ig.centerUnitOuterNormal();

            // contribution to residual on inside element, assumes that Dirichlet boundary is inflow
            r_s.accumulate(lfsu_s,0,(b*n)*gval*face_volume + k_inside*(x_s(lfsu_s,0)-gval)*face_volume/distance);

            RF g=0.0;
            for (int i=0; i<dim; i++) g+=n_F[i]*gravity[i];
            if (std::abs(g)>=1e-8)
              {
                // contribution to residual on inside element, other residual is computed by symmetric call
                r_s.accumulate(lfsu_s,0,c_s * k_inside * delta_rho * g * face_volume);
              }

            return;
          }

        if (bctype==ConvectionDiffusionBoundaryConditions::Neumann)
          {
            // Neumann boundary
            // evaluate flux boundary condition

            //evaluate boundary function
            typename T::Traits::RangeFieldType j = param.j(ig.intersection(),face_local);

            // contribution to residual on inside element
            r_s.accumulate(lfsu_s,0,j*face_volume);

            return;
          }

        if (bctype==ConvectionDiffusionBoundaryConditions::Outflow)
          {
            // evaluate velocity field and outer unit normal
            Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().global(face_local);
            typename T::Traits::RangeType b = param.b(inside_cell,iplocal_s);
            const Dune::FieldVector<DF,dim> n = ig.centerUnitOuterNormal();

            // evaluate outflow boundary condition
            typename T::Traits::RangeFieldType o = param.o(ig.intersection(),face_local);

            // integrate o
            r_s.accumulate(lfsu_s,0,((b*n)*x_s(lfsu_s,0) + o)*face_volume);

            return;
          }
      }

      template<typename IG, typename LFSU, typename X, typename LFSV, typename M>
      void jacobian_boundary (const IG& ig,
                              const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                              M& mat_ss) const
      {
        // domain and range field type
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        const int dim = IG::dimension;

        // center in face's reference element
        const Dune::FieldVector<DF,dim-1>
          face_local = Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).position(0,0);

        // face volume for integration
        RF face_volume = ig.geometry().integrationElement(face_local)
          *Dune::ReferenceElements<DF,dim-1>::general(ig.geometry().type()).volume();

        auto inside_cell = ig.inside();

        // cell center in reference element
        const Dune::FieldVector<DF,dim>
          inside_local = Dune::ReferenceElements<DF,IG::dimension>::general(inside_cell.type()).position(0,0);

        // evaluate boundary condition type
        BCType bctype;
        bctype = param.bctype(ig.intersection(),face_local);


        if (bctype==ConvectionDiffusionBoundaryConditions::Dirichlet)
          {
            // Dirichlet boundary
            // distance between cell center and face center
            Dune::FieldVector<DF,dim> inside_global = inside_cell.geometry().global(inside_local);
            Dune::FieldVector<DF,dim> outside_global = ig.geometry().global(face_local);
            inside_global -= outside_global;
            RF distance = inside_global.two_norm();

            // evaluate average concentration at face center
            typename XC::template LocalView<LFSCCache> c_view(*pc);
            lfsc_s.bind(inside_cell);
            lfsc_s_cache.update();
            std::vector<RFC> cl_s(lfsc_s.size());
            c_view.bind(lfsc_s_cache);
            c_view.read(cl_s);
            c_view.unbind();
            Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().center();
            std::vector<RangeTypeC> phi_s(lfsc_s.size());
            lfsc_s.finiteElement().localBasis().evaluateFunction(iplocal_s,phi_s);
            RF c_s=0.0;
            for (std::size_t i=0; i<lfsc_s.size(); i++)
              c_s += cl_s[i]*phi_s[i];

            // evaluate diffusion coefficient
            typename T::Traits::PermTensorType tensor_inside;
            tensor_inside = param.A(inside_cell,inside_local);
            const Dune::FieldVector<DF,dim> n_F = ig.centerUnitOuterNormal();
            Dune::FieldVector<RF,dim> An_F;
            tensor_inside.mv(n_F,An_F);
            RF k_inside = n_F*An_F/param.viscosity(c_s);

            // contribution to residual on inside element
            mat_ss.accumulate(lfsu_s,0,lfsv_s,0, k_inside*face_volume/distance );

            return;
          }

        if (bctype==ConvectionDiffusionBoundaryConditions::Outflow)
          {
            // evaluate velocity field and outer unit normal
            Dune::FieldVector<DF,dim> iplocal_s = ig.geometryInInside().global(face_local);
            typename T::Traits::RangeType b = param.b(inside_cell,iplocal_s);
            const Dune::FieldVector<DF,dim> n = ig.centerUnitOuterNormal();

            // integrate o
            mat_ss.accumulate(lfsu_s,0,lfsv_s,0, (b*n)*face_volume );

            return;
          }
      }

      //! set time in parameter class
      void setTime (double t)
      {
        param.setTime(t);
      }

    private:
      T& param;
    };

    //! \} group GridFunctionSpace
  } // namespace PDELab
} // namespace Dune

#endif
