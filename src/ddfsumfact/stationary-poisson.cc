// -*- tab-width: 4; indent-tabs-mode: nil -*-
/** \file
    \brief High-level test with Poisson equation
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

// enable basis function caching for faster assembly
#define USECACHE 1

#ifdef ENABLE_COUNTER
#include <dune/pdelab/common/opcounter.hh>
#endif

#include<iostream>
#include<vector>
#include<map>
#include<chrono>
#include<tbb/task_scheduler_init.h>
#include<dune/common/parametertreeparser.hh>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/timer.hh>

#include <dune/pdelab/common/timer.hh>

#include<dune/grid/yaspgrid.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/paamg/amg.hh>
#include<dune/istl/superlu.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include<dune/pdelab/finiteelementmap/monomfem.hh>
#include<dune/pdelab/finiteelementmap/opbfem.hh>
#include<dune/pdelab/finiteelementmap/qkdg.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/constraints/p0.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/istlsolverbackend.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include<dune/pdelab/localoperator/convectiondiffusiondg.hh>
#include<dune/pdelab/localoperator/diffusionccfv.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>

#include <dune/grid/utility/partitioning/ranged.hh>
#include "../models/df_convectiondiffusionccfv.hh"
#include "../models/df_darcy_CCFV.hh"
#include "../models/maxvelocity.hh"
#include "../models/cdefastdg.hh"
#include "../models/l2gl.hh"

#include <dune/pdelab/common/lockmanager.hh>
#include <dune/pdelab/gridoperator/tbb.hh>

#include "../models/l2ob.hh"

#include <dune/common/archive.hh>

#include <dune/common/memory/blocked_allocator.hh>


#include <dune/pdelab/backend/istl/blockvectorbackend.hh>
#include <dune/pdelab/backend/istl/bellmatrixbackend.hh>
#include <dune/pdelab/backend/istl/flatvectorbackend.hh>
#include <dune/pdelab/backend/istl/flatmatrixbackend.hh>

#include <dune/pdelab/backend/istlvectorbackend.hh>
#include <dune/pdelab/backend/istl/bcrsmatrixbackend.hh>
#include <dune/pdelab/backend/seqistlsolverbackend.hh>

#include <dune/pdelab/ordering/redblackdg.hh>
#include <dune/pdelab/ordering/permutedordering.hh>

#include<dune/istl/preconditioners/sequentialblockjacobi.hh>
#include<dune/istl/preconditioners/sequentialjacobi.hh>
//#include<dune/istl/preconditioners/sequentialsor.hh>

#ifndef SIMD_BLOCK_SIZE
#define SIMD_BLOCK_SIZE 16
#endif


// FEM factories

#define _CONCAT(a,b) a ## b
#define CONCAT(a,b) _CONCAT(a,b)

#define GET_FEM(LT) CONCAT(LT,_fem)

template<typename Real, int degree, int dim>
Dune::PDELab::QkDGGLLocalFiniteElementMap<Real,Real,degree,dim>
sumfactorized_fem(const Dune::GeometryType& gt)
{
  return {};
}

template<typename Real, int degree, int dim>
Dune::PDELab::QkDGLocalFiniteElementMap<Real,Real,degree,dim>
dg_fem(const Dune::GeometryType& gt)
{
  return {};
}

template<typename Real, int degree, int dim>
Dune::PDELab::P0LocalFiniteElementMap<Real,Real,dim>
fv_fem(const Dune::GeometryType& gt)
{
  return {gt};
}



// local operator factories

#define GET_LOP(LT) CONCAT(LT,_operator)


template<typename Problem, typename GFS, int degree>
Dune::PDELab::ConvectionDiffusionDGFast<Problem,degree,2*degree+1,true,true,false,true,true,false>
sumfactorized_operator(Problem& problem, const GFS& gfs, std::integral_constant<int,degree>)
{
  return {problem,Dune::PDELab::ConvectionDiffusionDGFastMethod::SIPG,Dune::PDELab::ConvectionDiffusionDGFastWeights::weightsOn,3.0,true};
}

template<typename Problem, typename GFS, int degree>
Dune::PDELab::ConvectionDiffusionDG<Problem,typename GFS::Traits::FiniteElementMap>
dg_operator(Problem& problem, const GFS& gfs, std::integral_constant<int,degree>)
{
  Dune::PDELab::ConvectionDiffusionDG<Problem,typename GFS::Traits::FiniteElementMap> lop(
    problem,
    Dune::PDELab::ConvectionDiffusionDGMethod::SIPG,
    Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn,
    3.0,
    0,
    true
    );
  Dune::PDELab::LocalFunctionSpace<GFS> lfs(gfs);
  auto cit = gfs.gridView().template begin<0>();
  const auto& entity = *cit;
  lfs.bind(entity);
  lop.setupCache(lfs,entity);
  return lop;
}







template<typename GV, typename RF>
class Parameter
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  Parameter(const typename Traits::DomainType& upper_right)
    : _upper_right(upper_right)
  {}

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::PermTensorType I;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? 1 : 0;
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    typename Traits::RangeFieldType norm2 = xglobal.two_norm2();
    return (2.0*GV::dimension-4.0*norm2)*std::exp(-norm2);
  }

  //! boundary condition type function
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    auto midpoint = _upper_right;
    midpoint *= 0.5;
    xglobal -= midpoint;
    typename Traits::RangeFieldType norm2 = xglobal.two_norm2();
    return std::exp(-norm2);
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

private:
  const typename Traits::DomainType _upper_right;
};


inline void reset_timers()
{
  HP_TIMER_RESET(alpha_volume);
  HP_TIMER_RESET(alpha_skeleton);
  HP_TIMER_RESET(alpha_boundary);

  HP_TIMER_RESET(lambda_volume);
  HP_TIMER_RESET(lambda_skeleton);
  HP_TIMER_RESET(lambda_boundary);

  HP_TIMER_RESET(jacobian_volume);
  HP_TIMER_RESET(jacobian_skeleton);
  HP_TIMER_RESET(jacobian_boundary);
}

#ifdef ENABLE_COUNTER

#define DUMP_TIMER(name,os,reset)                       \
  os << "===== "  #name  " =====" << std::endl; \
  os << "elapsed: " << HP_TIMER_ELAPSED(name) << std::endl; \
  HP_TIMER_OPCOUNTERS(name).reportOperations(os,reset);

#define DUMP_AND_ACCUMULATE_TIMER(name,os,reset,time,ops)  \
  os << "===== "  #name " =====" << std::endl; \
  os << "elapsed: " << HP_TIMER_ELAPSED(name) << std::endl; \
  time += HP_TIMER_ELAPSED(name); \
  ops += HP_TIMER_OPCOUNTERS(name); \
  HP_TIMER_OPCOUNTERS(name).reportOperations(os,reset);

#elif defined ENABLE_HP_TIMERS

#define DUMP_TIMER(name,os,reset)                       \
  os << "===== "  #name  " =====" << std::endl; \
  os << "elapsed: " << HP_TIMER_ELAPSED(name) << std::endl;

#define DUMP_AND_ACCUMULATE_TIMER(name,os,reset,time,ops)  \
  os << "===== "  #name " =====" << std::endl; \
  os << "elapsed: " << HP_TIMER_ELAPSED(name) << std::endl; \
  time += HP_TIMER_ELAPSED(name);

#else

#define DUMP_TIMER(name,os,reset)
#define DUMP_AND_ACCUMULATE_TIMER(name,os,reset,time,ops)

#endif

template<typename Stream>
inline void dump_timers(Stream& os, bool reset)
{
  double t = 0.0;

#ifdef ENABLE_COUNTER
  auto counter = HP_TIMER_OPCOUNTERS(alpha_volume);
  counter.reset();
#endif

  DUMP_AND_ACCUMULATE_TIMER(alpha_volume,os,reset,t,counter);
  DUMP_AND_ACCUMULATE_TIMER(alpha_skeleton,os,reset,t,counter);
  DUMP_AND_ACCUMULATE_TIMER(alpha_boundary,os,reset,t,counter);

  DUMP_AND_ACCUMULATE_TIMER(lambda_volume,os,reset,t,counter);
  DUMP_AND_ACCUMULATE_TIMER(lambda_skeleton,os,reset,t,counter);
  DUMP_AND_ACCUMULATE_TIMER(lambda_boundary,os,reset,t,counter);

  DUMP_AND_ACCUMULATE_TIMER(jacobian_volume,os,reset,t,counter);
  DUMP_AND_ACCUMULATE_TIMER(jacobian_skeleton,os,reset,t,counter);
  DUMP_AND_ACCUMULATE_TIMER(jacobian_boundary,os,reset,t,counter);

  os << std::endl
     << "=========== local operator ===========" << std::endl
     << "elapsed: " << t << std::endl;
#ifdef ENABLE_COUNTER
  counter.reportOperations(os);
#endif
}

HP_DECLARE_TIMER(total);

//! solve problem with DG method
template<class GV, class PROBLEM, int degree, int blocksize, typename COUT, typename CERR>
void runDG ( const GV& gv,
             //const FEM& fem,
             PROBLEM& problem,
             std::string basename,
             int level,
             std::string method,
             std::string weights,
             const Dune::ParameterTree& params,
             COUT& cout, CERR& cerr)
{

  cout << "SIMD block size: " << SIMD_BLOCK_SIZE << std::endl;

#if LEGACY_ISTL
#if BLOCK_SOLVER
  cout << "blocked linear algebra (legacy implementation)" << std::endl;
#else
  cout << "scalar linear algebra (legacy implementation)" << std::endl;
#endif
#else
#if BLOCK_SOLVER
  cout << "blocked linear algebra" << std::endl;
#else
  cout << "scalar linear algebra" << std::endl;
#endif
#endif

  cout << "DG order: " << degree << std::endl;
  cout << "DG block size: " << blocksize << std::endl;

  Dune::Timer timer;

  // coordinate and result type
#ifdef ENABLE_COUNTER
  using Real = typename GV::ctype;
#else
  typedef double Real;
#endif

  const int dim = GV::Grid::dimension;

  std::stringstream fullname;
  fullname << basename << "_" << method << "_w" << weights << "_k" << degree << "_dim" << dim << "_level" << level << "_simd" << SIMD_BLOCK_SIZE;

  std::size_t chunk_size = params.get<std::size_t>("tunables.host.chunk_size",512);

  cout << "host thread chunk size: " << chunk_size << std::endl;

  // ****************************** Allocators ******************************

  typedef Dune::Memory::blocked_cache_aligned_allocator<double,std::size_t,SIMD_BLOCK_SIZE> HostAllocator;

  // ************************************************************************

  // make grid function space
  typedef Dune::PDELab::P0ParallelConstraints CON;
  //const Dune::PDELab::ISTLParameters::Blocking blocking
  //  = Dune::PDELab::ISTLParameters::static_blocking;
  //typedef Dune::PDELab::ISTLVectorBackend<blocking,blocksize> VBE;

#if LEGACY_ISTL
#if BLOCK_SOLVER
  typedef Dune::PDELab::ISTLVectorBackend<
    Dune::PDELab::ISTLParameters::static_blocking,
    blocksize
    > VBE;
  VBE vbe;
#else
  typedef Dune::PDELab::ISTLVectorBackend<> VBE;
  VBE vbe;
#endif
#else
#if BLOCK_SOLVER
  typedef Dune::PDELab::istl::BlockVectorBackend<HostAllocator> VBE;
  VBE vbe(blocksize);
#else
  typedef Dune::PDELab::istl::FlatVectorBackend<HostAllocator> VBE;
  VBE vbe;
#endif
#endif

  typedef Dune::PDELab::DefaultLeafOrderingTag OrderingTag;
  OrderingTag orderingTag;

  /*
  typedef Dune::PDELab::ordering::Permuted<
    Dune::PDELab::DefaultLeafOrderingTag
    > OrderingTag;

  OrderingTag orderingTag;
  std::size_t black_offset;

  std::tie(orderingTag.template permuted<1>().permutation(),black_offset) = Dune::PDELab::redBlackDGOrdering(gv);

  std::cout << "offset of black partition: " << black_offset << std::endl;
  */

#if not SINGLETHREAD
  // Partitioning
  typedef Dune::RangedPartitioning<GV, 0> Partitioning;
  std::shared_ptr<Partitioning> partitioning = std::make_shared<Partitioning>
    (gv, tbb::task_scheduler_init::default_num_threads());

  // Locking
  typedef Dune::PDELab::PerElementLockManager<
    GV, std::mutex> LockManager;
  std::shared_ptr<LockManager> lockManager =
    std::make_shared<LockManager>(gv);
#endif // !SINGLETHREAD


  cout << "Creating GFS and Ordering... " << std::flush;

  auto fem = GET_FEM(LOP_TYPE)<Real,degree,dim>(Dune::GeometryType(Dune::GeometryType::cube,dim));
  using FEM = decltype(fem);
  //typedef Dune::PDELab::QkDGGLLocalFiniteElementMap<Real,Real,degree,dim> FEM;
  //FEM fem;

  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE,OrderingTag> GFS;
  GFS gfs(gv,fem,vbe,orderingTag);
  gfs.name("u");
  gfs.ordering();

  cout << timer.elapsed() << std::endl;
  cout << gfs.ordering().size() << " DOFs, " << gfs.ordering().blockCount() << " blocks" << std::endl;

  auto lop = GET_LOP(LOP_TYPE)(problem,gfs,std::integral_constant<int,degree>());
  using LOP = decltype(lop);

  //typedef Dune::PDELab::ConvectionDiffusionDGFast<PROBLEM,degree,2*degree+1,true,true,false,true,true,false> LOP;
  //LOP lop(problem,Dune::PDELab::ConvectionDiffusionDGFastMethod::SIPG,Dune::PDELab::ConvectionDiffusionDGFastWeights::weightsOn,3.0,true);
  //typedef Dune::PDELab::ConvectionDiffusionDG<PROBLEM,FEM> LOP;
  //LOP lop(problem,Dune::PDELab::ConvectionDiffusionDGMethod::SIPG,Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn,3.0,0,true);

#if LEGACY_ISTL
  typedef typename Dune::PDELab::istl::BCRSMatrixBackend<> MBE;
#if BLOCK_SOLVER
  MBE mbe(7);
#else
  MBE mbe(7 * blocksize);
#endif
#else
#if BLOCK_SOLVER
  typedef typename Dune::PDELab::istl::BELLMatrixBackend<HostAllocator> MBE;
#else
  typedef typename Dune::PDELab::istl::FlatMatrixBackend<HostAllocator> MBE;
#endif
  MBE mbe;
#endif

  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<PROBLEM> G;
  G g(gv,problem);
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;

  timer.reset();
  cout << "Evaluating constraints... " << std::flush;

  Dune::PDELab::constraints(g,gfs,cc,false);

  cout << timer.elapsed() << std::endl;
  timer.reset();


#if SINGLETHREAD
  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop,mbe);
#else
  typedef Dune::PDELab::TBBGridOperator<
    Partitioning,
    GFS,GFS,
    LOP,
    MBE,Real,Real,Real,
    LockManager,
    CC,CC
    > GO;
  GO go(gfs,cc,gfs,cc,lop,lockManager,mbe);
  go.assembler().setPartitioning(partitioning);
#endif

  // make a vector of degree of freedom vectors and initialize it with Dirichlet extension
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0);
#if not LEGACY_ISTL
  raw(u).setChunkSize(chunk_size);
#endif

  std::shared_ptr<U> r;
  std::shared_ptr<typename GO::Traits::Jacobian> mat;

  if (!params.get<bool>("io.system.load",false))
    {
      r = std::make_shared<U>(gfs,0.0);
#if not LEGACY_ISTL
      raw(*r).setChunkSize(chunk_size);
#endif

      timer.reset();
      cout << "Evaluating residual... " << std::flush;

      HP_TIMER_START(total);
      go.residual(u,*r);
      HP_TIMER_STOP(total);

      cout << timer.elapsed() << std::endl;

      dump_timers(cout,true);
      DUMP_TIMER(total,cout,true);

      timer.reset();
      cout << "Creating matrix... " << std::flush;

#if LEGACY_ISTL
      mat = std::make_shared<typename GO::Traits::Jacobian>(go);
#else
      mat = std::make_shared<typename GO::Traits::Jacobian>(
        go,
#if BLOCK_SOLVER
        Dune::PDELab::istl::MatrixParameters(7) // we can hardcode the entries per row for this problem
#else
        Dune::PDELab::istl::MatrixParameters(params.sub("tunables.matrix"))
#endif
        );
#endif

      cout << timer.elapsed() << std::endl;

#if LEGACY_ISTL
      std::size_t nonzeros = Dune::PDELab::istl::raw(*mat).nonzeroes();
#else
      std::size_t nonzeros = raw(*mat).layout().nonzeros();
#endif
      cout << nonzeros << " effective non-zero entries" << std::endl;

#if not LEGACY_ISTL
      raw(*mat).setChunkSize(chunk_size);
#endif

      cout << "Evaluating jacobian... " << std::flush;

      timer.reset();

      HP_TIMER_START(total);
      go.jacobian(u,*mat);
      HP_TIMER_STOP(total);

      cout << timer.elapsed() << std::endl;

      dump_timers(cout,true);
      DUMP_TIMER(total,cout,true);

      cout << timer.elapsed() << std::endl;

      /*
      if (params.get<bool>("io.system.dump",false))
        {
          std::cout << "Dumping assembled system to '" << params["io.system.file"] << "'..." << std::endl;

          std::ofstream f(params["io.system.file"],std::ios::binary);
          Dune::BinaryOutStreamArchive<std::ofstream> ar(f);
          ar & Dune::PDELab::istl::raw(*r);
          ar & Dune::PDELab::istl::raw(*mat);
        }
      */
    }
  /*
  else
    {
      std::cout << "Loading assembled system from '" << params["io.system.file"] << "'..." << std::endl;

      std::ifstream f(params["io.system.file"],std::ios::binary);
      Dune::BinaryInStreamArchive<std::ifstream> ar(f);

      std::cout << "Loading residual..." << std::endl;

      r = std::make_shared<U>(gfs,Dune::PDELab::tags::unattached_container());
      auto r_raw = std::make_shared<typename Dune::PDELab::istl::raw_type<U>::type>();
      ar & *r_raw;
      r->attach(r_raw);
      raw(*r).setChunkSize(chunk_size);

      std::cout << "Loading matrix..." << std::endl;

      mat = std::make_shared<typename GO::Traits::Jacobian>(Dune::PDELab::tags::attached_container());
      ar & Dune::PDELab::istl::raw(*mat);
      raw(*mat).setChunkSize(chunk_size);
    }
  */

  // make linear solver and solve problem
  typedef Dune::PDELab::OverlappingOperator<
    CC,
    typename GO::Traits::Jacobian,
    typename GO::Traits::Domain,
    typename GO::Traits::Range
    > POP;
  POP pop(cc,*mat);

  Dune::PDELab::istl::ParallelHelper<GFS> helper(gfs);

  typedef Dune::PDELab::OverlappingScalarProduct<
    GFS,
    typename GO::Traits::Domain
    > PSP;
  PSP psp(gfs,helper);

  /*
  typedef Dune::ISTL::SequentialRedBlackBlockSOR<
    typename GO::Traits::Jacobian::Container,
    typename U::Container,
    typename U::Container
    > PC;
    PC pc(raw(mat),black_offset,1.0,false,5);
  */

#if LEGACY_ISTL
  typedef Dune::SeqJac<
    typename GO::Traits::Jacobian::BaseT,
    typename U::BaseT,
    typename U::BaseT,
    1> PC;
  PC pc(
    Dune::PDELab::istl::raw(*mat),
    params.get<int>("solver.jacobi.iterations",5),
    params.get<double>("solver.jacobi.relaxation_factor",1.0)
    );
#else
#if BLOCK_SOLVER
  typedef Dune::ISTL::SequentialBlockJacobi<
    typename GO::Traits::Jacobian::Container,
    typename U::Container,
    typename U::Container
    > PC;
  PC pc(
    raw(*mat),
    params.get<double>("solver.jacobi.relaxation_factor",1.0),
    params.get<bool>("solver.jacobi.lu_pivot",false),
    params.get<int>("solver.jacobi.iterations",5)
    );
#else
  typedef Dune::ISTL::SequentialJacobi<
    typename GO::Traits::Jacobian::Container,
    typename U::Container,
    typename U::Container
    > PC;
  PC pc(
    raw(*mat),
    params.get<double>("solver.jacobi.relaxation_factor",1.0),
    params.get<int>("solver.jacobi.iterations",5)
    );
#endif
#endif

  typedef Dune::PDELab::OverlappingWrappedPreconditioner<
    CC,
    GFS,
    PC
    > PPC;
  PPC ppc(gfs,pc,cc,helper);

  U z(gfs,0.0);
#if not LEGACY_ISTL
  raw(z).setChunkSize(chunk_size);
#endif

  int verbosity = gfs.gridView().comm().rank() == 0 ? params.get<int>("solver.verbosity",2) : 0;

  typedef decltype(std::chrono::high_resolution_clock::now()) TimePoint;

  TimePoint solve_start, solve_end;

  Dune::InverseOperatorResult stat;

  if (method=="SIPG")
    {
      typedef Dune::BiCGSTABSolver<U> Solver;

      Solver solver(
        pop,
        psp,
        ppc,
        params.get<double>("solver.reduction",1e-10),
        params.get<int>("solver.iterations",500),
        verbosity
        );

      solve_start = std::chrono::high_resolution_clock::now();
      solver.apply(z,*r,stat);
    }
  else
    {
      typedef Dune::BiCGSTABSolver<U> Solver;

      Solver solver(
        pop,
        psp,
        ppc,
        params.get<double>("solver.reduction",1e-10),
        params.get<int>("solver.iterations",500),
        verbosity
        );

      solve_start = std::chrono::high_resolution_clock::now();
      solver.apply(z,*r,stat);
    }

  solve_end = std::chrono::high_resolution_clock::now();

  u -= z;

  auto solve_time = std::chrono::duration_cast<std::chrono::duration<double> >(solve_end - solve_start);

  if (verbosity > 0)
    {
      cout << "Solver wallclock time: " <<  solve_time.count() << std::endl;
      cout << "wallclock time / iteration: " <<  (solve_time.count() / stat.iterations) << std::endl;
    }

#ifndef ENABLE_COUNTER
  if(params.get<bool>("io.vtk",false)){
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,std::max(0,degree-1));
    Dune::PDELab::addSolutionToVTKWriter(vtkwriter,gfs,u);
    vtkwriter.write(fullname.str(),Dune::VTK::appendedraw);
  }
#endif // ENABLE_COUNTER

}


template<typename Stream>
struct Rank0Stream
{

  Rank0Stream(const Dune::MPIHelper& mpi_helper, Stream& stream)
    : _mpi_helper(mpi_helper)
    , _stream(stream)
  {}

  template<typename T>
  Rank0Stream& operator<<(const T& t)
  {
    if (_mpi_helper.rank() == 0)
      _stream << t;
    return *this;
  }

  typedef std::ostream& (*ostream_manipulator)(std::ostream&);
  Rank0Stream& operator<<(ostream_manipulator pf)
  {
    return this->operator<< <ostream_manipulator> (pf);
  }

private:

  const Dune::MPIHelper& _mpi_helper;
  Stream& _stream;

};


int main(int argc, char** argv)
{
  try
    {
      if (argc != 2)
        {
          std::cerr << "Usage: " << argv[0] << " <ini file>" << std::endl;
          return 1;
        }

      Dune::ParameterTree params;
      Dune::ParameterTreeParser::readINITree(argv[1],params);

      // start up TBB task scheduler
      int threads = params.get<int>("tunables.host.threads",tbb::task_scheduler_init::default_num_threads());
      tbb::task_scheduler_init tbb_task_scheduler_init(threads);

      //Maybe initialize Mpi
      Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
      Rank0Stream<decltype(std::cout)> cout(helper,std::cout);
      Rank0Stream<decltype(std::cerr)> cerr(helper,std::cerr);
      if(Dune::MPIHelper::isFake)
        cout<< "This is a sequential program." << std::endl;
      else
        cout << "parallel run on " << helper.size() << " process(es)" << std::endl;


      cout << "Using " << threads << " threads" << std::endl;

      int degree_dyn = params.get<int>("discretization.order");
      int nx = params.get<int>("mesh.xcells");
      int ny = params.get<int>("mesh.ycells");
      int nz = params.get<int>("mesh.zcells");

      cout << "mesh size: " << nx << "x" << ny << "x" << nz << std::endl;

      int px = params.get<int>("mesh.xpartitions",0);
      int py = params.get<int>("mesh.ypartitions",0);
      int pz = params.get<int>("mesh.zpartitions",0);

      int overlap = params.get<int>("mesh.overlap",1);

      const int dim = 3;
      Dune::FieldVector<double,dim> L(0.0);
      L[0] = params.get<double>("mesh.xmax",1.0);
      L[1] = params.get<double>("mesh.ymax",1.0);
      L[2] = params.get<double>("mesh.zmax",1.0);
      std::array<int,dim> N;
      N[0] = nx; N[1] = ny; N[2] = nz;
      std::bitset<dim> B(false);

      std::shared_ptr<
        Dune::YLoadBalance<dim>
        > yp;
      if( px*py*pz==0 ){
        // If px,py,pz were not specified choose the default load balancer
        if( helper.rank() == 0 )
          std::cout << "Using default partitioning of YASP." << std::endl;
        yp = std::make_shared<Dune::YLoadBalanceDefault<dim> >();
      }

      else if( px*py*pz != helper.size() ){
        // If px*py*pz is not equal to the available number of processors
        // wrong input, stop and output warning!
        if( helper.rank()==0 )
          std::cerr << "Wrong input: px*py*pz != np" << std::endl;
        exit(1);
      }

      else {
        std::array<int,dim> yasppartitions;
        yasppartitions[0] = px;
        yasppartitions[1] = py;
        yasppartitions[2] = pz;
        yp = std::make_shared<Dune::YaspFixedSizePartitioner<dim> >(yasppartitions);
        if( helper.rank() == 0 )
          {
            std::cout << "Partitioning of YASP:";
            for (auto i : yasppartitions)
              std::cout << " " << i;
            std::cout << std::endl;
          }
      }

      typedef Dune::YaspGrid<dim> Grid;

      Grid grid(L,N,B,overlap,Grid::CollectiveCommunicationType(),yp.get());
      typedef Grid::LeafGridView GV;

      const GV& gv=grid.leafGridView();
      typedef Parameter<GV,typename GV::ctype> PROBLEM;
      PROBLEM problem(L);
      if (degree_dyn==1) {
        const int degree=1;
        //typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        //FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,PROBLEM,degree,blocksize>(gv,problem,"CUBE",0,"SIPG","ON",params,cout,cerr);
        }
      if (degree_dyn==2) {
        const int degree=2;
        //typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        //FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,PROBLEM,degree,blocksize>(gv,problem,"CUBE",0,"SIPG","ON",params,cout,cerr);
        }
      if (degree_dyn==3) {
        const int degree=3;
        //typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        //FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,PROBLEM,degree,blocksize>(gv,problem,"CUBE",0,"SIPG","ON",params,cout,cerr);
        }
      if (degree_dyn==4) {
        const int degree=4;
        //typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        //FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,PROBLEM,degree,blocksize>(gv,problem,"CUBE",0,"SIPG","ON",params,cout,cerr);
        }
      if (degree_dyn==5) {
        const int degree=5;
        //typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        //FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,PROBLEM,degree,blocksize>(gv,problem,"CUBE",0,"SIPG","ON",params,cout,cerr);
        }
      if (degree_dyn==6) {
        const int degree=6;
        //typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        //FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,PROBLEM,degree,blocksize>(gv,problem,"CUBE",0,"SIPG","ON",params,cout,cerr);
        }
      if (degree_dyn==7) {
        const int degree=7;
        //typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        //FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,PROBLEM,degree,blocksize>(gv,problem,"CUBE",0,"SIPG","ON",params,cout,cerr);
        }
      if (degree_dyn==8) {
        const int degree=8;
        //typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        //FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,PROBLEM,degree,blocksize>(gv,problem,"CUBE",0,"SIPG","ON",params,cout,cerr);
        }

    }
  catch (Dune::Exception &e)
    {
      std::cerr << "Dune reported error: " << e << std::endl;
      return 1;
    }
  catch (...)
    {
      std::cerr << "Unknown exception thrown!" << std::endl;
      return 1;
    }
}
