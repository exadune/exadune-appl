// -*- tab-width: 4; indent-tabs-mode: nil -*-
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <memory>
#if not SINGLETHREAD
#include <mutex>

#include <tbb/task_scheduler_init.h>
#endif // !SINGLETHREAD

#include <dune/common/exceptions.hh>
#include <dune/common/forloop.hh>

#if not SINGLETHREAD
#include <dune/grid/utility/partitioning/ranged.hh>
#endif // !SINGLETHREAD

#include <dune/pdelab/boilerplate/pdelab.hh>
#if not SINGLETHREAD
#include <dune/pdelab/common/lockmanager.hh>
#endif // !SINGLETHREAD
#include <dune/pdelab/localoperator/convectiondiffusiondg.hh>
#include <dune/pdelab/localoperator/l2.hh>

#include "../models/df_convectiondiffusionccfv.hh"
#include "../models/df_darcy_CCFV.hh"
#include "../models/maxvelocity.hh"
#include "../models/cdefastdg.hh"
#include "../models/l2gl.hh"
#include "../models/l2ob.hh"

#include "problem_flow.hh"
#include "problem_transport.hh"

//#define BASELINE
//#define DIAGONAL

#ifndef MIN_DEGREE
#define MIN_DEGREE 2
#endif
#ifndef MAX_DEGREE
#define MAX_DEGREE 3
#endif

//***********************************************************************
//***********************************************************************
// solve flow and transport problem
//***********************************************************************
//***********************************************************************

template<typename Grid, int degree, Dune::GeometryType::BasicType elemtype,
         Dune::PDELab::MeshType meshtype, Dune::SolverCategory::Category solvertype>
void do_simulation_dg (Grid& grid, std::string basename, Dune::ParameterTree& ptree)
{
  // define parameters
  typedef typename Grid::Grid GM;
  const unsigned int dim = GM::dimension;
  typedef double NumberType;

  // extract parameters from ptree
  if (grid->leafGridView().comm().rank()==0) std::cout << "extract parameters" << std::endl;
  bool implicit(true);
  if (ptree["simulation.method"]=="explicit") implicit=false;
  int order = ptree.get<int>("simulation.order");
  int every = ptree.get<int>("output.every");
  // bool uselimiter = ptree.get<bool>("simulation.uselimiter");
  // double theta = ptree.get<double>("simulation.theta");

  NumberType LX,LY,LZ;
  int NX,NY,NZ;
  NumberType hx,hy,hz;
  LX = ptree.get<double>("grid.structured.LX");
  LY = ptree.get<double>("grid.structured.LY");
  if (dim==3) LZ = ptree.get<double>("grid.structured.LZ");
  NX = ptree.get<int>("grid.structured.NX");
  NY = ptree.get<int>("grid.structured.NY");
  if (dim==3) NZ = ptree.get<int>("grid.structured.NZ");
  hx = LX/NX;
  hy = LY/NY;
  if (dim==3) hz = LZ/NZ;
  NumberType h=hx; h=std::min(h,hy); if (dim==3) h=std::min(h,hz);
  int steps = ptree.get<int>("simulation.steps");
  NumberType Cr = ptree.get<double>("simulation.Courant");
  NumberType dtmaxi = ptree.get<double>("simulation.dtmaxi");
  NumberType dtmaxe = ptree.get<double>("simulation.dtmaxe");

  //=============================
  // set up flow part
  //=============================
  if (grid->leafGridView().comm().rank()==0) std::cout << "set up flow part" << std::endl;

  // flow part
  typedef Dune::PDELab::P0Space<GM,NumberType,elemtype,solvertype> FSF;
  FSF fsf(grid->leafGridView());
  typedef typename FSF::DOF VF;
  VF xf(fsf.getGFS(),0.0);
  typename FSF::DGF xfdgf(fsf.getGFS(),xf);

  // the problem
  typedef GenericFlowProblem<typename GM::LeafGridView,NumberType> ProblemF;
  ProblemF problemf(ptree);

  // make problem parameters
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<ProblemF> BCTypeF;
  BCTypeF bctypef(grid->leafGridView(),problemf);

  // initialize DOF vector with a function
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<ProblemF> GF;
  GF gf(grid->leafGridView(),problemf);
  Dune::PDELab::interpolate(gf,fsf.getGFS(),xf);

  // assemble constraints
  fsf.assembleConstraints(bctypef);
  fsf.setNonConstrainedDOFS(xf,0.0);


  //=============================
  // set up transport part
  //=============================
  if (grid->leafGridView().comm().rank()==0) std::cout << "set up transport (first part)" << std::endl;

  //typedef Dune::PDELab::DGPkSpace<GM,NumberType,degree,elemtype,solvertype> FS;
#ifdef BASELINE
#ifdef DIAGONAL
  typedef Dune::PDELab::DGQkOPBSpace<GM,NumberType,degree,elemtype,solvertype> FS;
#else
  typedef Dune::PDELab::DGQkSpace<GM,NumberType,degree,elemtype,solvertype> FS;
#endif
#else
  typedef Dune::PDELab::DGQkGLSpace<GM,NumberType,degree,elemtype,solvertype> FS;
#endif
  FS fs(grid->leafGridView());

  // x is the concentration solution
  typedef typename FS::DOF V;
  V x(fs.getGFS(),0.0);

  // make the darcy velocity depending on pressure (xf) and concentration (x)
  if (fs.getGFS().gridView().comm().rank()==0) std::cout << "make Darcy velocity ...";
  typedef DFDarcyVelocityFromHeadCCFV<ProblemF,typename FSF::DGF,typename FS::GFS,V> DarcyDGF;
  DarcyDGF darcydgf(problemf,xfdgf,fs.getGFS(),x);
  if (fs.getGFS().gridView().comm().rank()==0) std::cout << " done" << std::endl;

  // make transport problem
  typedef GenericTransportProblem<typename GM::LeafGridView,DarcyDGF> Problem;
  Problem problem(ptree,darcydgf);
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditionAdapter<Problem> BCType;
  BCType bctype(grid->leafGridView(),problem);

  // set initial value
  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<Problem> GT;
  GT gt(grid->leafGridView(),problem);
  problem.setTime(0.0);
  Dune::PDELab::interpolate(gt,fs.getGFS(),x);

  // initial mass computation
  // {
  //   typename FS::DGF::Traits::RangeType totalmass;
  //   typename FS::DGF xdgf(fs.getGFS(),x);
  //   integrateGridFunction(xdgf,totalmass,4);
  //   NumberType mass = fs.getGFS().gridView().comm().sum(totalmass);
  //   if (fs.getGFS().gridView().comm().rank()==0)
  //     std::cout << "MASS " << time << " " << mass << std::endl;
  // }

#if not SINGLETHREAD
  // Partitioning
  typedef Dune::RangedPartitioning<typename GM::LeafGridView, 0> Partitioning;
  std::shared_ptr<Partitioning> partitioning = std::make_shared<Partitioning>
    (grid->leafGridView(), tbb::task_scheduler_init::default_num_threads());

  // Locking
  typedef Dune::PDELab::PerElementLockManager<
    typename GM::LeafGridView, std::mutex> LockManager;
  std::shared_ptr<LockManager> lockManager =
    std::make_shared<LockManager>(grid->leafGridView());
#endif // !SINGLETHREAD

  //=============================
  // solve flow problem for initial guess
  //=============================
  if (fs.getGFS().gridView().comm().rank()==0) std::cout << "solve flow problem" << std::endl;

  // assembler for flow problem
  typedef Dune::PDELab::DFConvectionDiffusionCCFV<ProblemF,typename FS::GFS,V> LOPF;
  LOPF lopf(problemf,fs.getGFS(),x);
#if SINGLETHREAD
  typedef Dune::PDELab::GalerkinGlobalAssemblerNewBackend<FSF,LOPF,solvertype> ASSEMBLERF;
  ASSEMBLERF assemblerf(fsf,lopf,typename ASSEMBLERF::MBE(2*dim+1));
#else // !SINGLETHREAD
  typedef Dune::PDELab::TBBGalerkinGlobalAssemblerNewBackend<
    Partitioning, LockManager, FSF, LOPF, solvertype> ASSEMBLERF;
  ASSEMBLERF assemblerf(lockManager, fsf, lopf,
                        typename ASSEMBLERF::MBE(2*dim+1));
  assemblerf->assembler().setPartitioning(partitioning);
#endif // !SINGLETHREAD

  // make linear solver and solve problem
  // typedef Dune::PDELab::ISTLSolverBackend_CG_SSOR <FSF,ASSEMBLERF,solvertype> SBEF;
  // SBEF sbef(fsf,assemblerf,5000,2,2);
  //typedef Dune::PDELab::ISTLSolverBackend_IterativeDefault<FSF,ASSEMBLERF,solvertype> SBE;
  typedef Dune::PDELab::ISTLSolverBackend_CG_AMG_SSOR<FSF,ASSEMBLERF,solvertype> SBEF;
  SBEF sbef(fsf,assemblerf,5000,1);
  typedef Dune::PDELab::StationaryLinearProblemSolver<typename ASSEMBLERF::GO,typename SBEF::LS,VF> SLPF;
  SLPF slpf(*assemblerf,*sbef,xf,1e-12);

  // solve flow problem
  xf = 0.0;
  slpf.apply();
  darcydgf.update(fs.getGFS(),x); // the first Darcy velocity, we now have both, pressure and concentration

  // compute optimal time step size
  NumberType dt; // This is the time step size
  {
    NumberType maxv = maxvelocity(darcydgf);
    NumberType dtconvective = Cr*h/(((double)dim)*maxv); // convective time step
    if (implicit)
      dt = std::min(2*dtmaxi,dtconvective);
    else
      dt = std::min(2*dtmaxe,dtconvective);
    if (fs.getGFS().gridView().comm().rank()==0)
      {
        std::cout << "maximum velocity = " << maxv << " " << 0.0 << std::endl;
        std::cout << "dtconvective = " << dtconvective << " " << 0.0 << std::endl;
        std::cout << "h = " << h << " m" << std::endl;
        std::cout << "taking dt = " << dt << " s " << "implicit=" << implicit << std::endl;
      }
  }

  // graphics for initial guess
  if (fs.getGFS().gridView().comm().rank()==0) std::cout << "write first file" << std::endl;
  Dune::PDELab::FilenameHelper fn(basename);
  if (every>0)
    { // start a new block to automatically delete the VTKWriter object
      Dune::SubsamplingVTKWriter<typename GM::LeafGridView> vtkwriter(grid->leafGridView(),std::max(0,degree-1));
      vtkwriter.addVertexData(std::make_shared<typename FSF::VTKF>(xfdgf,"p_h"));
      typename FS::DGF xdgf(fs.getGFS(),x);
      vtkwriter.addVertexData(std::make_shared<typename FS::VTKF>(xdgf,"c_h"));
      typedef Dune::PDELab::VTKGridFunctionAdapter<DarcyDGF> DarcyVTKDGF;
      vtkwriter.addVertexData(std::make_shared<DarcyVTKDGF>(darcydgf,"v_h"));
      vtkwriter.pwrite(fn.getName(),"vtk","",Dune::VTK::appendedraw);
      fn.increment();
    }

  //=============================
  // set up the transport problem
  //=============================
  if (fs.getGFS().gridView().comm().rank()==0) std::cout << "set up transport (second part)" << std::endl;

  // assemblers for finite element problem
#ifdef BASELINE
  typedef Dune::PDELab::ConvectionDiffusionDG<Problem,typename FS::FEM> LOP;
  LOP lop(problem,Dune::PDELab::ConvectionDiffusionDGMethod::SIPG,Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn,3.0);
#else
  typedef Dune::PDELab::ConvectionDiffusionDGFast<Problem,degree,2*degree+1,true,true,false,false,true,true> LOP;
  LOP lop(problem,Dune::PDELab::ConvectionDiffusionDGFastMethod::SIPG,Dune::PDELab::ConvectionDiffusionDGFastWeights::weightsOn,3.0);
#endif
#if SINGLETHREAD
  typedef Dune::PDELab::GalerkinGlobalAssemblerNewBackend<FS,LOP,solvertype> SASS;
  SASS sass(fs,lop,typename SASS::MBE( 1 /*(2*dim+1)*Dune::QkStuff::QkSize<degree,dim>::value*/ ) );
#else // !SINGLETHREAD
  typedef Dune::PDELab::TBBGalerkinGlobalAssemblerNewBackend<
    Partitioning, LockManager, FS, LOP, solvertype> SASS;
  SASS sass(lockManager, fs, lop,
            typename SASS::MBE( 1 /*(2*dim+1)*Dune::QkStuff::QkSize<degree,dim>::value*/ ) );
  sass->assembler().setPartitioning(partitioning);
#endif // !SINGLETHREAD

#ifdef BASELINE
#ifdef DIAGONAL
  typedef Dune::PDELab::L2OBCached<typename FS::FEM> MLOP;
  MLOP mlop(2*degree);
#else
  typedef Dune::PDELab::L2 MLOP;
  MLOP mlop(2*degree);
#endif
#else
  typedef Dune::PDELab::L2GL<FS::dim,typename FS::ctype,NumberType,degree,true> MLOP;
  MLOP mlop;
#endif
#if SINGLETHREAD
  typedef Dune::PDELab::GalerkinGlobalAssemblerNewBackend<FS,MLOP,solvertype> TASS;
  TASS tass(fs,mlop,typename TASS::MBE( 1 /*Dune::QkStuff::QkSize<degree,dim>::value*/ ) );
#else // !SINGLETHREAD
  typedef Dune::PDELab::TBBGalerkinGlobalAssemblerNewBackend<
    Partitioning, LockManager, FS, MLOP, solvertype> TASS;
  TASS tass(lockManager, fs, mlop,
            typename TASS::MBE( 1 /*Dune::QkStuff::QkSize<degree,dim>::value*/ ) );
  tass->assembler().setPartitioning(partitioning);
#endif // !SINGLETHREAD

  // // ==== make jacobian for tests
  // typename TASS::GO::Traits::Jacobian D(tass.getGO());
  // std::cout << D.patternStatistics() << std::endl;
  // // ====

  // typedef Dune::PDELab::OneStepGlobalAssembler<SASS,TASS,true> ASSEMBLERI;
  // ASSEMBLERI assembleri(sass,tass);
  typedef Dune::PDELab::OneStepGlobalAssembler<SASS,TASS,false> ASSEMBLERE;
  ASSEMBLERE assemblere(sass,tass);

  // linear solver backends
  // typedef Dune::PDELab::ISTLSolverBackend_IterativeDefault<FS,ASSEMBLERI,solvertype> SBEI;
  // SBEI sbei(fs,assembleri,5000,1);
  typedef Dune::PDELab::ISTLSolverBackend_ExplicitDiagonal<FS,ASSEMBLERE,solvertype> SBEE;
  SBEE sbee(fs,assemblere,5000,0);

  // linear problem solver
  // typedef Dune::PDELab::StationaryLinearProblemSolver<typename ASSEMBLERI::GO,typename SBEI::LS,V> PDESOLVERI;
  // PDESOLVERI pdesolveri(*assembleri,*sbei,1e-12);
  typedef Dune::PDELab::StationaryLinearProblemSolver<typename ASSEMBLERE::GO,typename SBEE::LS,V> PDESOLVERE;
  PDESOLVERE pdesolvere(*assemblere,*sbee,1e-12);

  // implicit time-stepper
  // Dune::PDELab::OneStepThetaParameter<NumberType> methodi1(1.0);
  // Dune::PDELab::Alexander2Parameter<NumberType> methodi2;
  // Dune::PDELab::Alexander3Parameter<NumberType> methodi3;
  // Dune::PDELab::TimeSteppingParameterInterface<NumberType>* pmethodi;
  // if (order==1) pmethodi = &methodi1;
  // if (order==2) pmethodi = &methodi2;
  // if (order==3) pmethodi = &methodi2;
  // typedef Dune::PDELab::OneStepMethod<NumberType,typename ASSEMBLERI::GO,PDESOLVERI,V> OSMI;
  // OSMI osmi(*pmethodi,*assembleri,pdesolveri);
  // osmi.setVerbosityLevel(2);

  // explicit time-stepper
  Dune::PDELab::ExplicitEulerParameter<NumberType> methode1;
  Dune::PDELab::HeunParameter<NumberType> methode2;
  Dune::PDELab::Shu3Parameter<NumberType> methode3;
  Dune::PDELab::RK4Parameter<NumberType> methode4;
  typedef Dune::PDELab::SimpleTimeController<NumberType> TC;
  TC tc;
  Dune::PDELab::TimeSteppingParameterInterface<NumberType>* pmethode;
  if (order>=1) pmethode = &methode1;
  if (order>=2) pmethode = &methode2;
  //if (order>=3) pmethode = &methode3;
  //if (order>=4) pmethode = &methode4;
  int stages = std::min(order,4);
  typedef Dune::PDELab::ExplicitOneStepMethod<NumberType,typename ASSEMBLERE::GO,typename SBEE::LS,V,V,TC> OSME;
  OSME osme(*pmethode,*assemblere,*sbee,tc);
  osme.setVerbosityLevel(2);

  // time loop
  NumberType time = 0.0;
  int step = 1;
  if (fs.getGFS().gridView().comm().rank()==0) std::cout << "enter time loop" << std::endl;
  while (step <= steps)
    {
      //=============================
      // do one transport step
      //=============================

      // Darcy velocity is up to date

      // assemble constraints for new time step (assumed to be constant for all substeps)
      problem.setTime(time+0.5*dt);
      fs.assembleConstraints(bctype);

      // do time step
      V xnew(x);

      // start timer
      fs.getGFS().gridView().comm().barrier();
      Dune::Timer timer;
      // if (implicit)
      //   osmi.apply(time,0.5*dt,x,xnew);
      // else {
      osme.apply(time,0.5*dt,x,xnew);
      // }
      fs.getGFS().gridView().comm().barrier();
      double elapsed_ = timer.elapsed();
      if (fs.getGFS().gridView().comm().rank()==0) {
        std::cout << "one explicit time step=" << elapsed_ << " time/dof/stage=" << elapsed_/(stages*x.flatsize()) << " size=" << x.flatsize() << std::endl;
      }

      // Dune::Timer timer2;
      // V rnew(fs.getGFS(),0.0);
      // V beta(fs.getGFS(),0.0);
      // timer2.reset();
      // sass->residual(x,rnew);
      // double elapsed2 = timer2.elapsed();
      // std::cout << "one spatial residual comp with constraints: " << elapsed2 << " time/dof=" << elapsed2/(rnew.flatsize()) << std::endl;
      // fs.clearConstraints(); timer2.reset();
      // sass->residual(x,rnew);
      // elapsed2 = timer2.elapsed();
      // std::cout << "one spatial residual comp without constraints: " << elapsed2 << " time/dof=" << elapsed2/(rnew.flatsize()) << std::endl;
      //timer2.reset();
      // tass->residual(x,rnew);
      // elapsed2 = timer2.elapsed();
      // std::cout << "one temporal residual comp: " << elapsed2 << " time/dof=" << elapsed2/(rnew.flatsize()) << std::endl;
      // timer2.reset();
      // tass->jacobian(x,D);
      // elapsed2 = timer2.elapsed();
      // std::cout << "one temporal jacobian comp: " << elapsed2 << " time/dof=" << elapsed2/(rnew.flatsize()) << std::endl;
      // std::vector<V*> xxx(2); xxx[0] = &x; xxx[1] = &xnew;
      // timer2.reset();
      // assemblere->explicit_jacobian_residual(1,xxx,D,rnew,beta);
      // elapsed2 = timer2.elapsed();
      // std::cout << "one combined jacobian_residual comp: " << elapsed2 << " time/dof=" << elapsed2/(rnew.flatsize()) << std::endl;

      // accept time step
      x = xnew;
      time += 0.5*dt;

      //=============================
      // solve the flow problem after half time step
      //=============================

      // assembler for flow problem
      typedef Dune::PDELab::DFConvectionDiffusionCCFV<ProblemF,typename FS::GFS,V> LOPF;
      LOPF lopf(problemf,fs.getGFS(),x); // now here the flow problem depends on time through x
#if SINGLETHREAD
      typedef Dune::PDELab::GalerkinGlobalAssemblerNewBackend<FSF,LOPF,solvertype> ASSEMBLERF;
      ASSEMBLERF assemblerf(fsf,lopf,typename ASSEMBLERF::MBE(2*dim+1));
#else // !SINGLETHREAD
      typedef Dune::PDELab::TBBGalerkinGlobalAssemblerNewBackend<
        Partitioning, LockManager, FSF, LOPF, solvertype> ASSEMBLERF;
      ASSEMBLERF assemblerf(lockManager, fsf, lopf,
                            typename ASSEMBLERF::MBE(2*dim+1));
      assemblerf->assembler().setPartitioning(partitioning);
#endif // !SINGLETHREAD

      // make linear solver and solve problem
      // typedef Dune::PDELab::ISTLSolverBackend_CG_SSOR <FSF,ASSEMBLERF,solvertype> SBEF;
      // SBEF sbef(fsf,assemblerf,5000,2,2);
      //typedef Dune::PDELab::ISTLSolverBackend_IterativeDefault<FSF,ASSEMBLERF,solvertype> SBE;
      typedef Dune::PDELab::ISTLSolverBackend_CG_AMG_SSOR<FSF,ASSEMBLERF,solvertype> SBEF;
      SBEF sbef(fsf,assemblerf,5000,1);
      typedef Dune::PDELab::StationaryLinearProblemSolver<typename ASSEMBLERF::GO,typename SBEF::LS,VF> SLPF;
      SLPF slpf(*assemblerf,*sbef,xf,1e-12);

      // solve flow problem
      slpf.apply();

      // update Darcy velocity as we have new pressure and concentration
      darcydgf.update(fs.getGFS(),x);

      // compute new time step
      NumberType maxv = maxvelocity(darcydgf);
      NumberType dtconvective = Cr*h/(((double)dim)*maxv); // convective time step
      if (implicit)
        dt = std::min(2*dtmaxi,dtconvective);
      else
        dt = std::min(2*dtmaxe,dtconvective);
      if (fs.getGFS().gridView().comm().rank()==0)
        {
          std::cout << "maximum velocity = " << maxv << " " << time << std::endl;
          std::cout << "dtconvective = " << dtconvective << " " << time << std::endl;
          std::cout << "h = " << h << " m" << std::endl;
          std::cout << "taking dt = " << dt << " s " << "implicit=" << implicit << std::endl;
        }

      //=============================
      // solve the transport problem again
      //=============================

      // assemble constraints for new time step (assumed to be constant for all substeps)
      problem.setTime(time+0.5*dt);
      fs.assembleConstraints(bctype);

      // do time step
      xnew = x;
      // if (implicit)
      //   osmi.apply(time,0.5*dt,x,xnew);
      // else {
      osme.apply(time,0.5*dt,x,xnew);
      // }

      // timer evaluation at end of the full step, excluding output
      // double elapsed = timer.elapsed();
      // fs.getGFS().gridView().comm().barrier();
      // if (fs.getGFS().gridView().comm().rank()==0)
      //   std::cout << "elapsed time per full time step : " << elapsed << std::endl;

      // mass computation
      // {
      //   typename FS::DGF::Traits::RangeType totalmass;
      //   typename FS::DGF xdgf(fs.getGFS(),xnew);
      //   integrateGridFunction(xdgf,totalmass,4);
      //   NumberType mass = fs.getGFS().gridView().comm().sum(totalmass);
      //   if (fs.getGFS().gridView().comm().rank()==0)
      //     std::cout << "MASS " << time << " " << mass << std::endl;
      // }

      // output to VTK file
      if (every>0)
        if (step%every==0)
          {
            Dune::SubsamplingVTKWriter<typename GM::LeafGridView> vtkwriter(grid->leafGridView(),std::max(0,degree-1));
            vtkwriter.addVertexData(std::make_shared<typename FSF::VTKF>(xfdgf,"p_h"));
            typename FS::DGF xdgf(fs.getGFS(),xnew);
            vtkwriter.addVertexData(std::make_shared<typename FS::VTKF>(xdgf,"c_h"));
            typedef Dune::PDELab::VTKGridFunctionAdapter<DarcyDGF> DarcyVTKDGF;
            vtkwriter.addVertexData(std::make_shared<DarcyVTKDGF>(darcydgf,"v_h"));
            vtkwriter.pwrite(fn.getName(),"vtk","",Dune::VTK::appendedraw);
            if (fs.getGFS().gridView().comm().rank()==0)
              std::cout << "WRITE FILE " << fn.getName() << " " << time+0.5*dt << std::endl;
            fn.increment();
          }

      // accept time step
      x = xnew;
      time += 0.5*dt;
      step++;
    }
}

// selecting the order

template<typename Grid, Dune::GeometryType::BasicType elemtype,
         Dune::PDELab::MeshType meshtype,
         Dune::SolverCategory::Category solvertype>
struct SelectDegree
{
  template<int static_degree>
  struct Operation
  {
    static void apply(Grid& grid, const std::string &basename,
                      Dune::ParameterTree& ptree, int dynamic_degree)
    {
      if(static_degree == dynamic_degree)
        do_simulation_dg<Grid,static_degree,elemtype,meshtype,solvertype>
          (grid,basename,ptree);
    }
  };
};

template<typename Grid, Dune::GeometryType::BasicType elemtype,
         Dune::PDELab::MeshType meshtype,
         Dune::SolverCategory::Category solvertype>
void select_degree (Grid& grid, const std::string &basename,
                    Dune::ParameterTree& ptree, int degree)
{
  if(degree < MIN_DEGREE || degree > MAX_DEGREE)
    DUNE_THROW(Dune::NotImplemented, "Base functions of degree " << degree <<
               " (should be between " << MIN_DEGREE << " and " << MAX_DEGREE <<
               " inclusive).  Adjust the preprocessor defines MIN_DEGREE "
               "and/or MAX_DEGREE compile-in support for more degrees.");
  Dune::ForLoop<SelectDegree<Grid, elemtype, meshtype, solvertype>::
                  template Operation, MIN_DEGREE, MAX_DEGREE>
    ::apply(grid, basename, ptree, degree);
}

//***********************************************************************
//***********************************************************************
// the main function
//***********************************************************************
//***********************************************************************

int main(int argc, char **argv)
{
  // initialize MPI, finalize is done automatically on exit
  Dune::MPIHelper::instance(argc,argv);

  // parse parameters
  Dune::ParameterTree ptree;
  Dune::ParameterTreeParser ptreeparser;
  ptreeparser.readINITree("density_flow.ini",ptree);
  ptreeparser.readOptions(argc,argv,ptree);

  // parameters that are the same for all runs
  const Dune::SolverCategory::Category solvertype = Dune::SolverCategory::overlapping;
  const Dune::PDELab::MeshType meshtype = Dune::PDELab::MeshType::conforming;
  std::string basename = ptree.get<std::string>("output.basename");
  int order= ptree.get<int>("simulation.order");

  try {
    if (ptree.get("grid.dim",(int)2)==2)
      {
        const unsigned int dim=2;
        if (ptree["grid.type"]=="structured")
          {
            Dune::array<double,dim> lower_left;
            lower_left[0] = 0.0;
            lower_left[1] = 0.0;
            Dune::array<double,dim> upper_right;
            upper_right[0] = ptree.get("grid.structured.LX",(double)1.0);
            upper_right[1] = ptree.get("grid.structured.LY",(double)1.0);
            Dune::array<unsigned int,dim> cells;
            cells[0] = ptree.get("grid.structured.NX",(int)10);
            cells[1] = ptree.get("grid.structured.NY",(int)10);

            if (ptree["grid.manager"]=="yasp")
              {
                // make grid
                const Dune::GeometryType::BasicType elemtype = Dune::GeometryType::cube;
                typedef Dune::YaspGrid<dim> GM;
                typedef Dune::PDELab::StructuredGrid<GM> Grid;
                Grid grid(elemtype,lower_left,upper_right,cells);
                grid->refineOptions(false);
                grid->globalRefine(ptree.get("grid.refinement",(int)0));
                bool uselimiter = ptree.get<bool>("simulation.uselimiter");
                std::string lim("nolimiter");
                if (uselimiter) lim="withlimiter";
                std::stringstream fullbasename;
                fullbasename << basename << "_" << ptree["simulation.method"]
                             << "_" << lim << "_dim" << dim << "_order"
                             << order;
#if SINGLETHREAD
                fullbasename << "_st";
#endif
                select_degree<Grid,elemtype,meshtype,solvertype>
                  (grid,fullbasename.str(),ptree,order);
              }
          }
      }

    if (ptree.get("grid.dim",(int)2)==3)
      {
        const unsigned int dim=3;
        if (ptree["grid.type"]=="structured")
          {
            Dune::array<double,dim> lower_left;
            lower_left[0] = 0.0;
            lower_left[1] = 0.0;
            lower_left[2] = 0.0;
            Dune::array<double,dim> upper_right;
            upper_right[0] = ptree.get("grid.structured.LX",(double)1.0);
            upper_right[1] = ptree.get("grid.structured.LY",(double)1.0);
            upper_right[2] = ptree.get("grid.structured.LZ",(double)1.0);
            Dune::array<unsigned int,dim> cells;
            cells[0] = ptree.get("grid.structured.NX",(int)10);
            cells[1] = ptree.get("grid.structured.NY",(int)10);
            cells[2] = ptree.get("grid.structured.NZ",(int)10);

            if (ptree["grid.manager"]=="yasp")
              {
                // make grid
                const Dune::GeometryType::BasicType elemtype = Dune::GeometryType::cube;
                typedef Dune::YaspGrid<dim> GM;
                typedef Dune::PDELab::StructuredGrid<GM> Grid;
                Grid grid(elemtype,lower_left,upper_right,cells);
                grid->refineOptions(false);
                grid->globalRefine(ptree.get("grid.refinement",(int)0));
                bool uselimiter = ptree.get<bool>("simulation.uselimiter");
                std::string lim("nolimiter");
                if (uselimiter) lim="withlimiter";
                std::stringstream fullbasename;
                fullbasename << basename << "_" << ptree["simulation.method"]
                             << "_" << lim << "_dim" << dim << "_order"
                             << order;
#if SINGLETHREAD
                fullbasename << "_st";
#endif
                select_degree<Grid,elemtype,meshtype,solvertype>
                  (grid,fullbasename.str(),ptree,order);
              }
          }
      }

  }
  catch (std::exception & e) {
    std::cout << "STL ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (Dune::Exception & e) {
    std::cout << "DUNE ERROR: " << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cout << "Unknown ERROR" << std::endl;
    return 1;
  }

  // done
  return 0;
}
