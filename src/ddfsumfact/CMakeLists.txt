add_executable(density_flow density_flow.cc)
add_executable(density_flow_st density_flow.cc)

add_executable(stationary-poisson-blocked stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-blocked
  PUBLIC
  SIMD_BLOCK_SIZE=16
  BLOCK_SOLVER=1
  LOP_TYPE=sumfactorized
  )

add_executable(stationary-poisson-blocked-old stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-blocked-old
  PUBLIC
  SIMD_BLOCK_SIZE=16
  BLOCK_SOLVER=1
  LOP_TYPE=dg
  )

add_executable(stationary-poisson-blocked-counted stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-blocked-counted
  PUBLIC
  SIMD_BLOCK_SIZE=16
  BLOCK_SOLVER=1
  LOP_TYPE=sumfactorized
  ENABLE_HP_TIMERS
  ENABLE_COUNTER
  )

add_executable(stationary-poisson-blocked-timed stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-blocked-timed
  PUBLIC
  SIMD_BLOCK_SIZE=16
  BLOCK_SOLVER=1
  LOP_TYPE=sumfactorized
  ENABLE_HP_TIMERS
  )

add_executable(stationary-poisson-blocked-singlethreaded-assembly stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-blocked-singlethreaded-assembly
  PUBLIC
  SIMD_BLOCK_SIZE=16
  BLOCK_SOLVER=1
  SINGLETHREAD=1
  LOP_TYPE=sumfactorized
  )

add_executable(stationary-poisson-blocked-legacy stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-blocked-legacy
  PUBLIC
  SIMD_BLOCK_SIZE=16
  BLOCK_SOLVER=1
  LEGACY_ISTL=1
  LOP_TYPE=sumfactorized
  )

add_executable(stationary-poisson-blocked-legacy-singlethreaded-assembly stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-blocked-legacy-singlethreaded-assembly
  PUBLIC
  SIMD_BLOCK_SIZE=16
  BLOCK_SOLVER=1
  LEGACY_ISTL=1
  SINGLETHREAD=1
  LOP_TYPE=sumfactorized
  )

add_executable(stationary-poisson-flat stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-flat
  PUBLIC
  SIMD_BLOCK_SIZE=16
  LOP_TYPE=sumfactorized
  )

add_executable(stationary-poisson-flat-legacy stationary-poisson.cc)
target_compile_definitions(
  stationary-poisson-flat-legacy
  PUBLIC
  SIMD_BLOCK_SIZE=16
  LEGACY_ISTL=1
  LOP_TYPE=sumfactorized
  )
