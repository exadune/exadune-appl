noinst_PROGRAMS =				\
	density_flow				\
	density_flow_st

noinst_HEADERS =

dist_noinst_DATA =

density_flow_SOURCES = density_flow.cc
density_flow_CPPFLAGS=$(AM_CPPFLAGS) \
	$(TBB_CPPFLAGS) \
	$(DUNEMPICPPFLAGS) \
	$(SUPERLU_CPPFLAGS) \
	$(GMP_CPPFLAGS)
# The libraries have to be given in reverse order (most basic libraries
# last).  Also, due to some misunderstanding, a lot of libraries include the
# -L option in LDFLAGS instead of LIBS -- so we have to include the LDFLAGS
# here as well.
density_flow_LDADD = \
	$(DUNE_LDFLAGS) $(DUNE_LIBS) \
	$(SUPERLU_LDFLAGS) $(SUPERLU_LIBS) \
	$(GMP_LDFLAGS) $(GMP_LIBS) \
	$(DUNEMPILIBS)	\
	$(TBB_LIBS) \
	$(LDADD)
density_flow_LDFLAGS = $(AM_LDFLAGS) \
	$(TBB_LDFLAGS) \
	$(DUNEMPILDFLAGS) \
	$(SUPERLU_LDFLAGS) \
	$(DUNE_LDFLAGS)

density_flow_st_SOURCES = density_flow.cc
density_flow_st_CPPFLAGS=$(AM_CPPFLAGS) \
	-DSINGLETHREAD=1 \
	$(DUNEMPICPPFLAGS) \
	$(SUPERLU_CPPFLAGS) \
	$(GMP_CPPFLAGS)
# The libraries have to be given in reverse order (most basic libraries
# last).  Also, due to some misunderstanding, a lot of libraries include the
# -L option in LDFLAGS instead of LIBS -- so we have to include the LDFLAGS
# here as well.
density_flow_st_LDADD = \
	$(DUNE_LDFLAGS) $(DUNE_LIBS) \
	$(SUPERLU_LDFLAGS) $(SUPERLU_LIBS) \
	$(GMP_LDFLAGS) $(GMP_LIBS) \
	$(DUNEMPILIBS)	\
	$(LDADD)
density_flow_st_LDFLAGS = $(AM_LDFLAGS) \
	$(DUNEMPILDFLAGS) \
	$(SUPERLU_LDFLAGS) \
	$(DUNE_LDFLAGS)

CLEANFILES = *.vtu *.png

noinst_PROGRAMS += stationary-poisson-blocked
stationary_poisson_blocked_SOURCES = stationary-poisson.cc
stationary_poisson_blocked_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16				\
  -DBLOCK_SOLVER=1				\
  -DLOP_TYPE=sumfactorized

stationary_poisson_blocked_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_blocked_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)


noinst_PROGRAMS += stationary-poisson-blocked-old
stationary_poisson_blocked_old_SOURCES = stationary-poisson.cc
stationary_poisson_blocked_old_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16				\
  -DBLOCK_SOLVER=1				\
  -DLOP_TYPE=dg

stationary_poisson_blocked_old_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_blocked_old_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)


noinst_PROGRAMS += stationary-poisson-blocked-counted
stationary_poisson_blocked_counted_SOURCES = stationary-poisson.cc
stationary_poisson_blocked_counted_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16				\
  -DBLOCK_SOLVER=1				\
  -DLOP_TYPE=sumfactorized			\
  -DENABLE_HP_TIMERS				\
  -DENABLE_COUNTER

stationary_poisson_blocked_counted_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_blocked_counted_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)


noinst_PROGRAMS += stationary-poisson-blocked-timed
stationary_poisson_blocked_timed_SOURCES = stationary-poisson.cc
stationary_poisson_blocked_timed_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16				\
  -DBLOCK_SOLVER=1				\
  -DLOP_TYPE=sumfactorized			\
  -DENABLE_HP_TIMERS

stationary_poisson_blocked_timed_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_blocked_timed_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)



noinst_PROGRAMS += stationary-poisson-flat
stationary_poisson_flat_SOURCES = stationary-poisson.cc
stationary_poisson_flat_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16

stationary_poisson_flat_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_flat_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)


noinst_PROGRAMS += stationary-poisson-blocked-legacy
stationary_poisson_blocked_legacy_SOURCES = stationary-poisson.cc
stationary_poisson_blocked_legacy_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16				\
  -DBLOCK_SOLVER=1				\
  -DLEGACY_ISTL=1

stationary_poisson_blocked_legacy_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_blocked_legacy_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)

noinst_PROGRAMS += stationary-poisson-flat-legacy
stationary_poisson_flat_legacy_SOURCES = stationary-poisson.cc
stationary_poisson_flat_legacy_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16				\
  -DLEGACY_ISTL=1

stationary_poisson_flat_legacy_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_flat_legacy_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)

noinst_PROGRAMS += stationary-poisson-blocked-legacy-singlethreaded-assembly
stationary_poisson_blocked_legacy_singlethreaded_assembly_SOURCES = stationary-poisson.cc
stationary_poisson_blocked_legacy_singlethreaded_assembly_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16				\
  -DBLOCK_SOLVER=1				\
  -DLEGACY_ISTL=1				\
  -DSINGLETHREAD=1

stationary_poisson_blocked_legacy_singlethreaded_assembly_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_blocked_legacy_singlethreaded_assembly_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)


noinst_PROGRAMS += stationary-poisson-blocked-singlethreaded-assembly
stationary_poisson_blocked_singlethreaded_assembly_SOURCES = stationary-poisson.cc
stationary_poisson_blocked_singlethreaded_assembly_CPPFLAGS =			\
  $(AM_CPPFLAGS)				\
  $(TBB_CPPFLAGS)				\
  $(DUNEMPICPPFLAGS)				\
  -DSIMD_BLOCK_SIZE=16				\
  -DSINGLETHREAD=1

stationary_poisson_blocked_singlethreaded_assembly_LDADD =			\
  $(DUNE_LDFLAGS) $(DUNE_LIBS)			\
  $(DUNEMPILIBS)				\
  $(TBB_LIBS) $(TBB_LDFLAGS)			\
  $(LDADD)

stationary_poisson_blocked_singlethreaded_assembly_LDFLAGS =			\
  $(AM_LDFLAGS)					\
  $(DUNEMPILDFLAGS)				\
  $(TBB_LDFLAGS)				\
  $(DUNE_LDFLAGS)

# pass most important options when "make distcheck" is used
DISTCHECK_CONFIGURE_FLAGS = --with-dune-common=$(DUNE_COMMON_ROOT) --with-dune-geometry=$(DUNE_GEOMETRY_ROOT) --with-dune-grid=$(DUNE_GRID_ROOT) --with-dune-istl=$(DUNE_ISTL_ROOT) --with-dune-localfunctions=$(DUNE_LOCALFUNCTIONS_ROOT) --with-dune-pdelab=$(DUNE_PDELAB_ROOT)  CXX="$(CXX)" CC="$(CC)"

include $(top_srcdir)/am/global-rules
