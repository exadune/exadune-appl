//***********************************************************************
//***********************************************************************
// diffusion problem with time dependent coefficients
//***********************************************************************
//***********************************************************************

template<typename GV, typename VDGF>
class GenericTransportProblem
{
  typedef typename VDGF::Traits::RangeFieldType RF;
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;
  const VDGF& vdgf;
  RF LX,LY,LZ;
  int NX,NY,NZ;
  RF hx,hy,hz;
  RF Ra;
  RF height,h;
  RF wavelength;
  RF amplitude;
  RF ramp;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  GenericTransportProblem (Dune::ParameterTree& ptree, const VDGF& vdgf_) : vdgf(vdgf_), time(0.0)
  {
    // grid parameters
    LX = ptree.get<double>("grid.structured.LX");
    LY = ptree.get<double>("grid.structured.LY");
    if (Traits::dimDomain==3) LZ = ptree.get<double>("grid.structured.LZ");
    NX = ptree.get<int>("grid.structured.NX");
    NY = ptree.get<int>("grid.structured.NY");
    if (Traits::dimDomain==3) NZ = ptree.get<int>("grid.structured.NZ");
    hx = LX/NX;
    hy = LY/NY;
    if (Traits::dimDomain==3) hz = LZ/NZ;

    if (Traits::dimDomain==2) height = LY;
    if (Traits::dimDomain==3) height = LZ;
    if (Traits::dimDomain==2) h = hy;
    if (Traits::dimDomain==3) h = hz;

    Ra = ptree.get<double>("problem.Ra");
    wavelength = ptree.get<double>("problem.wavelength");
    amplitude = ptree.get<double>("problem.amplitude");
    ramp = ptree.get<double>("problem.ramp");

    // diffusion tensor
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? (1.0/Ra) : 0;
  }

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename VDGF::Traits::RangeType velo(0.0);
    vdgf.evaluate(e,x,velo);
    v[0] = velo[0];
    v[1] = velo[1];
    if (Traits::dimDomain==3) v[2] = velo[2];
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {

    return 0.0;
  }

  //! boundary condition type function
  /* return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet for Dirichlet boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann for flux boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Outflow for outflow boundary conditions
   */
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& xlocal) const
  {
    typename Traits::DomainType xglobal = is.geometry().global(xlocal);

    // top boundary
    if (xglobal[Traits::dimDomain-1]>height-1e-6)
      {
        return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
      }

    // bottom boundary
    if (xglobal[Traits::dimDomain-1]<1e-6)
      {
        typename Traits::DomainType iplocal = is.geometryInInside().global(xlocal);
        typename VDGF::Traits::RangeType velo;
        vdgf.evaluate(is.inside(),iplocal,velo);
        typename Traits::RangeFieldType normalvelo = velo*(is.centerUnitOuterNormal());
        if (normalvelo<=0.0)
          return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
        else
          return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Outflow;
      }
    // else lateral boundaries
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
  {
    typename Traits::DomainType x = e.geometry().global(xlocal);

    // initial condition
    if (time<1e-8)
      {
        typename Traits::RangeFieldType ztop;
        if (Traits::dimDomain==2)
          ztop = height-h-amplitude*h - amplitude*h*cos(2.0*M_PI*x[0]/(wavelength*hx));
        if (Traits::dimDomain==3)
          ztop = height-h-amplitude*h - amplitude*h*cos(2.0*M_PI*x[0]/(wavelength*hx))*cos(2.0*M_PI*x[1]/(wavelength*hy));
        typename Traits::RangeFieldType z;
        if (x[Traits::dimDomain-1]>ztop-1e-6)
          z=1.0;
        else if (x[Traits::dimDomain-1]<ztop-ramp*h)
          z=0.0;
        else
          z = (x[Traits::dimDomain-1]-(ztop-ramp*h))/(ramp*h);
        return z;
      }

    // check for interior (happens in parallel)
    if (Traits::dimDomain==2 && x[0]>1e-6 && x[0]<LX-1e-6 && x[1]>1e-6 && x[1]<LY-1e-6)
      return 0.0;
    if (Traits::dimDomain==3 && x[0]>1e-6 && x[0]<LX-1e-6 && x[1]>1e-6 && x[1]<LY-1e-6 && x[2]>1e-6 && x[2]<LZ-1e-6)
      return 0.0;

    // boundary condition
    if (x[Traits::dimDomain-1]>height-1e-6)
      return 1.0;
    return 0.0;
  }

  //! flux boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! set time for subsequent evaluation
  void setTime (RF t)
  {
    time = t;
  }

private:
  typename Traits::PermTensorType I;
  mutable typename Traits::RangeType v;
  RF time;
};
