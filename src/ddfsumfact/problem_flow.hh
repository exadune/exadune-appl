/** Parameter class for the stationary convection-diffusion equation of the following form:
 *
 * \f{align*}{
 *   \nabla\cdot(-A(x) \nabla u + b(x) u) + c(x)u &=& f \mbox{ in } \Omega,  \ \
 *                                              u &=& g \mbox{ on } \partial\Omega_D (Dirichlet)\ \
 *                (b(x,u) - A(x)\nabla u) \cdot n &=& j \mbox{ on } \partial\Omega_N (Flux)\ \
 *                        -(A(x)\nabla u) \cdot n &=& o \mbox{ on } \partial\Omega_O (Outflow)
 * \f}
 * Note:
 *  - This formulation is valid for velocity fields which are non-divergence free.
 *  - Outflow boundary conditions should only be set on the outflow boundary
 *
 * The template parameters are:
 *  - GV a model of a GridView
 *  - RF numeric type to represent results
 */

template<typename GV, typename RF>
class GenericFlowProblem
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

  RF LX,LY,LZ;
  int NX,NY,NZ;
  RF hx,hy,hz;
  RF height,h;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  GenericFlowProblem (Dune::ParameterTree& ptree_) : ptree(ptree_)
  {
    // read structured grid parameters
    LX = ptree.get<double>("grid.structured.LX");
    LY = ptree.get<double>("grid.structured.LY");
    if (Traits::dimDomain==3) LZ = ptree.get<double>("grid.structured.LZ");
    NX = ptree.get<int>("grid.structured.NX");
    NY = ptree.get<int>("grid.structured.NY");
    if (Traits::dimDomain==3) NZ = ptree.get<int>("grid.structured.NZ");
    hx = LX/NX;
    hy = LY/NY;
    if (Traits::dimDomain==3) hz = LZ/NZ;

    if (Traits::dimDomain==2) height = LY;
    if (Traits::dimDomain==3) height = LZ;
    if (Traits::dimDomain==2) h = hy;
    if (Traits::dimDomain==3) h = hz;

    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? 1.0 : 0.0;
  }

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! gravity vector
  typename Traits::RangeType
  gravity () const
  {
    typename Traits::RangeType v(0.0);
    v[Traits::dimDomain-1] = -1;
    return v;
  }

  //! density DIFFERENCE (heavy fluid - lighter fluid)
  typename Traits::RangeFieldType
  delta_rho () const
  {
    return 1.0;
  }

  //! viscosity function depending on concentration
  RF viscosity (RF c) const
  {
    return 1.0;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! boundary condition type function
  /* return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet for Dirichlet boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann for flux boundary conditions
   * return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Outflow for outflow boundary conditions
   */
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    typename Traits::DomainType xglobal = is.geometry().global(x);
    if (xglobal[Traits::dimDomain-1]<1e-6)
      return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Neumann;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& xlocal) const
  {
    return 0.0;
  }

  //! flux boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

private:
  Dune::ParameterTree& ptree;
  typename Traits::PermTensorType I;
};
