import operator

class Operation(object):

    def __init__(self,read,write = 0):
        self.read = read
        self.write = write

    def __add__(self,other):
        return Operation(self.read + other.read, self.write + other.write)

    def __mul__(self,other):
        return Operation(self.read * other, self.write * other)

    def __rmul__(self,other):
        return self.__mul__(other)

    @property
    def total(self):
        return self.read + self.write

    def __str__(self):
        return str(self.total)

    def __repr__(self):
        return 'Operation(read = {}, write = {})'.format(self.read,self.write)



class Bandwidth(object):

    def __init__(self, dofs, nonzeros, simd_size, iterations, t, preconditioner_iterations = 5):
        self.dofs = dofs
        self.nonzeros = nonzeros
        self.simd_size = simd_size
        self.iterations = iterations
        self.t = t
        self.preconditioner_iterations = preconditioner_iterations

    @property
    def norm(self):
        return Operation(read = self.dofs)

    @property
    def dot(self):
        return Operation(read = 2 * self.dofs)

    @property
    def axpy(self):
        return Operation(read = 2 * self.dofs, write = self.dofs)

    @property
    def usmv(self):
        read  = 2 * self.dofs / self.simd_size # block offsets
        read += self.dofs # load target vector
        read += self.nonzeros # matrix data
        read += self.nonzeros # matrix column indices
        read += self.nonzeros # gather source vector using matrix column indices
        write = self.dofs # write target vector
        return Operation(read = read, write = write)

    mv = usmv

    @property
    def jacobi_kernel(self):
        read  = 2 * self.dofs / self.simd_size # block offsets
        read += self.dofs # load target vector
        read += self.nonzeros # matrix data
        read += self.nonzeros # matrix column indices
        read += self.nonzeros # gather source vector using matrix column indices
        write = self.dofs # write target vector
        return Operation(read = read, write = write)

    @property
    def preconditioner(self):
        return self.preconditioner_iterations * (self.jacobi_kernel + self.axpy)

    @property
    def setup(self):
        return self.usmv + self.norm + self.preconditioner + self.dot

    @property
    def iteration(self):
        return self.mv + self.dot + self.axpy + self.axpy + self.norm + self.preconditioner + self.dot

    @property
    def solver(self):
        return self.setup + self.iterations * self.iteration

    @property
    def bandwidth(self):
        return self.solver * (1.0 / self.t)

    @property
    def bandwidth_GB_per_s(self):
        return self.bandwidth * (8.0 / 1024 / 1024 / 1024)


class BlockedBandwidth(Bandwidth):

    def __init__(self, blocks, nonzero_blocks, block_size, simd_size, iterations, t, preconditioner_iterations = 5):
        super(BlockedBandwidth,self).__init__(blocks * block_size, nonzero_blocks * block_size * block_size, simd_size, iterations, t, preconditioner_iterations)
        self.blocks = blocks
        self.nonzero_blocks = nonzero_blocks
        self.block_size = block_size

    @property
    def usmv(self):
        read  = 2 * self.blocks / float(self.simd_size) # block offsets
        read += self.dofs # load target vector
        read += self.nonzeros # matrix data
        read += self.nonzeros / float(self.block_size * self.block_size) # matrix column indices
        read += self.nonzeros # gather source vector using matrix column indices
        write = self.dofs # write target vector
        return Operation(read = read, write = write)

    mv = usmv

    @property
    def jacobi_kernel(self):
        read  = 2 * self.blocks / float(self.simd_size) # block offsets
        read += self.dofs # load target vector
        read += self.nonzeros # matrix data
        read += self.nonzeros / float(self.block_size * self.block_size) # matrix column indices
        read += self.nonzeros # gather source vector using matrix column indices
        read += self.block_size * self.dofs # solve on diagonal block
        write = self.dofs # write target vector
        return Operation(read = read, write = write)


cell_dofs = (-1,8,27,64)

def matrix_size(order,size,simd_size=16):
    block_size = cell_dofs[order]
    if isinstance(size,tuple):
        if len(tuple) != 3:
            raise ValueError("If providing more than one extent, you need to provide exactly 3")
        cells = reduce(operator.__mul__(size))
    else:
        cells = size * size * size
        size = (size,size,size)
    nonzero_blocks = 7 * cells
    nonzeros = nonzero_blocks * block_size * block_size
    dofs = cells * block_size
    scalar_size = (2 * nonzeros + dofs + dofs / float(simd_size)) * 8 / 1024 / 1024
    blocked_size = (nonzeros + nonzeros / float(block_size * block_size) + dofs / float(block_size) + dofs / float(block_size * simd_size)) * 8 / 1024 / 1024
    scalar_chunks = dofs / simd_size
    blocked_chunks = cells / simd_size
    return "({:2},{:2},{:2}) | Q{} | SIMD{:2} | cells = {:7} | dofs = {:7} | matrix blocks = {:9} | matrix entries = {:9} | scalar {:7.1f} MB, {:6} chunks | blocked {:7.1f} MB, {:5} chunks".format(size[0],size[1],size[2],order,simd_size,cells,dofs,nonzero_blocks,nonzeros,scalar_size,scalar_chunks,blocked_size,blocked_chunks)


def make_ini(template_file,order,size,simd_size,threads,blocked=False,max_chunk_size=4096,iterations=100,reduction=1e-10,ranks=1):
    block_size = cell_dofs[order]
    cells = size * size * size
    scalar_chunks = cells * block_size / simd_size
    blocked_chunks = cells / simd_size
    max_scalar_chunk_size = min(max_chunk_size,scalar_chunks / (threads * ranks))
    max_blocked_chunk_size = min(max_chunk_size,blocked_chunks / (threads * ranks))
    chunk_size = max_blocked_chunk_size if blocked else max_scalar_chunk_size
    entries_per_row = 7 * block_size
    filename = "{}-q{}-simd-{}-size-{}-iterations-{}-chunksize-{}-threads-{}.ini".format("blocked" if blocked else "scalar",order,simd_size,size,iterations,chunk_size,threads)
    replacement_map = {
        '%SIZE%' : str(size),
        '%ORDER%' : str(order),
        '%REDUCTION%' : str(reduction),
        '%ITERATIONS%' : str(iterations),
        '%THREADS%' : str(threads),
        '%CHUNK_SIZE%' : str(chunk_size),
        '%ENTRIES_PER_ROW%' : str(entries_per_row)
    }
    with open(filename,'w') as output:
        with open(template_file) as template:
            for line in template:
                for key, value in replacement_map.iteritems():
                    line = line.replace(key,value)
                output.write(line)


import re, collections
from os import path

class Run(object):

    filename_re = re.compile(r'^(?P<variant>scalar|blocked)-q(?P<order>\d)-simd-(?P<simd>\d+)-size-(?P<size>\d+)-iterations-(?P<iterations>\d+)-chunksize-(?P<chunk_size>\d+)-threads-(?P<threads>\d+)')

    def parse_line(self,f,pattern,flatten_single=True,continue_until_found=False,optional=True):
        try:
            while True:
                line = next(f)[:-1]
                m = re.search(pattern,line)
                if not m:
                    if continue_until_found:
                        continue
                    else:
                        raise IOError("Cannot parse file {}, line: '{}'".format(self.fn,line))
                if flatten_single and len(m.groups()) == 1:
                    return m.groups()[0]
                else:
                    return m.groups()
        except StopIteration:
            if optional:
                return None
            else:
                raise IOError("Cannot parse file {}".format(fn))

    def __init__(self,fn,minimal_parse=False):
        m = self.filename_re.match(path.basename(fn))
        self.fn = fn
        if not m:
            raise IOError("File {} not found or invalid filename".format(self.fn))

        try:
            np = Run.NameParameters(**m.groupdict())
        except AttributeError:
            Run.NameParameters = collections.namedtuple("NameParameters",tuple(m.groupdict().keys()))
            np = Run.NameParameters(**m.groupdict())

        self.order = int(np.order)
        self.variant = np.variant
        self.size = int(np.size)

        with open(fn) as f:
            self.procs = int(self.parse_line(f,r'parallel run on (\d+) proc',continue_until_found=minimal_parse))
            if not minimal_parse:
                self.threads = int(self.parse_line(f,r'Using (\d+) threads'))
                next(f)
                self.simd = int(self.parse_line(f,r'SIMD block size: (\d+)'))
                self.chunk_size = int(self.parse_line(f,r'chunk size: (\d+)'))
                self.gfs_creation = float(self.parse_line(f,r'Creating GFS and Ordering... (.*)'))
                self.dofs, self.blocks = self.parse_line(f,r'(\d+) DOFs, (\d+) blocks')
                self.dofs = int(self.dofs)
                self.blocks = int(self.blocks)
                self.constraints_time = float(self.parse_line(f,r'Evaluating constraints... (.*)'))
                self.residual_time = float(self.parse_line(f,r'Evaluating residual... (.*)'))
                self.matrix_time = float(self.parse_line(f,r'Creating matrix... (.*)'))
                self.nonzero_blocks = int(self.parse_line(f,r'(\d+) effective non-zero'))
                self.jacobian_time = float(self.parse_line(f,r'Evaluating jacobian... (.*)'))
            self.rate, self.user_time, self.user_time_per_iteration, self.iterations = self.parse_line(f,r'=== rate=([^,]+), T=([^,]+), TIT=([^,]+), IT=(\d+)',continue_until_found=True)
            self.rate = float(self.rate)
            self.user_time = float(self.user_time)
            self.user_time_per_iteration = float(self.user_time_per_iteration)
            self.iterations = int(self.iterations)
            self.w_time = float(self.parse_line(f,r'wallclock time: (.*)'))
            self.memory = int(self.parse_line(f,r'\s*(\d+)\s*maximum resident set size',continue_until_found=True,optional=True))

        if not minimal_parse:
            if self.variant == 'blocked':
                self.block_size = cell_dofs[self.order]
            else:
                self.block_size = 1

            self.nonzeros = self.nonzero_blocks * self.block_size * self.block_size

        #assert(self.threads == np.threads)
        #assert(self.simd == np.simd)


    def bandwidth(self):
        if self.variant == 'scalar':
            return Bandwidth(self.dofs,self.nonzeros,self.simd,self.iterations,self.w_time)
        else:
            return BlockedBandwidth(self.blocks, self.nonzero_blocks, self.block_size, self.simd, self.iterations, self.w_time)
