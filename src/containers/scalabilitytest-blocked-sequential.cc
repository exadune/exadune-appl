// -*- tab-width: 4; indent-tabs-mode: nil -*-
/** \file
    \brief High-level test with Poisson equation
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

// enable basis function caching for faster assembly
#define USECACHE 1

#include<iostream>
#include<vector>
#include<map>
#include<chrono>
#include<tbb/task_scheduler_init.h>
#include<dune/common/parametertreeparser.hh>
#include<dune/common/parallel/mpihelper.hh>
#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/timer.hh>
#include<dune/grid/yaspgrid.hh>
#include<dune/istl/bvector.hh>
#include<dune/istl/operators.hh>
#include<dune/istl/solvers.hh>
#include<dune/istl/preconditioners.hh>
#include<dune/istl/io.hh>
#include<dune/istl/paamg/amg.hh>
#include<dune/istl/superlu.hh>
#include<dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

#include<dune/pdelab/finiteelementmap/monomfem.hh>
#include<dune/pdelab/finiteelementmap/opbfem.hh>
#include<dune/pdelab/finiteelementmap/qkdg.hh>
#include<dune/pdelab/finiteelementmap/pkfem.hh>
#include<dune/pdelab/finiteelementmap/p0fem.hh>
#include<dune/pdelab/constraints/common/constraints.hh>
#include<dune/pdelab/constraints/conforming.hh>
#include<dune/pdelab/constraints/p0.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspaceutilities.hh>
#include<dune/pdelab/gridfunctionspace/interpolate.hh>
#include<dune/pdelab/common/function.hh>
#include<dune/pdelab/common/functionutilities.hh>
#include<dune/pdelab/common/vtkexport.hh>
#include<dune/pdelab/backend/istlvectorbackend.hh>
#include<dune/pdelab/backend/istlmatrixbackend.hh>
#include<dune/pdelab/backend/seqistlsolverbackend.hh>
#include<dune/pdelab/localoperator/convectiondiffusionparameter.hh>
#include<dune/pdelab/localoperator/convectiondiffusiondg.hh>
#include<dune/pdelab/localoperator/diffusionccfv.hh>
#include<dune/pdelab/stationary/linearproblem.hh>
#include<dune/pdelab/gridoperator/gridoperator.hh>
#include<dune/pdelab/gridfunctionspace/vtk.hh>

#include"problemA.hh"


#include <dune/common/memory/blocked_allocator.hh>

#include <dune/pdelab/backend/istl/blockvectorbackend.hh>
#include <dune/pdelab/backend/istl/bellmatrixbackend.hh>

#include<dune/istl/preconditioners/sequentialblockjacobi.hh>

#ifndef SIMD_BLOCK_SIZE
#define SIMD_BLOCK_SIZE 32
#endif

#if HAVE_CUDA
#include <dune/istl/vector/cuda.hh>
#include <dune/istl/ellmatrix/cuda.hh>

#if SIMD_BLOCK_SIZE != 32
#error CUDA currently requires a SIMD block size of 32
#endif

#endif // HAVE_CUDA


template<typename GV, typename RF>
class Parameter
{
  typedef Dune::PDELab::ConvectionDiffusionBoundaryConditions::Type BCType;

public:
  typedef Dune::PDELab::ConvectionDiffusionParameterTraits<GV,RF> Traits;

  //! tensor diffusion coefficient
  typename Traits::PermTensorType
  A (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::PermTensorType I;
    for (std::size_t i=0; i<Traits::dimDomain; i++)
      for (std::size_t j=0; j<Traits::dimDomain; j++)
        I[i][j] = (i==j) ? 1 : 0;
    return I;
  }

  //! velocity field
  typename Traits::RangeType
  b (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::RangeType v(0.0);
    return v;
  }

  //! sink term
  typename Traits::RangeFieldType
  c (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    return 0.0;
  }

  //! source term
  typename Traits::RangeFieldType
  f (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    typename Traits::RangeFieldType norm = xglobal.two_norm2();
    return (2.0*GV::dimension-4.0*norm)*exp(-norm);
  }

  //! boundary condition type function
  BCType
  bctype (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return Dune::PDELab::ConvectionDiffusionBoundaryConditions::Dirichlet;
  }

  //! Dirichlet boundary condition value
  typename Traits::RangeFieldType
  g (const typename Traits::ElementType& e, const typename Traits::DomainType& x) const
  {
    typename Traits::DomainType xglobal = e.geometry().global(x);
    typename Traits::RangeFieldType norm = xglobal.two_norm2();
    return exp(-norm);
  }

  //! Neumann boundary condition
  typename Traits::RangeFieldType
  j (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }

  //! outflow boundary condition
  typename Traits::RangeFieldType
  o (const typename Traits::IntersectionType& is, const typename Traits::IntersectionDomainType& x) const
  {
    return 0.0;
  }
};


//! solve problem with DG method
template<class GV, class FEM, class PROBLEM, int degree>
void runDG ( const GV& gv,
             const FEM& fem,
             PROBLEM& problem,
             std::string basename,
             int level,
             std::string method,
             std::string weights,
             int blocksize,
             const Dune::ParameterTree& params)
{

  std::cout << "SIMD block size: " << SIMD_BLOCK_SIZE << std::endl;

  Dune::Timer timer;

  typedef double Real;
  const int dim = GV::Grid::dimension;

  std::stringstream fullname;
  fullname << basename << "_" << method << "_w" << weights << "_k" << degree << "_dim" << dim << "_level" << level << "_simd" << SIMD_BLOCK_SIZE;

  std::size_t chunk_size = params.get<std::size_t>("tunables.host.chunk_size",512);

  std::cout << "host thread chunk size: " << chunk_size << std::endl;

  // ****************************** Allocators ******************************

  typedef Dune::Memory::blocked_cache_aligned_allocator<double,std::size_t,SIMD_BLOCK_SIZE> HostAllocator;

#if HAVE_CUDA

  typedef Dune::Memory::CudaAllocator<double> GPUAllocator;

#endif // HAVE_CUDA

  // ************************************************************************


  // make grid function space
  typedef Dune::PDELab::P0ParallelConstraints CON;
  //const Dune::PDELab::ISTLParameters::Blocking blocking
  //  = Dune::PDELab::ISTLParameters::static_blocking;
  //typedef Dune::PDELab::ISTLVectorBackend<blocking,blocksize> VBE;

  typedef Dune::PDELab::istl::BlockVectorBackend<HostAllocator> VBE;
  VBE vbe(blocksize);

  std::cout << "Creating GFS and Ordering... " << std::flush;

  typedef Dune::PDELab::GridFunctionSpace<GV,FEM,CON,VBE> GFS;
  GFS gfs(gv,fem,vbe);
  gfs.ordering();

  std::cout << timer.elapsed() << std::endl;
  std::cout << gfs.ordering().size() << " DOFs, " << gfs.ordering().blockCount() << " blocks" << std::endl;

  // make local operator
  Dune::PDELab::ConvectionDiffusionDGMethod::Type m;
  if (method=="SIPG") m = Dune::PDELab::ConvectionDiffusionDGMethod::SIPG;
  if (method=="NIPG") m = Dune::PDELab::ConvectionDiffusionDGMethod::NIPG;
  Dune::PDELab::ConvectionDiffusionDGWeights::Type w;
  if (weights=="ON") w = Dune::PDELab::ConvectionDiffusionDGWeights::weightsOn;
  if (weights=="OFF") w = Dune::PDELab::ConvectionDiffusionDGWeights::weightsOff;
  typedef Dune::PDELab::ConvectionDiffusionDG<
    PROBLEM,
    FEM
    > LOP;
  LOP lop(problem,m,w,params.get<double>("discretization.alpha",2.0));

  {
    // we need to pre-fill the local basis cache, as it is read-only inside the GridOperator
    // for thread-safety reasons
    Dune::PDELab::LocalFunctionSpace<GFS> lfs(gfs);
    auto it = gfs.gridView().template begin<0>();
    lfs.bind(*it);
    lop.setupCache(lfs,*it);
  }


  typedef typename Dune::PDELab::istl::BELLMatrixBackend<HostAllocator> MBE;

  typedef Dune::PDELab::ConvectionDiffusionDirichletExtensionAdapter<PROBLEM> G;
  G g(gv,problem);
  typedef typename GFS::template ConstraintsContainer<Real>::Type CC;
  CC cc;

  timer.reset();
  std::cout << "Evaluating constraints... " << std::flush;

  Dune::PDELab::constraints(g,gfs,cc,false);

  std::cout << timer.elapsed() << std::endl;
  timer.reset();

  typedef Dune::PDELab::GridOperator<GFS,GFS,LOP,MBE,Real,Real,Real,CC,CC> GO;
  GO go(gfs,cc,gfs,cc,lop);

  // make a vector of degree of freedom vectors and initialize it with Dirichlet extension
  typedef typename GO::Traits::Domain U;
  U u(gfs,0.0);
  raw(u).setChunkSize(chunk_size);

  U r(gfs,0.0);
  raw(r).setChunkSize(chunk_size);

  timer.reset();
  std::cout << "Evaluating residual... " << std::flush;

  go.residual(u,r);

  std::cout << timer.elapsed() << std::endl;

  timer.reset();
  std::cout << "Creating matrix... " << std::flush;

  typename GO::Traits::Jacobian mat(
    go,
    Dune::PDELab::istl::MatrixParameters(params.sub("tunables.matrix"))
    );

  std::cout << timer.elapsed() << std::endl;

  std::cout << raw(mat).layout().nonzeros() << " effective non-zero entries" << std::endl;

  timer.reset();
  std::cout << "Evaluating jacobian... " << std::flush;

  raw(mat).setChunkSize(chunk_size);
  go.jacobian(u,mat);

  std::cout << timer.elapsed() << std::endl;



  // ****************************** Linear Algebra ******************************

  // update vector - for simplicity, let's keep it on the host for now
  U z(gfs,0.0);
  raw(z).setChunkSize(chunk_size);

#if HAVE_CUDA

  typedef Dune::ISTL::BlockVector<typename GO::Traits::Domain::value_type, GPUAllocator > RawVector;
  typedef Dune::ISTL::BELLMatrix<typename GO::Traits::Domain::value_type, GPUAllocator> RawMatrix;
  std::size_t cuda_blocksize = params.get<std::size_t>("tunables.cuda.blocksize",128);
  std::cout << "cuda block size: " << cuda_blocksize << std::endl;

  // copy constructors host -> device
  RawVector raw_r(raw(r));
  raw_r.set_cuda_blocksize(cuda_blocksize);
  RawVector raw_z(raw(z));
  raw_z.set_cuda_blocksize(cuda_blocksize);

  RawMatrix raw_mat(raw(mat));
  raw_mat.set_cuda_blocksize(cuda_blocksize);

#else // HAVE_CUDA

  typedef typename GO::Traits::Domain::Container RawVector;
  typedef typename GO::Traits::Jacobian::Container RawMatrix;

  // create some references to make the code below work on both host and device
  RawVector& raw_r = raw(r);
  RawVector& raw_z = raw(z);

  RawMatrix& raw_mat = raw(mat);

#endif // HAVE_CUDA

  // make linear solver and solve problem
  typedef Dune::MatrixAdapter<
    RawMatrix,
    RawVector,
    RawVector
    > MatrixOperator;
  MatrixOperator matrix_operator(raw_mat);

  typedef Dune::ISTL::SequentialBlockJacobi<
    RawMatrix,
    RawVector,
    RawVector
    > Preconditioner;
  Preconditioner preconditioner(
    raw_mat,
    params.get<double>("solver.jacobi.relaxation_factor",1.0),
    params.get<bool>("solver.jacobi.lu_pivot",false),
    params.get<int>("solver.jacobi.iterations",5)
    );

  int verbosity = params.get<int>("solver.verbosity",2);

  typedef decltype(std::chrono::high_resolution_clock::now()) TimePoint;

  TimePoint solve_start, solve_end;

  Dune::InverseOperatorResult stat;

  if (method=="SIPG")
    {
      typedef Dune::CGSolver<RawVector> Solver;

      Solver solver(
        matrix_operator,
        preconditioner,
        params.get<double>("solver.reduction",1e-10),
        params.get<int>("solver.iterations",500),
        verbosity
        );

      solve_start = std::chrono::high_resolution_clock::now();
      solver.apply(raw_z,raw_r,stat);
    }
  else
    {
      typedef Dune::BiCGSTABSolver<RawVector> Solver;
      Solver solver(
        matrix_operator,
        preconditioner,
        params.get<double>("solver.reduction",1e-10),
        params.get<int>("solver.iterations",500),
        verbosity
        );

      solve_start = std::chrono::high_resolution_clock::now();
      solver.apply(raw_z,raw_r,stat);
    }

  solve_end = std::chrono::high_resolution_clock::now();

#if HAVE_CUDA

  // copy update back to host
  //raw(z) = raw_z; // ?????
  raw_z.download_to(raw(z));

#endif // HAVE_CUDA

  // update solution
  u -= z;

  auto solve_time = std::chrono::duration_cast<std::chrono::duration<double> >(solve_end - solve_start);

  if (verbosity > 0)
    {
      std::cout << "Solver wallclock time: " <<  solve_time.count() << std::endl;
      std::cout << "wallclock time / iteration: " <<  (solve_time.count() / stat.iterations) << std::endl;
    }

  if(params.get<bool>("io.vtk",false)){
    Dune::SubsamplingVTKWriter<GV> vtkwriter(gv,std::max(0,degree-1));
    Dune::PDELab::addSolutionToVTKWriter(
      vtkwriter,
      gfs,
      u,
      Dune::PDELab::vtk::DefaultFunctionNameGenerator("","h")
      );
    vtkwriter.write(fullname.str(),Dune::VTK::appendedraw);
  }

}



int main(int argc, char** argv)
{
  //Maybe initialize Mpi
  Dune::MPIHelper& helper = Dune::MPIHelper::instance(argc, argv);
  if(Dune::MPIHelper::isFake)
    std::cout<< "This is a sequential program." << std::endl;
  else
    {
      if(helper.rank()==0)
        std::cout << "parallel run on " << helper.size() << " process(es)" << std::endl;
    }

  if (argc!=2) {
    if(helper.rank()==0) {
      std::cout << "usage: " << argv[0] << " <parameter file>" << std::endl;
    }
    return 1;
  }

  try
    {

      Dune::ParameterTree params;
      Dune::ParameterTreeParser::readINITree(argv[1],params);

      // start up TBB task scheduler
      int threads = params.get<int>("tunables.host.threads",tbb::task_scheduler_init::default_num_threads());
      tbb::task_scheduler_init tbb_task_scheduler_init(threads);

      if (helper.rank() == 0)
        std::cout << "Using " << threads << " threads" << std::endl;

      int degree_dyn = params.get<int>("discretization.order");
      int nx = params.get<int>("mesh.xcells");
      int ny = params.get<int>("mesh.ycells");
      int nz = params.get<int>("mesh.zcells");

      int px = params.get<int>("mesh.xpartitions",0);
      int py = params.get<int>("mesh.ypartitions",0);
      int pz = params.get<int>("mesh.zpartitions",0);

      int overlap = params.get<int>("mesh.overlap",1);

      const int dim = 3;
      Dune::FieldVector<double,dim> L(1.0);
      std::array<int,dim> N;
      N[0] = nx; N[1] = ny; N[2] = nz;
      std::bitset<dim> B(false);

      std::shared_ptr<
        Dune::YLoadBalance<dim>
        > yp;
      if( px*py*pz==0 ){
        // If px,py,pz were not specified choose the default load balancer
        if( helper.rank() == 0 )
          std::cout << "Using default partitioning of YASP." << std::endl;
        yp = std::make_shared<Dune::YLoadBalanceDefault<dim> >();
      }

      else if( px*py*pz != helper.size() ){
        // If px*py*pz is not equal to the available number of processors
        // wrong input, stop and output warning!
        if( helper.rank()==0 )
          std::cerr << "Wrong input: px*py*pz != np" << std::endl;
        exit(1);
      }

      else {
        std::array<int,dim> yasppartitions;
        yasppartitions[0] = px;
        yasppartitions[1] = py;
        yasppartitions[2] = pz;
        yp = std::make_shared<Dune::YaspFixedSizePartitioner<dim> >(yasppartitions);
        if( helper.rank() == 0 )
          {
            std::cout << "Partitioning of YASP:";
            for (auto i : yasppartitions)
              std::cout << " " << i;
            std::cout << std::endl;
          }
      }

      typedef Dune::YaspGrid<dim> Grid;

      Grid grid(L,N,B,overlap,Grid::CollectiveCommunicationType(),yp.get());

      typedef Grid::LeafGridView GV;

      const GV& gv=grid.leafGridView();
      typedef Parameter<GV,double> PROBLEM;
      PROBLEM problem;

      if (degree_dyn==1) {
        const int degree=1;
        typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,FEMDG,PROBLEM,degree>(gv,femdg,problem,"CUBE",0,"SIPG","ON",blocksize,params);
      }
      if (degree_dyn==2) {
        const int degree=2;
        typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,FEMDG,PROBLEM,degree>(gv,femdg,problem,"CUBE",0,"SIPG","ON",blocksize,params);
      }
      if (degree_dyn==3) {
        const int degree=3;
        typedef Dune::PDELab::QkDGLocalFiniteElementMap<Grid::ctype,double,degree,dim> FEMDG;
        FEMDG femdg;
        const int blocksize = Dune::QkStuff::QkSize<degree,dim>::value;
        runDG<GV,FEMDG,PROBLEM,degree>(gv,femdg,problem,"CUBE",0,"SIPG","ON",blocksize,params);
      }
    }
  catch (Dune::Exception &e)
    {
      std::cerr << "Dune reported error: " << e << std::endl;
      return 1;
    }
  catch (...)
    {
      std::cerr << "Unknown exception thrown!" << std::endl;
      return 1;
    }
}
